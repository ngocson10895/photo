# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-repackageclasses
-keep class org.apache.http.** { *; }
-dontwarn com.android.internal.**
-dontwarn org.apache.http.**
-dontwarn android.net.http.**

-keep class com.piccollage.collagemaker.picphotomaker.entity.** { *; }
-keep class com.piccollage.collagemaker.picphotomaker.api.** { *; }
-keep class com.piccollage.collagemaker.picphotomaker.data.** { *; }
-keep class cn.pedant.SweetAlert.Rotate3dAnimation {
    public <init>(...);
}

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-dontwarn android.support.**
-dontwarn android.support.design.**

## Android Advertiser ID
-keep class com.google.android.gms.common.GooglePlayServicesUtil {*;}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {*;}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {*;}

#### Other
-dontwarn com.google.errorprone.annotations.*

### Exoplayer2
-dontwarn com.google.android.exoplayer2.**

-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-dontwarn net.fortuna.ical4j.**
-dontwarn org.joda.time.**
-dontwarn com.utility.**
-dontwarn com.opencsv.**
-dontwarn com.facebook.ads.**
-dontwarn com.bumptech.glide.**
-dontwarn okio.**
-dontwarn com.evernote.android.**
-dontwarn com.google.android.gms.**

#glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

#logger
-dontwarn com.evernote.android.job.gcm.**
-dontwarn com.evernote.android.job.util.GcmAvailableHelper

-keep public class com.evernote.android.job.v21.PlatformJobService
-keep public class com.evernote.android.job.v14.PlatformAlarmService
-keep public class com.evernote.android.job.v14.PlatformAlarmReceiver
-keep public class com.evernote.android.job.JobBootReceiver
-keep public class com.evernote.android.job.JobRescheduleService

-keep class com.google.android.gms.corebase.**{ *; } # All classes in the com.example package
-keep class com.commonsware.cwac.wakeful.**{ *; }
-keep class com.androidnetworking.**{ *; }
-keep class com.google.gson.**{ *; }
-keep class com.google.android.exoplayer2.**{ *; }
-keep class com.evernote.android.job.**{ *; }
-keep class com.startapp.android.**{ *; }
-keep class okhttp3.**{ *; }
-keep class okio.**{ *; }
-keep class com.airbnb.lottie.** {*;}
-dontwarn com.airbnb.lottie.**

-dontwarn okio.**
-dontwarn com.facebook.ads.internal.**
-keeppackagenames com.google.android.gms.corebase.* # All classes in the com.example package
-keeppackagenames com.evernote.android.job.* # All classes in the com.example package
-keeppackagenames com.startapp.android.* # All classes in the com.example package
-keeppackagenames com.androidnetworking.*
-keeppackagenames okhttp3.*
-keeppackagenames com.google.gson.*
-keeppackagenames okio.*

-keep class com.truenet.** {
      *;
}
-keep class com.startapp.** {
      *;
}
-dontwarn android.webkit.JavascriptInterface
-dontwarn com.startapp.**
-dontwarn org.jetbrains.annotations.**
-keep class org.jetbrains.** {
            *;
}
-keep class com.google.ads.consent.** {
            *;
}
-dontwarn com.google.ads.consent.**
-keep class com.android.vending.billing.**

-keepclassmembers class * {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
