package com.piccollage.collagemaker.picphotomaker.ui.editoractivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdSize;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.actions.BaseAction;
import com.piccollage.collagemaker.picphotomaker.actions.CropAction;
import com.piccollage.collagemaker.picphotomaker.actions.DrawAction;
import com.piccollage.collagemaker.picphotomaker.actions.EffectAction;
import com.piccollage.collagemaker.picphotomaker.actions.FocusAction;
import com.piccollage.collagemaker.picphotomaker.actions.RotationAction;
import com.piccollage.collagemaker.picphotomaker.actions.TouchBlurAction;
import com.piccollage.collagemaker.picphotomaker.adapter.IconsAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityImageProcessingBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.entity.TextOfUser;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.DrawableSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.Sticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.StickerView;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.TextSticker;
import com.piccollage.collagemaker.picphotomaker.ui.DialogExitFeature;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.PreviewActivity;
import com.piccollage.collagemaker.picphotomaker.ui.morefeature.AddTextFragment;
import com.piccollage.collagemaker.picphotomaker.ui.mycreativeactivity.MyCreativeActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.DateTimeUtils;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;
import com.piccollage.collagemaker.picphotomaker.utils.view.AddStickerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Objects;

import dauroi.com.imageprocessing.ImageProcessingView;
import dauroi.com.imageprocessing.ImageProcessor;
import dauroi.com.imageprocessing.filter.ImageFilter;
import dauroi.photoeditor.database.DatabaseManager;
import dauroi.photoeditor.utils.PhotoUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: Tính năng edit ảnh cho user
 * Des: Chứa các action để edit ảnh như filter, crop, add text, .....
 */
public class EditorActivity extends BaseAppActivity implements AddTextFragment.ISendTextView {
    public static final String EDITOR = "editor";
    private static final String TAG = "EditorActivity";

    private ActivityImageProcessingBinding binding;
    private ImageFilter mFilter;
    private Bitmap mImage;
    private int photoWidth, photoHeight;
    private OnDoneActionsClickListener mDoneActionsClickListener;
    private BaseAction mCurrentAction;
    private BaseAction[] mActions;
    private DialogLoading dialog;
    private DialogExitFeature dialogExit;
    private String from, pathsPhoto;
    private ApplicationViewModel applicationViewModel;
    private boolean isPreview;
    private ArrayList<StickerTheme> stickerThemes;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityImageProcessingBinding.inflate(getLayoutInflater());
    }

    @Override
    public void onHasNetwork() {
        loadAds();
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.NAME);
        applicationViewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        stickerThemes = new ArrayList<>();
        dialogExit = new DialogExitFeature(this, EditorActivity.this::discard);
        dialog = new DialogLoading(this, DialogLoading.CREATE);
        dialog.show();
        ArrayList<Icon> icons = new ArrayList<>();
        InitData.addIconImageProcess(icons, this);
        IconsAdapter adapter = new IconsAdapter(icons, this, this::usingFeature, EDITOR);
        binding.rvFeatureEditor.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvFeatureEditor.addItemDecoration(new BetweenSpacesItemDecoration(0, 8));
        binding.rvFeatureEditor.setAdapter(adapter);

        // listener cho add sticker view
        binding.addStickerView.setListener(new AddStickerView.IStickerViewListener() {
            @Override
            public void previewAction(StickerItem stickerItem) {
                add(stickerItem);
            }

            @Override
            public void jumpToShop() {
                Intent intent = new Intent(EditorActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, Constant.STICKER);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction() {
                binding.tbChild.setVisibility(View.VISIBLE);
                isPreview = false;
            }

            @Override
            public void backAction() {
                binding.tbChild.setVisibility(View.VISIBLE);
                isPreview = false;
            }
        });
    }

    @Override
    public void buttonClick() {
        onIvBackClicked();
        onIvApplyClicked();
        binding.ivNotApply.setOnClickListener(v -> onIvNotApplyClicked());
        onIvSaveShareClicked();
    }

    private void loadAds() {
        Advertisement.loadBannerAdExit(this);
        binding.adsLayout.post(() -> Advertisement.showBannerAdHome(this, binding.adsLayout, AdSize.BANNER, new BannerAdsUtils.BannerAdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdFailed() {

            }
        }, () -> {

        }));
        Advertisement.loadInterstitialAdInApp(this);
    }

    public void initData() {
        // Khởi tạo và get data base từ db. Muốn hiển thị các item cho các feature thì phải chỉnh sửa trong file db của app
        if (!DatabaseManager.getInstance(this).isDbFileExisted()) {
            DatabaseManager.getInstance(this).createDb();
        } else {
            if (DatabaseManager.getInstance(this).openDb()) {
                Log.d(TAG, "initData: database open");
            } else {
                Log.d(TAG, "initData: database not open");
            }
        }
        if (getIntent() != null) {
            pathsPhoto = getIntent().getStringExtra(PickPhotoActivity.PHOTOS_PICKED);
            from = getIntent().getStringExtra(Constant.VALUE_INTENT);
        }
        Glide.with(this).asBitmap().load(pathsPhoto).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    // set image cho image precessing
                    binding.imageProcessing.setImage(resource);
                    dialog.dismiss();
                }, 500);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }
        });
        mImage = ImageDecoder.decodeFileToBitmap(pathsPhoto);
        binding.imageProcessing.setScaleType(ImageProcessor.ScaleType.CENTER_INSIDE);

        // set background cho image processing
        final int color = getResources().getColor(R.color.background);
        float r = Color.red(color) / 255.0f;
        float g = Color.green(color) / 255.0f;
        float b = Color.blue(color) / 255.0f;
        float a = Color.alpha(color) / 255.0f;
        binding.imageProcessing.setBackground(r, g, b, a);

        // photoViewLayout phải được rend xong thì mới create các action cho editor
        binding.photoViewLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                photoHeight = binding.photoViewLayout.getHeight();
                photoWidth = binding.photoViewLayout.getWidth();

                mActions = new BaseAction[6];
                mActions[0] = new EffectAction(EditorActivity.this);
                mActions[1] = new CropAction(EditorActivity.this);
                mActions[2] = new RotationAction(EditorActivity.this);
                mActions[3] = new DrawAction(EditorActivity.this);
                mActions[4] = new FocusAction(EditorActivity.this);
                mActions[5] = new TouchBlurAction(EditorActivity.this);
                binding.photoViewLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        // get data sticker
        applicationViewModel.getDataStickers().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (!itemsDownloaded.isEmpty()) {
                    for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                        for (StickerTheme stickerTheme : stickerThemes) {
                            if (itemDownloaded.getName().equals(stickerTheme.getNameTheme())) {
                                stickerTheme.setDownloaded(true);
                            }
                        }
                    }
                } else {
                    for (StickerTheme stickerTheme : stickerThemes) {
                        stickerTheme.setDownloaded(false);
                    }
                }
                binding.addStickerView.setMoreSticker(stickerThemes);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    // get data sticker
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getStickerData(EventItemShopSticker eventItemShopSticker) {
        stickerThemes.clear();
        stickerThemes.addAll(eventItemShopSticker.getStickerThemes());
    }

    private void usingFeature(int position) {
        switch (position) {
            case 0:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.EFFECT);
                binding.rvFeatureEditor.setVisibility(View.INVISIBLE);
                binding.ivBack.setVisibility(View.INVISIBLE);
                binding.ivSaveShare.setVisibility(View.INVISIBLE);
                binding.clSpecFeature.setVisibility(View.VISIBLE);
                binding.tvNameFeature.setText(getString(R.string.effects));
                selectAction(0);
                break;
            case 1:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.CROP);
                binding.rvFeatureEditor.setVisibility(View.INVISIBLE);
                binding.ivBack.setVisibility(View.INVISIBLE);
                binding.ivSaveShare.setVisibility(View.INVISIBLE);
                binding.clSpecFeature.setVisibility(View.VISIBLE);
                binding.tvNameFeature.setText(getString(R.string.crop));
                selectAction(1);
                break;
            case 2:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.ROTATE);
                binding.rvFeatureEditor.setVisibility(View.INVISIBLE);
                binding.ivBack.setVisibility(View.INVISIBLE);
                binding.ivSaveShare.setVisibility(View.INVISIBLE);
                binding.clSpecFeature.setVisibility(View.VISIBLE);
                binding.tvNameFeature.setText(getString(R.string.rotate));
                selectAction(2);
                break;
            case 3:
                if (checkDoubleClick()) {
                    MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.TEXT);
                    AddTextFragment addTextFragment = new AddTextFragment();
                    addTextFragment.show(getSupportFragmentManager(), addTextFragment.getTag());
                }
                break;
            case 4:
                if (checkDoubleClick()) {
                    binding.addStickerView.show();
                    binding.tbChild.setVisibility(View.INVISIBLE);
                }
                break;
            case 5:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.DRAW);
                binding.rvFeatureEditor.setVisibility(View.INVISIBLE);
                binding.ivBack.setVisibility(View.INVISIBLE);
                binding.ivSaveShare.setVisibility(View.INVISIBLE);
                binding.clSpecFeature.setVisibility(View.VISIBLE);
                binding.tvNameFeature.setText(getString(R.string.draw));
                selectAction(3);
                break;
            case 6:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.FOCUS);
                binding.rvFeatureEditor.setVisibility(View.INVISIBLE);
                binding.ivBack.setVisibility(View.INVISIBLE);
                binding.ivSaveShare.setVisibility(View.INVISIBLE);
                binding.clSpecFeature.setVisibility(View.VISIBLE);
                binding.tvNameFeature.setText(getString(R.string.focus));
                selectAction(4);
                break;
            case 7:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.BLUR);
                binding.rvFeatureEditor.setVisibility(View.INVISIBLE);
                binding.ivBack.setVisibility(View.INVISIBLE);
                binding.ivSaveShare.setVisibility(View.INVISIBLE);
                binding.clSpecFeature.setVisibility(View.VISIBLE);
                binding.tvNameFeature.setText(getString(R.string.blur));
                selectAction(5);
                break;
            default:
        }
    }

    public void onIvBackClicked() {
        binding.ivBack.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                dialogExit.show();
            }
        });
    }

    public void onIvSaveShareClicked() {
        binding.ivSaveShare.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                saveAndShare();
            }
        });
    }

    // chức năng save and share
    public void saveAndShare() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.DONE);
        // nếu border và sticker của khung sticker đang hiện khi export thì phải ẩn đi
        if (binding.sticker.isShowBorder() && binding.sticker.isShowIcons()) {
            binding.sticker.setShowBorder(false);
            binding.sticker.setShowIcons(false);
        }
        Glide.with(this).load(binding.imageProcessing.getBm()).into(binding.ivImage); // get bitmap của image processing sau đó lưu vào ivImage
        Glide.with(this).load(ImageUtils.getBitmapFromView(binding.sticker)).into(binding.ivText); // get bitmap của sticker view sau đó lưu vào ivText
        dialog.show();

        // delay ít nhất 2s nếu không sẽ không kịp lấy ảnh bitmap
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            String parentFilePath = FileUtil.getParentFileString("My Creative");
            File parentFile = new File(parentFilePath);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "saveAndShare: create");
                } else {
                    Log.d(TAG, "saveAndShare: not create");
                }
            }
            File file = new File(parentFile, DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png"));

            try {
                // export bitmap to png
                Objects.requireNonNull(ImageUtils.getBitmapFromView(binding.clFinal)).compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            // check permission tránh trường hợp user hủy permission gây ra lôi
            if (new PermissionChecker(this).isPermissionOK()) {
                PhotoUtils.addImageToGallery(file.getAbsolutePath(), this); // add image to gallery
                Toasty.success(this, getString(R.string.saving_done), Toasty.LENGTH_SHORT).show();
                Advertisement.showInterstitialAdInApp(this, new InterstitialAdsPopupOthers.OnAdsListener() {
                    @Override
                    public void onAdsClose() {
                        Intent intent = new Intent(EditorActivity.this, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.EDITOR);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                    @Override
                    public void close() {
                        Intent intent = new Intent(EditorActivity.this, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.EDITOR);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                });
            }
            dialog.dismiss();
        }, 2000);
    }

    @Override
    public void turnOff() {
        binding.rvFeatureEditor.setVisibility(View.VISIBLE);
        binding.ivBack.setVisibility(View.VISIBLE);
        binding.ivSaveShare.setVisibility(View.VISIBLE);
        binding.clFeature.setVisibility(View.VISIBLE);
    }

    // add text cho image
    @Override
    public void sendText(TextOfUser textOfUser) {
        binding.sticker.setLocked(false);
        binding.sticker.setConstrained(true);
        binding.sticker.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.sticker.setShowBorder(true);
                binding.sticker.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerClicked: ");
                binding.sticker.swapLayers(binding.sticker.getStickers().indexOf(sticker), binding.sticker.getStickerCount() - 1);
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.sticker.setShowBorder(false);
                    binding.sticker.setShowIcons(false);
                } else {
                    binding.sticker.setShowBorder(true);
                    binding.sticker.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.sticker.configDefaultIcons("text");

        TextSticker textSticker = new TextSticker(this, getResources().getDrawable(R.drawable.sticker_transparent_background_hor));
        if (!textOfUser.getContent().isEmpty()) {
            textSticker.setTextOfUser(textOfUser);
            textSticker.resizeText();
            binding.sticker.addSticker(textSticker);
        }
    }

    // apply tính năng cho ảnh
    public void onIvApplyClicked() {
        binding.ivApply.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.APPLY);
            binding.ivBack.setVisibility(View.VISIBLE);
            binding.ivSaveShare.setVisibility(View.VISIBLE);
            binding.rvFeatureEditor.setVisibility(View.VISIBLE);
            binding.clSpecFeature.setVisibility(View.INVISIBLE);
            mDoneActionsClickListener.onApplyButtonClick();
        });
    }

    // hủy apply tính năng
    public void onIvNotApplyClicked() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.CANCEL_APPLY);
        binding.ivBack.setVisibility(View.VISIBLE);
        binding.ivSaveShare.setVisibility(View.VISIBLE);
        binding.rvFeatureEditor.setVisibility(View.VISIBLE);
        binding.clSpecFeature.setVisibility(View.INVISIBLE);

        binding.bottomLayout.setVisibility(View.INVISIBLE);
        if (getCurrentAction() != null) {
            getCurrentAction().onDetach();
            if (getCurrentAction() instanceof RotationAction) {
                ((RotationAction) getCurrentAction()).reset();
            }
        }
    }

    public void setDoneActionsClickListener(OnDoneActionsClickListener doneActionsClickListener) {
        mDoneActionsClickListener = doneActionsClickListener;
    }

    public void setCurrentAction(BaseAction currentAction) {
        this.mCurrentAction = currentAction;
    }

    public BaseAction getCurrentAction() {
        return mCurrentAction;
    }

    public void attachBottomMenu(View view) {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.photo_editor_slide_in_bottom);
        binding.bottomLayout.setVisibility(View.VISIBLE);
        binding.bottomLayout.removeAllViews();
        binding.bottomLayout.addView(view);
        view.startAnimation(anim);
    }

    public Bitmap getImage() {
        if (mImage == null) {
            if (pathsPhoto != null) {
                mImage = ImageDecoder.decodeFileToBitmap(pathsPhoto);
            }
        }
        return mImage;
    }

    // bật mask view cho 1 số tính năng cần sử dụng đến nó
    public void attachMaskView(View v) {
        binding.imageViewLayout.removeAllViews();
        if (v != null) {
            binding.imageViewLayout.addView(v);
            binding.imageViewLayout.setVisibility(View.VISIBLE);
        } else {
            binding.imageViewLayout.setVisibility(View.GONE);
        }
    }

    // apply filter
    public void applyFilter(ImageFilter filter) {
        if (mFilter != filter) {
            mFilter = filter;
            binding.imageProcessing.setFilter(filter);
            binding.imageProcessing.requestRender();
        }
    }

    public ImageFilter getFilter() {
        return mFilter;
    }

    public void setImage(Bitmap image, boolean recycleOld) {
        if (image != null && !image.isRecycled()) {
            if (recycleOld && mImage != null && mImage != image) {
                mImage = null;
                System.gc();
            }

            if (binding.imageProcessing.getImageProcessor() != null) {
                binding.imageProcessing.getImageProcessor().deleteImage();
                mImage = image;
                binding.imageProcessing.setImage(mImage);
            }
        }
    }

    public int getImageWidth() {
        if (mImage != null && !mImage.isRecycled()) {
            return mImage.getWidth();
        }
        return 0;
    }

    public int getImageHeight() {
        if (mImage != null && !mImage.isRecycled()) {
            return mImage.getHeight();
        }
        return 0;
    }

    public int[] calculateThumbnailSize() {
        return calculateThumbnailSize(getImageWidth(), getImageHeight());
    }

    public float calculateScaleRatio(int imageWidth, int imageHeight) {
        float ratioWidth = ((float) imageWidth) / getPhotoViewWidth();
        float ratioHeight = ((float) imageHeight) / getPhotoViewHeight();
        return Math.max(ratioWidth, ratioHeight);
    }

    public int[] calculateThumbnailSize(int imageWidth, int imageHeight) {
        int[] size = new int[2];
        float ratioWidth = ((float) imageWidth) / getPhotoViewWidth();
        float ratioHeight = ((float) imageHeight) / getPhotoViewHeight();
        float ratio = Math.max(ratioWidth, ratioHeight);
        if (ratio == ratioWidth) {
            size[0] = getPhotoViewWidth();
            size[1] = (int) (imageHeight / ratio);
        } else {
            size[0] = (int) (imageWidth / ratio);
            size[1] = getPhotoViewHeight();
        }
        return size;
    }

    public int getPhotoViewWidth() {
        return photoWidth;
    }

    public int getPhotoViewHeight() {
        return photoHeight;
    }

    public ImageView getNormalImageView() {
        return binding.sourceImage;
    }

    public ImageProcessingView getImageProcessingView() {
        return binding.imageProcessing;
    }

    public float calculateScaleRatio() {
        return calculateScaleRatio(getImageWidth(), getImageHeight());
    }

    public RotationAction getRotationAction() {
        if (mActions != null && mActions[2] != null) {
            Log.d(TAG, "getRotationAction() called");
            return (RotationAction) mActions[2];
        } else {
            return null;
        }
    }

    public CropAction getCropAction() {
        if (mActions != null && mActions[1] != null) {
            Log.d(TAG, "getCropAction() called");
            return (CropAction) mActions[1];
        } else {
            return null;
        }
    }

    // select action của user
    public void selectAction(int position) {
        switch (position) {
            case 0:
                if (mActions[position] == null) {
                    mActions[position] = new EffectAction(this);
                }
                break;
            case 1:
                if (mActions[position] == null) {
                    mActions[position] = new CropAction(this);
                }
                break;
            case 2:
                if (mActions[position] == null) {
                    mActions[position] = new RotationAction(this);
                }
                break;
            case 3:
                if (mActions[position] == null) {
                    mActions[position] = new DrawAction(this);
                }
                break;
            case 4:
                if (mActions[position] == null) {
                    mActions[position] = new FocusAction(this);
                }
                break;
            case 5:
                if (mActions[position] == null) {
                    mActions[position] = new TouchBlurAction(this);
                }
                break;
            default:
                break;
        }
        if (mActions[position] != null) {
            mActions[position].attach();
        }
    }

    // vì có 2 luông đều dẫn tới màn này nên phải check cờ from xem nó bắt đầu từ đâu
    public void discard() {
        if (from.equals(PickPhotoActivity.TAG)) {
            Intent intent = new Intent(this, PickPhotoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(PickPhotoActivity.MODE, Constant.EDITOR);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        } else {
            Intent intent = new Intent(this, MyCreativeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(PickPhotoActivity.MODE, Constant.EDITOR);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.clSpecFeature.getVisibility() == View.VISIBLE) {
            onIvNotApplyClicked();
        } else if (binding.addStickerView.getVisibility() == View.VISIBLE) {
            binding.addStickerView.backAction();
        } else {
            dialogExit.show();
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Editor.CANCEL_APPLY);
    }

    // add sticker
    public void add(StickerItem item) {
        binding.sticker.setLocked(false);
        binding.sticker.setConstrained(true);
        binding.sticker.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.sticker.setShowBorder(true);
                binding.sticker.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerClicked: ");
                binding.sticker.swapLayers(binding.sticker.getStickers().indexOf(sticker), binding.sticker.getStickerCount() - 1);
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.sticker.setShowBorder(false);
                    binding.sticker.setShowIcons(false);
                } else {
                    binding.sticker.setShowBorder(true);
                    binding.sticker.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.sticker.configDefaultIcons("sticker");
        Glide.with(this).load(item.getUrl()).centerCrop().into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable drawable, @Nullable Transition<? super Drawable> transition) {
                if (isPreview) {
                    binding.sticker.removeCurrentSticker();
                } else {
                    isPreview = true;
                }
                binding.sticker.addSticker(new DrawableSticker(drawable));
            }

            @Override
            public void onLoadCleared(@Nullable Drawable drawable) {

            }
        });
    }
}
