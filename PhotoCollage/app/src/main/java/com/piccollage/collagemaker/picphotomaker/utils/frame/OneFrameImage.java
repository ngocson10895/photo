package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.PointF;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.List;

public class OneFrameImage {
    public static void collage_1_0(int position, List<Photo> photosPicked) {
        photosPicked.get(position).bound.set(0, 0, 1, 1);
        photosPicked.get(position).pointList.add(new PointF(0, 0));
        photosPicked.get(position).pointList.add(new PointF(1, 0));
        photosPicked.get(position).pointList.add(new PointF(1, 1));
        photosPicked.get(position).pointList.add(new PointF(0, 1));
    }
}
