package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.GradientAdapterEx;
import com.piccollage.collagemaker.picphotomaker.adapter.ItemFeatureAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.PaletteAdapterEx;
import com.piccollage.collagemaker.picphotomaker.adapter.PatternAdapterEx;
import com.piccollage.collagemaker.picphotomaker.base.BaseView;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.BackgroundViewBinding;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradients;
import com.piccollage.collagemaker.picphotomaker.viewholder.Palette;
import com.piccollage.collagemaker.picphotomaker.viewholder.Palettes;
import com.piccollage.collagemaker.picphotomaker.viewholder.Pattern;
import com.piccollage.collagemaker.picphotomaker.viewholder.Patterns;

import java.util.ArrayList;

/**
 * Create from Administrator
 * Purpose: Cho user chọn background cho ảnh
 * Des: Hiển thị các loại background như gradients, pattern để hiện thị cho user
 */
public class BackgroundView extends BaseView {
    private BackgroundViewBinding binding;
    private IBackgroundListener listener;
    private ItemFeatureAdapter featureAdapter;
    private int curFeature = 0;
    private Bitmap curBm, oldBm;
    private String type;

    public interface IBackgroundListener {
        void previewBackground(int color);

        void previewPattern(String path);

        void previewGradient(String stColor, String endColor);

        void previewPalette(String c1, String c2, String c3, String c4, String c5);

        void jumpToShop(String type);

        void doneAction(Bitmap bm);

        void closeAction(Bitmap bm);
    }

    public void setListener(IBackgroundListener listener) {
        this.listener = listener;
    }

    public BackgroundView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Retrieve attributes from xml
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BackgroundView);
        try {
            if (typedArray.getString(R.styleable.BackgroundView_feature) != null) {
                type = typedArray.getString(R.styleable.BackgroundView_feature); // phải khai báo type trong view để nhận diện feature
            } else {
                type = "";
            }
        } finally {
            typedArray.recycle();
        }
        initData();
    }

    @Override
    public void initData() {
        if (type.equals("Quote")) {
            binding.ivHideView.setVisibility(INVISIBLE);
            featureAdapter = new ItemFeatureAdapter(InitData.addBackgroundIconQuote(), getContext());
            binding.rvFeature.setAdapter(featureAdapter);
            featureAdapter.setOnItemClickListener((adapter, view, position) -> {
                if (curFeature != position) {
                    int temp = curFeature;
                    curFeature = position;
                    featureAdapter.getData().get(temp).setPick(false);
                    featureAdapter.notifyItemChanged(temp);
                    featureAdapter.getData().get(curFeature).setPick(true);
                    featureAdapter.notifyItemChanged(curFeature);
                    choseFeature(position);
                }
            });
        } else {
            binding.ivHideView.setVisibility(VISIBLE);
            featureAdapter = new ItemFeatureAdapter(InitData.addBackgroundIcon(), getContext());
            binding.rvFeature.setAdapter(featureAdapter);
            featureAdapter.setOnItemClickListener((adapter, view, position) -> {
                if (curFeature != position) {
                    int temp = curFeature;
                    curFeature = position;
                    featureAdapter.getData().get(temp).setPick(false);
                    featureAdapter.notifyItemChanged(temp);
                    featureAdapter.getData().get(curFeature).setPick(true);
                    featureAdapter.notifyItemChanged(curFeature);
                    choseFeature(position);
                }
            });
        }
    }

    private void choseFeature(int position) {
        switch (position) {
            case 0:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.SOLID);
                showSolidBackground();
                break;
            case 1:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.GRADIENT);
                binding.rvGradients.setVisibility(VISIBLE);
                binding.sbColor.setVisibility(INVISIBLE);
                binding.rvPatterns.setVisibility(INVISIBLE);
                binding.rvPalette.setVisibility(INVISIBLE);
                binding.typeName.setText(getContext().getString(R.string.gradient_background));
                break;
            case 2:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.PATTERN);
                binding.rvPatterns.setVisibility(VISIBLE);
                binding.sbColor.setVisibility(INVISIBLE);
                binding.rvGradients.setVisibility(INVISIBLE);
                binding.rvPalette.setVisibility(INVISIBLE);
                binding.typeName.setText(getContext().getString(R.string.patterns));
                break;
            case 3:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.PALETTE);
                binding.rvPalette.setVisibility(VISIBLE);
                binding.sbColor.setVisibility(INVISIBLE);
                binding.rvGradients.setVisibility(INVISIBLE);
                binding.rvPatterns.setVisibility(INVISIBLE);
                binding.typeName.setText(getContext().getString(R.string.palette));
                break;
        }
    }

    private void showSolidBackground() {
        featureAdapter.getData().get(curFeature).setPick(false);
        featureAdapter.notifyItemChanged(curFeature);
        curFeature = 0;
        featureAdapter.getData().get(curFeature).setPick(true);
        featureAdapter.notifyItemChanged(curFeature);
        binding.sbColor.setVisibility(VISIBLE);
        binding.rvGradients.setVisibility(INVISIBLE);
        binding.rvPatterns.setVisibility(INVISIBLE);
        binding.rvPalette.setVisibility(INVISIBLE);
        binding.typeName.setText(getContext().getString(R.string.solid_background));
    }

    @Override
    protected void initView() {
        binding = BackgroundViewBinding.bind(this);
        binding.sbColor.setHexColors(InitData.colors());
        binding.sbColor.setListener((position, color) -> {
            ImageView imageView = (ImageView) featureAdapter.getViewByPosition(binding.rvFeature, 0, R.id.iconC);
            if (imageView != null) {
                Glide.with(getContext()).load(new ColorDrawable(color)).transform(new CircleCrop()).into(imageView);
            }
            listener.previewBackground(color);
            curBm = ImageDecoder.drawableToBitmap(new ColorDrawable(color));
        });

        binding.rvFeature.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvFeature.addItemDecoration(new BetweenSpacesItemDecoration(0, 16));

        binding.rvGradients.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvPatterns.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvPalette.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        curBm = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.WHITE));
        oldBm = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.WHITE));
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.NAME);
    }

    @Override
    public int getLayoutID() {
        return R.layout.background_view;
    }

    @Override
    public void buttonClick() {
        onDoneClick();
        onHideView();
    }

    @Override
    public void backAction() {
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.CANCEL);
        binding.sbColor.setSelection(0);
        if (curBm != oldBm) {
            curBm = oldBm;
        }
        showSolidBackground();
        listener.closeAction(curBm);
        hide();
    }

    public void setDataGradientsEx(ArrayList<Gradients> gradients) {
        GradientAdapterEx gradientAdapterEx = new GradientAdapterEx(gradients, getContext());
        gradientAdapterEx.setiPickGradient(new GradientAdapterEx.IPickGradient() {
            @Override
            public void pick(Gradient gradient, String title) {
                listener.previewGradient(gradient.getStColor(), gradient.getEndColor());
                curBm = ImageDecoder.drawableToBitmap(new GradientDrawable(GradientDrawable.Orientation.TL_BR
                        , new int[]{Color.parseColor(gradient.getStColor()), Color.parseColor(gradient.getEndColor())}));
            }

            @Override
            public void pickMore() {
                listener.jumpToShop(Constant.GRADIENT);
            }
        });
        if (gradients.size() == 2) {
            gradientAdapterEx.toggleGroup(1);
        }
        binding.rvGradients.setAdapter(gradientAdapterEx);
    }

    public void setDataPaletteEx(ArrayList<Palettes> palettes) {
        PaletteAdapterEx paletteAdapterEx = new PaletteAdapterEx(palettes);
        paletteAdapterEx.setiPickPalette(new PaletteAdapterEx.IPickPalette() {
            @Override
            public void pick(Palette palette, String title) {
                listener.previewPalette(palette.getColor1(), palette.getColor2(), palette.getColor3(), palette.getColor4(), palette.getColor5());
            }

            @Override
            public void pickMore() {
                listener.jumpToShop(Constant.PALETTE);
            }
        });
        if (palettes.size() == 2) {
            paletteAdapterEx.toggleGroup(1);
        }
        binding.rvPalette.setAdapter(paletteAdapterEx);
    }

    public void setDataPatternsEx(ArrayList<Patterns> patterns) {
        PatternAdapterEx patternAdapterEx = new PatternAdapterEx(patterns, getContext());
        patternAdapterEx.setiPickPattern(new PatternAdapterEx.IPickPattern() {
            @Override
            public void pick(Pattern pattern, String title) {
                listener.previewPattern(pattern.getPath());
                if (pattern.getPath().contains("storage")) {
                    curBm = ImageDecoder.decodeFileToBitmap(pattern.getPath());
                } else
                    curBm = ImageDecoder.getBitmapFromAssets("background/" + pattern.getPath(), getContext());
            }

            @Override
            public void pickMore() {
                listener.jumpToShop(Constant.PATTERN);
            }
        });
        if (patterns.size() == 2) {
            patternAdapterEx.toggleGroup(1);
        }
        binding.rvPatterns.setAdapter(patternAdapterEx);
    }

    private void onDoneClick() {
        binding.ivDoneView.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewBackground.DONE);
            oldBm = curBm;
            listener.doneAction(curBm);
            showSolidBackground();
            hide();
        });
    }

    private void onHideView() {
        binding.ivHideView.setOnClickListener(v -> backAction());
    }
}
