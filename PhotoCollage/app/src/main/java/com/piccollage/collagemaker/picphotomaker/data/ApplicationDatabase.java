package com.piccollage.collagemaker.picphotomaker.data;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.entity.Quote;

/**
 * creates db. Nếu có thay đổi trong db thì phải thay đổi version
 */
@Database(entities = {ItemDownloaded.class, PipPicture.class}, version = 3)
public abstract class ApplicationDatabase extends RoomDatabase {
    private static ApplicationDatabase instance;

    abstract ApplicationDao applicationDao();

    public static synchronized ApplicationDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), ApplicationDatabase.class, "data_item_downloaded")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }
}
