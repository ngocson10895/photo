package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.LocaleHelper;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemLanguageBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * Change language Adapter
 */
public class ChangeLanguageAdapter extends RecyclerView.Adapter<ChangeLanguageAdapter.ViewHolder> implements Filterable {
    private int selectedLanguagePosition;
    private Context context;
    private ArrayList<String> item, itemClone;
    private String languageSelected;
    private int oldLanguagePosition;
    private ISelectLanguage iSelectLanguage;

    public ChangeLanguageAdapter(Context context, ArrayList<Language> languageArrayList) {
        this.context = context;
        item = new ArrayList<>();
        itemClone = new ArrayList<>();
        for (Language language : languageArrayList) {
            item.add(language.getLanguage());
            itemClone.add(language.getLanguage());
        }
        languageSelected = LocaleHelper.getLanguageCountryName(context);
    }

    public interface ISelectLanguage {
        void select(String language);
    }

    public void setiSelectLanguage(ISelectLanguage iSelectLanguage) {
        this.iSelectLanguage = iSelectLanguage;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_language, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bindView(i);
    }

    @Override
    public int getItemCount() {
        return itemClone.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<String> filteredResults;
                if (constraint.length() == 0) {
                    filteredResults = item;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemClone = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    private List<String> getFilteredResults(String key) {
        String constraint = key.trim();
        List<String> results = new ArrayList<>();
        for (String item : item) {
            if (item.toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemLanguageBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemLanguageBinding.bind(itemView);
        }

        void bindView(int p) {
            String language = itemClone.get(p);
            binding.tvLanguage.setText(language);
            boolean checked = TextUtils.equals(language, languageSelected);
            if (checked) {
                selectedLanguagePosition = p;
                binding.ivCheck.setVisibility(View.VISIBLE);
            } else {
                binding.ivCheck.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(v -> {
                iSelectLanguage.select(language);
                languageSelected = language;
                oldLanguagePosition = selectedLanguagePosition;
                selectedLanguagePosition = p;
                notifyItemChanged(oldLanguagePosition);
                binding.ivCheck.setVisibility(View.VISIBLE);
            });
        }
    }
}
