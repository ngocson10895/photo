package com.piccollage.collagemaker.picphotomaker.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.piccollage.collagemaker.picphotomaker.R;

/**
 * Base View
 */
public abstract class BaseView extends ConstraintLayout {

    private Animation animSlideUp, animSlideDown; // animation for view

    public BaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(getLayoutID(), this);
        initView();
        buttonClick();
        animSlideUp = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
        animSlideDown = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
    }

    public abstract void initData();

    protected abstract void initView();

    public abstract int getLayoutID();

    public void show(){
        setVisibility(VISIBLE);
        startAnimation(animSlideUp);
    }

    public void hide(){
        setVisibility(GONE);
        startAnimation(animSlideDown);
    }

    public abstract void buttonClick();

    public abstract void backAction();
}
