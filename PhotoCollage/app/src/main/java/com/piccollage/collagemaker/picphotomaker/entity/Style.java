package com.piccollage.collagemaker.picphotomaker.entity;

/**
 * Style
 */
public class Style{
    private String nameStyle;
    private String description;
    private int resource;
    private String proVer1;
    private String proVer2;
    private boolean isSale;

    public Style(String nameStyle, String description, int resource, String proVer1, String proVer2, boolean isSale) {
        this.nameStyle = nameStyle;
        this.description = description;
        this.resource = resource;
        this.proVer1 = proVer1;
        this.proVer2 = proVer2;
        this.isSale = isSale;
    }

    public String getNameStyle() {
        return nameStyle;
    }

    public String getDescription() {
        return description;
    }

    public int getResource() {
        return resource;
    }

    public String getProVer1() {
        return proVer1;
    }

    public String getProVer2() {
        return proVer2;
    }

    public boolean isSale() {
        return isSale;
    }
}
