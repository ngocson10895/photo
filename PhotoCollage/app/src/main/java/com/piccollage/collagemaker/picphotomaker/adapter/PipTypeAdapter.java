package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemPipTypeBinding;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;

import java.util.ArrayList;

/**
 * Pip type adapter trong màn hình pip style
 */
public class PipTypeAdapter extends BaseQuickAdapter<PipPicture, BaseViewHolder> {
    private Context context;

    public PipTypeAdapter(@Nullable ArrayList<PipPicture> data, Context context) {
        super(R.layout.item_pip_type, data);
        this.context = context;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, PipPicture item) {
        ItemPipTypeBinding binding = ItemPipTypeBinding.bind(helper.itemView);
        if (item.getPathPreview().contains("file:///android_asset/")) { // check item from assets
            Glide.with(context).asBitmap().load(Uri.parse(item.getPathPreview()))
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_imgplaceholder)).into(binding.ivFrame);
            binding.iconDownload.setVisibility(View.INVISIBLE);
        } else { // check item from server
            Glide.with(context).load(item.getPathPreview())
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_imgplaceholder)).into(binding.ivFrame);
            if (item.isDownloaded()) {
                binding.iconDownload.setVisibility(View.INVISIBLE);
            } else binding.iconDownload.setVisibility(View.VISIBLE);
        }
        binding.v.setVisibility((item.isPick()) ? View.VISIBLE : View.INVISIBLE);
    }
}
