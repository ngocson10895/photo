package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.GeometryUtils;

import java.util.HashMap;
import java.util.List;

/**
 * Three Frame Images
 */
public class ThreeFrameImage {
    public static void collage_3_47(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(2, 1));
        }
    }


    public static void collage_3_46(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.4167f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.6f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.75f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8333f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_3_45(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.4f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.2f, 0, 0.8f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.6f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_3_44(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.4167f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.6f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3333f));
            photosPicked.get(position).pointList.add(new PointF(1, 0.8333f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_3_43(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.4f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.2f, 1, 0.8f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.6667f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.3333f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.6f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_3_42(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.6f, 0.8f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.75f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.4f, 0, 1, 0.7f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 0.8571f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.6f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.6f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(2, 1));
        }
    }

    public static void collage_3_41(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.6666f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3333f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.6667f));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_3_40(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(5), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_39(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_38(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_37(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_36(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_35(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
        }
    }

    public static void collage_3_34(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
        }
    }

    public static void collage_3_33(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_3_32(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_3_31(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_3_30(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_3_29(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_28(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_27(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_26(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
    }

    public static void collage_3_25(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
    }

    public static void collage_3_24(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_23(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
    }

    public static void collage_3_22(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_3_21(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.3333f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_20(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_19(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_18(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_17(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0.3333f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_16(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_15(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.6667f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6667f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_14(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.3333f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_13(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).fitBound = true;
            photosPicked.get(position).clearPath = new Path();
            photosPicked.get(position).clearPath.addRect(0, 0, 512, 512, Path.Direction.CCW);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.5f, 0.25f, 1.5f, 0.75f);
            photosPicked.get(position).clearPathInCenterVertical = true;
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_13;
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).fitBound = true;
            photosPicked.get(position).clearPath = new Path();
            photosPicked.get(position).clearPath.addRect(0, 0, 512, 512, Path.Direction.CCW);
            photosPicked.get(position).clearPathRatioBound = new RectF(-0.5f, 0.25f, 0.5f, 0.75f);
            photosPicked.get(position).clearPathInCenterVertical = true;
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_13;
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.25f, 0f, 0.75f, 1f);
            photosPicked.get(position).path = new Path();
            photosPicked.get(position).path.addRect(0, 0, 512, 512, Path.Direction.CCW);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).pathInCenterVertical = true;
            photosPicked.get(position).pathInCenterHorizontal = true;
            photosPicked.get(position).fitBound = true;
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_13;
        }
    }

    public static void collage_3_12(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_11(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6667f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_10(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_USING_MAP;
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(-2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(-2, -1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, -1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, -1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(5), new PointF(-1, -1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(6), new PointF(-1, -1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(7), new PointF(2, -1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_USING_MAP;
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(-1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(-1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(5), new PointF(-2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(6), new PointF(-2, -2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(7), new PointF(2, -2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.25f, 0.25f, 0.75f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_9(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.6667f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_8(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = new Path();
            photosPicked.get(position).clearPath.addCircle(256, 256, 256, Path.Direction.CCW);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.5f, 0.25f, 1.5f, 0.75f);
            photosPicked.get(position).clearPathInCenterVertical = true;
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = new Path();
            photosPicked.get(position).clearPath.addCircle(256, 256, 256, Path.Direction.CCW);
            photosPicked.get(position).clearPathRatioBound = new RectF(-0.5f, 0.25f, 0.5f, 0.75f);
            photosPicked.get(position).clearPathInCenterVertical = true;
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_8;
            photosPicked.get(position).bound.set(0.25f, 0f, 0.75f, 1f);
            photosPicked.get(position).path = new Path();
            photosPicked.get(position).path.addCircle(256, 256, 256, Path.Direction.CCW);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).pathInCenterVertical = true;
        }
    }

    public static void collage_3_7(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_6(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_6;
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_6;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = new Path();
            GeometryUtils.createRegularPolygonPath(photosPicked.get(position).clearPath, 512, 6, 0);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.5f, 0.25f, 1.5f, 0.75f);
            photosPicked.get(position).clearPathInCenterVertical = true;
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_6;
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_6;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = new Path();
            GeometryUtils.createRegularPolygonPath(photosPicked.get(position).clearPath, 512, 6, 0);
            photosPicked.get(position).clearPathRatioBound = new RectF(-0.5f, 0.25f, 0.5f, 0.75f);
            photosPicked.get(position).clearPathInCenterVertical = true;
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_6;
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_6;
            photosPicked.get(position).bound.set(0.25f, 0f, 0.75f, 1f);
            photosPicked.get(position).path = new Path();
            GeometryUtils.createRegularPolygonPath(photosPicked.get(position).path, 512, 6, 0);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).pathInCenterVertical = true;
        }
    }

    public static void collage_3_5(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_3_4(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = FrameImageUtils.createHeartItem(0, 512);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.25f, 0.5f, 0.75f, 1.5f);
            photosPicked.get(position).clearPathInCenterHorizontal = true;
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = FrameImageUtils.createHeartItem(0, 512);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.25f, -0.5f, 0.75f, 0.5f);
            photosPicked.get(position).clearPathInCenterHorizontal = true;
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).path = FrameImageUtils.createHeartItem(0, 512);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0, 1, 1);
            photosPicked.get(position).pathInCenterHorizontal = true;
            photosPicked.get(position).pathInCenterVertical = true;
        }
    }

    public static void collage_3_3(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_3;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0.75f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_3;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.75f));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 0.25f));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.25f, 0.25f, 0.75f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
        }
    }

    public static void collage_3_2(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_6;
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).path = new Path();
            GeometryUtils.createRegularPolygonPath(photosPicked.get(position).path, 512, 6, 0);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0, 1, 1);
            photosPicked.get(position).pathInCenterVertical = true;
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_6;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).path = new Path();
            GeometryUtils.createRegularPolygonPath(photosPicked.get(position).path, 512, 6, 0);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0, 1, 1);
            photosPicked.get(position).pathInCenterVertical = true;
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).cornerMethod = Photo.CORNER_METHOD_3_6;
            photosPicked.get(position).bound.set(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).path = new Path();
            GeometryUtils.createRegularPolygonPath(photosPicked.get(position).path, 512, 6, 0);
            photosPicked.get(position).pathRatioBound = new RectF(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).pathInCenterVertical = true;
            photosPicked.get(position).pathAlignParentRight = true;
        }
    }

    public static void collage_3_1(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = new Path();
            photosPicked.get(position).clearPath.addCircle(256, 256, 256, Path.Direction.CCW);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.25f, 0.5f, 0.75f, 1.5f);
            photosPicked.get(position).clearPathInCenterHorizontal = true;
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).clearPath = new Path();
            photosPicked.get(position).clearPath.addCircle(256, 256, 256, Path.Direction.CCW);
            photosPicked.get(position).clearPathRatioBound = new RectF(0.25f, -0.5f, 0.75f, 0.5f);
            photosPicked.get(position).clearPathInCenterHorizontal = true;
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_3_8;
            photosPicked.get(position).bound.set(0, 0.25f, 1, 0.75f);
            photosPicked.get(position).path = new Path();
            photosPicked.get(position).path.addCircle(256, 256, 256, Path.Direction.CCW);
            photosPicked.get(position).pathRatioBound = new RectF(0.25f, 0, 0.75f, 1);
            photosPicked.get(position).pathInCenterVertical = true;
        }
    }

    public static void collage_3_0(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }
}
