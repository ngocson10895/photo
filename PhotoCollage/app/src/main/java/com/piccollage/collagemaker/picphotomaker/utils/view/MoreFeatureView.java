package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Context;
import android.util.AttributeSet;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.BaseView;
import com.piccollage.collagemaker.picphotomaker.databinding.MoreFeatureViewBinding;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

/**
 * Create from Administrator
 * Purpose: thể hiện các chức năng phụ
 * Des: thể hiện các chức năng phụ đối với từng ảnh như flip, đổi ảnh.
 */
public class MoreFeatureView extends BaseView {
    private MoreFeatureViewBinding binding;
    private IMoreFeatureListener listener;

    public interface IMoreFeatureListener extends IBaseViewListener {
        void changeImage();

        void flipImage();
    }

    public void setListener(IMoreFeatureListener listener) {
        this.listener = listener;
    }

    public MoreFeatureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData();
    }

    @Override
    public void initData() {

    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMoreFeature.NAME);
    }

    @Override
    protected void initView() {
        binding = MoreFeatureViewBinding.bind(this);
    }

    @Override
    public int getLayoutID() {
        return R.layout.more_feature_view;
    }

    @Override
    public void buttonClick() {
        onCompleteClick();
        onChangeImageClick();
        onFlipImageClick();
    }

    @Override
    public void backAction() {
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMoreFeature.CANCEL);
        hide();
    }

    private void onCompleteClick() {
        binding.btnComplete.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMoreFeature.DONE);
            listener.doneAction();
            hide();
        });
    }

    private void onChangeImageClick() {
        binding.ivChangeImage.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMoreFeature.CHANGE_IMAGE);
            listener.changeImage();
        });
    }

    private void onFlipImageClick() {
        binding.ivFlip.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMoreFeature.FLIP);
            listener.flipImage();
        });
    }
}
