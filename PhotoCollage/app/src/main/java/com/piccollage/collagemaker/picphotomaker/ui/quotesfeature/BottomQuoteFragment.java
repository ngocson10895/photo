package com.piccollage.collagemaker.picphotomaker.ui.quotesfeature;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentBottomQuoteBinding;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

import es.dmoral.toasty.Toasty;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Create from Administrator
 * Purpose: bottom quote
 * Des: hiện thị các tác vụ user có thể sử dụng
 */
public class BottomQuoteFragment extends BottomSheetDialogFragment {
    public static final String TAG = "BottomQuoteFragment";
    private static final String QUOTE = "quote";

    private String quote;
    private IBottomContract iBottomContract;
    private FragmentBottomQuoteBinding binding;

    public interface IBottomContract {
        void edit();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IBottomContract) {
            iBottomContract = (IBottomContract) context;
        } else {
            Log.d(TAG, "onAttach: must implement this interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iBottomContract = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public static BottomQuoteFragment newInstance(String quote) {
        BottomQuoteFragment bottomQuoteFragment = new BottomQuoteFragment();
        Bundle bundle = new Bundle();
        bundle.putString(QUOTE, quote);
        bottomQuoteFragment.setArguments(bundle);
        return bottomQuoteFragment;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @SuppressLint({"RestrictedApi", "SetTextI18n"})
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        if (getContext() != null) {
            SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(getContext(), Constant.NAME_SHARE);
            View view = View.inflate(getContext(), R.layout.fragment_bottom_quote, null);
            binding = FragmentBottomQuoteBinding.bind(view);
            dialog.setContentView(view);
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();

            if (behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).addBottomSheetCallback(mBottomSheetBehaviorCallback);
            }
            if (sharedPrefs.get(Constant.IS_GOT_PURCHASE, Boolean.class)) {
                binding.tvEdit.setText(getString(R.string.edit_quote));
            }
            onIvCloseClicked();
            onClEditClicked();
            onTvCopyClicked();
            onTvShareClicked();
            initData();
        }
    }

    private void initData() {
        if (getArguments() != null) {
            quote = getArguments().getString(QUOTE);
        }
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        super.show(manager, tag);
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomQuote.NAME);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void onIvCloseClicked() {
        binding.ivClose.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomQuote.CLOSE);
            dismiss();
        });
    }

    // copy quote to clip board
    private void onTvCopyClicked() {
        binding.tvCopy.setOnClickListener(v -> {
            if (getContext() != null) {
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomQuote.COPY);
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", quote);
                clipboard.setPrimaryClip(clip);
                Toasty.success(getContext(), getString(R.string.copy_done), Toasty.LENGTH_SHORT).show();
            }
        });
    }

    private void onTvShareClicked() {
        binding.tvShare.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomQuote.SHARE);
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.TEXT", quote);
            startActivity(Intent.createChooser(intent, quote));
        });
    }

    private void onClEditClicked() {
        binding.clEdit.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomQuote.EDIT);
            dismiss();
            iBottomContract.edit();
        });
    }
}
