package com.piccollage.collagemaker.picphotomaker.ui.quotesfeature;

import android.content.Intent;
import android.provider.Settings;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.QuoteHomeAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.QuoteThemeAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.api.APIServiceWrapper;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityPickQuotesBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Quote;
import com.piccollage.collagemaker.picphotomaker.entity.ThemeQuote;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.AlertDialogCustom;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ItemOffsetDecoration;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Create from Administrator
 * Purpose: màn hình lựa chọn quote để chỉnh sửa
 * Des: user sử dụng để chọn quote theo ý thích
 */
public class PickQuotesActivity extends BaseAppActivity implements QuoteHomeAdapter.IPickQuote,
        QuoteThemeAdapter.IPickThemeQuote, BottomQuoteFragment.IBottomContract {
    private static final String TAG = "PickQuotesActivity";

    private QuoteHomeAdapter quoteHomeAdapter;
    private QuoteThemeAdapter quoteThemeAdapter;
    private ArrayList<Quote> quoteArrayList;
    private ArrayList<String> themeTitles;
    private ArrayList<ThemeQuote> themeQuotesHome;
    private DialogLoading dialogLoading;
    private BottomQuoteFragment bottomQuoteFragment;
    private Quote quote;
    private AlertDialogCustom alertDialogCustom;
    private ActivityPickQuotesBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityPickQuotesBinding.inflate(getLayoutInflater());
    }

    public void initData() {
        themeQuotesHome = new ArrayList<>();
    }

    @Override
    public void onHasNetwork() {
        if (alertDialogCustom != null && alertDialogCustom.isShowing()) alertDialogCustom.dismiss();
        dialogLoading.show();
        binding.adsLayout.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayout, false,
                new BannerAdsUtils.BannerAdListener() {
                    @Override
                    public void onAdLoaded() {
                        binding.viewPro.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAdFailed() {

                    }
                }));
        APIServiceWrapper.getInstance().loadAPIQuote(new APIServiceWrapper.ILoadQuote() {

            @Override
            public void loadDone(ArrayList<ThemeQuote> themeQuotes) {
                themeQuotesHome.addAll(themeQuotes);
                quoteArrayList.addAll(themeQuotes.get(0).getQuotes());
                quoteHomeAdapter.setQuotes(quoteArrayList);
                quoteHomeAdapter.notifyDataSetChanged();

                themeTitles.clear();
                for (ThemeQuote themeQuote : themeQuotes) {
                    themeTitles.add(themeQuote.getNameTheme());
                    quoteThemeAdapter.setThemes(themeTitles);
                    quoteThemeAdapter.notifyDataSetChanged();
                }
                dialogLoading.dismiss();
            }

            @Override
            public void error() {
                dialogLoading.dismiss();
            }
        }, this);
        binding.clNotInternet.setVisibility(View.GONE);
    }

    @Override
    public void notHasNetwork() {
        binding.clNotInternet.setVisibility(View.VISIBLE);
        alertDialogCustom.show();
    }

    public void initView() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickQuote.NAME);
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.GONE);
        }
        alertDialogCustom = new AlertDialogCustom(this, SweetAlertDialog.ERROR_TYPE);
        alertDialogCustom.setUpFeature();
        alertDialogCustom.setiAlertDialogListener(new AlertDialogCustom.IAlertDialogListener() {
            @Override
            public void okClick() {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }

            @Override
            public void cancelClick() {
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        dialogLoading = new DialogLoading(this, DialogLoading.CREATE);
        quoteArrayList = new ArrayList<>();
        quoteHomeAdapter = new QuoteHomeAdapter(this, quoteArrayList, this);
        // sử dụng staggered grid layout
        binding.rvQuotes.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        binding.rvQuotes.addItemDecoration(new ItemOffsetDecoration(8, this));
        binding.rvQuotes.setAdapter(quoteHomeAdapter);
        binding.rvQuotes.setHasFixedSize(true);
        binding.rvQuotes.setItemViewCacheSize(20);
        binding.rvQuotes.setDrawingCacheEnabled(true); // cache item
        binding.rvQuotes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        themeTitles = new ArrayList<>();
        quoteThemeAdapter = new QuoteThemeAdapter(themeTitles, this, this);
        binding.rvTheme.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvTheme.addItemDecoration(new ItemOffsetDecoration(8, this));
        binding.rvTheme.setAdapter(quoteThemeAdapter);
    }

    @Override
    public void buttonClick() {
        onIvBackClicked();
        onViewClicked();
    }

    public void onIvBackClicked() {
        binding.ivBack.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickQuote.BACK);
            finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        });
    }

    @Override
    public void pickQuote(Quote quote) {
        if (checkDoubleClick()) {
            this.quote = quote;
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickQuote.ACTION_QUOTE + quote.getTheme());
            bottomQuoteFragment = BottomQuoteFragment.newInstance(quote.getQuote());
            bottomQuoteFragment.show(getSupportFragmentManager(), BottomQuoteFragment.TAG);
        }
    }

    @Override
    public void pickTheme(String string) {
        binding.rvTheme.scrollToPosition(themeTitles.indexOf(string));
        for (ThemeQuote themeQuote : themeQuotesHome) {
            if (string.equals(themeQuote.getNameTheme())) {
                quoteArrayList.clear();
                quoteArrayList.addAll(themeQuote.getQuotes());
                quoteHomeAdapter.setQuotes(quoteArrayList);
                quoteHomeAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        APIServiceWrapper.clearTag(Constant.QUOTE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bottomQuoteFragment != null && bottomQuoteFragment.getDialog() != null && bottomQuoteFragment.getDialog().isShowing()
                && !bottomQuoteFragment.isRemoving()) bottomQuoteFragment.dismiss();
    }

    @Override
    public void edit() {
        Intent intent = new Intent(this, QuoteEditorActivity.class);
        intent.putExtra(QuoteEditorActivity.QUOTE, quote.getQuote());
        intent.putExtra(QuoteEditorActivity.URL_QUOTE, quote.getUrlThumbs());
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    public void onViewClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
