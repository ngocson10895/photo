package com.piccollage.collagemaker.picphotomaker.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.piccollage.collagemaker.picphotomaker.BuildConfig;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * Help change language for application
 */
public class LocaleHelper {

    private static String getStringPref(Context c, String name, String defaultValue) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        return sharedPrefs.getString(name, defaultValue);

    }

    private static void putStringPref(Context c, String name, String values) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(name, values);
        editor.apply();
    }


    private static final String[] COUNTRY_CODES = new String[]{"af", "am", "ar", "az", "be", "bg",
            "bn", "bs", "ca", "co", "cs", "da", "de", "el", "en", "en-rAU", "en-rCA", "en-rDO",
            "en-rGB", "en-rHK", "en-rIE", "en-rIN", "en-rJO", "en-rMY", "en-rNZ", "en-rPH", "en-rSG",
            "en-rZA", "es", "es-rAR", "es-rCL", "es-rCO", "es-rES", "es-rMX", "es-rPE", "es-rUS",
            "es-rUY", "es-rVE", "et", "eu", "fa", "fi", "fr", "fr-rBE", "fr-rCA", "fr-rCH", "fy",
            "gu", "ha", "hi", "hr", "ht", "hu", "hy", "id", "ig", "is", "it", "it-rCH",
            "it-rLT", "it-rLT", "iw", "ja", "jw", "ka", "kk", "km", "kn", "ko", "lo", "lt", "lv",
            "mk", "ml", "mn", "mr", "ms", "my", "nb", "ne", "nl", "pl", "pt", "pt-rBR", "pt-rPT",
            "ro", "ru", "si", "sk", "sl", "so", "sq", "sr", "sv", "sw", "te", "th", "tl", "tr",
            "uk", "vi", "zh-rCN", "zh-rHK", "zh-rTW", "zu"};
    private static String LANGUAGE_SELECTED = BuildConfig.APPLICATION_ID + "LANGUAGE_SELECTED";
    private static final String DEFAULT_LANGUAGE = "en";

    public static Context setLocale(Context c) {
        return updateResources(c, getLanguage(c));
    }

    public static Context setNewLocale(Context c, String language) {
        persistLanguage(c, language);
        return updateResources(c, language);
    }

    private static String getLanguage(Context c) {
        //return Paper.book().read(LANGUAGE_SELECTED, Locale.getDefault().getLanguage() + (TextUtils.isEmpty(Locale.getDefault().getCountry()) ? "" : ("-" + Locale.getDefault().getCountry())));
//        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
//        return  sharedPrefs.getString(LANGUAGE_SELECTED, Locale.getDefault().getLanguage() + (TextUtils.isEmpty(Locale.getDefault().getCountry()) ? "" : ("-" + Locale.getDefault().getCountry())));
        return getStringPref(c, LANGUAGE_SELECTED, Locale.getDefault().getLanguage() + (TextUtils.isEmpty(Locale.getDefault().getCountry()) ? "" : ("-" + Locale.getDefault().getCountry())));
    }

    @SuppressLint("ApplySharedPref")
    private static void persistLanguage(Context c, String language) {
//        Paper.book().write(LANGUAGE_SELECTED, language);
//        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPrefs.edit();
//        editor.putString(LANGUAGE_SELECTED, language);
//        editor.commit();
        putStringPref(c, LANGUAGE_SELECTED, language);
    }


    private static Context updateResources(Context context, String language) {
        Locale locale;
        if (language.equals(MyApplication.getInstance().getString(R.string.auto))) {
            locale = Resources.getSystem().getConfiguration().locale;
        } else {
            switch (language) {
                case "zh-rCN":
                case "zh":
                    locale = Locale.SIMPLIFIED_CHINESE;
                    break;
                case "zh-rTW":
                    locale = Locale.TRADITIONAL_CHINESE;
                    break;
                default:
                    String[] spk = language.split("-");
                    if (spk.length > 1) {
                        locale = new Locale(spk[0], spk[1]);
                    } else {
                        locale = new Locale(spk[0]);
                    }
                    break;
            }
        }
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = res.getConfiguration();
        config.setLocale(locale);
        context = context.createConfigurationContext(config);
        applyLanguageForApplicationContext(language);
        return context;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= 24 ? config.getLocales().get(0) : config.locale;
    }

    public static ArrayList<String> getListCountries() {
        ArrayList<String> countries = new ArrayList<>();
        Locale locale;
        for (String code : LocaleHelper.COUNTRY_CODES) {
            String[] spk = code.split("-");
            if (spk.length > 1) {
                locale = new Locale(spk[0], spk[1]);
            } else {
                locale = new Locale(code);
            }
            countries.add(LocaleHelper.toDisplayCase(locale.getDisplayName(locale)));
        }
        Collections.sort(countries);
        countries.add(0, MyApplication.getInstance().getString(R.string.auto));
        return countries;
    }

    public static String getLanguageCodeByDisplayName(String displayName) {
        if (MyApplication.getInstance().getString(R.string.auto).equalsIgnoreCase(displayName))
            return displayName;
        for (String key : COUNTRY_CODES) {
            Locale locale;
            String[] spk = key.split("-");
            if (spk.length > 1) {
                locale = new Locale(spk[0], spk[1]);
            } else {
                locale = new Locale(key);
            }
            if (displayName.equalsIgnoreCase(locale.getDisplayName(locale))) {
                return key;
            }
        }
        return DEFAULT_LANGUAGE;
    }

//    public static void showChangeLanguageDialog(FragmentManager supportFragmentManager) {
//        DialogChangeLanguageFragment dialogFragment = new DialogChangeLanguageFragment();
//        dialogFragment.show(supportFragmentManager, "");
//    }

    public static String getSelectedLanguage(Context context) {
//        return Paper.book().read(Constant.KEY_SELECTED_LANGUAGE_POSITION, MyApplication.getInstance().getString(R.string.auto));
        return getStringPref(context, Constant.KEY_SELECTED_LANGUAGE_POSITION, MyApplication.getInstance().getString(R.string.auto));
    }

    public static void setSelectedLanguage(Context context, String language) {
//        Paper.book().write(Constant.KEY_SELECTED_LANGUAGE_POSITION, language);
        putStringPref(context, Constant.KEY_SELECTED_LANGUAGE_POSITION, language);
    }

    private static String toDisplayCase(String s) {
        final String ACTIONABLE_DELIMITERS = " '-/";
        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0);
        }
        return sb.toString();
    }

    public static void applyLanguageForApplicationContext() {
        applyLanguageForApplicationContext(getLanguage(MyApplication.getInstance()));
    }

    public static String getLanguageCountryName(Context c) {
        Locale locale;
        String code = getLanguage(c);
        String[] spk = code.split("-");
        if (spk.length > 1) {
            locale = new Locale(spk[0], spk[1]);
        } else {
            locale = new Locale(code);
        }
        return LocaleHelper.toDisplayCase(locale.getDisplayName(locale));
    }

    public static void applyLanguageForApplicationContext(String language) {
        final Resources resourcesApp = MyApplication.getInstance().getResources();
        DisplayMetrics dm = resourcesApp.getDisplayMetrics();
        final Configuration configuration = resourcesApp.getConfiguration();
        Locale locale;
        if (language.equals(MyApplication.getInstance().getString(R.string.auto))) {
            locale = Resources.getSystem().getConfiguration().locale;
        } else {
            switch (language) {
                case "zh-rCN":
                case "zh":
                    locale = Locale.SIMPLIFIED_CHINESE;
                    break;
                case "zh-rTW":
                    locale = Locale.TRADITIONAL_CHINESE;
                    break;
                default:
                    String[] spk = language.split("-");
                    if (spk.length > 1) {
                        locale = new Locale(spk[0], spk[1]);
                    } else {
                        locale = new Locale(spk[0]);
                    }
                    break;
            }
        }
        configuration.setLocale(locale);
        resourcesApp.updateConfiguration(configuration, dm);
    }
}
