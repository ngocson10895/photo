package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogDownloadBinding;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.util.Objects;

/**
 * Create from Administrator
 * Purpose: hiển thị quá trình download
 * Des: show % download cho user
 */
public class DialogDownload extends BaseDialog {
    private ICancel iCancel;
    private DialogDownloadBinding binding;

    @Override
    public void beforeSetContentView() {
        binding = DialogDownloadBinding.inflate(getLayoutInflater());
    }

    private void onCancelClicked() {
        binding.tvCancel.setOnClickListener(v -> {
            dismiss();
            iCancel.cancel();
        });
    }

    public interface ICancel {
        void cancel();
    }

    public DialogDownload(@NonNull Context context, ICancel iCancel) {
        super(context);
        this.iCancel = iCancel;
    }

    public void setInfo(String title, String sub) {
        binding.tvTitle.setText(title);
        binding.tvSub.setText(sub);
    }

    public void setMax(long max) {
        binding.circlePb.setMax((int) max);
    }

    public void setCurrent(long current) {
        binding.circlePb.setProgress((int) current);
    }

    @Override
    public void initView() {
        Advertisement.showBannerAdExit(getContext(), binding.adsLayout);
        onCancelClicked();
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.85);
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
