package com.piccollage.collagemaker.picphotomaker.ui.collageactivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdSize;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.IconsAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityCollagePreviewBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.entity.TextOfUser;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.DrawableSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.Sticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.StickerView;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.TextSticker;
import com.piccollage.collagemaker.picphotomaker.ui.DialogExitFeature;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.PreviewActivity;
import com.piccollage.collagemaker.picphotomaker.ui.collageactivity.layout.FramePhotoLayout;
import com.piccollage.collagemaker.picphotomaker.ui.morefeature.AddTextFragment;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.ChangeImageActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.PipActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.frame.FrameUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;
import com.piccollage.collagemaker.picphotomaker.utils.view.AddStickerView;
import com.piccollage.collagemaker.picphotomaker.utils.view.BackgroundView;
import com.piccollage.collagemaker.picphotomaker.utils.view.MoreFeatureView;
import com.piccollage.collagemaker.picphotomaker.utils.view.ViewMargin;
import com.piccollage.collagemaker.picphotomaker.utils.view.ViewType;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradients;
import com.piccollage.collagemaker.picphotomaker.viewholder.Pattern;
import com.piccollage.collagemaker.picphotomaker.viewholder.Patterns;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import dauroi.photoeditor.utils.DateTimeUtils;
import dauroi.photoeditor.utils.PhotoUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: Chứa các chức năng giúp user chỉnh sửa, tạo ảnh collage theo ý muốn
 * Des: Màn hình chính để user sử dụng collage feature. Bao gồm các chức năng hỗ trợ user chỉnh sửa theo ý muốn
 */
public class CollageActivity extends BaseAppActivity implements FramePhotoLayout.IClickImage, AddTextFragment.ISendTextView {
    public static final String TAG = "CollageActivity";
    public static final String COLLAGE = "Collage";
    public static final String PHOTO = "photo";
    public static final int REQUEST_CODE_CHANGE_IMAGE = 998;

    private ActivityCollagePreviewBinding binding;
    private List<Photo> photosPicked;
    private float currentCorner = 0f, currentMargin = 2f; // corner và margin mặc định
    private int countFlip = 0; // sử dụng để tính số lần flip ảnh
    private int index;
    private String pathFrame;
    private Bitmap bmBackground;
    private ConstraintSet set;
    private IconsAdapter adapterMainFeature;
    private FramePhotoLayout framePhotoLayout;
    private ArrayList<Icon> iconFeature;
    private DialogLoading dialog, dialogSave;
    private DialogExitFeature dialogExit;
    private SharedPrefsImpl sharedPrefs;
    private ArrayList<StickerTheme> stickerThemes;
    private boolean isPreview;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityCollagePreviewBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    // get sticker data sau khi load thành công từ api ở màn hình home
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getStickerData(EventItemShopSticker eventItemShopSticker) {
        stickerThemes.clear();
        stickerThemes.addAll(eventItemShopSticker.getStickerThemes());
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Collage.NAME);
        stickerThemes = new ArrayList<>();

        sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);

        dialogExit = new DialogExitFeature(this, CollageActivity.this::discard);

        dialog = new DialogLoading(this, DialogLoading.CREATE);
        dialogSave = new DialogLoading(this, DialogLoading.CREATE);

        set = new ConstraintSet();
        bmBackground = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.WHITE));
        binding.background.setImageBitmap(bmBackground);

        iconFeature = new ArrayList<>();
        adapterMainFeature = new IconsAdapter(iconFeature, this, this::pickFeature, COLLAGE);
        binding.rvMainFeature.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvMainFeature.setAdapter(adapterMainFeature);

        // listener khi user tương tác với view type
        binding.viewType.setCollageListener(new ViewType.ITypeViewCollageListener() {
            @Override
            public void doneAction() {
                clickOk();
            }

            @Override
            public void backAction() {
                clickOk();
            }

            @Override
            public void previewAction(String path, int pos) {
                dialog.show();
                pathFrame = path;
                posFramePick(path, pos);
            }
        });

        // listener khi user tương tác với view margin
        binding.viewMargin.setListener(new ViewMargin.IMarginViewListener() {
            @Override
            public void previewAction(float margin, float corner, float shadow) {
                if (framePhotoLayout != null) {
                    currentMargin = margin;
                    currentCorner = corner;
                    framePhotoLayout.setSpace(margin, corner);
                    // set shadow cho view. padding của view = shadow radius + shadow distance
                    binding.containerLayout.setShadowDistance(BetweenSpacesItemDecoration.dpToPixels(shadow / 2));
                    binding.containerLayout.setShadowRadius(BetweenSpacesItemDecoration.dpToPixels(shadow / 2));
                    // set màu sắc cho shadow
                    if (shadow == 0) binding.containerLayout.setShadowColor(Color.TRANSPARENT);
                    else binding.containerLayout.setShadowColor(Color.DKGRAY);
                }
            }

            @Override
            public void previewRatio(String ratio) {
                posRatioPick(ratio);
            }

            @Override
            public void doneAction() {
                clickOk();
            }

            @Override
            public void backAction() {
                clickOk();
            }
        });

        // listener khi user tương tác với background view
        binding.backgroundView.setListener(new BackgroundView.IBackgroundListener() {
            @Override
            public void previewBackground(int color) {
                binding.background.setImageBitmap(ImageDecoder.drawableToBitmap(new ColorDrawable(color)));
            }

            @Override
            public void previewPattern(String path) {
                if (path.contains("storage")) {
                    Glide.with(CollageActivity.this).load(path).centerCrop().error(R.drawable.sticker_default).into(binding.background);
                } else
                    Glide.with(CollageActivity.this).load(Uri.parse("file:///android_asset/background/" + path)).centerCrop().error(R.drawable.sticker_default).into(binding.background);
            }

            @Override
            public void previewGradient(String stColor, String endColor) {
                binding.background.setImageDrawable(new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{Color.parseColor(stColor), Color.parseColor(endColor)}));
            }

            @Override
            public void previewPalette(String c1, String c2, String c3, String c4, String c5) {

            }

            @Override
            public void jumpToShop(String type) {
                Intent intent = new Intent(CollageActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, type);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction(Bitmap bm) {
                bmBackground = bm;
                binding.background.setImageBitmap(bm);
                clickOk();
            }

            @Override
            public void closeAction(Bitmap bm) {
                bmBackground = bm;
                binding.background.setImageBitmap(bm);
                clickOk();
            }
        });

        // listener khi user tương tác với add sticker view
        binding.addStickerView.setListener(new AddStickerView.IStickerViewListener() {
            @Override
            public void doneAction() {
                clickOk();
                if (framePhotoLayout != null) {
                    framePhotoLayout.setCount(0);
                }
                isPreview = false;
            }

            @Override
            public void backAction() {
                clickOk();
                if (framePhotoLayout != null) {
                    framePhotoLayout.setCount(0);
                }
                isPreview = false;
                binding.sticker.removeCurrentSticker();
            }

            @Override
            public void previewAction(StickerItem stickerItem) {
                addSticker(stickerItem);
            }

            @Override
            public void jumpToShop() {
                Intent intent = new Intent(CollageActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, Constant.STICKER);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        // listener khi user muốn thay đổi hoặc flip ảnh
        binding.moreFeatureView.setListener(new MoreFeatureView.IMoreFeatureListener() {
            @Override
            public void changeImage() {
                Intent intent = new Intent(CollageActivity.this, ChangeImageActivity.class);
                intent.putExtra(PHOTO, photosPicked.get(index));
                intent.putExtra(PipActivity.CHANGE_BG, "");
                startActivityForResult(intent, REQUEST_CODE_CHANGE_IMAGE);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void flipImage() {
                // giá trị flip (-1;1)
                countFlip++;
                if (countFlip % 2 == 0) {
                    framePhotoLayout.flipImage(1);
                } else if (countFlip % 2 == 1) {
                    framePhotoLayout.flipImage(-1);
                }
            }

            @Override
            public void doneAction() {
                framePhotoLayout.editDone();
                framePhotoLayout.setIdle(false);
                framePhotoLayout.setCount(0);
                binding.tbCollagePre.setVisibility(View.VISIBLE);
                binding.rvMainFeature.setVisibility(View.VISIBLE);
                countFlip = 0;
            }

            @Override
            public void backAction() {

            }
        });
    }

    public void buttonClick() {
        onCloseClicked();
        onSaveAndShareClicked();
    }

    @Override
    public void onHasNetwork() {
        loadAds();
    }

    private void loadAds() {
        Advertisement.loadBannerAdExit(this);
        binding.adsLayout.post(() -> Advertisement.showBannerAdHome(this, binding.adsLayout,
                AdSize.BANNER, new BannerAdsUtils.BannerAdListener() {
                    @Override
                    public void onAdLoaded() {

                    }

                    @Override
                    public void onAdFailed() {

                    }
                }, () -> {

                }));
        Advertisement.loadInterstitialAdInApp(this);
    }

    public void initData() {
        ApplicationViewModel applicationViewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        photosPicked = new ArrayList<>();
        if (getIntent() != null) {
            ArrayList<String> paths = getIntent().getStringArrayListExtra(PickPhotoActivity.PHOTOS_PICKED);
            for (String path : paths) {
                photosPicked.add(new Photo(path, 0));
            }
        } else photosPicked.add(new Photo("", 0));

        int numberOfPhotos = photosPicked.size();
        if (numberOfPhotos != 0) {
            binding.viewType.setDataCollage(numberOfPhotos);
        }

        InitData.addFeatureIcon(iconFeature);
        adapterMainFeature.setIconArrayList(iconFeature);
        adapterMainFeature.notifyDataSetChanged();

        // Get data sticker downloaded
        applicationViewModel.getDataStickers().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (!itemsDownloaded.isEmpty()) {
                    for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                        for (StickerTheme stickerTheme : stickerThemes) {
                            if (itemDownloaded.getName().equals(stickerTheme.getNameTheme())) {
                                stickerTheme.setDownloaded(true);
                            }
                        }
                    }
                } else {
                    for (StickerTheme stickerTheme : stickerThemes) {
                        stickerTheme.setDownloaded(false);
                    }
                }
                binding.addStickerView.setMoreSticker(stickerThemes);
            }
        });

        // get data gradient downloaded
        applicationViewModel.getDataGradients().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Gradients> gradients = new ArrayList<>();
                gradients.add(new Gradients(Constant.DEFAULT, InitData.addGradients()));
                for (ItemDownloaded item : itemsDownloaded) {
                    List<Gradient> list = new ArrayList<>();
                    String[] code = item.getPath().split("_");
                    String[] nameItem = item.getNameItem().split("_");
                    for (int i = 0; i < code.length; i += 2) {
                        if (code[i].length() == 6) {
                            Gradient gra = new Gradient("#" + code[i], "#" + code[i + 1], false, item.getName());
                            list.add(gra);
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    gradients.add(new Gradients(item.getName(), list));
                }
                gradients.add(0, new Gradients(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataGradientsEx(gradients);
            }
        });

        // get data pattern downloaded
        ArrayList<Pattern> pattern = new ArrayList<>();
        for (String p : FileUtil.listAssetFiles("background", this)) {
            pattern.add(new Pattern(p, String.valueOf(FileUtil.listAssetFiles("background", this).indexOf(p)), Constant.DEFAULT, false));
        }
        applicationViewModel.getDataPatterns().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Patterns> patterns = new ArrayList<>();
                patterns.add(new Patterns(Constant.DEFAULT, pattern));
                for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                    ArrayList<String> paths = new ArrayList<>();
                    List<Pattern> list = new ArrayList<>();
                    FileUtil.getAllFiles(new File(itemDownloaded.getPath()), paths);
                    for (String path : paths) {
                        list.add(new Pattern(path, "", itemDownloaded.getName(), false));
                    }
                    String[] nameItem = itemDownloaded.getNameItem().split("_");
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    patterns.add(new Patterns(itemDownloaded.getName(), list));
                }
                patterns.add(0, new Patterns(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataPatternsEx(patterns);
            }
        });
    }

    public void discard() {
        Intent intent = new Intent(this, PickPhotoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(PickPhotoActivity.MODE, Constant.COLLAGE);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    // Create a layout main cho user. Chức năng chính
    private static class BuildLayout extends AsyncTask<Void, Void, List<Photo>> {
        private List<Photo> photos;
        private WeakReference<CollageActivity> weakReference;
        private String path;
        private boolean isIdle;
        private int count;

        BuildLayout(List<Photo> photos, CollageActivity activity, String path, boolean isIdle, int count) {
            this.photos = photos;
            weakReference = new WeakReference<>(activity);
            this.path = path;
            this.isIdle = isIdle;
            this.count = count;
        }

        @Override
        protected void onPostExecute(List<Photo> aVoid) {
            super.onPostExecute(aVoid);
            CollageActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            activity.buildLayoutMain(aVoid);
            activity.framePhotoLayout.setIdle(isIdle);
            activity.framePhotoLayout.setCount(count);
        }

        @Override
        protected List<Photo> doInBackground(Void... voids) {
            CollageActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            FrameUtils.createFrame(path, photos);
            return new ArrayList<>(photos);
        }
    }

    // Build layout main
    private void buildLayoutMain(List<Photo> items) {
        // Create Frame Layout
        if (sharedPrefs.get("Y29tLnBpY2NvbGxhZ2UuYLnBpY3Bob3RvbWFrZXI=", Boolean.class)) {
            framePhotoLayout = new FramePhotoLayout(this, items, this);
            int mg = BetweenSpacesItemDecoration.dpToPixels(16);

            // tính toán kích thước của container layout
            int containerWidth = binding.containerLayout.getWidth() - mg * 2;
            int containerHeight = binding.containerLayout.getHeight() - mg * 2;

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(containerWidth, containerHeight);
            framePhotoLayout.buildFrameLayout(containerWidth, containerHeight, currentMargin, currentCorner); // create ảnh collage bao gồm kích thước và corner, margin
            params.addRule(RelativeLayout.CENTER_IN_PARENT);

            binding.containerLayout.removeAllViews();
            binding.containerLayout.addView(framePhotoLayout, params);
            binding.containerLayout.resetShadow(); // rebuild lại shadow
            binding.viewType.setBuild(false);
            dialog.dismiss();
        }
    }

    private void clickOk() {
        binding.tbCollagePre.setVisibility(View.VISIBLE);
        binding.rvMainFeature.setVisibility(View.VISIBLE);
        framePhotoLayout.setIdle(false);
        framePhotoLayout.setCount(0);
    }

    // reset photo
    private void resetPhoto() {
        List<Photo> copyData = new ArrayList<>(); // create a list to copy a data. If you change data of list copy, original data not change
        for (Photo photo : photosPicked) {
            copyData.add(new Photo(photo.getPathPhoto(), 1)); // copy data from original data
        }
        photosPicked.clear(); // clear original data
        photosPicked.addAll(copyData); // add data
    }

    // get index of photo was clicked
    public void clickImage(int index, List<Photo> photosPicked) {
        this.index = index;
        binding.tbCollagePre.setVisibility(View.GONE);
        binding.rvMainFeature.setVisibility(View.GONE);
        binding.moreFeatureView.setVisibility(View.VISIBLE);
    }

    @Override
    public void drop() {
        // khi drag drop ảnh thì phải rebuild lại shadow
        binding.containerLayout.resetShadow();
    }

    // Add sticker for main layout
    public void addSticker(StickerItem item) {
        binding.sticker.setLocked(false);
        binding.sticker.setConstrained(true);
        binding.sticker.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.sticker.setShowBorder(true);
                binding.sticker.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                binding.sticker.swapLayers(binding.sticker.getStickers().indexOf(sticker), binding.sticker.getStickerCount() - 1);
                Log.d(TAG, "onStickerClicked: ");
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.sticker.setShowBorder(false);
                    binding.sticker.setShowIcons(false);
                } else {
                    binding.sticker.setShowBorder(true);
                    binding.sticker.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.sticker.configDefaultIcons("sticker");
        Glide.with(this).load(item.getUrl()).centerCrop().into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable drawable, @Nullable Transition<? super Drawable> transition) {
                if (isPreview) {
                    binding.sticker.removeCurrentSticker();
                } else {
                    isPreview = true;
                }
                binding.sticker.addSticker(new DrawableSticker(drawable));
            }

            @Override
            public void onLoadCleared(@Nullable Drawable drawable) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHANGE_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    // thay thế photo user muốn thay
                    photosPicked.remove(photosPicked.get(index));
                    if (data.getParcelableExtra(PHOTO) != null) {
                        photosPicked.add(index, data.getParcelableExtra(PHOTO));
                    }
                    if (framePhotoLayout != null && data.getParcelableExtra(PHOTO) != null) {
                        framePhotoLayout.changeImage(index, data.getParcelableExtra(PHOTO));
                    }
                }
            }
        } else {
            if (resultCode == Activity.RESULT_OK) {
                // visible lại ảnh chính để user có thể chỉnh sửa tiếp
                binding.clMain.setVisibility(View.VISIBLE);
                binding.ivImage.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        // khi các view còn visible thì phải invisible. nếu không còn view nào visible thì mới hiện dialog để back về màn hình trước
        if (binding.viewType.getVisibility() == View.VISIBLE) {
            binding.viewType.backAction();
            binding.tbCollagePre.setVisibility(View.VISIBLE);
            binding.rvMainFeature.setVisibility(View.VISIBLE);
            framePhotoLayout.setCount(0);
            framePhotoLayout.setIdle(false);
        } else if (binding.viewMargin.getVisibility() == View.VISIBLE) {
            binding.viewMargin.backAction();
            clickOk();
        } else if (binding.backgroundView.getVisibility() == View.VISIBLE) {
            binding.backgroundView.backAction();
            clickOk();
        } else if (binding.addStickerView.getVisibility() == View.VISIBLE) {
            binding.addStickerView.backAction();
            clickOk();
        } else if (binding.moreFeatureView.getVisibility() == View.VISIBLE) {
            framePhotoLayout.editDone();
            framePhotoLayout.setIdle(false);
            framePhotoLayout.setCount(0);
            binding.tbCollagePre.setVisibility(View.VISIBLE);
            binding.rvMainFeature.setVisibility(View.VISIBLE);
            binding.moreFeatureView.hide();
            countFlip = 0;
        } else {
            dialogExit.show();
        }
    }

    // chức năng save ảnh sau khi đã chỉnh sửa xong
    private static class Task extends AsyncTask<Void, Void, File> {
        private WeakReference<CollageActivity> weakReference;

        Task(CollageActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            CollageActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            // Kiểm tra người dùng đã cấp quyền chưa vì nhiều user tự tắt quyền sau khi cấp
            if (new PermissionChecker(activity).isPermissionOK()) {
                Toasty.success(activity, activity.getString(R.string.saving_done), Toasty.LENGTH_SHORT).show();
                PhotoUtils.addImageToGallery(file.getAbsolutePath(), activity); // add image và gallery của máy
                Advertisement.showInterstitialAdInApp(activity, new InterstitialAdsPopupOthers.OnAdsListener() {
                    @Override
                    public void onAdsClose() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.COLLAGE);
                        activity.startActivityForResult(intent, 999);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                    @Override
                    public void close() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.COLLAGE);
                        activity.startActivityForResult(intent, 999);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                });
                activity.dialogSave.dismiss();
            }
        }

        @Override
        protected File doInBackground(Void... voids) {
            CollageActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            // get bitmap từ view main
            Bitmap bitmap = ImageUtils.getBitmapFromView(activity.binding.clMain);
            String parentFilePath = FileUtil.getParentFileString("My Creative");
            File parentFile = new File(parentFilePath);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "doInBackground: ");
                }
            }
            File photoFile = new File(parentFile, DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png")); // tạo và đặt tên file
            try {
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(photoFile)); // export từ bitmap ra ảnh png
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return photoFile;
        }

    }

    // Pick main feature
    public void pickFeature(int position) {
        framePhotoLayout.setCount(1);
        binding.tbCollagePre.setVisibility(View.GONE);
        switch (position) {
            case 0:
                if (checkDoubleClick()) {
                    binding.viewType.show();
                    binding.rvMainFeature.setVisibility(View.INVISIBLE);
                    framePhotoLayout.setIdle(true);
                }
                break;
            case 1:
                if (checkDoubleClick()) {
                    binding.viewMargin.show();
                    binding.rvMainFeature.setVisibility(View.INVISIBLE);
                    framePhotoLayout.setIdle(true);
                }
                break;
            case 2:
                if (checkDoubleClick()) {
                    binding.backgroundView.show();
                    binding.rvMainFeature.setVisibility(View.INVISIBLE);
                    framePhotoLayout.setIdle(true);
                }
                break;
            case 3:
                if (checkDoubleClick()) {
                    binding.addStickerView.show();
                    binding.rvMainFeature.setVisibility(View.INVISIBLE);
                    framePhotoLayout.setIdle(true);
                }
                break;
            case 4:
                if (checkDoubleClick()) {
                    AddTextFragment addTextFragment = new AddTextFragment();
                    binding.rvMainFeature.setVisibility(View.GONE);
                    addTextFragment.show(getSupportFragmentManager(), addTextFragment.getTag());
                }
                break;
            default:
        }
    }

    // chức năng thay đổi tỉ lệ của ảnh.
    private void posRatioPick(String ratio) {
        dialog.show();
        if (ratio.equals("9:16")) {
            binding.adsLayout.setVisibility(View.INVISIBLE);
        } else binding.adsLayout.setVisibility(View.VISIBLE);
        set.clone(binding.clParent);
        set.setDimensionRatio(binding.clMain.getId(), ratio);
        set.applyTo(binding.clParent);

        set.clone(binding.clMain);
        set.setDimensionRatio(binding.sticker.getId(), ratio);
        set.applyTo(binding.clMain);

        binding.containerLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // khi thay đổi tỉ lệ thì phải thay đổi toàn bộ thuộc tính của ảnh cha nên phải rebuild lại ảnh này
                new BuildLayout(photosPicked, CollageActivity.this, pathFrame, true, 1).execute();
                binding.containerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        binding.background.setImageBitmap(bmBackground);
    }

    // phải vẽ lại ảnh khi user chọn sang type khác
    private void posFramePick(String path, int pos) {
        // Container Layout will listen changes of view child
        binding.containerLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (pos == 0) {
                    resetPhoto();
                    new BuildLayout(photosPicked, CollageActivity.this, path, false, 0).execute();
                } else {
                    resetPhoto();
                    new BuildLayout(photosPicked, CollageActivity.this, path, true, 1).execute();
                }

                binding.containerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public void onCloseClicked() {
        binding.btnClose.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                dialogExit.show();
            }
        });
    }

    public void onSaveAndShareClicked() {
        binding.ivSaveShare.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Collage.DONE);
                if (binding.sticker.isShowBorder() && binding.sticker.isShowIcons()) {
                    binding.sticker.setShowBorder(false);
                    binding.sticker.setShowIcons(false);
                }
                dialogSave.show();
                // vì view không vẽ lại khi visible nên phải invisible view chính trước khi get bitmap từ nó
                binding.clMain.setVisibility(View.INVISIBLE);
                // set lại bitmap vào view image để không bị trống trong quá trình export
                binding.ivImage.setImageBitmap(ImageUtils.getBitmapFromView(binding.clMain));
                binding.ivImage.setVisibility(View.VISIBLE);
                // delay 2s để export và load ads
                new Handler().postDelayed(() -> {
                    Task task = new Task(this);
                    task.execute();
                }, 2000);
            }
        });
    }

    @Override
    public void turnOff() {
        framePhotoLayout.setCount(0);
        binding.tbCollagePre.setVisibility(View.VISIBLE);
        binding.rvMainFeature.setVisibility(View.VISIBLE);
    }

    // Add text for main layout
    @SuppressLint("NewApi")
    @Override
    public void sendText(TextOfUser textOfUser) {
        framePhotoLayout.setCount(0);
        binding.sticker.setLocked(false);
        binding.sticker.setConstrained(true);
        binding.sticker.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.sticker.setShowBorder(true);
                binding.sticker.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                binding.sticker.swapLayers(binding.sticker.getStickers().indexOf(sticker), binding.sticker.getStickerCount() - 1);
                Log.d(TAG, "onStickerClicked: ");
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.sticker.setShowBorder(false);
                    binding.sticker.setShowIcons(false);
                } else {
                    binding.sticker.setShowBorder(true);
                    binding.sticker.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.sticker.configDefaultIcons("text");

        if (sharedPrefs.get("Y29tLnBpY2NvbGxhZ2UuY29sbGFn", Boolean.class)) {
            TextSticker textSticker = new TextSticker(this, getResources().getDrawable(R.drawable.sticker_transparent_background_hor));
            if (!textOfUser.getContent().isEmpty()) {
                textSticker.setTextOfUser(textOfUser);
                textSticker.resizeText();
                binding.sticker.addSticker(textSticker);
            }
        }
    }
}
