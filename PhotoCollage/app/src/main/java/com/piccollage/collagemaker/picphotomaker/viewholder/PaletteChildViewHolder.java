package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class PaletteChildViewHolder extends ChildViewHolder {
    public ImageView iv1, iv2, iv3, iv4, iv5, ivChose;
    public TextView tvName;

    public PaletteChildViewHolder(View itemView) {
        super(itemView);
        tvName = itemView.findViewById(R.id.ivName);
        iv1 = itemView.findViewById(R.id.iv1);
        iv2 = itemView.findViewById(R.id.iv2);
        iv3 = itemView.findViewById(R.id.iv3);
        iv4 = itemView.findViewById(R.id.iv4);
        iv5 = itemView.findViewById(R.id.iv5);
        ivChose = itemView.findViewById(R.id.ivChose);
    }

    public void setPalette(String color1, String color2, String color3, String color4, String color5, String name) {
        iv1.setImageDrawable(new ColorDrawable(Color.parseColor(color1)));
        iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iv2.setImageDrawable(new ColorDrawable(Color.parseColor(color2)));
        iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iv3.setImageDrawable(new ColorDrawable(Color.parseColor(color3)));
        iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iv4.setImageDrawable(new ColorDrawable(Color.parseColor(color4)));
        iv4.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iv5.setImageDrawable(new ColorDrawable(Color.parseColor(color5)));
        iv5.setScaleType(ImageView.ScaleType.CENTER_CROP);
        tvName.setText(name);
    }
}
