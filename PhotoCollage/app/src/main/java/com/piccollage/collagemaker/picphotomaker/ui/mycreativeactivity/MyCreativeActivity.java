package com.piccollage.collagemaker.picphotomaker.ui.mycreativeactivity;

import android.content.Intent;
import android.util.Base64;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.MyCreativeAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityMyCreativeBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ItemOffsetDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Create from Administrator
 * Purpose: Activity show các sản phẩm của user
 * Des: show các sản phẩm của user và user có thể chuyển sang màn hình editor từ màn hình này
 */
public class MyCreativeActivity extends BaseAppActivity implements MyCreativeAdapter.IChoseImage {
    public static final String TAG = "MyCreativeActivity";

    private ActivityMyCreativeBinding binding;
    private ArrayList<String> uriMyCreative = new ArrayList<>();
    private MyCreativeAdapter adapter;
    private ArrayList<Photo> photos;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityMyCreativeBinding.inflate(getLayoutInflater());
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        MyTrackingFireBase.trackingScreen(this, TAG);
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.INVISIBLE);
        }
        binding.adsLayout.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayout,
                false, new BannerAdsUtils.BannerAdListener() {
                    @Override
                    public void onAdLoaded() {
                        binding.viewPro.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAdFailed() {

                    }
                }));
        photos = new ArrayList<>();
        adapter = new MyCreativeAdapter(photos, this, this);
        binding.rvMyCreative.setLayoutManager(new GridLayoutManager(this, 3));
        binding.rvMyCreative.addItemDecoration(new ItemOffsetDecoration(8, this));
        binding.rvMyCreative.setAdapter(adapter);
    }

    public void initData() {
        String parentFilePath = FileUtil.getParentFileString("My Creative");
        File parentFile = new File(parentFilePath);

        FileUtil.getAllFiles(parentFile, uriMyCreative);
        for (String s : uriMyCreative) {
            Photo p = new Photo(s, 0);
            p.setTimeMf(FileUtil.getDateModifier(s));
            photos.add(p);
        }
        Collections.sort(photos, (o1, o2) -> Long.compare(o1.getTimeMf(), o2.getTimeMf()));
        Collections.reverse(photos);

        adapter.setMyCreativePaths(photos);
        adapter.notifyDataSetChanged();
        if (uriMyCreative.isEmpty()) {
            binding.clNotImage.setVisibility(View.VISIBLE);
            binding.rvMyCreative.setVisibility(View.INVISIBLE);
        } else {
            binding.clNotImage.setVisibility(View.INVISIBLE);
            binding.rvMyCreative.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void buttonClick() {
        onBackClicked();
        onGoToPreClicked();
        onViewClicked();
    }

    public void onBackClicked() {
        binding.toolbarBack.ivBack.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        });
    }

    @Override
    public void image(int position) {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        Intent intent = new Intent(this, EditorActivity.class);
        intent.putExtra(PickPhotoActivity.PHOTOS_PICKED, adapter.getMyCreativePaths().get(position).getPathPhoto());
        intent.putExtra(Constant.VALUE_INTENT, TAG);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }

    public void onViewClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }

    public void onGoToPreClicked() {
        binding.goToPre.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
