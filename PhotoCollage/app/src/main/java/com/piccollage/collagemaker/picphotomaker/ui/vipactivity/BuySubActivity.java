package com.piccollage.collagemaker.picphotomaker.ui.vipactivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.view.View;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityShopPremiumBinding;
import com.piccollage.collagemaker.picphotomaker.networking.NetworkChangeReceiver;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.purchase.AppPurchases;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.purchase.BasePremiumActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.AlertDialogCustom;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: màn hình mua pro
 * Des: giúp user đăng kí subs
 */
public class BuySubActivity extends BasePremiumActivity {
    public static final String SAVE_MONTH = "save_month";
    public static final String SAVE_YEAR = "save_year";

    private CountDownTimer countDownTimer;
    private long mTimeLeftInMillis;
    private boolean timeRunning;
    private long mEndtime;
    private int option = 1;
    private boolean isSale;
    private SharedPrefsImpl sharedPrefs;
    private AlertDialogCustom alertDialogCustom;
    private NetworkChangeReceiver networkChangeReceiver;
    private ActivityShopPremiumBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityShopPremiumBinding.inflate(getLayoutInflater());
    }

    @Override
    public void onViewCreated() {
        initView();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setDataForViews(AppPurchases appPurchases) {
        String saleMonth = appPurchases.priceSubscriptionMonthly1;
        String saleYear = appPurchases.priceSubscriptionYearly1;
        String pMonth = appPurchases.priceSubscriptionMonthly3;
        String pYear = appPurchases.priceSubscriptionYearly3;

        if (appPurchases.savePriceMonthly13 != null && appPurchases.savePriceYearly13 != null) {
            if (!appPurchases.savePriceMonthly13.equals("") && !sharedPrefs.get(SAVE_MONTH, String.class).equals(appPurchases.savePriceMonthly13)) {
                sharedPrefs.put(SAVE_MONTH, appPurchases.savePriceMonthly13);
            }
            if (!appPurchases.savePriceYearly13.equals("") && !sharedPrefs.get(SAVE_YEAR, String.class).equals(appPurchases.savePriceYearly13)) {
                sharedPrefs.put(SAVE_YEAR, appPurchases.savePriceYearly13);
            }
        }

        if (pMonth != null && pYear != null) {
            binding.priceMonthly.setText(" " + pMonth + getString(R.string.month) + ". ");
            binding.priceAnnual.setText(" " + pYear + getString(R.string.year) + ". ");
        }
        if (isSale) {
            binding.icon.setVisibility(View.VISIBLE);
            binding.priceMonthlySave.setVisibility(View.VISIBLE);
            if (saleMonth != null) {
                binding.priceMonthlySave.setText(saleMonth + getString(R.string.month));
            }
            binding.saveMonth.setVisibility(View.VISIBLE);
            if (appPurchases.savePriceMonthly13 != null) {
                if (appPurchases.savePriceMonthly13.equals("")) {
                    binding.saveMonth.setText(getString(R.string.save) + " " + sharedPrefs.get(SAVE_MONTH, String.class));
                } else {
                    binding.saveMonth.setText(getString(R.string.save) + " " + appPurchases.savePriceMonthly13);
                }
            }
        } else {
            binding.saveMonth.setVisibility(View.GONE);
            binding.priceMonthlySave.setVisibility(View.GONE);
            binding.icon.setVisibility(View.GONE);
        }

        if (isSale) {
            if (saleYear != null) {
                binding.priceAnnualSave.setText(" " + saleYear + getString(R.string.year) + ". ");
            }
            binding.priceAnnualSave.setVisibility(View.VISIBLE);
            binding.saveYear.setVisibility(View.VISIBLE);
            if (appPurchases.savePriceYearly13 != null) {
                if (appPurchases.savePriceYearly13.equals("")) {
                    binding.saveYear.setText(getString(R.string.save) + " " + sharedPrefs.get(SAVE_YEAR, String.class));
                } else {
                    binding.saveYear.setText(getString(R.string.save) + " " + appPurchases.savePriceYearly13);
                }
            }
        } else {
            binding.saveYear.setVisibility(View.GONE);
            binding.priceAnnualSave.setVisibility(View.GONE);
        }
    }

    private void startTime() {
        mEndtime = System.currentTimeMillis() + mTimeLeftInMillis;
        isSale = true;
        countDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                int hours = (int) ((mTimeLeftInMillis / 1000) / 3600);
                int minutes = (int) ((mTimeLeftInMillis / 1000) % 3600) / 60;
                int seconds = (int) (mTimeLeftInMillis / 1000) % 60;
                binding.tvTimeCount.setText(" " + hours + " " + getString(R.string.hours) + " " + minutes + " " + getString(R.string.minutes)
                        + " " + seconds + " " + getString(R.string.second));
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFinish() {
                isSale = false;
                binding.saveMonth.setVisibility(View.GONE);
                binding.priceMonthlySave.setVisibility(View.GONE);
                binding.saveYear.setVisibility(View.GONE);
                binding.priceAnnualSave.setVisibility(View.GONE);
                binding.icon.setVisibility(View.GONE);
                binding.tvTimeCount.setVisibility(View.GONE);
                binding.tv5.setVisibility(View.GONE);
            }
        };
        countDownTimer.start();
        timeRunning = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getSharedPreferences(Constant.TIMER_SHARE, MODE_PRIVATE);
        mTimeLeftInMillis = prefs.getLong(Constant.TIME_LEFT, Constant.START_TIME_IN_MILLIS);
        timeRunning = prefs.getBoolean(Constant.TIME_RUNNING, false);

        if (timeRunning) {
            mEndtime = prefs.getLong(Constant.TIME_END, 0);
            mTimeLeftInMillis = mEndtime - System.currentTimeMillis();
            startTime();
            if (mTimeLeftInMillis < 0) {
                mTimeLeftInMillis = 0;
                timeRunning = false;
            }
        } else {
            startTime();
        }
    }

    public void initView() {
        Advertisement.initSdk(this);
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.BuySub.NAME);
        sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);
        alertDialogCustom = new AlertDialogCustom(this, SweetAlertDialog.ERROR_TYPE);
        alertDialogCustom.setUpFeature();
        alertDialogCustom.setiAlertDialogListener(new AlertDialogCustom.IAlertDialogListener() {
            @Override
            public void okClick() {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }

            @Override
            public void cancelClick() {
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
        mEndtime = System.currentTimeMillis() + mTimeLeftInMillis;

        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        networkChangeReceiver.setListener(new NetworkChangeReceiver.NetworkStateReceiverListener() {
            @Override
            public void onNetworkAvailable() {
                alertDialogCustom.dismiss();
                binding.clNotInternet.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onNetworkUnavailable() {
                binding.clNotInternet.setVisibility(View.VISIBLE);
                alertDialogCustom.show();
            }
        });

        onBuyClicked();
        onOption1Clicked();
        onOption2Clicked();
        onRestoreClicked();
        onViewClicked();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkChangeReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.BuySub.BACK);
            finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = getSharedPreferences(Constant.TIMER_SHARE, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putLong(Constant.TIME_LEFT, mTimeLeftInMillis);
        editor.putBoolean(Constant.TIME_RUNNING, timeRunning);
        editor.putLong(Constant.TIME_END, mEndtime);
        editor.apply();

        if (countDownTimer != null) countDownTimer.cancel();
    }

    public void onOption1Clicked() {
        binding.optionMonthly.setOnClickListener(v -> {
            binding.optionMonthly.setBackground(getResources().getDrawable(R.drawable.option_pick));
            binding.optionAnnual.setBackground(getResources().getDrawable(R.drawable.option));
            binding.tvPopular.setVisibility(View.VISIBLE);
            binding.priceAnnual.setTextColor(getResources().getColor(R.color.gray));
            binding.priceAnnualSave.setTextColor(getResources().getColor(R.color.gray));
            binding.saveYear.setTextColor(getResources().getColor(R.color.gray));
            binding.titleAnnual.setTextColor(getResources().getColor(R.color.gray));
            binding.tvSubA.setTextColor(getResources().getColor(R.color.gray));
            binding.tvSubM.setTextColor(getResources().getColor(R.color.pink));
            binding.priceMonthly.setTextColor(getResources().getColor(R.color.pink));
            binding.titleMonthly.setTextColor(getResources().getColor(R.color.pink));
            binding.priceMonthlySave.setTextColor(getResources().getColor(R.color.pink));
            binding.saveMonth.setTextColor(getResources().getColor(R.color.pink));
            option = 1;
        });
    }

    public void onOption2Clicked() {
        binding.optionAnnual.setOnClickListener(v -> {
            binding.optionAnnual.setBackground(getResources().getDrawable(R.drawable.option_pick));
            binding.optionMonthly.setBackground(getResources().getDrawable(R.drawable.option));
            binding.tvPopular.setVisibility(View.INVISIBLE);
            binding.priceMonthly.setTextColor(getResources().getColor(R.color.gray));
            binding.priceMonthlySave.setTextColor(getResources().getColor(R.color.gray));
            binding.saveMonth.setTextColor(getResources().getColor(R.color.gray));
            binding.titleMonthly.setTextColor(getResources().getColor(R.color.gray));
            binding.tvSubM.setTextColor(getResources().getColor(R.color.gray));
            binding.priceAnnual.setTextColor(getResources().getColor(R.color.pink));
            binding.titleAnnual.setTextColor(getResources().getColor(R.color.pink));
            binding.tvSubA.setTextColor(getResources().getColor(R.color.pink));
            binding.priceAnnualSave.setTextColor(getResources().getColor(R.color.pink));
            binding.saveYear.setTextColor(getResources().getColor(R.color.pink));
            option = 2;
        });
    }

    public void onBuyClicked() {
        binding.btnContinue.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.BuySub.SUB);
                if (option == 2) {
                    onSubscriptionTrialFree(isSale);
                } else if (option == 1)
                    onSubscriptionMonthlyTrial(isSale);
            }
        });
    }

    public void onRestoreClicked() {
        binding.clRestore.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.BuySub.RESTORE);
            onRestorePurchase();
            Toasty.success(this, getString(R.string.done), Toasty.LENGTH_SHORT).show();
        });
    }
}
