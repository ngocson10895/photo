package com.piccollage.collagemaker.picphotomaker.ui.vipactivity.purchase;

public class AppPurchases {
    public double valueSubMonthly3 = 0;
    public double valueSubYearly3 = 0;
    public double valueSubYearly1 = 0;
    public double valueSubMonthly1 = 0;
    public double valueSubYearly2 = 0;
    public double valueSubMonthly2 = 0;

    public String priceProduct = "";
    public String priceSubscriptionYearly3 = "";
    public String priceSubscriptionYearly1 = "";
    public String priceSubscriptionMonthly3 = "";
    public String priceSubscriptionMonthly1 = "";

    public String savePriceMonthly13 = "";
    public String savePriceYearly13 = "";
}
