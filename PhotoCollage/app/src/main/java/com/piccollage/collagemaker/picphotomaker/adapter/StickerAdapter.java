package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemStickerBinding;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;

import java.util.List;

/**
 * sticker adapter
 */
public class StickerAdapter extends BaseQuickAdapter<StickerItem, BaseViewHolder> {
    private Context context;

    public StickerAdapter(@Nullable List<StickerItem> data, Context context) {
        super(R.layout.item_sticker, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, StickerItem item) {
        ItemStickerBinding binding = ItemStickerBinding.bind(helper.itemView);
        Glide.with(context).load(item.getUrl()).into(binding.ivSticker);
    }
}
