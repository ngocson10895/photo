package com.piccollage.collagemaker.picphotomaker.entity;

import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Layout;

import androidx.annotation.NonNull;

/**
 * Create from Administrator
 * Purpose: Cung cấp text của user khi thực hiện add text vào photo
 * Des: Bao gôm các thuộc tính của text;
 */
public class TextOfUser implements Parcelable {
    private String content;
    private int colorText, colorShadow;
    private float dx, dy, radius;
    private Layout.Alignment alignment;
    private float lineSpacing;
    private float letterSpacing;
    private Typeface typeface;

    public TextOfUser() {
    }

    private TextOfUser(Parcel in) {
        content = in.readString();
        colorText = in.readInt();
        colorShadow = in.readInt();
        dx = in.readFloat();
        dy = in.readFloat();
        radius = in.readFloat();
        lineSpacing = in.readFloat();
        letterSpacing = in.readFloat();
    }

    public static final Creator<TextOfUser> CREATOR = new Creator<TextOfUser>() {
        @Override
        public TextOfUser createFromParcel(Parcel in) {
            return new TextOfUser(in);
        }

        @Override
        public TextOfUser[] newArray(int size) {
            return new TextOfUser[size];
        }
    };

    public String getContent() {
        return content;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getColorText() {
        return colorText;
    }

    public void setColorText(int colorText) {
        this.colorText = colorText;
    }

    public int getColorShadow() {
        return colorShadow;
    }

    public void setColorShadow(int colorShadow) {
        this.colorShadow = colorShadow;
    }

    public float getDx() {
        return dx;
    }

    public void setDx(float dx) {
        this.dx = dx;
    }

    public float getDy() {
        return dy;
    }

    public void setDy(float dy) {
        this.dy = dy;
    }

    public Layout.Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Layout.Alignment alignment) {
        this.alignment = alignment;
    }

    public float getLineSpacing() {
        return lineSpacing;
    }

    public void setLineSpacing(float lineSpacing) {
        this.lineSpacing = lineSpacing;
    }

    public float getLetterSpacing() {
        return letterSpacing;
    }

    public void setLetterSpacing(float letterSpacing) {
        this.letterSpacing = letterSpacing;
    }

    @NonNull
    @Override
    public String toString() {
        return getContent() + "-" + getAlignment() + "-" + getColorShadow() + "-" + getColorText() + "-" +
                getDx() + "-" + getDy() + "-" + getLetterSpacing() + "-" + getLineSpacing();
    }

    public Typeface getTypeface() {
        return typeface;
    }

    public void setTypeface(Typeface typeface) {
        this.typeface = typeface;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(content);
        dest.writeInt(colorText);
        dest.writeInt(colorShadow);
        dest.writeFloat(dx);
        dest.writeFloat(dy);
        dest.writeFloat(radius);
        dest.writeFloat(lineSpacing);
        dest.writeFloat(letterSpacing);
    }
}
