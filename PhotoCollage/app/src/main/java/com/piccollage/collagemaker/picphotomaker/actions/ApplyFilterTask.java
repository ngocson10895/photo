package com.piccollage.collagemaker.picphotomaker.actions;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

import java.lang.ref.WeakReference;

import dauroi.photoeditor.listener.ApplyFilterListener;

/**
 * Create from Administrator
 * Purpose: Thực hiện quá trình filter cho ảnh
 * Des: thực hiện quá trình filter khi user chọn filter cho ảnh
 */
public class ApplyFilterTask extends AsyncTask<Void, Void, Bitmap> {
    private WeakReference<EditorActivity> weakReference; // cần phải có weak reference nếu không sẽ bị lỗi leak memory
    private ApplyFilterListener mListener;

    ApplyFilterTask(EditorActivity activity, ApplyFilterListener listener) {
        weakReference = new WeakReference<>(activity);
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        return mListener.applyFilter();
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        EditorActivity activity = weakReference.get();
        if (activity == null || activity.isDestroyed()) {
            return;
        }
        if (result != null) {
            activity.setImage(result, true);
        }
        // go to filter mode
        activity.getCurrentAction().onDetach();
        mListener.onFinishFiltering();
    }
}
