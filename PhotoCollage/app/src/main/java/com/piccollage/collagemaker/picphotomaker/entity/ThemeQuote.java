package com.piccollage.collagemaker.picphotomaker.entity;

import java.util.ArrayList;

public class ThemeQuote {
    private String nameTheme;
    private ArrayList<Quote> quotes;

    public ThemeQuote() {

    }

    public void setNameTheme(String nameTheme) {
        this.nameTheme = nameTheme;
    }

    public void setQuotes(ArrayList<Quote> quotes) {
        this.quotes = quotes;
    }

    public String getNameTheme() {
        return nameTheme;
    }

    public ArrayList<Quote> getQuotes() {
        return quotes;
    }
}
