package com.piccollage.collagemaker.picphotomaker.utils.uui;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * Custom grid layout manager to disable scroll or not
 */
public class ObservableGridManager extends GridLayoutManager {
    // true if we can scroll (not locked)
    // false if we cannot scroll (locked)
    private boolean mScrollable = false;

    public void setScrollingEnabled(boolean enabled) {
        mScrollable = enabled;
    }

    public ObservableGridManager(Context context) {
        super(context, 0);
    }

    public ObservableGridManager(Context context, int spanCount) {
        super(context, spanCount);
    }

    public ObservableGridManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean canScrollVertically() {
        return mScrollable;
    }
}
