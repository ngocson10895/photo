package com.piccollage.collagemaker.picphotomaker.eventbus;

import com.piccollage.collagemaker.picphotomaker.entity.FontTheme;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;

import java.util.ArrayList;

/**
 * event khi load font từ api
 */
public class EventItemShopFont {
    private ArrayList<FontTheme> fontThemes;

    public EventItemShopFont(ArrayList<FontTheme> fontThemes) {
        this.fontThemes = fontThemes;
    }

    public ArrayList<FontTheme> getFontThemes() {
        return fontThemes;
    }
}
