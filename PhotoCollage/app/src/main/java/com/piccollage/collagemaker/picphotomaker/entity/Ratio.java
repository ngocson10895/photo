package com.piccollage.collagemaker.picphotomaker.entity;

public class Ratio {
    private String ratio;
    private int iconPicked;
    private int icon;
    private boolean pick;
    private String name;

    public Ratio(String ratio, int iconPicked, int icon, boolean pick, String name) {
        this.ratio = ratio;
        this.iconPicked = iconPicked;
        this.icon = icon;
        this.pick = pick;
        this.name = name;
    }

    public String getRatio() {
        return ratio;
    }

    public int getIconPicked() {
        return iconPicked;
    }

    public int getIcon() {
        return icon;
    }

    public boolean isPick() {
        return pick;
    }

    public String getName() {
        return name;
    }

    public void setPick(boolean pick) {
        this.pick = pick;
    }
}
