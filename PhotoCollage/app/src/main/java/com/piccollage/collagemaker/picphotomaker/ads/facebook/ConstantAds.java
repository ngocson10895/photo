package com.piccollage.collagemaker.picphotomaker.ads.facebook;

import com.facebook.ads.NativeAdLayout;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

public class ConstantAds {
    public static AdView BANNER_GOOGLE_AD_EXIT = null;
    public static AdView BANNER_GOOGLE_AD_OTHER = null;
    public static com.facebook.ads.AdView BANNER_FACEBOOK_AD_EXIT;

    public static NativeAdLayout NATIVE_AD_FACEBOOK;
    public static UnifiedNativeAdView NATIVE_AD_GOOGLE;

//    public static InterstitialAd INTERSTITIAL_OPEN_APP;
//    public static InterstitialAd INTERSTITIAL_IN_APP;
//    public static com.facebook.ads.InterstitialAd INTERSTITIAL_IN_APP_FACEBOOK;

    /**
     * call when destroy MainActivity.
     */
    public static void destroyAds() {
//        INTERSTITIAL_OPEN_APP = null;
//        INTERSTITIAL_IN_APP = null;
//        INTERSTITIAL_IN_APP_FACEBOOK = null;
        BANNER_GOOGLE_AD_EXIT = null;
        BANNER_FACEBOOK_AD_EXIT = null;
        BANNER_GOOGLE_AD_OTHER = null;
    }
}
