package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemBannerNewBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Not use
 */
public class BannerWhatNewAdapter extends RecyclerView.Adapter<BannerWhatNewAdapter.ViewHolder> {
    private List<Drawable> list = new ArrayList<>();
    private Context context;
    private IClickQuote iClickQuote;
    private final RequestManager glide;

    public BannerWhatNewAdapter(Context context, IClickQuote iClickQuote, RequestManager glide) {
        this.context = context;
        this.iClickQuote = iClickQuote;
        this.glide = glide;
    }

    public interface IClickQuote{
        void clickQuote();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_banner_new, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        glide.load(list.get(i % list.size())).into(viewHolder.binding.img);
        viewHolder.itemView.setOnClickListener(v -> iClickQuote.clickQuote());
    }

    @Override
    public int getItemCount() {
        return 2000;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemBannerNewBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemBannerNewBinding.bind(itemView);
        }
    }
}
