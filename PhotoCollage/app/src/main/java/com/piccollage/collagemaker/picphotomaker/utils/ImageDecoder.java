package com.piccollage.collagemaker.picphotomaker.utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;

import java.io.IOException;
import java.io.InputStream;

import dauroi.photoeditor.utils.FileUtils;
import es.dmoral.toasty.Toasty;

/**
 * Image Decode
 */
public class ImageDecoder {
    private static int SAMPLER_SIZE = 512; //2^9

    public static Bitmap decodeResource(Context context, int resId) {
        return decodeSampledBitmapFromResource(context.getResources(), resId,
                SAMPLER_SIZE, SAMPLER_SIZE);
    }

    // get bitmap from assets
    public static Bitmap getBitmapFromAssets(String fileName, Context context) {
        Bitmap bitmap = null;
        try {
            if (context != null) {
                InputStream inputStream = context.getAssets().open(fileName);
                bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    // drawable to bitmap
    public static Bitmap drawableToBitmap(Drawable drawable) throws OutOfMemoryError {
        try {
            Bitmap bitmap;

            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                if (bitmapDrawable.getBitmap() != null) {
                    return bitmapDrawable.getBitmap();
                }
            }

            if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
            throw e;
        }
    }

    public static Bitmap decodeUriToBitmap(Context context, Uri uri) {
        if (uri == null) {
            return null;
        }
        try {
            String file = FileUtils.getPath(context, uri);
            //ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
            //FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file, options);
            //BitmapFactory.decodeFileDescriptor(fileDescriptor, new Rect(), options);
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            int requiredSize = SAMPLER_SIZE;
            while (width_tmp / 2 > requiredSize
                    && height_tmp / 2 > requiredSize) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            // decode with inSampleSize
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(file, options);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    // file to bitmap
    public static Bitmap decodeFileToBitmap(String pathName) throws OutOfMemoryError {
        try {
            // decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(pathName, options);
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            int requiredSize = SAMPLER_SIZE;
            while (width_tmp / 2 > requiredSize
                    && height_tmp / 2 > requiredSize) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            options.inJustDecodeBounds = false;
            options.inSampleSize = scale;
            Bitmap bm = null;
            try {
                ExifInterface ei = new ExifInterface(pathName);
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                switch (orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bm = ImageUtils.rotateImage(BitmapFactory.decodeFile(pathName, options), 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bm = ImageUtils.rotateImage(BitmapFactory.decodeFile(pathName, options), 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bm = ImageUtils.rotateImage(BitmapFactory.decodeFile(pathName, options), 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        bm = BitmapFactory.decodeFile(pathName, options);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bm;
        } catch (Exception | OutOfMemoryError ex) {
            Toasty.error(MyApplication.getContext(), R.string.image_too_large, Toasty.LENGTH_SHORT).show();
            ex.printStackTrace();
            System.gc();
        }
        return null;
    }

    // uri to drawable
    public static BitmapDrawable decodeUriToDrawable(Context context, Uri uri) throws OutOfMemoryError {
        try {
            String file = FileUtils.getPath(context, uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file, options);
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            int requiredSize = SAMPLER_SIZE;
            while (width_tmp / 2 > requiredSize
                    && height_tmp / 2 > requiredSize) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            // decode with inSampleSize
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            Bitmap bm = BitmapFactory.decodeFile(file, options);
            return new BitmapDrawable(context.getResources(), bm);
        } catch (Exception | OutOfMemoryError ex) {
            ex.printStackTrace();
            System.gc();
        }
        return null;
    }

    public static Bitmap decodeBlobToBitmap(byte[] data) throws OutOfMemoryError {
        try {
            if (data != null) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(data, 0, data.length, options);

                // Find the correct scale value. It should be the power of 2.
                int width_tmp = options.outWidth, height_tmp = options.outHeight;
                int scale = 1;
                int requiredSize = SAMPLER_SIZE;
                while (width_tmp / 2 > requiredSize
                        && height_tmp / 2 > requiredSize) {
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale *= 2;
                }

                // decode with inSampleSize
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                return BitmapFactory.decodeByteArray(data, 0, data.length,
                        options);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Drawable decodeBlobToDrawable(byte[] data, int reqWidth, int reqHeight, Resources res) throws OutOfMemoryError {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return new BitmapDrawable(res, BitmapFactory.decodeByteArray(data,
                    0, data.length, options));
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }
        return null;
    }

    public static Bitmap decodeBlobToBitmap(byte[] data, int reqWidth, int reqHeight, Resources res) throws OutOfMemoryError {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(data, 0, data.length, options);
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Drawable decodeBlobToDrawable(byte[] data, Resources res) throws OutOfMemoryError {
        try {
            // decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, options);

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            int requiredSize = SAMPLER_SIZE;
            while (width_tmp / 2 > requiredSize
                    && height_tmp / 2 > requiredSize) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            options.inJustDecodeBounds = false;
            options.inSampleSize = scale;
            Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length,
                    options);

            return new BitmapDrawable(res, bm);
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Drawable decodeStreamToDrawable(InputStream is, Resources res) throws OutOfMemoryError {
        try {
            // decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Rect outPadding = new Rect();
            BitmapFactory.decodeStream(is, outPadding, options);

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            int requiredSize = SAMPLER_SIZE;
            while (width_tmp / 2 > requiredSize
                    && height_tmp / 2 > requiredSize) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            options.inJustDecodeBounds = false;
            options.inSampleSize = scale;

            return new BitmapDrawable(res, BitmapFactory.decodeStream(is,
                    outPadding, options));
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Bitmap decodeStreamToBitmap(InputStream is) throws OutOfMemoryError {
        try {
            // decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Rect outPadding = new Rect();
            BitmapFactory.decodeStream(is, outPadding, options);

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            int requiredSize = SAMPLER_SIZE;
            while (width_tmp / 2 > requiredSize
                    && height_tmp / 2 > requiredSize) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            options.inJustDecodeBounds = false;
            options.inSampleSize = scale;

            return BitmapFactory.decodeStream(is, outPadding, options);
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Bitmap decodeStreamToBitmap(InputStream is, int reqWidth, int reqHeight) throws OutOfMemoryError {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Rect outPadding = new Rect();
            BitmapFactory.decodeStream(is, outPadding, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(is, outPadding, options);
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Drawable decodeStreamToDrawable(InputStream is, int reqWidth, int reqHeight, Resources res) throws OutOfMemoryError {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Rect outPadding = new Rect();
            BitmapFactory.decodeStream(is, outPadding, options);
            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return new BitmapDrawable(res, BitmapFactory.decodeStream(is,
                    outPadding, options));
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    private static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) throws OutOfMemoryError {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resId, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeResource(res, resId, options);
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }

        return null;
    }

    public static Drawable decodeSampledDrawableFromResource(Resources res, int resId, int reqWidth, int reqHeight) throws OutOfMemoryError {
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resId, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return new BitmapDrawable(res, BitmapFactory.decodeResource(res,
                    resId, options));
        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            throw e;
        }

        return null;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = Math.min(heightRatio, widthRatio);
        }

        return inSampleSize;
    }

    public static String getRealPathFromURI(Context activity, Uri contentURI) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null,
                null, null, null);
        // Source is Dropbox or other similar local file path
        if (cursor == null)
            return contentURI.getPath();
        else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
}
