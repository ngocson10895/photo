package com.piccollage.collagemaker.picphotomaker.networking;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.facebook.ads.AudienceNetworkAds;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.corebase.lg;
import com.google.android.gms.corebase.logger;
import com.onesignal.OneSignal;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;

import dauroi.photoeditor.PhotoEditorApp;
import okhttp3.OkHttpClient;

/**
 * Create from Administrator
 * Purpose: Application class
 * Des: chứa các đăng ký như flurry, Android networking
 */
public class MyApplication extends PhotoEditorApp {
    private static final String TAG = "MyApplication";
    private static MyApplication sInstance;

    public static MyApplication getInstance() {
        return sInstance;
    }

    public static Context getContext() {
        return sInstance;
    }

    public static boolean isAppRunning;

    @Override
    public void onCreate() {
        super.onCreate();
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, "KSB8GJ5B8F8PJNGV7V9Z"); // add flury

        sInstance = this;
        setupLifecycleListener();
        initAndroidNetworking();
        logger.e(this);

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        AudienceNetworkAds.initialize(this);
    }

    public void setupLifecycleListener() {
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            void onMoveToForeground() {
                lg.logs("MultiClassApplication", "onMoveToForeground: ");
                isAppRunning = true;
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            void onMoveToBackground() {
                lg.logs("MultiClassApplication", "onMoveToBackground: ");
                isAppRunning = false;
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            void onDestroy(){

            }
        });
    }

    private void initAndroidNetworking() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(lg.isDebug ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addNetworkInterceptor(loggingInterceptor)
                .build();
        AndroidNetworking.initialize(getApplicationContext(), okHttpClient);
    }
}
