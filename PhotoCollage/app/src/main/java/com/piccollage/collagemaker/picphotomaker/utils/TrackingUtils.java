package com.piccollage.collagemaker.picphotomaker.utils;

public class TrackingUtils {

    public interface TrackingConstant {
        String VERSION = "_VERSION_2_";

        interface Home {
            String NAME = "HOME" + VERSION;
            String STORE = NAME + "HOME_STORE";
            String MY_CREATIVE = NAME + "HOME_MY_CREATIVE";
            String RATE_APP = NAME + "HOME_RATE_APP";
            String UP_TO_PRO = NAME + "HOME_NAV_UP_TO_PRO";
            String UP_TO_PRO_TOOLBAR = NAME + "HOME_TB_PRO";
            String UP_TO_PRO_SALE_TOOLBAR = NAME + "HOME_TB_PRO_SALE";
        }

        interface DetailItemShop {
            String NAME = "DETAIL_ITEM_SHOP" + VERSION;
            String BACK = NAME + "BACK";
            String DOWNLOAD = NAME + "DOWNLOAD";
            String DOWNLOAD_DONE = NAME + "DOWNLOAD_DONE";
            String DOWNLOAD_CANCEL = NAME + "DOWNLOAD_CANCEL";
            String DOWNLOAD_ERROR = NAME + "DOWNLOAD_ERROR";
            String ACTION_USE = NAME + "ACTION_USE";
        }

        interface PickPhoto {
            String NAME = "PICK_PHOTO" + VERSION;
            String NEXT = NAME + "NEXT";
            String BACK = NAME + "BACK";
            String REMOVE_PHOTO = NAME + "REMOVE_PHOTO";
            String PICK_PHOTO = NAME + "PICK_PHOTO";
            String BACK_PRESSED = NAME + "BACK_PRESSED";
            String NUMBER_GALLERY = NAME + "NUMBER_GALLERY";
            String TOTAL_IMAGES = NAME + "TOTAL_IMAGE";
        }

        interface ChangeImage {
            String NAME = "CHANGE_IMAGE" + VERSION;
            String DONE = NAME + "DONE";
        }

        interface ViewType {
            String NAME = "VIEW_TYPE" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
        }

        // name đặt ở lúc show. done đặt lúc hoàn thành. cancel lúc hủy hoặc back
        interface ViewMargin {
            String NAME = "VIEW_MARGIN" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
            String MARGIN = NAME + "MARGIN";
            String CORNER = NAME + "CORNER";
            String SHADOW = NAME + "SHADOW";
            String RATIO = NAME + "RATIO";
        }

        interface ViewBackground {
            String NAME = "VIEW_BACKGROUND" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
            String SOLID = NAME + "SOLID";
            String GRADIENT = NAME + "GRADIENT";
            String PATTERN = NAME + "PATTERN";
            String PALETTE = NAME + "PALETTE";
        }

        interface ViewSticker {
            String NAME = "VIEW_STICKER" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
        }

        interface ViewText {
            String NAME = "VIEW_TEXT" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
            String TEXT = NAME + "TEXT";
            String FONT = NAME + "FONT";
            String CUSTOM = NAME + "CUSTOM";
            String CHARACTER_SPACE = NAME + "CHARACTER_SPACE";
            String LINE_SPACE = NAME + "LINE_SPACE";
            String SHADOW = NAME + "SHADOW";
            String OPACITY_COLOR = NAME + "OPACITY_COLOR";
            String ALIGNMENT = NAME + "ALIGNMENT";
        }

        interface ViewMoreFeature {
            String NAME = "VIEW_MORE_FEATURE" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
            String FLIP = NAME + "FLIP";
            String CHANGE_IMAGE = NAME + "CHANGE_IMAGE";
        }

        interface ViewRatio {
            String NAME = "VIEW_RATIO" + VERSION;
            String DONE = NAME + "DONE";
            String CANCEL = NAME + "CANCEL";
        }

        interface Collage {
            String NAME = "COLLAGE" + VERSION;
            String DONE = NAME + "DONE";
        }

        interface Editor {
            String NAME = "EDITOR" + VERSION;
            String DONE = NAME + "DONE";
            String EFFECT = NAME + "EFFECT";
            String CROP = NAME + "CROP";
            String ROTATE = NAME + "ROTATE";
            String TEXT = NAME + "TEXT";
            String DRAW = NAME + "DRAW";
            String FOCUS = NAME + "FOCUS";
            String BLUR = NAME + "BLUR";
            String APPLY = NAME + "APPLY";
            String CANCEL_APPLY = NAME + "CANCEL_APPLY";
        }

        interface PipStyle {
            String NAME = "PIP_STYLE" + VERSION;
            String BACK = NAME + "BACK";
            String DOWNLOAD = NAME + "DOWNLOAD";
            String DOWNLOAD_DONE = NAME + "DOWNLOAD_DONE";
            String DOWNLOAD_CANCEL = NAME + "DOWNLOAD_CANCEL";
            String DOWNLOAD_ERROR = NAME + "DOWNLOAD_ERROR";
        }

        interface Pip {
            String NAME = "PIP" + VERSION;
            String DONE = NAME + "DONE";
        }

        interface Scrapbook {
            String NAME = "SCRAPBOOK" + VERSION;
            String DONE = NAME + "DONE";
        }

        interface PickQuote {
            String NAME = "PICK_QUOTE" + VERSION;
            String BACK = NAME + "BACK";
            String ACTION_QUOTE = NAME + "PICK_QUOTE";
        }

        interface Quote {
            String NAME = "QUOTE" + VERSION;
            String DONE = NAME + "DONE";
            String PHOTO_BG = NAME + "PHOTO_BG";
            String ROTATE_HOR = NAME + "ROTATE_HOR";
            String ROTATE_VER = NAME + "ROTATE_VER";
        }

        interface Preview {
            String NAME = "PREVIEW" + VERSION;
            String SHARE = NAME + "SHARE";
            String DONE = NAME + "DONE";
            String BACK_TO_FEATURE = NAME + "BACK_TO_FEATURE";
            String RECREATE_IMAGE = NAME + "RECREATE_IMAGE";
        }

        interface BuySub {
            String NAME = "BUY_SUB" + VERSION;
            String BACK = NAME + "BACK";
            String SUB = NAME + "SUB";
            String RESTORE = NAME + "RESTORE";
        }

        interface Store {
            String NAME = "STORE" + VERSION;
            String BACK = NAME + "BACK";
            String GO_TO_MANAGE = NAME + "GO_TO_MANAGE";
            String DOWNLOAD = NAME + "DOWNLOAD";
            String DOWNLOAD_DONE = NAME + "DOWNLOAD_DONE";
            String DOWNLOAD_CANCEL = NAME + "DOWNLOAD_CANCEL";
            String DOWNLOAD_ERROR = NAME + "DOWNLOAD_ERROR";
        }

        interface PackageItem {
            String NAME = "PACKAGE_ITEM" + VERSION;
            String BACK = NAME + "BACK";
            String WANT_DELETE_ITEM = NAME + "WANT_DELETE_ITEM";
        }

        interface PackageTheme {
            String NAME = "PACKAGE_THEME" + VERSION;
            String BACK = NAME + "BACK";
        }

        interface DetailItem {
            String NAME = "DETAIL_ITEM" + VERSION;
            String BACK = NAME + "BACK";
            String USE = NAME + "USE";
        }

        interface DialogExitFeature {
            String NAME = "EXIT_FEATURE" + VERSION;
            String OK = NAME + "OK";
            String CANCEL = NAME + "CANCEL";
        }

        interface DialogExitApp {
            String NAME = "EXIT_APP" + VERSION;
            String OK = NAME + "OK";
            String CANCEL = NAME + "CANCEL";
            String RATE_US = NAME + "RATE_US";
        }

        interface DialogNews {
            String NAME = "NEWS" + VERSION;
            String BACK = NAME + "BACK";
            String TRY = NAME + "TRY";
        }

        interface DialogRate {
            String NAME = "RATE_APP" + VERSION;
            String RATE = NAME + "RATE";
            String SKIP = NAME + "SKIP";
        }

        interface DialogDelete {
            String NAME = "DIALOG_DELETE_ITEM" + VERSION;
            String DELETE = NAME + "DELETE";
            String CANCEL = NAME + "CANCEL";
        }

        interface DialogChangeLanguage {
            String NAME = "DIALOG_CHANGE_LANGUAGE" + VERSION;
            String DONE = NAME + "DONE";
        }

        interface BottomQuote {
            String NAME = "BOTTOM_QUOTE" + VERSION;
            String CLOSE = NAME + "CLOSE";
            String EDIT = NAME + "EDIT";
            String COPY = NAME + "COPY";
            String SHARE = NAME + "SHARE";
        }

        interface BottomUsing {
            String NAME = "BOTTOM_USING" + VERSION;
            String CLOSE = NAME + "CLOSE";
            String ACTION = NAME + "ACTION";
        }

        interface Ads {
            interface IntersOpen {
                String NAME = "INTERS_OPEN" + VERSION;
                String REQUEST = NAME + "REQUEST";
                String LOADED = NAME + "LOADED";
                String FAIL = NAME + "FAIL";
                String SHOW = NAME + "SHOW";
            }

            interface IntersOthers {
                String NAME = "INTERS_ADS_OTHERS" + VERSION;
                String REQUEST = NAME + "REQUEST";
                String LOADED = NAME + "LOADED";
                String FAIL = NAME + "FAIL";
                String SHOW = NAME + "SHOW";
            }

            interface Native {
                String NAME = "NATIVE_ADS" + VERSION;
                String REQUEST = NAME + "REQUEST";
                String LOADED = NAME + "LOADED";
                String FAIL = NAME + "FAIL";
                String SHOW = NAME + "SHOW";
            }

            interface Banner {
                String NAME = "BANNER_ADS" + VERSION;
                String REQUEST = NAME + "REQUEST";
                String LOADED = NAME + "LOADED";
                String FAIL = NAME + "FAIL";
            }
        }

        interface EVENT_NEW {
            String LOADING_INFO_OK = "LOADING_INFO_OK";
            String LOADING_INFO_FAIL = "LOADING_INFO_FAIL";
        }
    }
}
