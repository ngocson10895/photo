package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.piccollage.collagemaker.picphotomaker.R;

public class UnifiedNativeAdViewHolder extends RecyclerView.ViewHolder {
    private UnifiedNativeAdView adView;

    public UnifiedNativeAdViewHolder(@NonNull View itemView) {
        super(itemView);
        adView = itemView.findViewById(R.id.ad_view);

        // The MediaView will display a video asset if one is present in the ad, and the
        // first image asset otherwise.
        adView.setMediaView(adView.findViewById(R.id.ad_media));

        // Register the view used for each individual asset.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
    }

    public UnifiedNativeAdView getAdView() {
        return adView;
    }
}
