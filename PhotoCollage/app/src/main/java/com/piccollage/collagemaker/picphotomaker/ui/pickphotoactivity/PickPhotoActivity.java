package com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.PhotoPickedAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityPickPhotoBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventReload;
import com.piccollage.collagemaker.picphotomaker.ui.collageactivity.CollageActivity;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;
import com.piccollage.collagemaker.picphotomaker.ui.homeactivity.HomeActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.PipActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer.PipStyleActivity;
import com.piccollage.collagemaker.picphotomaker.ui.scrapbookactivity.ScrapbookActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ItemOffsetDecoration;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: chứa và thể hiện các photo mà user pick
 * Des: thể hiện các photo mà user pick
 */
public class PickPhotoActivity extends BaseAppActivity implements
        PhotoPickedAdapter.IRemove, DetailGalleryFragment.IDetailGalleryContract {
    public static final String TAG = "PickPhotoActivity";
    public static final String PHOTOS_PICKED = "photos";
    public static final String PIP_PICTURE = "pip_picture";
    public static final String MODE = "mode";

    private String mode;
    private ActivityPickPhotoBinding binding;
    private SharedPrefsImpl sharedPrefs;
    private PhotoPickedAdapter adapter;
    private List<Photo> photosPicked;
    private PipPicture pipPicture;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityPickPhotoBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
    }

    // thay đổi số lượng ảnh được pick theo từng tính năng
    @SuppressLint("SetTextI18n")
    public void initData() {
        sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO, 0);
        if (getIntent() != null) {
            mode = getIntent().getStringExtra(MODE);
            switch (mode) {
                case Constant.QUOTE:
                case Constant.EDITOR:
                    binding.tvTypeFrame.setText("/1");
                    sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO_MAX, 1);
                    break;
                case Constant.COLLAGE:
                case Constant.SCRAPBOOK:
                    binding.tvTypeFrame.setText("/10");
                    sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO_MAX, 10);
                    break;
                case Constant.PIP:
                    pipPicture = getIntent().getParcelableExtra(PipStyleActivity.PIP_PICTURE);
                    if (pipPicture != null) {
                        binding.tvTypeFrame.setText("/" + pipPicture.getType());
                        sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO_MAX, pipPicture.getType());
                    }
                    break;
                default:
            }
        } else {
            mode = "";
        }
        binding.tvNumberPick.setText(String.valueOf(photosPicked.size()));
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickPhoto.NAME);
        sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);
        photosPicked = new ArrayList<>();
        adapter = new PhotoPickedAdapter(photosPicked, this, this);
        binding.rvListPhotoPicked.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvListPhotoPicked.addItemDecoration(new ItemOffsetDecoration(8, this));
        binding.rvListPhotoPicked.setAdapter(adapter);

        fragmentTrans(R.id.frGallery, GalleryAlbumFragment.newInstance(Constant.PICK_MORE, photosPicked));
    }

    @Override
    public void onHasNetwork() {
        loadAds();
    }

    @Override
    public void buttonClick() {
        onViewClicked();
        onClearAllClicked();
    }

    private void loadAds() {
        Advertisement.showBannerAdOther(this, null, true, null);
    }

    public void onViewClicked() {
        binding.btnNext.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickPhoto.NEXT + photosPicked.size());
                if (mode.equals(Constant.COLLAGE)) {
                    if (photosPicked != null && photosPicked.isEmpty()) {
                        Toasty.warning(this, getString(R.string.at_least_1_photo), Toasty.LENGTH_SHORT).show();
                    } else {
                        ArrayList<String> paths = new ArrayList<>();
                        for (Photo p : photosPicked) {
                            paths.add(p.getPathPhoto());
                        }
                        Intent intent = new Intent(this, CollageActivity.class);
                        intent.putStringArrayListExtra(PHOTOS_PICKED, paths);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
                if (mode.equals(Constant.QUOTE)) {
                    if (photosPicked != null && !photosPicked.isEmpty()) {
                        Intent data = new Intent();
                        data.putExtra(PHOTOS_PICKED, photosPicked.get(0));
                        setResult(AppCompatActivity.RESULT_OK, data);
                        finish();
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                    } else {
                        Toasty.warning(this, getResources().getString(R.string.have_to_pick_1_image), Toasty.LENGTH_SHORT).show();
                    }
                }
                if (mode.equals(Constant.SCRAPBOOK)) {
                    if (photosPicked != null && photosPicked.isEmpty()) {
                        Toasty.warning(this, getString(R.string.at_least_1_photo), Toasty.LENGTH_SHORT).show();
                    } else {
                        ArrayList<String> paths = new ArrayList<>();
                        for (Photo p : photosPicked) {
                            paths.add(p.getPathPhoto());
                        }
                        Intent intent = new Intent(this, ScrapbookActivity.class);
                        intent.putStringArrayListExtra(PHOTOS_PICKED, paths);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
                if (mode.equals(Constant.PIP)) {
                    if (photosPicked != null && pipPicture != null) {
                        if (photosPicked.isEmpty() || photosPicked.size() < pipPicture.getType()) {
                            Toasty.warning(this, getString(R.string.u_should_chose) + " " + pipPicture.getType() + " " + getString(R.string.image), Toasty.LENGTH_SHORT).show();
                        } else {
                            ArrayList<String> paths = new ArrayList<>();
                            for (Photo p : photosPicked) {
                                paths.add(p.getPathPhoto());
                            }
                            Intent intent = new Intent(this, PipActivity.class);
                            intent.putStringArrayListExtra(PHOTOS_PICKED, paths);
                            intent.putExtra(PIP_PICTURE, pipPicture);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            finish();
                        }
                    }
                }
                if (mode.equals(Constant.EDITOR)) {
                    if (photosPicked != null && photosPicked.isEmpty()) {
                        Toasty.warning(this, getString(R.string.at_least_1_photo), Toasty.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(this, EditorActivity.class);
                        intent.putExtra(PHOTOS_PICKED, photosPicked.get(0).getPathPhoto());
                        intent.putExtra(Constant.VALUE_INTENT, TAG);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
            }
        });
    }

    @Override
    public void remove(Photo photo) {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickPhoto.REMOVE_PHOTO);
        int i = sharedPrefs.get(Constant.QUANTITY_PICK_PHOTO, Integer.class);
        int j = i - 1;
        if (j >= 0) {
            sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO, j);
        } else {
            j = 0;
            sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO, j);
        }
        if (photo.getTimesPick() > 0) {
            photo.setTimesPick(photo.getTimesPick() - 1);
        } else photo.setTimesPick(0);
        photosPicked.remove(photo);
        adapter.setPathsPhotoPicked(photosPicked);
        binding.rvListPhotoPicked.scrollToPosition(photosPicked.size() - 1);
        binding.tvNumberPick.setText(String.valueOf(photosPicked.size()));

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frGallery);
        if (fragment instanceof DetailGalleryFragment) {
            EventBus.getDefault().post(new EventReload());
        }
        if (photosPicked.isEmpty()) {
            binding.rvListPhotoPicked.setVisibility(View.GONE);
        }
    }

    @Override
    public void pick(Photo photo) {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickPhoto.PICK_PHOTO);
        photosPicked.add(photo);
        binding.rvListPhotoPicked.setVisibility(View.VISIBLE);

        adapter.setPathsPhotoPicked(photosPicked);
        binding.rvListPhotoPicked.scrollToPosition(photosPicked.size() - 1);
        binding.tvNumberPick.setText(String.valueOf(photosPicked.size()));
    }

    @Override
    public void onBackPressed() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PickPhoto.BACK_PRESSED);
        super.onBackPressed();
    }

    public void onClearAllClicked() {
        binding.ivClearAll.setOnClickListener(v -> {
            if (!photosPicked.isEmpty()) {
                for (Photo p : photosPicked) p.setTimesPick(0);
                photosPicked.clear();
                binding.tvNumberPick.setText(String.valueOf(photosPicked.size()));
                sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO, 0);
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frGallery);
                if (fragment instanceof DetailGalleryFragment) {
                    EventBus.getDefault().post(new EventReload());
                }
                if (photosPicked.isEmpty()) {
                    binding.rvListPhotoPicked.setVisibility(View.GONE);
                }
            }
        });
    }
}
