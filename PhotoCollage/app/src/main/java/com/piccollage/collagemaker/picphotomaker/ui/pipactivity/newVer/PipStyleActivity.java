package com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.tabs.TabLayout;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.PagerAdapterPIP;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.api.APIServiceWrapper;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityPip2Binding;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.entity.PipTheme;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventPipApi;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Create from Administrator
 * Purpose: Show các loại pip theo loại 1,2,3,4
 * Des: thể hiện các loại pip cho user lựa chọn
 */
public class PipStyleActivity extends BaseAppActivity implements PipStyleFragment.IPipStyleListener {
    private static final String TAG = "PipActivity2";
    public static final String PIP_PICTURE = "pip_picture";
    public static final String PIP_THEME = "pip_theme";

    private ActivityPip2Binding binding;
    private PagerAdapterPIP pagerAdapterPIP;
    private ArrayList<PipTheme> pipThemes;
    private DialogLoading dialogLoading, loading;
    private ApplicationViewModel viewModel;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityPip2Binding.inflate(getLayoutInflater());
    }

    @Override
    public void initView() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PipStyle.NAME);
        viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        pipThemes = new ArrayList<>();

        dialogLoading = new DialogLoading(this, DialogLoading.PREPARING);
        dialogLoading.setCancelable(true);
        dialogLoading.setCanceledOnTouchOutside(true);

        loading = new DialogLoading(this, DialogLoading.CREATE);

        binding.viewPagerPIP.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabBarPIP));
        binding.tabBarPIP.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(binding.viewPagerPIP));

        pagerAdapterPIP = new PagerAdapterPIP(getSupportFragmentManager(), pipThemes);
        binding.viewPagerPIP.setAdapter(pagerAdapterPIP);
        binding.tabBarPIP.setupWithViewPager(binding.viewPagerPIP);
    }

    @Override
    public void initData() {
        loading.show();
        APIServiceWrapper.getInstance().loadAPIPipMode(this, new APIServiceWrapper.IPipAPIListener() {
            @Override
            public void loadDone(ArrayList<PipPicture> pipPictures) {
                viewModel.getPipPictures().observe(PipStyleActivity.this, pipPictures1 -> {
                    if (pipPictures1 != null) {
                        Collections.reverse(pipPictures1);
                        for (PipPicture pipPicture : pipPictures) {
                            for (PipPicture pp : pipPictures1) {
                                if (pipPicture.getType() == pp.getType()) {
                                    if (pipPicture.getId() == pp.getId()) {
                                        pipPicture.setPathPreviewOnline(pp.getPathPreviewOnline());
                                        pipPicture.setPathPreview(pp.getPathPreview());
                                        pipPicture.setForeGround(pp.getForeGround());
                                        pipPicture.setDownloaded(true);
                                    }
                                }
                            }
                        }
                        pipPictures1.addAll(pipPictures);
                        filterPip(pipPictures1);
                    }
                });
            }

            @Override
            public void error(String error) {
                Log.d(TAG, "error: " + error);
                loading.dismiss();
            }
        });
    }

    @Override
    public void onHasNetwork() {
        Advertisement.loadInterstitialAdInApp(this);
        binding.adsLayout.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayout, false,
                new BannerAdsUtils.BannerAdListener() {
                    @Override
                    public void onAdLoaded() {

                    }

                    @Override
                    public void onAdFailed() {

                    }
                }));
    }

    // sử dụng rxandroid để nhóm các pip theo type
    private void filterPip(List<PipPicture> pipPictures1) {
        Observable.fromIterable(pipPictures1)
                .distinct(PipPicture::getId)
                .groupBy(PipPicture::getType)
                .flatMapSingle(groups -> groups.collect(PipTheme::new, (pipTheme, pipPicture) -> {
                    if (pipPicture.getPathPreview().contains("file:///android_asset/pip/")) {
                        pipPicture.setDownloaded(true);
                        String ss = pipPicture.getPathPreviewOnline().replace("file:///android_asset/", "");
                        for (String s : FileUtil.listAssetFiles(ss, this)) {
                            if (!s.contains("preview") && !s.contains("fg")) {
                                String sss = ss.replace("pip/", "");
                                pipPicture.getMasks().add(sss + "/" + s);
                            }
                        }
                    } else {
                        File f = new File(pipPicture.getPathPreviewOnline());
                        if (f.exists()) {
                            for (File cf : f.listFiles()) {
                                if (!cf.getName().contains("preview") && !cf.getName().contains("fg"))
                                    pipPicture.getMasks().add(cf.getAbsolutePath());
                            }
                        }
                        if (pipPicture.getPathPreview().contains("http")) {
                            pipPicture.setDownloaded(false);
                        } else pipPicture.setDownloaded(true);
                    }
                    pipTheme.setName(String.valueOf(pipPicture.getType()));
                    pipTheme.getPipPictures().add(pipPicture);
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PipTheme>() {
                    Disposable disposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                        pipThemes.clear();
                    }

                    @Override
                    public void onNext(PipTheme pipTheme) {
                        pipThemes.add(pipTheme);
                        runOnUiThread(() -> EventBus.getDefault().postSticky(new EventPipApi(pipThemes)));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        pagerAdapterPIP.notifyDataSetChanged();
                        customTabBar();
                        loading.dismiss();
                        disposable.dispose();
                    }
                });
    }

    @Override
    public void notHasNetwork() {

    }

    // 16/03/2020: custom tab bar
    private void customTabBar() {
        if (binding != null) {
            for (int i = 0; i < binding.tabBarPIP.getTabCount(); i++) {
                @SuppressLint("InflateParams")
                TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                tv.setText(pagerAdapterPIP.getPageTitle(i));
                tv.setTypeface(TextUtils.loadTypeface(this, Constant.FONT_DEFAULT));
                Objects.requireNonNull(binding.tabBarPIP.getTabAt(i)).setCustomView(tv);

                View tab = ((ViewGroup) binding.tabBarPIP.getChildAt(0)).getChildAt(i);
                if (tab != null) {
                    ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
                    p.setMargins(BetweenSpacesItemDecoration.dpToPixels(8), 0, BetweenSpacesItemDecoration.dpToPixels(8), 0);
                    tab.requestLayout();
                }
            }
        }
    }

    @Override
    public void buttonClick() {
        onViewClicked();
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PipStyle.BACK);
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Advertisement.loadInterstitialAdInApp(this);
    }

    // 17/03/2020: Pick pip style
    @Override
    public void pickStyle(PipPicture pipPicture) {
        if (pipPicture.getPathPreview().contains("file:///android_asset/")) {
            Intent intent = new Intent(this, PickPhotoActivity.class);
            intent.putExtra(PIP_PICTURE, pipPicture);
            intent.putExtra(PIP_THEME, pipThemes);
            intent.putExtra(PickPhotoActivity.MODE, Constant.PIP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else {
            if (pipPicture.isDownloaded()) {
                Intent intent = new Intent(this, PickPhotoActivity.class);
                intent.putExtra(PIP_PICTURE, pipPicture);
                intent.putExtra(PIP_THEME, pipThemes);
                intent.putExtra(PickPhotoActivity.MODE, Constant.PIP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            } else {
                Advertisement.showInterstitialAdInApp(this, new InterstitialAdsPopupOthers.OnAdsListener() {
                    @Override
                    public void onAdsClose() {
                        downloadPIP(pipPicture);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                    @Override
                    public void close() {
                        downloadPIP(pipPicture);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                });
            }
        }
    }

    // 18/03/2020: download pip when user pick
    private void downloadPIP(PipPicture pipPicture) {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PipStyle.DOWNLOAD + pipPicture.getType());
        dialogLoading.show();
        File fileDelete = FileUtil.getParentFile(this, "PIP");
        if (fileDelete.exists()) {
            FileUtil.deleteFile(fileDelete);
        }
        File parentFile = FileUtil.getParentFile(this, "PIPStyleDownloaded");
        if (!parentFile.exists()) {
            if (parentFile.mkdirs()) {
                Log.d(TAG, "pickStyle: create folder");
            }
        }
        DownloadTask task = new DownloadTask.Builder(pipPicture.getLinkToDownload(), parentFile).setFilename(String.valueOf(pipPicture.getId()))
                .setMinIntervalMillisCallbackProcess(DownloadTask.Builder.DEFAULT_MIN_INTERVAL_MILLIS_CALLBACK_PROCESS)
                .setPassIfAlreadyCompleted(DownloadTask.Builder.DEFAULT_PASS_IF_ALREADY_COMPLETED).build();
        task.enqueue(new DownloadListener4WithSpeed() {
            @Override
            public void taskStart(@NonNull DownloadTask task) {

            }

            @Override
            public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {

            }

            @Override
            public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {

            }

            @Override
            public void infoReady(@NonNull DownloadTask task, @NonNull BreakpointInfo info, boolean fromBreakpoint, @NonNull Listener4SpeedAssistExtend.Listener4SpeedModel model) {

            }

            @Override
            public void progressBlock(@NonNull DownloadTask task, int blockIndex, long currentBlockOffset, @NonNull SpeedCalculator blockSpeed) {

            }

            @Override
            public void progress(@NonNull DownloadTask task, long currentOffset, @NonNull SpeedCalculator taskSpeed) {

            }

            @Override
            public void blockEnd(@NonNull DownloadTask task, int blockIndex, BlockInfo info, @NonNull SpeedCalculator blockSpeed) {

            }

            @Override
            public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause, @NonNull SpeedCalculator taskSpeed) {
                if (cause == EndCause.COMPLETED) {
                    String childParentFile = parentFile.getAbsolutePath() + "/" + pipPicture.getId();
                    File childFile = new File(childParentFile + "unzip");

                    try {
                        InputStream inputStream = new FileInputStream(childParentFile);
                        if (!childFile.exists()) {
                            if (childFile.mkdirs()) {
                                Log.d(TAG, "taskEnd: ");
                            }
                        }
                        FileUtil.unzip(inputStream, childFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (parentFile.isDirectory()) {
                        if (parentFile.listFiles() != null) {
                            for (File child : parentFile.listFiles()) {
                                if (!child.isDirectory()) {
                                    if (child.delete()) {
                                        Log.d(TAG, "taskEnd: delete loaded");
                                    }
                                }
                            }
                        }
                    }
                    if (childFile.listFiles() != null) {
                        pipPicture.setPathPreviewOnline(childFile.getAbsolutePath());
                        for (File f : childFile.listFiles()) {
                            if (!f.isDirectory()) {
                                if (f.getName().contains("preview"))
                                    pipPicture.setPathPreview(f.getAbsolutePath());
                                else if (f.getName().contains("fg"))
                                    pipPicture.setForeGround(f.getAbsolutePath());
                                else {
                                    pipPicture.getMasks().add(f.getAbsolutePath());
                                }
                            }
                        }
                    }
                    dialogLoading.dismiss();
                    MyTrackingFireBase.trackingScreen(PipStyleActivity.this, TrackingUtils.TrackingConstant.PipStyle.DOWNLOAD_DONE);
                    Intent intent = new Intent(PipStyleActivity.this, PickPhotoActivity.class);
                    intent.putExtra(PIP_PICTURE, pipPicture);
                    intent.putExtra(PIP_THEME, pipThemes);
                    intent.putExtra(PickPhotoActivity.MODE, Constant.PIP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    viewModel.insertPip(pipPicture);
                } else if (cause == EndCause.ERROR) {
                    MyTrackingFireBase.trackingScreen(PipStyleActivity.this, TrackingUtils.TrackingConstant.PipStyle.DOWNLOAD_ERROR);
                    dialogLoading.dismiss();
                    Toasty.error(PipStyleActivity.this, getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
                } else {
                    MyTrackingFireBase.trackingScreen(PipStyleActivity.this, TrackingUtils.TrackingConstant.PipStyle.DOWNLOAD_CANCEL);
                    dialogLoading.dismiss();
                }
            }
        });
    }
}

