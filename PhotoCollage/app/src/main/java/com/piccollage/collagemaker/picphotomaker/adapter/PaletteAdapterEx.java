package com.piccollage.collagemaker.picphotomaker.adapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Palette;
import com.piccollage.collagemaker.picphotomaker.viewholder.PaletteChildViewHolder;
import com.piccollage.collagemaker.picphotomaker.viewholder.PaletteViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class PaletteAdapterEx extends ExpandableRecyclerViewAdapter<PaletteViewHolder, PaletteChildViewHolder> {
    private IPickPalette iPickPalette;
    private List<? extends ExpandableGroup> mGroups;

    public interface IPickPalette {
        void pick(Palette palette, String title);

        void pickMore();
    }

    public PaletteAdapterEx(List<? extends ExpandableGroup> groups) {
        super(groups);
        mGroups = groups;
    }

    public void setiPickPalette(IPickPalette iPickPalette) {
        this.iPickPalette = iPickPalette;
    }

    @Override
    public PaletteViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop_detail_pallete_parent, parent, false);
        return new PaletteViewHolder(view);
    }

    @Override
    public PaletteChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop_detail_pallete, parent, false);
        return new PaletteChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(PaletteChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Palette palette = (Palette) group.getItems().get(childIndex);
        holder.ivChose.setVisibility((palette.isPick()) ? View.VISIBLE : View.INVISIBLE);
        holder.setPalette(palette.getColor1(), palette.getColor2(), palette.getColor3(), palette.getColor4(), palette.getColor5(), palette.getName());
        holder.itemView.setOnClickListener(v -> {
            for (ExpandableGroup expandableGroup : getGroups()) {
                for (Object obj : expandableGroup.getItems()) {
                    Palette pla = (Palette) obj;
                    pla.setPick(false);
                }
            }
            palette.setPick(true);
            notifyDataSetChanged();
            iPickPalette.pick(palette, group.getTitle());
        });
    }

    @Override
    public void onBindGroupViewHolder(PaletteViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setTheme(group);
        if (!group.getTitle().equals(Constant.MORE)) {
            if (isGroupExpanded(flatPosition)) {
                holder.ivBack.setVisibility(View.VISIBLE);
                holder.iv1.setImageDrawable(new ColorDrawable(Color.WHITE));
                holder.iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv2.setImageDrawable(new ColorDrawable(Color.WHITE));
                holder.iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv3.setImageDrawable(new ColorDrawable(Color.WHITE));
                holder.iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv4.setImageDrawable(new ColorDrawable(Color.WHITE));
                holder.iv4.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv5.setImageDrawable(new ColorDrawable(Color.WHITE));
                holder.iv5.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.tvName.setVisibility(View.INVISIBLE);
            } else {
                holder.ivBack.setVisibility(View.INVISIBLE);
                Palette palette = (Palette) group.getItems().get(0);
                holder.iv1.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor1())));
                holder.iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv2.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor2())));
                holder.iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv3.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor3())));
                holder.iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv4.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor4())));
                holder.iv4.setScaleType(ImageView.ScaleType.CENTER_CROP);
                holder.iv5.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor5())));
                holder.iv5.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        } else {
            holder.ivMore.setVisibility(View.VISIBLE);
            holder.ivBack.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (!group.getTitle().equals(Constant.MORE)) {
                toggleGroup(holder.getAdapterPosition());
                if (isGroupExpanded(holder.getAdapterPosition())) {
                    holder.ivBack.setVisibility(View.VISIBLE);
                    holder.iv1.setImageDrawable(new ColorDrawable(Color.WHITE));
                    holder.iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv2.setImageDrawable(new ColorDrawable(Color.WHITE));
                    holder.iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv3.setImageDrawable(new ColorDrawable(Color.WHITE));
                    holder.iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv4.setImageDrawable(new ColorDrawable(Color.WHITE));
                    holder.iv4.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv5.setImageDrawable(new ColorDrawable(Color.WHITE));
                    holder.iv5.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.tvName.setVisibility(View.INVISIBLE);
                } else {
                    holder.ivBack.setVisibility(View.GONE);
                    Palette palette = (Palette) group.getItems().get(0);
                    holder.iv1.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor1())));
                    holder.iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv2.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor2())));
                    holder.iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv3.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor3())));
                    holder.iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv4.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor4())));
                    holder.iv4.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.iv5.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor5())));
                    holder.iv5.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    holder.tvName.setVisibility(View.VISIBLE);
                }
            } else {
                iPickPalette.pickMore();
            }
        });
    }

    // Expand only group user picking
    @Override
    public void onGroupExpanded(int positionStart, int itemCount) {
        if (itemCount > 0) {
            int groupIndex = expandableList.getUnflattenedPosition(positionStart).groupPos;
            notifyItemRangeInserted(positionStart, itemCount);
            for (ExpandableGroup grp : mGroups) {
                if (grp != mGroups.get(groupIndex)) {
                    if (isGroupExpanded(grp)) {
                        toggleGroup(grp);
                        notifyItemChanged(mGroups.indexOf(grp));
                    }
                }
            }
        }
    }
}
