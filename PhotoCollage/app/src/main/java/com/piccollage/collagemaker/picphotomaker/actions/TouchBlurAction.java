package com.piccollage.collagemaker.picphotomaker.actions;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;
import com.piccollage.collagemaker.picphotomaker.utils.uui.CustomMenuAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dauroi.com.imageprocessing.ImageProcessor;
import dauroi.com.imageprocessing.filter.ImageFilter;
import dauroi.com.imageprocessing.filter.processing.TouchBlurFilter;
import dauroi.photoeditor.config.ALog;
import dauroi.photoeditor.horizontalListView.widget.HListView;
import dauroi.photoeditor.listener.ApplyFilterListener;
import dauroi.photoeditor.model.ItemInfo;
import dauroi.photoeditor.utils.PhotoUtils;
import dauroi.photoeditor.utils.Utils;
import dauroi.photoeditor.view.TouchBlurView;
import dauroi.photoeditor.view.TouchBlurView.OnTouchBlurListener;

/**
 * Create from Administrator
 * Purpose: touch blur action
 * Des: thực hiện action làm mờ khi user chạm vào 1 điểm
 */
public class TouchBlurAction extends BlurAction implements OnTouchBlurListener {
    private static final String TAG = BlurAction.class.getSimpleName();
    private static final String[] BLUR_NAMES = {"10", "20", "30", "40", "50", "60", "70", "80", "90", "100"};

    private float[] mBlurRadius;
    private HListView mListView;
    private String[] mRadiusNames;
    private CustomMenuAdapter mMenuAdapter;
    private List<ItemInfo> mMenuItems;
    private int mCurrentPosition = 0;
    private TouchBlurFilter mTouchBlurFilter;
    private TouchBlurView mTouchBlurView;

    public TouchBlurAction(EditorActivity activity) {
        super(activity);
        this.activity.setDoneActionsClickListener(this);
    }

    @Override
    protected void onInit() {
        super.onInit();
        final float[] radius = {50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150};
        int size = radius.length;
        mBlurRadius = new float[size];
        for (int idx = 0; idx < size; idx++) {
            mBlurRadius[idx] = Utils.pxFromDp(Objects.requireNonNull(activity), radius[idx]);
        }
    }

    @Override
    public void saveInstanceState(Bundle bundle) {
        super.saveInstanceState(bundle);
        bundle.putFloatArray("dauroi.photoeditor.actions.TouchBlurAction.mBlurRadius", mBlurRadius);
        bundle.putStringArray("dauroi.photoeditor.actions.TouchBlurAction.mRadiusNames", mRadiusNames);
        bundle.putInt("dauroi.photoeditor.actions.TouchBlurAction.mCurrentPosition", mCurrentPosition);
        if (mTouchBlurFilter != null) {
            bundle.putFloat("dauroi.photoeditor.actions.TouchBlurAction.mTouchBlurFilter.mRadius",
                    mTouchBlurFilter.getRadius());
            bundle.putFloat("dauroi.photoeditor.actions.TouchBlurAction.mTouchBlurFilter.mBlurSize",
                    mTouchBlurFilter.getBlurSize());
            bundle.putFloatArray("dauroi.photoeditor.actions.TouchBlurAction.mTouchBlurFilter.mCenterPoint",
                    mTouchBlurFilter.getCenterPoint());
        }
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        super.restoreInstanceState(bundle);
        float[] blurRadius = bundle.getFloatArray("dauroi.photoeditor.actions.TouchBlurAction.mBlurRadius");
        if (blurRadius != null) {
            mBlurRadius = blurRadius;
        }

        String[] radiusNames = bundle.getStringArray("dauroi.photoeditor.actions.TouchBlurAction.mRadiusNames");
        if (radiusNames != null) {
            mRadiusNames = radiusNames;
        }

        mCurrentPosition = bundle.getInt("dauroi.photoeditor.actions.TouchBlurAction.mCurrentPosition",
                mCurrentPosition);
        float radius = bundle.getFloat("dauroi.photoeditor.actions.TouchBlurAction.mTouchBlurFilter.mRadius", -1);
        float blurSize = bundle.getFloat("dauroi.photoeditor.actions.TouchBlurAction.mTouchBlurFilter.mBlurSize", -1);
        float[] centerPoints = bundle
                .getFloatArray("dauroi.photoeditor.actions.TouchBlurAction.mTouchBlurFilter.mCenterPoint");
        if (centerPoints != null && radius > 0 && blurSize > 0) {
            mTouchBlurFilter = new TouchBlurFilter(centerPoints, radius, blurSize);
        }
    }

    @Override
    public void apply(final boolean finish) {
        if (!isAttached()) {
            return;
        }

        if (mTouchBlurFilter == null) {
            return;
        }

        ApplyFilterListener listener = new ApplyFilterListener() {

            @Override
            public void onFinishFiltering() {
                if (finish) {
                    done();
                }
            }

            @Override
            public Bitmap applyFilter() {
                final int[] size = activity.calculateThumbnailSize();
                final float ratio = activity.calculateScaleRatio();
                final float dx = (activity.getPhotoViewWidth() - size[0]) / 2.0f;
                final float dy = (activity.getPhotoViewHeight() - size[1]) / 2.0f;
                Bitmap result;
                final float[] center = mTouchBlurFilter.getCenterPoint();
                center[0] = (center[0] - dx) * ratio;
                center[1] = (center[1] - dy) * ratio;

                TouchBlurFilter filter = new TouchBlurFilter(center, mTouchBlurFilter.getRadius() * ratio,
                        mTouchBlurFilter.getBlurSize() * ratio);
                filter.setBitmap(mBlurredImage);

                result = ImageProcessor.getFiltratedBitmap(activity.getImage(), filter);

                mTouchBlurFilter.setRecycleBitmap(true);
                mTouchBlurFilter.destroy();
                mTouchBlurFilter = null;

                return result;
            }
        };

        ApplyFilterTask task = new ApplyFilterTask(activity, listener);
        task.execute();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.attachMaskView(null);
        activity.applyFilter(new ImageFilter());
    }

    @SuppressLint("InflateParams")
    @Override
    public View inflateMenuView() {
        LayoutInflater inflater = LayoutInflater.from(activity);
        mRootActionView = inflater.inflate(R.layout.photo_editor_action_touch_blur, null);
        mListView = mRootActionView.findViewById(R.id.bottomListView);
        // create menus
        mRadiusNames = BLUR_NAMES;
        mMenuItems = new ArrayList<>();
        for (String mRadiusName : mRadiusNames) {
            ItemInfo item = new ItemInfo();
            item.setTitle(mRadiusName);
            item.setThumbnail("drawable://" + R.drawable.ic_touch_blur);
            item.setSelectedThumbnail("drawable://" + R.drawable.ic_touch_blur_selected);
            mMenuItems.add(item);
        }

        mMenuAdapter = new CustomMenuAdapter(activity, mMenuItems, true);
        mListView.setAdapter(mMenuAdapter);
        mListView.setOnItemClickListener(
                (parent, view, position, id) -> {
                    if (mCurrentPosition != position) {
                        selectItem(position);
                    }
                    onClicked();
                });
        mTouchBlurView = new TouchBlurView(activity);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        mTouchBlurView.setLayoutParams(params);
        mTouchBlurView.setTouchBlurListener(this);

        mCurrentPosition = 0;

        return mRootActionView;
    }

    @Override
    public void attach() {
        super.attach();
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                recycleImages();
                long blurredTime = System.currentTimeMillis();
                mBlurredImage = PhotoUtils.blurImage(activity.getImage(), Utils.pxFromDp(Objects.requireNonNull(activity), FAST_BLUR_RADIUS_TOUCH_BLUR));
                ALog.d(TAG, "blurred time = " + (System.currentTimeMillis() - blurredTime));
                if (mTouchBlurFilter != null) {
                    TouchBlurFilter touchBlurFilter = new TouchBlurFilter(mTouchBlurFilter.getCenterPoint(),
                            mTouchBlurFilter.getRadius(), mTouchBlurFilter.getBlurSize());
                    mTouchBlurFilter.destroy();
                    mTouchBlurFilter = touchBlurFilter;
                } else {
                    float[] center = new float[]{(float) activity.getPhotoViewWidth() / 2,
                            (float) activity.getPhotoViewHeight() / 2};
                    mTouchBlurFilter = new TouchBlurFilter(center, mBlurRadius[0],
                            Utils.pxFromDp(activity, EXCLUDE_BLURRED_SIZE));

                }

                mTouchBlurFilter.setRecycleBitmap(false);
                mTouchBlurFilter.setBitmap(mBlurredImage);

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                activity.attachMaskView(mTouchBlurView);
                activity.applyFilter(mTouchBlurFilter);
                selectItem(mCurrentPosition);
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onActivityResume() {
        super.onActivityResume();
        if (isAttached()) {
            activity.attachMaskView(mTouchBlurView);
            activity.applyFilter(mTouchBlurFilter);
            selectItem(mCurrentPosition);
        }
    }

    private void selectItem(int position) {
        mMenuItems.get(mCurrentPosition).setSelected(false);
        if (mCurrentPosition < position) {
            if (position < mMenuItems.size() - 1) {
                mListView.smoothScrollToPosition(position + 1);
            } else {
                mListView.smoothScrollToPosition(position);
            }
        } else {
            if (position > 0) {
                mListView.smoothScrollToPosition(position - 1);
            } else {
                mListView.smoothScrollToPosition(position);
            }
        }
        mMenuItems.get(position).setSelected(true);
        mMenuAdapter.notifyDataSetChanged();
        // apply frame
        mTouchBlurFilter.setRadius(mBlurRadius[position]);
        activity.getImageProcessingView().requestRender();
        mCurrentPosition = position;
    }

    @Override
    public void onTouchBlur(float x, float y) {
        mTouchBlurFilter.setCenterPoint(new float[]{x, mTouchBlurView.getHeight() - y});
        activity.getImageProcessingView().requestRender();
    }

    @Override
    public String getActionName() {
        return "TouchBlurAction";
    }
}
