package com.piccollage.collagemaker.picphotomaker.utils;

import com.piccollage.collagemaker.picphotomaker.data.item.DataSpecShop;
import com.piccollage.collagemaker.picphotomaker.entity.FontItem;
import com.piccollage.collagemaker.picphotomaker.entity.FontTheme;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;

import java.util.ArrayList;

/**
 * Convert from object to another object
 */
public class ConvertObject {
    // item shop -> sticker theme
    public static StickerTheme eventItemShopToStickerTheme(ItemShop itemShop, String urlRoot) {
        StickerTheme stickerTheme = new StickerTheme(itemShop.getNameItem(), new ArrayList<>(), false, itemShop.getUrlFile(), itemShop.isPro());
        for (DataSpecShop data : itemShop.getPathItems()) {
            for (int i = data.getIdBegin(); i <= data.getIdEnd(); i++) {
                String url = urlRoot + data.getPathViewPerSticker() + i + data.getImgType();
                StickerItem sticker = new StickerItem(itemShop.getNameItem(), url);
                stickerTheme.getStickers().add(sticker);
            }
        }
        return stickerTheme;
    }

    // item shop -> font theme
    public static FontTheme eventItemShopToFontTheme(ItemShop itemShop) {
        FontTheme fontTheme = new FontTheme(itemShop.getNameItem(), new ArrayList<>(), false, itemShop.getUrlFile(), itemShop.isPro());
        for (DataSpecShop data : itemShop.getPathItems()) {
            String url = data.getUrlThumb();
            FontItem font = new FontItem(data.getTitle(), url, false, itemShop.getNameItem());
            fontTheme.getFontItems().add(font);
        }
        return fontTheme;
    }
}
