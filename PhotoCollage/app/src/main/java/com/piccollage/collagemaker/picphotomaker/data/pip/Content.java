package com.piccollage.collagemaker.picphotomaker.data.pip;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Content data for pip from api
 */
public class Content {
    private int id;
    private String urlPreview;
    private String imgForeground;
    private List<String> imgMask;
    private boolean isPro;
    private String zipFile;

    public int getId() {
        return id;
    }

    public String getUrlPreview() {
        return urlPreview;
    }

    public String getImgForeground() {
        return imgForeground;
    }

    public void setUrlPreview(String urlPreview) {
        this.urlPreview = urlPreview;
    }

    public void setImgForeground(String imgForeground) {
        this.imgForeground = imgForeground;
    }

    public void setImgMask(List<String> imgMask) {
        this.imgMask = imgMask;
    }

    public List<String> getImgMask() {
        return imgMask;
    }

    public boolean isPro() {
        return isPro;
    }

    public void setZipFile(String zipFile) {
        this.zipFile = zipFile;
    }

    public String getZipFile() {
        return zipFile;
    }

    @NonNull
    @Override
    public String toString() {
        return getUrlPreview() + " " + imgForeground + " " + imgMask.get(0) + " " + isPro + " " + zipFile;
    }
}
