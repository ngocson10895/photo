package com.piccollage.collagemaker.picphotomaker.utils;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Date time Utils
 */
@SuppressLint("SimpleDateFormat")
public class DateTimeUtils {

    private static final String DATE_TIME_FORMAT_GMT = "yyyy-MM-dd HH:mm:ss 'GMT'";
    private static final String DATE_TIME_FORMAT_STANDARDIZED_UTC = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT_STANDARDIZED_UTC = "yyyy-MM-dd";
    private static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
    private static final String DATE_FORMAT_DD_MM_YYYY = "dd/MM/yyyy";

    // get current time zone
    public static String getCurrentDateTimeGMT() {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat(DATE_TIME_FORMAT_GMT, Locale.US);
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormatGmt.format(new Date());
    }

    // get current date time
    public static String getCurrentDateTime() {
        return new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC).format(new Date());
    }

    // get current date
    private static String getCurrentDate() {
        return new SimpleDateFormat(DATE_FORMAT_STANDARDIZED_UTC).format(new Date());
    }

    // convert string to date GMT
    public static Date convertGMTString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_GMT);
        try {
            return sdf.parse(dateString);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        return null;
    }

    // convert string to date UTC
    public static Date convertUTCString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
        try {
            return sdf.parse(dateString);
        } catch (ParseException pe) {
            sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
            try {
                return sdf.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static boolean isGMTString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_GMT);
        try {
            sdf.parse(dateString);
            return true;
        } catch (ParseException pe) {
            return false;
        }
    }

    public static boolean isUTCString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
        try {
            sdf.parse(dateString);
            return true;
        } catch (ParseException pe) {
            return false;
        }
    }

    public static boolean isUTCDateString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_STANDARDIZED_UTC);
        try {
            sdf.parse(dateString);
            return true;
        } catch (ParseException pe) {
            return false;
        }
    }

    public static String toUTCDateString(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY);
        try {
            Date date = sdf.parse(dateString);
            sdf = new SimpleDateFormat(DATE_FORMAT_STANDARDIZED_UTC);
            return sdf.format(date);
        } catch (ParseException pe) {
            sdf = new SimpleDateFormat(DATE_FORMAT_MM_DD_YYYY);
            try {
                Date date = sdf.parse(dateString);
                sdf = new SimpleDateFormat(DATE_FORMAT_STANDARDIZED_UTC);
                return sdf.format(date);
            } catch (ParseException pe2) {
                return getCurrentDate();
            }
        }
    }

    /*
     * 28 Nov 2012 03:08:45 GMT -> 2012-11-28 03:08:45
     */
    public static String toStandardizedUTCFromGMT(String dateStringInGMT) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_GMT);
        try {
            Date date = sdf.parse(dateStringInGMT);
            sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
            return sdf.format(date);
        } catch (ParseException e) {
            // e.printStackTrace();
            return null;
        }
    }

    public static int compare(String backupDbDateTimeStr, String localDbDateTimeStr) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
        Date backupDbDate;
        try {
            backupDbDate = sdf.parse(backupDbDateTimeStr);
        } catch (ParseException pe) {
            throw new Exception("backupDbDateTimeStr '" + backupDbDateTimeStr + "' Is NOT In the UTC Format '"
                    + DATE_TIME_FORMAT_STANDARDIZED_UTC + "'");
        }
        Date localDbDate;
        try {
            localDbDate = sdf.parse(localDbDateTimeStr);
        } catch (ParseException pe) {
            throw new Exception("localDbDateTimeStr '" + localDbDateTimeStr + "' Is NOT In the UTC Format '"
                    + DATE_TIME_FORMAT_STANDARDIZED_UTC + "'");
        }
        return backupDbDate.compareTo(localDbDate);
    }

    public static Date dateTimeStringInUtcFormat2Date(String dateTimeStringInUtcFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
        return sdf.parse(dateTimeStringInUtcFormat);
    }

    public static String getCurrentLocalDateTime() {
        String mydate = null;
        try {
            mydate = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        } catch (Exception ignored) {
        }
        return mydate;
    }

    public static String millis2LocalDateString(long dateTimeMillis) {
        return DateFormat.getDateTimeInstance().format(new Date(dateTimeMillis));
    }

    // convert date to UTC date time string
    public static String toUTCDateTimeString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
        return sdf.format(date);
    }

    public static String toUTCDateTimeString(long dateTimeMillis) {
        Date date = new Date(dateTimeMillis);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARDIZED_UTC);
        return sdf.format(date);
    }

    // convert long time to UTC date string
    public static String toUTCDateString(long dateTimeMillis) {
        Date date = new Date(dateTimeMillis);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY);
        return sdf.format(date);
    }

}
