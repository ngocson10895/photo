package com.piccollage.collagemaker.picphotomaker.ui.homeactivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.corebase.lg;
import com.piccollage.collagemaker.picphotomaker.BuildConfig;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.BannerAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.HomeShopAdsAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.MainFeatureAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.NewsAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.InterstitialAdUtilsListener;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.InterstitialAdsOpenUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyFirebaseConfig;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.ConstantAds;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.api.APIServiceWrapper;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.base.LocaleHelper;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.data.item.DataSpecShop;
import com.piccollage.collagemaker.picphotomaker.data.news.DataNews;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityMainBinding;
import com.piccollage.collagemaker.picphotomaker.entity.BannerOfHome;
import com.piccollage.collagemaker.picphotomaker.entity.FontTheme;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopFont;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopSticker;
import com.piccollage.collagemaker.picphotomaker.ui.DetailNewsActivity;
import com.piccollage.collagemaker.picphotomaker.ui.DialogExitApp;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.mycreativeactivity.MyCreativeActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer.PipStyleActivity;
import com.piccollage.collagemaker.picphotomaker.ui.quotesfeature.PickQuotesActivity;
import com.piccollage.collagemaker.picphotomaker.ui.settingactivity.DialogChangeLanguage;
import com.piccollage.collagemaker.picphotomaker.ui.settingactivity.DialogPolicy;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.PackageThemeActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.dialograting.C3638a;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.dialograting.RateDialog;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.ConvertObject;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.AlertDialogCustom;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BannerLayoutManager;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ObservableLinearManager;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import dauroi.photoeditor.utils.NetworkUtils;

/**
 * Create from Administrator
 * Purpose: Màn hình Home Activity
 * Des: màn hình home chứa shop sticker, news, ...
 */
public class HomeActivity extends BaseAppActivity implements View.OnClickListener {
    private static final String TAG = "HomeActivity";

    private ActivityMainBinding binding;
    private DrawerLayout drawerLayout;
    private BannerAdapter adapter;
    private NewsAdapter newsAdapter;
    private HomeShopAdsAdapter homeShopAdapter;
    private ArrayList<Object> recyclerItems;
    private ArrayList<DataNews> dataNews;
    private DialogLoading dialogLoading;
    private CountDownTimer countDownTimer; // count down thời gian sale
    private long mTimeLeftInMillis; // thời gian sale còn lại
    private AlertDialogCustom alertDialogCustom;
    private boolean timeRunning;
    private long mEndtime; // thời gian kết thúc
    private DialogExitApp dialogExitApp;
    private AlertDialog dialog;
    public Handler f5179ao = new Handler();
    private ApplicationViewModel viewModel;
    private int countResume = 0;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    // bắt đầu tính giờ
    private void startTime() {
        binding.content.tbHome.ivVip.setVisibility(View.INVISIBLE);
        // Vì không muốn user xóa cache file thì reset time nên phải lưu 1 file ra bộ nhớ ngoài => sử dụng file txt để lưu thời gian và lưu file cùng file với ảnh của user
        File file = new File(FileUtil.getParentFileString("Notes/Notes.txt"));
        if (file.exists()) {
            BufferedReader br;
            try {
                String st;
                br = new BufferedReader(new FileReader(file));
                while ((st = br.readLine()) != null) {
                    mTimeLeftInMillis = Long.parseLong(st.split("\t")[2]); // get time còn lại
                }
                mEndtime = System.currentTimeMillis() + mTimeLeftInMillis; // current time + mTimeLeftInMillis
                countDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTick(long millisUntilFinished) {
                        mTimeLeftInMillis = millisUntilFinished;
                        // convert time to HH/MM/SS
                        int hours = (int) ((mTimeLeftInMillis / 1000) / 3600);
                        int minutes = (int) ((mTimeLeftInMillis / 1000) % 3600) / 60;
                        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;
                        String timeLeftFormatted;
                        if (hours > 0) {
                            timeLeftFormatted = String.format(Locale.getDefault(),
                                    "%d:%02d:%02d", hours, minutes, seconds);
                        } else {
                            timeLeftFormatted = String.format(Locale.getDefault(),
                                    "00:%02d:%02d", minutes, seconds);
                        }
                        binding.content.tbHome.tvCountTimeTop.setText(timeLeftFormatted);
                    }

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onFinish() {
                        // cập nhật lại file txt
                        generateNoteOnSD(String.valueOf(0), String.valueOf(false), String.valueOf(mEndtime));
                        binding.content.tbHome.ivVipSale.setVisibility(View.GONE);
                        binding.content.tbHome.tvCountTimeTop.setVisibility(View.GONE);
                        if (!MyUtils.isAppPurchased()) {
                            binding.content.tbHome.ivVip.setVisibility(View.VISIBLE);
                        }
                    }
                };
                countDownTimer.start();
                timeRunning = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onHasNetwork() {
        if (alertDialogCustom != null && alertDialogCustom.isShowing()) {
            alertDialogCustom.dismiss();
        }
        loadAds();
        binding.content.tvMaterial.setVisibility(View.VISIBLE);
        binding.content.tvDiscover.setVisibility(View.VISIBLE);
        binding.content.moreMaterial.setVisibility(View.VISIBLE);
        binding.content.rvMaterial.setVisibility(View.VISIBLE);
        binding.content.rvNews.setVisibility(View.VISIBLE);
        if (!BuildConfig.DEBUG) {
            foreUpdate();
        }
    }

    @Override
    public void notHasNetwork() {
        binding.content.rvMaterial.setVisibility(View.GONE);
        binding.content.rvNews.setVisibility(View.GONE);
        binding.content.tvMaterial.setVisibility(View.GONE);
        binding.content.tvDiscover.setVisibility(View.GONE);
        binding.content.moreMaterial.setVisibility(View.GONE);
    }

    private void loadAds() {
        if (!MyUtils.isAppPurchased()) {
            binding.content.adsLayoutBanner.post(() -> Advertisement.showBannerAdHome(this, binding.content.adsLayoutBanner,
                    AdSize.MEDIUM_RECTANGLE, new BannerAdsUtils.BannerAdListener() {
                        @Override
                        public void onAdLoaded() {
                            binding.content.rlIntercept.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAdFailed() {

                        }
                    }, () -> binding.content.rlIntercept.setVisibility(View.VISIBLE)));

            binding.bottom.adsBottom.post(() -> Advertisement.showBannerAdOther(this, binding.bottom.adsBottom, false,
                    new BannerAdsUtils.BannerAdListener() {
                        @Override
                        public void onAdLoaded() {
                            binding.bottom.viewPro.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAdFailed() {

                        }
                    }));

            Advertisement.loadBannerAdExit(this);
        }
    }

    // check update phiên bản mới nhất
    private void foreUpdate() {
        checkForUpdate();
    }

    private void checkForUpdate() {
        long latestAppVersion = MyFirebaseConfig.getKeyVersionCode(); // mFirebaseRemoteConfig.getDouble(Constant.KEY_VERSION_CODE);
        if (latestAppVersion > getCurrentVersionCode()) {
            dialog = new AlertDialog.Builder(HomeActivity.this)
                    .setTitle(getString(R.string.new_version_available))
                    .setMessage(getString(R.string.pls_update_it))
                    .setPositiveButton(getString(R.string.update),
                            (dialog1, which) -> redirectStore())
                    .setNegativeButton(getString(R.string.cancel),
                            (dialog12, which) -> {
                                //check fore update or not
                                long forceVersion = MyFirebaseConfig.getKeyForceUpdate();
                                if (forceVersion > getCurrentVersionCode()) {
                                    finish();
                                } else {
                                    dialog.dismiss();
                                }
                            }).create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            if (!isFinishing()) {
                dialog.show();
            }
        } else {
            lg.logs(TAG, "checkForUpdate: this app is already up to date");
        }
    }

    private int getCurrentVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    private void redirectStore() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_APP));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (countResume == 0) {
            Advertisement.loadInterstitialAdInApp(this);
        }
        countResume++;

        InterstitialAdsOpenUtils.getInstances().showAdIfNeeded(this, new InterstitialAdUtilsListener() {
            @Override
            public void showLoading() {
                lg.logs("MainActivity", "showLoading...");
                if (dialogLoading != null) {
                    dialogLoading.show();
                }
            }

            @Override
            public void hideLoading() {
                lg.logs("MainActivity", "hideLoading...");
                if (dialogLoading != null) {
                    dialogLoading.dismiss();
                }
            }

            @Override
            public void hideAds() {
                if (dialogLoading != null) {
                    dialogLoading.dismiss();
                }
            }
        });

        // cập nhập file txt
        File file1 = new File(FileUtil.getParentFileString("Notes/Notes.txt"));
        if (file1.exists()) {
            BufferedReader br;
            try {
                String st;
                br = new BufferedReader(new FileReader(file1));
                while ((st = br.readLine()) != null) {
                    timeRunning = Boolean.parseBoolean(st.split("\t")[0]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // nếu time running = true
        if (timeRunning) {
            File file = new File(FileUtil.getParentFileString("Notes/Notes.txt"));
            if (file.exists()) {
                BufferedReader br;
                try {
                    String st;
                    br = new BufferedReader(new FileReader(file));
                    while ((st = br.readLine()) != null) {
                        mEndtime = Long.parseLong(st.split("\t")[1]);
                    }
                    mTimeLeftInMillis = mEndtime - System.currentTimeMillis(); // time left = thời gian kết thúc - thời gian hiện tại
                    generateNoteOnSD(String.valueOf(mTimeLeftInMillis), String.valueOf(true), String.valueOf(mEndtime));
                    startTime();
                    if (mTimeLeftInMillis < 0) {
                        mTimeLeftInMillis = 0; // nếu time left < 0 thì sẽ đặt lại = 0
                        timeRunning = false; // time running = false
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            generateNoteOnSD(String.valueOf(Constant.START_TIME_IN_MILLIS), String.valueOf(timeRunning), String.valueOf(System.currentTimeMillis() + Constant.START_TIME_IN_MILLIS));
            startTime();
        }
    }

    // khi activity destroy nhớ phải clear api và cache ads
    @Override
    protected void onDestroy() {
        super.onDestroy();
        APIServiceWrapper.clearTag(APIServiceWrapper.HOME_API);
        APIServiceWrapper.clearTag(APIServiceWrapper.NEWS_API);
        APIServiceWrapper.clearTag(Constant.STICKER);
        APIServiceWrapper.clearTag(APIServiceWrapper.CHECKING);
        APIServiceWrapper.clearTag(APIServiceWrapper.PIP_API);
        ConstantAds.destroyAds();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        // Cập nhật file txt
        generateNoteOnSD(String.valueOf(mTimeLeftInMillis), String.valueOf(timeRunning), String.valueOf(mEndtime));

        SharedPreferences prefs = getSharedPreferences(Constant.TIMER_SHARE, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putLong(Constant.TIME_LEFT, mTimeLeftInMillis);
        editor.putBoolean(Constant.TIME_RUNNING, timeRunning);
        editor.putLong(Constant.TIME_END, mEndtime);
        editor.apply();

        if (countDownTimer != null) countDownTimer.cancel();
    }

    // 4/10/2020: Tạo file note trong thẻ nhớ SD. File note lưu trữ thời gian còn lại của sale
    public void generateNoteOnSD(String timeLeft, String timeRunning, String timeEnd) {
        try {
            File root = new File(FileUtil.getParentFileString(""), "Notes");
            if (!root.exists()) {
                if (root.mkdirs()) Log.d(TAG, "generateNoteOnSD: created");
            }
            File gpxfile = new File(root, "Notes.txt");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(timeRunning).append("\t").append(timeEnd).append("\t").append(timeLeft);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityMainBinding.inflate(getLayoutInflater());
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        try {
            MyFirebaseConfig.getInstance().fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.NAME);
        recyclerItems = new ArrayList<>();
        viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        binding.tvLanguage.setText(LocaleHelper.getLanguageCountryName(this));
        dialogExitApp = new DialogExitApp(this);

        // dialog cảnh báo khi không bật mạng
        alertDialogCustom = new AlertDialogCustom(this, SweetAlertDialog.ERROR_TYPE);
        alertDialogCustom.setUp();
        alertDialogCustom.setiAlertDialogListener(new AlertDialogCustom.IAlertDialogListener() {
            @Override
            public void okClick() {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }

            @Override
            public void cancelClick() {

            }
        });

        if (MyUtils.isAppPurchased()) {
            binding.bottom.viewPro.setVisibility(View.INVISIBLE);
        }

        binding.bottom.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        MyTrackingFireBase.trackingScreen(this, TAG);

        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);

        dialogLoading = DialogLoading.getInstance(this, DialogLoading.LOADING);

        if (MyFirebaseConfig.getKeyEventComming() != 0 || !NetworkUtils.checkNetworkAvailable(this)) {
            binding.content.adsLayoutBanner.setVisibility(View.GONE);
        } else {
            binding.content.adsLayoutBanner.setVisibility(View.VISIBLE);
        }

        mainFeature();
        menuLeftSide();

        if (sharedPrefs.get(Constant.IS_GOT_PURCHASE, Boolean.class)) {
            binding.content.rlIntercept.setVisibility(View.VISIBLE);
        }

        List<BannerOfHome> bannerOfHomeList = new ArrayList<>();
        if (sharedPrefs.get(Constant.IS_GOT_PURCHASE, Boolean.class)) {
            binding.content.tbHome.ivVip.setVisibility(View.GONE);
            binding.clUp.setVisibility(View.GONE);
            binding.content.tbHome.ivVipSale.setVisibility(View.GONE);
        }
        binding.content.rlIntercept.setIntercept(false);
        BannerLayoutManager bannerLayoutManager = new BannerLayoutManager(this, binding.content.rvBanners, 4, OrientationHelper.HORIZONTAL);
        binding.content.rvBanners.setLayoutManager(bannerLayoutManager);

        dataNews = new ArrayList<>();
        newsAdapter = new NewsAdapter(this, dataNews, position -> {
            Intent intent = new Intent(this, DetailNewsActivity.class);
            intent.putExtra(DetailNewsActivity.HTML, dataNews.get(position));
            startActivity(intent);
        });

        binding.content.rvNews.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.content.rvNews.addItemDecoration(new BetweenSpacesItemDecoration(0, 8));
        binding.content.rvNews.setAdapter(newsAdapter);

        bannerOfHomeList.add(new BannerOfHome("", ""));
        adapter = new BannerAdapter(bannerOfHomeList, this);
        adapter.setiReloadNetwork(() -> {
            Intent intent = new Intent(HomeActivity.this, StoreActivity.class);
            startActivity(intent);
        });
        binding.content.rvBanners.setAdapter(adapter);

        loadAPI();
    }

    public void buttonClick() {
        onChangeLanguageClicked();
        onRateClicked();
        onPolicyClicked();
        onTvUpClicked();
        onRateBlueClicked();
        onVipClicked();
        onManagePackageClicked();
        onUpClicked();
        onVipSaleClicked();
        onViewClicked();
    }

    private void loadAPI() {
        APIServiceWrapper.getInstance().loadAPIHome(new APIServiceWrapper.IHomeLoad() {
            @Override
            public void loadDone(ArrayList<BannerOfHome> list) {
                adapter.setListBannerOfHome(list);
            }

            @Override
            public void error() {

            }

            @Override
            public void getMaterial(String api) {
                if (api != null && !api.equals("")) {
                    APIServiceWrapper.getInstance().loadAPIEachStore(api, new ArrayList<>(), Constant.STICKER, new APIServiceWrapper.ILoadEachStoreDone() {
                        @Override
                        public void done(ArrayList<ItemShop> itemShops) {
                            ArrayList<StickerTheme> stickerThemes = new ArrayList<>();
                            for (ItemShop itemShop : itemShops) {
                                stickerThemes.add(ConvertObject.eventItemShopToStickerTheme(itemShop, itemShop.getUrlRoot()));
                            }
                            EventBus.getDefault().postSticky(new EventItemShopSticker(stickerThemes)); // load api sticker done thì send sang các màn hình khác
                            updateData(itemShops);
                        }

                        @Override
                        public void error() {
                            EventBus.getDefault().postSticky(new EventItemShopSticker(new ArrayList<>()));
                        }
                    }, HomeActivity.this);
                }
            }

            @Override
            public void getFont(String api) {
                if (api != null && !api.isEmpty()) {
                    APIServiceWrapper.getInstance().loadAPIEachStore(api, new ArrayList<>(), Constant.FONT,
                            new APIServiceWrapper.ILoadEachStoreDone() {
                                @Override
                                public void done(ArrayList<ItemShop> itemShops) {
                                    ArrayList<FontTheme> fontThemes = new ArrayList<>();
                                    for (ItemShop itemShop : itemShops) {
                                        fontThemes.add(ConvertObject.eventItemShopToFontTheme(itemShop));
                                    }
                                    EventBus.getDefault().postSticky(new EventItemShopFont(fontThemes)); // load api font done thì gửi sang các màn hình khác
                                }

                                @Override
                                public void error() {

                                }
                            }, HomeActivity.this);
                }
            }
        }, this);

        APIServiceWrapper.getInstance().loadAPINews(new APIServiceWrapper.ILoadNews() {
            @Override
            public void loadDone(ArrayList<DataNews> news) {
                newsAdapter.setNews(news);
                dataNews.clear();
                dataNews.addAll(news);
            }

            @Override
            public void loadError() {
            }
        }, this);
        APIServiceWrapper.getInstance().checking(this);
    }

    // cập nhật và ktra item sticker shop ở màn hình home
    private void updateData(ArrayList<ItemShop> itemShops) {
        viewModel.getAllData().observe(HomeActivity.this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (itemsDownloaded.isEmpty()) {
                    for (ItemShop itemShop : itemShops)
                        itemShop.setDownloaded(false);
                } else {
                    for (ItemShop itemShop : itemShops) {
                        for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                            if (itemDownloaded.getName().equals(itemShop.getNameItem()) && itemDownloaded.getTheme().equals(Constant.STICKER)) {
                                itemShop.setDownloaded(true);
                                break;
                            } else itemShop.setDownloaded(false);
                        }
                    }
                }
            }
        });
        ArrayList<ItemShop> items = new ArrayList<>();
        for (ItemShop i : itemShops) {
            items.add(i);
            if (itemShops.indexOf(i) == 10) break;
        }
        recyclerItems.addAll(items);
        insertAdsInMenuItems();
        homeShopAdapter.setmRecyclerViewItems(recyclerItems);
    }

    private void insertAdsInMenuItems() {
        if (recyclerItems.size() <= 0) {
            return;
        }

        int offset = 5;
        int index = 2;
        if (NativeAdAdapter.getNativeAds() != null) {
            for (UnifiedNativeAd ad : NativeAdAdapter.getNativeAds()) {
                if (index <= recyclerItems.size() - offset) {
                    recyclerItems.add(index, ad);
                    index = index + offset;
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else if (C3638a.m16910a(this, 1, Constant.SHARE_EMAIL, getString(R.string.app_name))) {
            m7708ar();
        } else {
            if (!isFinishing()) {
                dialogExitApp.show();
            }
        }
    }

    // load ads sau đó cho truy cập vào các tính năng
    private void loadAdsFeature(AppCompatActivity activity, Class toClass, String from) {
        if (checkDoubleClick() && new PermissionChecker(this).isPermissionOK()) {
            Advertisement.showInterstitialAdInApp(this, new InterstitialAdsPopupOthers.OnAdsListener() {
                @Override
                public void onAdsClose() {
                    Intent it = new Intent(activity, toClass);
                    it.putExtra(PickPhotoActivity.MODE, from);
                    startActivity(it);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    countResume = 0;
                }

                @Override
                public void showLoading() {
                    if (dialogLoading != null) dialogLoading.show();
                }

                @Override
                public void hideLoading() {
                    if (dialogLoading != null) dialogLoading.dismiss();
                }
            }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                @Override
                public void close() {
                    Intent it = new Intent(activity, toClass);
                    it.putExtra(PickPhotoActivity.MODE, from);
                    startActivity(it);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    countResume = 0;
                }

                @Override
                public void showLoading() {
                    if (dialogLoading != null) dialogLoading.show();
                }

                @Override
                public void hideLoading() {
                    if (dialogLoading != null) dialogLoading.dismiss();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_show_menu:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.iv1:
                loadAdsFeature(this, PickPhotoActivity.class, Constant.COLLAGE);
                break;
            case R.id.iv2:
                loadAdsFeature(this, PipStyleActivity.class, Constant.PIP);
                break;
            case R.id.cl_shop:
                if (checkDoubleClick()) {
                    MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.STORE);
                    Intent it5 = new Intent(this, StoreActivity.class);
                    startActivity(it5);
                    drawerLayout.closeDrawers();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
                break;
            default:
        }
    }

    /**
     * main Feature
     */
    private void mainFeature() {
        ImageView btnCollage = findViewById(R.id.iv1);
        btnCollage.setOnClickListener(this);

        ImageView btnPIP = findViewById(R.id.iv2);
        btnPIP.setOnClickListener(this);

        ArrayList<Icon> iconsMFeature = new ArrayList<>();
        InitData.dataIconMFeature(iconsMFeature);
        MainFeatureAdapter mainFeatureAdapter = new MainFeatureAdapter(this, iconsMFeature, this::usingFeature);
        binding.content.rvFeature.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.content.rvFeature.addItemDecoration(new BetweenSpacesItemDecoration(0, 24));
        binding.content.rvFeature.setAdapter(mainFeatureAdapter);

        homeShopAdapter = new HomeShopAdsAdapter(this, new ArrayList<>());
        homeShopAdapter.setiShopHomeListener(new HomeShopAdsAdapter.IShopHomeListener() {
            @Override
            public void pickItemShop(int pos) {
                if (homeShopAdapter.getmRecyclerViewItems().get(pos) instanceof ItemShop) {
                    ItemShop itemShop = (ItemShop) homeShopAdapter.getmRecyclerViewItems().get(pos);
                    Intent intent = new Intent(HomeActivity.this, DetailItemActivity.class);
                    intent.putExtra("Name", itemShop.getNameItem());
                    intent.putExtra("Zip", itemShop.getUrlFile());
                    intent.putExtra("Thumb", itemShop.getPathItems().get(0).getUrlThumb());
                    intent.putExtra("Preview", itemShop.getUrlPreview());
                    StringBuilder item = new StringBuilder();
                    for (DataSpecShop data : itemShop.getPathItems()) {
                        item.append(data.getTitle()).append("_");
                    }
                    intent.putExtra("NameItems", item.toString());
                    startActivity(intent);
                }
            }

            @Override
            public void pickItemShopPro(int pos) {
                if (homeShopAdapter.getmRecyclerViewItems().get(pos) instanceof ItemShop) {
                    ItemShop itemShop = (ItemShop) homeShopAdapter.getmRecyclerViewItems().get(pos);
                    Intent intent = new Intent(HomeActivity.this, DetailItemActivity.class);
                    intent.putExtra("Downloaded", 2);
                    intent.putExtra("Name", itemShop.getNameItem());
                    intent.putExtra("Zip", itemShop.getUrlFile());
                    intent.putExtra("Thumb", itemShop.getPathItems().get(0).getUrlThumb());
                    intent.putExtra("Preview", itemShop.getUrlPreview());
                    StringBuilder item = new StringBuilder();
                    for (DataSpecShop data : itemShop.getPathItems()) {
                        item.append(data.getTitle()).append("_");
                    }
                    intent.putExtra("NameItems", item.toString());
                    startActivity(intent);
                }
            }

            @Override
            public void itemDownloaded(int pos) {
                if (homeShopAdapter.getmRecyclerViewItems().get(pos) instanceof ItemShop) {
                    ItemShop itemShop = (ItemShop) homeShopAdapter.getmRecyclerViewItems().get(pos);
                    Intent intent = new Intent(HomeActivity.this, DetailItemActivity.class);
                    intent.putExtra("Downloaded", 1);
                    intent.putExtra("Name", itemShop.getNameItem());
                    intent.putExtra("Zip", itemShop.getUrlFile());
                    intent.putExtra("Thumb", itemShop.getPathItems().get(0).getUrlThumb());
                    intent.putExtra("Preview", itemShop.getUrlPreview());
                    StringBuilder item = new StringBuilder();
                    for (DataSpecShop data : itemShop.getPathItems()) {
                        item.append(data.getTitle()).append("_");
                    }
                    intent.putExtra("NameItems", item.toString());
                    startActivity(intent);
                }
            }
        });

        binding.content.rvMaterial.setLayoutManager(new ObservableLinearManager(this, LinearLayoutManager.VERTICAL, false));
        binding.content.rvMaterial.addItemDecoration(new BetweenSpacesItemDecoration(16, 0));
        binding.content.rvMaterial.setAdapter(homeShopAdapter);

        viewModel.getAllData().observe(HomeActivity.this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (itemsDownloaded.isEmpty()) {
                    for (Object obj : homeShopAdapter.getmRecyclerViewItems()) {
                        if (obj instanceof ItemShop) {
                            ItemShop itemShop = (ItemShop) obj;
                            itemShop.setDownloaded(false);
                        }
                    }
                } else {
                    for (Object obj : homeShopAdapter.getmRecyclerViewItems()) {
                        if (obj instanceof ItemShop) {
                            ItemShop itemShop = (ItemShop) obj;
                            for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                                if (itemDownloaded.getName().equals(itemShop.getNameItem()) && itemDownloaded.getTheme().equals(Constant.STICKER)) {
                                    itemShop.setDownloaded(true);
                                    break;
                                } else itemShop.setDownloaded(false);
                            }
                        }
                    }
                }
                homeShopAdapter.notifyDataSetChanged();
            }
        });
    }

    private void usingFeature(int position) {
        switch (position) {
            case 0:
                loadAdsFeature(this, PickPhotoActivity.class, Constant.EDITOR);
                break;
            case 1:
                loadAdsFeature(this, PickPhotoActivity.class, Constant.SCRAPBOOK);
                break;
            case 2:
                loadAdsFeature(this, PickQuotesActivity.class, Constant.QUOTE);
                break;
            case 3:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.MY_CREATIVE);
                Intent it10 = new Intent(this, MyCreativeActivity.class);
                startActivity(it10);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case 4:
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.STORE);
                Intent it4 = new Intent(this, StoreActivity.class);
                startActivity(it4);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            default:
        }
    }

    /**
     * menu left side
     */
    @SuppressLint("SetTextI18n")
    private void menuLeftSide() {
        ImageView btnShowMenu = findViewById(R.id.iv_show_menu);
        btnShowMenu.setOnClickListener(this);

        ConstraintLayout shop = findViewById(R.id.cl_shop);
        shop.setOnClickListener(this);

        drawerLayout = findViewById(R.id.drawer_layout);

        binding.tvVersionName.setText(getResources().getString(R.string.version) + " " + BuildConfig.VERSION_NAME);
    }

    public void onRateClicked() {
        binding.clRate.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.RATE_APP);
                m8210l_rateDialog(this);
            }
        });
    }

    public void onTvUpClicked() {
        binding.tvUpgrade.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.UP_TO_PRO);
                Intent it7 = new Intent(this, BuySubActivity.class);
                startActivity(it7);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                if (drawerLayout != null) {
                    drawerLayout.closeDrawers();
                }
            }
        });
    }

    public void onVipClicked() {
        binding.content.tbHome.ivVip.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.UP_TO_PRO_TOOLBAR);
                Intent it7 = new Intent(this, BuySubActivity.class);
                startActivity(it7);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                if (drawerLayout != null) {
                    drawerLayout.closeDrawers();
                }
            }
        });
    }

    public void onVipSaleClicked() {
        binding.content.tbHome.ivVipSale.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.UP_TO_PRO_SALE_TOOLBAR);
                Intent it7 = new Intent(this, BuySubActivity.class);
                startActivity(it7);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
    }

    public void onRateBlueClicked() {
        binding.content.rate.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.RATE_APP);
                m8210l_rateDialog(this);
            }
        });
    }

    //new rating app
    public void m8210l_rateDialog(Context context) {
        if (context != null) {
            RateDialog rateDialog = new RateDialog(this);
            rateDialog.setString(Constant.SHARE_EMAIL);
            if (!isFinishing()) {
                rateDialog.show();
                rateDialog.setCancelable(true);
                rateDialog.setCanceledOnTouchOutside(true);
            }
        }
    }

    public Runnable f5182ar = new Runnable() {
        public void run() {
            if (C3638a.m16908a()) {
                C3638a.m16907a(false);
                f5179ao.removeCallbacks(f5182ar);
                f5182ar = null;
                f5179ao = null;
                finish();
                return;
            }
            f5179ao.postDelayed(this, 100);
        }
    };

    private void m7708ar() {
        this.f5179ao.postDelayed(this.f5182ar, 100);
    }

    public void onViewClicked() {
        binding.content.moreMaterial.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                Intent intent = new Intent(this, StoreActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
    }

    public void onChangeLanguageClicked() {
        binding.clChangeLanguage.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                DialogChangeLanguage dialog = new DialogChangeLanguage(this);
                dialog.show();
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
            }
        });
    }

    public void onManagePackageClicked() {
        binding.clManagePackaged.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                Intent intent = new Intent(this, PackageThemeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
    }

    public void onPolicyClicked() {
        binding.clPrivacy.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                DialogPolicy dialogPolicy = new DialogPolicy(this);
                dialogPolicy.show();
            }
        });
    }

    public void onUpClicked() {
        binding.clUp.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Home.UP_TO_PRO);
                Intent it7 = new Intent(this, BuySubActivity.class);
                startActivity(it7);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                if (drawerLayout != null) {
                    drawerLayout.closeDrawers();
                }
            }
        });
    }
}
