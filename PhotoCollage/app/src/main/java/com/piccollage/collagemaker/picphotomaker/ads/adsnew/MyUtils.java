package com.piccollage.collagemaker.picphotomaker.ads.adsnew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.corebase.lg;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

public class MyUtils {
    private static ProgressDialog progressDialog;

    public static void showProgress(Context context, String message) {
        try {
            dismissCurrentProgress();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        } catch (Exception e) {
            lg.logs("Utils showProgress failed ", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void showProgress(Context context) {
        showProgress(context, context.getString(R.string.loading_ads));
    }

    public static void dismissCurrentProgress() {
        if (progressDialog != null) {
            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            progressDialog = null;
        }
    }

    public static boolean isAppPurchased() {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(MyApplication.getInstance(), Constant.NAME_SHARE);
        return sharedPrefs.get(Constant.IS_GOT_PURCHASE, Boolean.class);
    }

    public static String getStringPref(Context c, String name, String defaultValue) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        return sharedPrefs.getString(name, defaultValue);

    }

    public static void putStringPref(Context c, String name, String values) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(name, values);
        editor.apply();
    }

    public static long getLongPref(Context c, String name, long defaultValue) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        return sharedPrefs.getLong(name, defaultValue);

    }

    public static void putLongPref(Context c, String name, long values) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putLong(name, values);
        editor.apply();
    }

    public static int getIntPref(Context c, String name, int defaultValue) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        return sharedPrefs.getInt(name, defaultValue);

    }

    public static void putIntPref(Context c, String name, int values) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(Constant.NAME_SHARE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt(name, values);
        editor.apply();
    }

    public static int convertLongTimeMillis(long time) {
        int timeConvert = (int) time / 1000;
        lg.logs("TimeUtils " + timeConvert);
        return timeConvert;
    }
}
