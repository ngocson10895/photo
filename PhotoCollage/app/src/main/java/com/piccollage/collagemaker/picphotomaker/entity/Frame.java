package com.piccollage.collagemaker.picphotomaker.entity;

/**
 * Frame
 */
public class Frame{
    private String path;
    private int type;
    private boolean isPick;

    public Frame(String path, int type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public int getType() {
        return type;
    }

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }
}
