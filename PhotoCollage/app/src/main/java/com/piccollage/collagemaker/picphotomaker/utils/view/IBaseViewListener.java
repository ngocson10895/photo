package com.piccollage.collagemaker.picphotomaker.utils.view;

public interface IBaseViewListener {
    void doneAction();

    void backAction();
}
