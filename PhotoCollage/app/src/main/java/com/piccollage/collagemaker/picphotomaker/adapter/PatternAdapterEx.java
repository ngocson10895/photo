package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Pattern;
import com.piccollage.collagemaker.picphotomaker.viewholder.PatternChildViewHolder;
import com.piccollage.collagemaker.picphotomaker.viewholder.PatternViewHolder;
import com.piccollage.collagemaker.picphotomaker.viewholder.Patterns;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class PatternAdapterEx extends ExpandableRecyclerViewAdapter<PatternViewHolder, PatternChildViewHolder> {
    private Context context;
    private IPickPattern iPickPattern;
    private List<? extends ExpandableGroup> mGroups;

    public interface IPickPattern {
        void pick(Pattern pattern, String title);

        void pickMore();
    }

    public PatternAdapterEx(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.context = context;
        mGroups = groups;
    }

    public void setiPickPattern(IPickPattern iPickPattern) {
        this.iPickPattern = iPickPattern;
    }

    @Override
    public PatternViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pattern_parent, parent, false);
        return new PatternViewHolder(view);
    }

    @Override
    public PatternChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pattern, parent, false);
        return new PatternChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(PatternChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Pattern pattern = (Pattern) group.getItems().get(childIndex);
        holder.ivChose.setVisibility((pattern.isPick()) ? View.VISIBLE : View.INVISIBLE);
        holder.setPattern(pattern.getPath(), context, pattern.getName(), group.getTitle());
        holder.itemView.setOnClickListener(v -> {
            for (ExpandableGroup expandableGroup : getGroups()){
                for (Object obj : expandableGroup.getItems()){
                    Pattern pat = (Pattern) obj;
                    pat.setPick(false);
                }
            }
            pattern.setPick(true);
            notifyDataSetChanged();
            iPickPattern.pick(pattern, group.getTitle());
        });
    }

    @Override
    public void onBindGroupViewHolder(PatternViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setTheme(group, context);
        if (!group.getTitle().equals(Constant.MORE)) {
            if (isGroupExpanded(flatPosition)) {
                holder.ivBack.setVisibility(View.VISIBLE);
            } else {
                if (group.getTitle().equals(Constant.DEFAULT)) {
                    holder.ivBack.setVisibility(View.GONE);
                    holder.imageView.setImageBitmap(ImageDecoder.
                            getBitmapFromAssets("background/" + ((Patterns) group).getItems().get(0).getPath(), context));
                    holder.tvTitle.setVisibility(View.VISIBLE);
                } else {
                    holder.ivBack.setVisibility(View.GONE);
                    Glide.with(context).load(((Patterns) group).getItems().get(0).getPath()).centerCrop().
                            error(R.drawable.sticker_default).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.imageView);
                    holder.tvTitle.setVisibility(View.VISIBLE);
                }
            }
        } else {
            Glide.with(context).load(R.drawable.ic_itemnone).centerCrop().into(holder.imageView);
            holder.ivBack.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (group.getTitle().equals(Constant.MORE)) {
                iPickPattern.pickMore();
            } else {
                toggleGroup(holder.getAdapterPosition());
                if (isGroupExpanded(holder.getAdapterPosition())) {
                    holder.ivBack.setVisibility(View.VISIBLE);
                } else {
                    if (group.getTitle().equals(Constant.DEFAULT)) {
                        holder.ivBack.setVisibility(View.GONE);
                        holder.imageView.setImageBitmap(ImageDecoder.
                                getBitmapFromAssets("background/" + ((Patterns) group).getItems().get(0).getPath(), context));
                        holder.tvTitle.setVisibility(View.VISIBLE);
                    } else {
                        holder.ivBack.setVisibility(View.GONE);
                        Glide.with(context).load(((Patterns) group).getItems().get(0).getPath()).centerCrop().
                                error(R.drawable.sticker_default).into(holder.imageView);
                        holder.tvTitle.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    // Expand only group user picking
    @Override
    public void onGroupExpanded(int positionStart, int itemCount) {
        if (itemCount > 0) {
            int groupIndex = expandableList.getUnflattenedPosition(positionStart).groupPos;
            notifyItemRangeInserted(positionStart, itemCount);
            for (ExpandableGroup grp : mGroups) {
                if (grp != mGroups.get(groupIndex)) {
                    if (isGroupExpanded(grp)) {
                        toggleGroup(grp);
                        notifyItemChanged(mGroups.indexOf(grp));
                    }
                }
            }
        }
    }
}
