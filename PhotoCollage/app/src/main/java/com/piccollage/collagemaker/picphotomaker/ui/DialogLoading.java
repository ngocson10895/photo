package com.piccollage.collagemaker.picphotomaker.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogLoadingBinding;

/**
 * Create from Administrator
 * Purpose: dialog loading
 * Des: hiển thị khi user sử dụng các feature cần phải đợi
 */
public class DialogLoading extends BaseDialog {
    public static final String CREATE = "create";
    public static final String LOADING = "loading";
    public static final String PREPARING = "preparing";

    private String type;
    private Context context;
    private DialogLoadingBinding binding;

    @SuppressLint("StaticFieldLeak")
    public static DialogLoading dialog;

    public static DialogLoading getInstance(@NonNull Context context, String type) {
        DialogLoading dialogLoading;
        synchronized (DialogLoading.class) {
            if (dialog == null) dialog = new DialogLoading(context, type);
            dialogLoading = dialog;
        }
        return dialogLoading;
    }

    public DialogLoading(@NonNull Context context, String type) {
        super(context);
        this.type = type;
        this.context = context;
    }

    @Override
    public void beforeSetContentView() {
        binding = DialogLoadingBinding.inflate(getLayoutInflater());
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    // có nhiều kiểu loading cho nhiều trường hợp
    @Override
    public void initData() {
        if (type != null) {
            switch (type) {
                case LOADING:
                    binding.title.setText(context.getString(R.string.loading_ads));
                    binding.animation.setAnimation(R.raw.loading);
                    break;
                case CREATE:
                    binding.title.setText(context.getResources().getString(R.string.waiting));
                    binding.animation.setAnimation(R.raw.create);
                    break;
                case PREPARING:
                    binding.title.setText(context.getString(R.string.preparing));
                    binding.animation.setAnimation(R.raw.loading);
                    break;
            }
        }
    }

    /**
     * Custom Dialog
     */
    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = ViewGroup.LayoutParams.WRAP_CONTENT;
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
