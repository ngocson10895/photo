package com.piccollage.collagemaker.picphotomaker.ui.pipactivity.layout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import androidx.appcompat.widget.AppCompatImageView;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;

import dauroi.photoeditor.utils.PhotoUtils;
import dauroi.photoeditor.view.MultiTouchHandler;

/**
 * Create from Administrator
 * Purpose: photo item layout
 * Des: chỉnh sửa và đặt vị trí ảnh của user theo ảnh mẫu
 */
@SuppressLint("ViewConstructor")
public class PhotoItemLayout extends AppCompatImageView {
    private Bitmap mImage, mMaskImage;
    private Photo photoPicked;
    private Paint mPaint;
    private final GestureDetector mGestureDetector;
    private Matrix mImageMatrix, mScaleMatrix, mMaskMatrix, mScaleMaskMatrix;
    private MultiTouchHandler mTouchHandler;
    private boolean mEnableTouch = true, click;
    private float mViewWidth, mViewHeight;

    private OnImageClickListener onImageClickListener;

    public interface OnImageClickListener {
        void onLongClickImage(PhotoItemLayout view);

        void clickImage(int index);
    }

    public PhotoItemLayout(Context context, Photo photo, int index, String type) {
        super(context);
        photoPicked = photo;
        mImage = ImageDecoder.decodeFileToBitmap(photoPicked.getPathPhoto());
        if (type.equals("default")) {
            mMaskImage = ImageDecoder.getBitmapFromAssets("pip/" + photo.maskPath, getContext());
        } else
            mMaskImage = PhotoUtils.decodePNGImage(context, photo.maskPath);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG); // to paint a view
        mPaint.setFilterBitmap(true);

        setScaleType(ScaleType.MATRIX);
        setLayerType(LAYER_TYPE_SOFTWARE, mPaint);
        mImageMatrix = new Matrix();
        mScaleMatrix = new Matrix();
        mMaskMatrix = new Matrix();
        mScaleMaskMatrix = new Matrix();

        mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            public void onLongPress(MotionEvent e) {
                if (!photoPicked.getPathPhoto().equals("")) {
                    if (onImageClickListener != null) {
                        onImageClickListener.onLongClickImage(PhotoItemLayout.this);
                    }
                }
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (!photoPicked.getPathPhoto().equals("")) {
                    onImageClickListener.clickImage(index);
                }
                return true;
            }
        });
    }

    public void changeImage(String photoPath) {
        setImage(ImageDecoder.decodeFileToBitmap(photoPath));
        resetImageMatrix();
    }

    public void setOnImageClickListener(OnImageClickListener onImageClickListener) {
        this.onImageClickListener = onImageClickListener;
    }

    public void build(final float viewWidth, final float viewHeight, final float scale) {
        mViewWidth = viewWidth;
        mViewHeight = viewHeight;
        if (mImage != null) {
            mImageMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(viewWidth, viewHeight, mImage.getWidth(), mImage.getHeight()));
            mScaleMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(scale * viewWidth, scale * viewHeight, mImage.getWidth(), mImage.getHeight()));
        }
        //mask
        if (mMaskImage != null) {
            mMaskMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(viewWidth, viewHeight, mMaskImage.getWidth(), mMaskImage.getHeight()));
            mScaleMaskMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(scale * viewWidth, scale * viewHeight, mMaskImage.getWidth(), mMaskImage.getHeight()));
        }

        mTouchHandler = new MultiTouchHandler();
        mTouchHandler.setMatrices(mImageMatrix, mScaleMatrix);
        mTouchHandler.setScale(scale);
        mTouchHandler.setEnableRotation(true);
        invalidate();
    }

    public void swapImage(PhotoItemLayout view) {
        Bitmap temp = view.getImage();
        view.setImage(mImage);
        mImage = temp;

        String tmpPath = view.getPhotoItem().getPathPhoto();
        view.getPhotoItem().setPathPhoto(photoPicked.getPathPhoto());
        photoPicked.setPathPhoto(tmpPath);
        resetImageMatrix();
        view.resetImageMatrix();
    }

    public Photo getPhotoItem() {
        return photoPicked;
    }

    public void resetImageMatrix() {
        mImageMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(mViewWidth, mViewHeight, mImage.getWidth(), mImage.getHeight()));
        mTouchHandler.setMatrices(mImageMatrix, mScaleMatrix);
        postInvalidate();
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap image) {
        mImage = image;
    }

    public Bitmap getMaskImage() {
        return mMaskImage;
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mImage, mImageMatrix, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mMaskImage, mMaskMatrix, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!mEnableTouch) {
            if (click) {
                mGestureDetector.onTouchEvent(event);
                return true;
            } else
                return super.onTouchEvent(event);
        } else {
            mGestureDetector.onTouchEvent(event);
            mTouchHandler.touch(event);
            mImageMatrix.set(mTouchHandler.getMatrix());
            mScaleMatrix.set(mTouchHandler.getScaleMatrix());
            postInvalidate();
            return true;
        }
    }

    public void setmEnableTouch(boolean mEnableTouch) {
        this.mEnableTouch = mEnableTouch;
    }

    public void setClick(boolean click) {
        this.click = click;
    }
}
