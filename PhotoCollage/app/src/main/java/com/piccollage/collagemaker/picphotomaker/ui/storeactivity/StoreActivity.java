package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.PagerAdapterShop;
import com.piccollage.collagemaker.picphotomaker.api.APIServiceWrapper;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentStoreBinding;
import com.piccollage.collagemaker.picphotomaker.entity.FontItem;
import com.piccollage.collagemaker.picphotomaker.networking.NetworkChangeReceiver;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.AlertDialogCustom;

import java.util.List;
import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Create from Administrator
 * Purpose: màn hình store
 * Des: hiển thị các item user có thể download về để sử dụng
 */
public class StoreActivity extends BaseAppActivity {
    public static final String VALUE = "value";

    private DialogLoading dialog;
    private Typeface font;
    private String value;
    private PagerAdapterShop adapterShop;
    private NetworkChangeReceiver networkChangeReceiver;
    private AlertDialogCustom dialogCustom;
    private FragmentStoreBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = FragmentStoreBinding.inflate(getLayoutInflater());
    }

    @Override
    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        dialogCustom = new AlertDialogCustom(this, SweetAlertDialog.ERROR_TYPE);
        dialogCustom.setUpFeature();
        dialogCustom.setiAlertDialogListener(new AlertDialogCustom.IAlertDialogListener() {
            @Override
            public void okClick() {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }

            @Override
            public void cancelClick() {
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        dialog = new DialogLoading(this, DialogLoading.CREATE);
        dialog.show();
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabBar));
        binding.tabBar.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager));

        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        networkChangeReceiver.setListener(new NetworkChangeReceiver.NetworkStateReceiverListener() {
            @Override
            public void onNetworkAvailable() {
                dialogCustom.dismiss();
                APIServiceWrapper.getInstance().loadAPIStore(new APIServiceWrapper.ILoadStoreHomeDone() {
                    @Override
                    public void done(List<String> themes, List<String> urls) {
                        adapterShop = new PagerAdapterShop(getSupportFragmentManager(), themes, urls);
                        binding.viewPager.setAdapter(adapterShop);
                        for (int i = 0; i < binding.tabBar.getTabCount(); i++) {
                            @SuppressLint("InflateParams")
                            TextView tv = (TextView) LayoutInflater.from(StoreActivity.this).inflate(R.layout.custom_tab, null);
                            tv.setText(adapterShop.getPageTitle(i));
                            tv.setTypeface(font);
                            Objects.requireNonNull(binding.tabBar.getTabAt(i)).setCustomView(tv);
                        }
                        adapterShop.notifyDataSetChanged();
                        for (String title : themes) {
                            if (title.equals(value))
                                binding.viewPager.setCurrentItem(themes.indexOf(title));
                        }
                        binding.tabBar.setupWithViewPager(binding.viewPager);
                        binding.viewPager.setOffscreenPageLimit(themes.size());
                        binding.clNotInternet.setVisibility(View.GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void error() {
                        dialog.dismiss();
                    }
                }, StoreActivity.this);
            }

            @Override
            public void onNetworkUnavailable() {
                dialog.dismiss();
                binding.clNotInternet.setVisibility(View.VISIBLE);
                dialogCustom.show();
            }
        });

        onViewClicked();
        onBackClicked();
    }

    @SuppressLint("InflateParams")
    public void initData() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        if (getIntent() != null) {
            value = getIntent().getStringExtra(VALUE);
        }
        List<FontItem> initData = TextUtils.listAssetFiles(Constant.NAME_FONT_ASSET, this);
        for (FontItem item : initData) {
            if (item.getFontName().equals(Constant.NAME_FONT_DEFAULT)) {
                font = TextUtils.loadTypeface(this, item.getFontPath());
            }
        }
    }

    public void onViewClicked() {
        binding.btnPurchase.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Store.GO_TO_MANAGE);
                Intent intent = new Intent(this, PackageThemeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
    }

    public void onBackClicked() {
        binding.ivBack.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Store.BACK);
                this.finish();
                this.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        APIServiceWrapper.clearTag(APIServiceWrapper.STORE_API);
        unregisterReceiver(networkChangeReceiver); // unregister receiver khi destroy
    }
}
