package com.piccollage.collagemaker.picphotomaker.ads.adsnew;

import android.content.Context;

import com.facebook.ads.DisplayAdController;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.corebase.lg;
import com.google.android.gms.corebase.logger;
import com.google.android.gms.corebase.ut;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.LogUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class InterstitialAdsOpenUtils {

    //    public long MIN_TIME_TO_SHOW_INTERSTITIAL_AD = 0 * 60 * 1000;//TODO test time = 0
    public static boolean isResumeFromDetailMail = false;
    public static InterstitialAdsOpenUtils instances;
    private InterstitialAd openInterstitialAd;
    //    public static boolean readyAds = false;
    public static long timeStart = 0;
    private boolean isFinishLoadAds = false;

    public static InterstitialAdsOpenUtils getInstances() {
        InterstitialAdsOpenUtils admobHandler;
        synchronized (InterstitialAdsOpenUtils.class) {
            if (instances == null) {
                instances = new InterstitialAdsOpenUtils();
            }
            admobHandler = instances;
        }

        return admobHandler;
    }

    //TODO cached Ads new
    private int idAds = 0;

    public synchronized int getIdAds() {
        return idAds;
    }

    public void loadAdIfNeeded() {
        isResumeFromDetailMail = false;
        if (MyUtils.isAppPurchased()) {
            return;
        }
        if (isAdLoaded())
            return;
//        if(openInterstitialAd == null)
        loadAd();
    }

    private boolean isAdLoaded() {
        return openInterstitialAd != null && openInterstitialAd.isLoaded();
    }

    private void loadAd() {
        idAds = 0;
//        readyAds = false;
        isFinishLoadAds = false;
        timeStart = System.currentTimeMillis();
        MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOpen.REQUEST);
        loadAd(getIdAds());//0
    }

    private void loadAd(final int count) {

        int maxLength = logger.getAdmob_popup_openapp7(MyApplication.getInstance()).length;
        if (count >= maxLength) {
            isFinishLoadAds = true;
//            DialogLoadingUtils.getInstances().dissmissDialog();
//            DialogLoading.getInstance(MyApplication.getInstance(), DialogLoading.LOADING).dismiss();
            return;
        }
        String idAds = logger.getAdmob_popup_openapp7(MyApplication.getInstance())[count];
        lg.logs("InterstitialAdsOpenUtils", "loadAd: " + count);
        lg.logs("InterstitialAdsOpenUtils", "loadAd: start loading " + idAds);
        openInterstitialAd = new InterstitialAd(MyApplication.getInstance());
        if (LogUtils.isForceDebug) {
            openInterstitialAd.setAdUnitId(idAds);
        } else
            openInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

        openInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                lg.logs("InterstitialAdsOpenUtils", "onAdFailedToLoad: " + InterstitialAdsOpenUtils.this.idAds);
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOpen.FAIL + i);
                InterstitialAdsOpenUtils.this.idAds = count + 1;
                loadAd(getIdAds());
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
//                readyAds = false;
//                InterstitialAdUtils.lastTimeShownAd = System.currentTimeMillis();

                if (ut.isAppPurchased(MyApplication.getInstance())) {
                    return;
                }
                if (isAdLoaded() || isAdLoading())
                    return;


//                InterstitialAdUtils.getInstance().loadAdIfNeeded();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
//                readyAds = false;
                lg.logs("InterstitialAdsOpenUtils", "onAdOpened: ");
//                InterstitialAdUtils.lastTimeShownAd = System.currentTimeMillis();
//                MyUtils.dismissCurrentProgress();
//                DialogLoading.getInstance(MyApplication.getInstance(), DialogLoading.LOADING).dismiss();
//                DialogLoadingUtils.getInstances().dissmissDialog();
                isResumeFromDetailMail = true;
//                InterstitialAdUtils.getInstance().loadAdIfNeeded();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                lg.logs("InterstitialAdsOpenUtils", "onAdLoaded: " + InterstitialAdsOpenUtils.this.idAds);
                long timeEndSub = System.currentTimeMillis() - timeStart;
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOpen.LOADED);
                MyTrackingFireBase.trackingEventNew(
                        TrackingUtils.TrackingConstant.Ads.IntersOpen.LOADED + MyUtils.convertLongTimeMillis(timeEndSub));
                isFinishLoadAds = true;
//                if (readyAds && isAdReadyToShow() && MyApplication.isAppRunning) {
//                    lg.logs("InterstitialAdsOpenUtils", "onAdLoaded: show() " + InterstitialAdsOpenUtils.this.idAds);
////                        listener.hideLoading();
//                    openInterstitialAd.show();
//                    MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.POP_OPEN_SHOW);
//                }
            }
        });
        openInterstitialAd.loadAd(DisplayAdController.getOpenAdRequest());
    }

    private boolean isAdLoading() {
        return openInterstitialAd != null && openInterstitialAd.isLoading();
    }

    public boolean isAdReadyToShow() {
        return true;
//        long minTimeShowingInterstitialAd = MyFirebaseConfig.getKeyTimeDelayAds();//InterstitialAdUtils.MIN_TIME_TO_SHOW_INTERSTITIAL_AD;
//        return isAdLoaded() && (System.currentTimeMillis() - InterstitialAdUtils.lastTimeShownAd > minTimeShowingInterstitialAd);
    }

    public void showAdIfNeeded(Context context, InterstitialAdUtilsListener listener) {
        isResumeFromDetailMail = false;
        lg.logs("InterstitialAdsOpenUtils", "isResumeFromDetailMail false");
        if (context == null) {
            return;
        }
        if (MyUtils.isAppPurchased()) {
            return;
        }

        if (openInterstitialAd == null) {
            loadAd();
        }
//        else {
//        }
        lg.logs("InterstitialAdsOpenUtils", "showForceAdsIfNeeded showLoading");

//        if (getIdAds() > 0 && getIdAds() < logger.getAdmob_popup_openapp7(MyApplication.getInstance()).length) {
//            readyAds = true;
//            listener.showLoading();
//        }
//        if (isAdLoaded() && isAdReadyToShow() && MyApplication.isAppRunning) {
//            lg.logs("InterstitialAdsOpenUtils", "showForceAdsIfNeeded show()");
//            lg.logs("InterstitialAdsOpenUtils", "showForceAdsIfNeeded show() id " + getIdAds());
//            readyAds = true;
//            openInterstitialAd.show();
////            listener.hideLoading();
//        } else {
//            lg.logs("InterstitialAdsOpenUtils", "showForceAdsIfNeeded hide" + getIdAds());
//        }

        //new
//        Utils.showProgress(activity, Utils.getString(R.string.loading_ads));

        //if (getIdAds() > 0 && getIdAds() < logger.getAdmob_popup_openapp7(MyApplication.getInstance()).length) {
//            readyAds = true;
//            listener.showLoading();
//        }
        listener.showLoading();
        Observable.interval(0, 0, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .doOnDispose(() -> {
//                        isLoadingOpenAds = false;
//                        Utils.dismissCurrentProgress();
                    listener.hideLoading();
                    listener.hideAds();
                })
                .subscribe(new Observer<Long>() {
                    private Disposable openAppDisposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        openAppDisposable = d;
                    }

                    @Override
                    public void onNext(Long count) {
                        lg.logs("InterstitialAdsOpenUtils", "OnNext: " + count);
                        if (isFinishLoadAds || count >= MyFirebaseConfig.getKeyAdsTimeOut()) {
                            if (openInterstitialAd != null && openInterstitialAd.isLoaded()
                                    && MyApplication.isAppRunning) {

                                openInterstitialAd.show();
//                                InterstitialAdUtils.lastTimeShownAd  = System.currentTimeMillis();
                                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOpen.SHOW);
                            }
                            openAppDisposable.dispose();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
//                        Utils.dismissCurrentProgress();
                        listener.hideLoading();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
