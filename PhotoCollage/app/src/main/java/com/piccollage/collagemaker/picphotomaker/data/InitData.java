package com.piccollage.collagemaker.picphotomaker.data;

import android.content.Context;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.Ratio;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Palette;

import java.util.ArrayList;
import java.util.List;

/**
 * chứa các local data
 */
public class InitData {

    public static ArrayList<Icon> addBackgroundIcon() {
        ArrayList<Icon> icons = new ArrayList<>();
        icons.add(new Icon(0, 0, true, MyApplication.getContext().getString(R.string.solid)));
        icons.add(new Icon(R.drawable.ic_circletypegradientbg_focus, R.drawable.ic_circletypegradientbg, false, MyApplication.getContext().getString(R.string.gradient_background)));
        icons.add(new Icon(R.drawable.ic_icpatternfocused, R.drawable.ic_icpattern, false, MyApplication.getContext().getString(R.string.patterns)));
        return icons;
    }

    public static ArrayList<Icon> addBackgroundIconQuote() {
        ArrayList<Icon> icons = new ArrayList<>();
        icons.add(new Icon(0, 0, true, MyApplication.getContext().getString(R.string.solid)));
        icons.add(new Icon(R.drawable.ic_circletypegradientbg_focus, R.drawable.ic_circletypegradientbg, false, MyApplication.getContext().getString(R.string.gradient_background)));
        icons.add(new Icon(R.drawable.ic_icpatternfocused, R.drawable.ic_icpattern, false, MyApplication.getContext().getString(R.string.patterns)));
        icons.add(new Icon(R.drawable.circlepalettebgfocus, R.drawable.circlepalettebg, false, MyApplication.getContext().getString(R.string.palette)));
        return icons;
    }

    public static void addFeatureIcon(ArrayList<Icon> icons) {
        icons.add(new Icon(R.drawable.ic_icframe, R.drawable.ic_icframe, false, MyApplication.getContext().getString(R.string.collage_style)));
        icons.add(new Icon(R.drawable.ic_icborder, R.drawable.ic_icborder, false, MyApplication.getContext().getString(R.string.margin)));
        icons.add(new Icon(R.drawable.ic_icbgcolor, R.drawable.ic_icbgcolor, false, MyApplication.getContext().getString(R.string.background)));
        icons.add(new Icon(R.drawable.ic_icsticker, R.drawable.ic_icsticker, false, MyApplication.getContext().getString(R.string.sticker)));
        icons.add(new Icon(R.drawable.ic_ictext, R.drawable.ic_ictext, false, MyApplication.getContext().getString(R.string.text)));
    }

    public static void addFeatureIconPIP(ArrayList<Icon> icons) {
        icons.add(new Icon(R.drawable.ic_icpipstyle, R.drawable.ic_icpipstyle, false, MyApplication.getContext().getString(R.string.pip_style)));
        icons.add(new Icon(R.drawable.ic_icbgcolor, R.drawable.ic_icbgcolor, false, MyApplication.getContext().getString(R.string.background)));
        icons.add(new Icon(R.drawable.ic_icsticker, R.drawable.ic_icsticker, false, MyApplication.getContext().getString(R.string.sticker)));
        icons.add(new Icon(R.drawable.ic_ictext, R.drawable.ic_ictext, false, MyApplication.getContext().getString(R.string.text)));
        icons.add(new Icon(R.drawable.ic_icchangeimage, R.drawable.ic_icchangeimage, false, MyApplication.getContext().getString(R.string.change_image_background)));
    }

    public static void addFeatureIconScrap(ArrayList<Icon> icons) {
        icons.add(new Icon(R.drawable.ic_icbgcolor, R.drawable.ic_icbgcolor, false, MyApplication.getContext().getString(R.string.background)));
        icons.add(new Icon(R.drawable.ic_icsticker, R.drawable.ic_icsticker, false, MyApplication.getContext().getString(R.string.sticker)));
        icons.add(new Icon(R.drawable.ic_ictext, R.drawable.ic_ictext, false, MyApplication.getContext().getString(R.string.text)));
        icons.add(new Icon(R.drawable.ic_icborder, R.drawable.ic_icborder, false, MyApplication.getContext().getString(R.string.aspect_ratio)));
    }

    public static ArrayList<Icon> addBorderIcon() {
        ArrayList<Icon> icons = new ArrayList<>();
        icons.add(new Icon(R.drawable.ic_icmargin_focused, R.drawable.ic_icmargin, true, MyApplication.getContext().getString(R.string.margin)));
        icons.add(new Icon(R.drawable.ic_iccornerradius_focused, R.drawable.ic_iccornerradius, false, MyApplication.getContext().getString(R.string.corner_radius)));
        icons.add(new Icon(R.drawable.ic_icaspectratio_focused, R.drawable.ic_icaspectratio, false, MyApplication.getContext().getString(R.string.aspect_ratio)));
        icons.add(new Icon(R.drawable.ic_icshadow_focused, R.drawable.ic_icshadow, false, MyApplication.getContext().getString(R.string.shadow)));
        return icons;
    }

    public static ArrayList<Ratio> ratioData() {
        ArrayList<Ratio> ratios = new ArrayList<>();
        ratios.add(new Ratio("1:1", R.drawable.ic_iccroporiginfocus, R.drawable.ic_iccroporigin, true, "Original"));
        ratios.add(new Ratio("16:9", R.drawable.ic_iccropfbcoverfocused, R.drawable.ic_iccropfbcover, false, "Fb Cover"));
        ratios.add(new Ratio("1.01:1", R.drawable.ic_iccropfbimgpostfocused, R.drawable.ic_iccropfbimgpost, false, "Fb Post'"));
        ratios.add(new Ratio("1.001:1", R.drawable.ic_iccropfbprofilefocused, R.drawable.ic_iccropfbprofile, false, "Fb Profile"));
        ratios.add(new Ratio("9:16", R.drawable.ic_iccropinsstoryfocus, R.drawable.ic_iccropinsfocus, false, "Ins Story"));
        ratios.add(new Ratio("1.0001:1", R.drawable.ic_iccropinsprofilefocused, R.drawable.ic_iccropinsprofile, false, "Ins Image"));
        ratios.add(new Ratio("3:1", R.drawable.ic_iccroptwittercoverfocused, R.drawable.ic_iccroptwittercover, false, "Twitter Cover"));
        ratios.add(new Ratio("2:1", R.drawable.ic_iccroptwitterimgpostfocused, R.drawable.ic_iccroptwitterimgpost, false, "Twitter Post"));
        return ratios;
    }

    // default gradients
    public static List<Gradient> addGradients() {
        List<Gradient> gradient = new ArrayList<>();
        gradient.add(new Gradient("#FF46C5FE", "#FF960ACD", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FF50DDE6", "#FF130DB7", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FF5FF7E9", "#FF7270FD", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FF75EBA4", "#FF0F5EAD", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FF7DF9B5", "#FF2AC770", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FF8BF5EA", "#FF32CCBC", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FF8EF9BD", "#FF012761", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFA4F265", "#FFFA026D", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFA6D9FF", "#FF0497FF", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFCB9DFB", "#FF7568F0", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFDDA7FC", "#FFA046D4", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFE3CA7C", "#FF5514D4", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFEAA994", "#FF641CDA", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFEBC71B", "#FFB211FD", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFEA5C56", "#FF370A40", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFEEC576", "#FFA74FB5", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFF5C8EB", "#FFD93BCD", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFF15DA1", "#FF8C1BAB", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFF37595", "#FF633AA2", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFF9D29E", "#FFE96E72", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFBC733", "#FFF55655", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFCD118", "#FFE80805", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFDEC77", "#FFF8D904", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFDB390", "#FFEB5657", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFCBC60", "#FFDE4413", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFF9CF73", "#FF3878FE", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFF8D505", "#FF0F1A7D", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFDEDB2", "#FFCB2AFD", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFAEEC2", "#FF9553A5", false, Constant.DEFAULT));
        gradient.add(new Gradient("#FFFFEDB3", "#FFF6466E", false, Constant.DEFAULT));
        for (Gradient gra : gradient) gra.setName(String.valueOf(gradient.indexOf(gra)));
        return gradient;
    }

    // default palettes
    public static List<Palette> addPalettes() {
        List<Palette> palettes = new ArrayList<>();
        palettes.add(new Palette("#f88e36", "#7f2908", "#3d261b", "#ea6628", "#e18a69", false));
        palettes.add(new Palette("#a25333", "#dbcd95", "#a2c3a6", "#5b2e30", "#848666", false));
        palettes.add(new Palette("#1d5155", "#1c4442", "#122021", "#b7bbaf", "#3f6f71", false));
        palettes.add(new Palette("#fbc308", "#fbe01e", "#4e5915", "#888839", "#bd2004", false));
        palettes.add(new Palette("#d4540b", "#cd8919", "#794932", "#f5e0b0", "#c29361", false));
        palettes.add(new Palette("#592005", "#b67019", "#76531e", "#e5d075", "#dc9728", false));
        for (Palette pal : palettes) pal.setName(String.valueOf(palettes.indexOf(pal)));
        return palettes;
    }

    public static ArrayList<Icon> addTextCustomFeature() {
        ArrayList<Icon> icons = new ArrayList<>();
        icons.add(new Icon(R.drawable.ic_icabspace_focused, R.drawable.ic_icabspace, true, MyApplication.getContext().getString(R.string.letter_space)));
        icons.add(new Icon(R.drawable.ic_icspacevertical_focused, R.drawable.ic_icspacevertical, false, MyApplication.getContext().getString(R.string.line_space)));
        icons.add(new Icon(R.drawable.ic_ictextshadow_focused, R.drawable.ic_ictextshadow, false, MyApplication.getContext().getString(R.string.shadow)));
        icons.add(new Icon(R.drawable.ic_ictextalpha_focused, R.drawable.ic_ictextalpha, false, MyApplication.getContext().getString(R.string.opacity)));
        icons.add(new Icon(R.drawable.ic_icmenualignleft, R.drawable.ic_icmenualignleft, false, MyApplication.getContext().getString(R.string.align)));
        return icons;
    }

    public static ArrayList<Icon> addTextFeature() {
        ArrayList<Icon> icons = new ArrayList<>();
        icons.add(new Icon(R.drawable.ic_ickeyboard_focused, R.drawable.ic_ickeyboard, true, MyApplication.getContext().getString(R.string.edit_text)));
        icons.add(new Icon(R.drawable.ic_ictypefont_focused, R.drawable.ic_ictypefont, false, MyApplication.getContext().getString(R.string.font)));
        icons.add(new Icon(R.drawable.ic_iccustomize_focused, R.drawable.ic_iccustomize, false, MyApplication.getContext().getString(R.string.custom)));
        return icons;
    }

    public static void addIconImageProcess(ArrayList<Icon> icons, Context context) {
        icons.add(new Icon(R.drawable.ic_icediteffect, R.drawable.ic_icediteffect, false, context.getString(R.string.effects)));
        icons.add(new Icon(R.drawable.ic_iceditcrop, R.drawable.ic_iceditcrop, false, context.getString(R.string.crop)));
        icons.add(new Icon(R.drawable.ic_iceditrotate, R.drawable.ic_iceditrotate, false, context.getString(R.string.rotate)));
        icons.add(new Icon(R.drawable.ic_icedittext, R.drawable.ic_icedittext, false, context.getString(R.string.text)));
        icons.add(new Icon(R.drawable.ic_icsticker_white, R.drawable.ic_icsticker_white, false, context.getString(R.string.sticker)));
        icons.add(new Icon(R.drawable.ic_iceditdraw, R.drawable.ic_iceditdraw, false, context.getString(R.string.draw)));
        icons.add(new Icon(R.drawable.ic_iceditfocus, R.drawable.ic_iceditfocus, false, context.getString(R.string.focus)));
        icons.add(new Icon(R.drawable.ic_iceditblur, R.drawable.ic_iceditblur, false, context.getString(R.string.blur)));
    }

    // data color for color seekbar
    public static String[] colors() {
        return new String[]{"#FFFFFF", "#F6F1A2", "#F3EA73", "#F3EA73", "#D6E04D", "#BED764", "#85CF51", "#4CAA56", "#59BF81", "#77CFBE", "#B9EBEE", "#9ADEFB", "#77B5EF", "#6B88EF",
                "#5863ED", "#804CEC", "#9854EF", "#AE63EC", "#C681ED", "#DA8FEB", "#CE65BD", "#DC5E92", "#E773A3", "#EC88AB", "#F3A8BB", "#F09887", "#EA755D", "#E54E46", "#E23526",
                "#E97131", "#EFAD42", "#F4CE4A", "#FADD72", "#FCEFB9", "#C6C7C7", "#8D8C8C", "#444444", "#000000"};
    }

    public static ArrayList<Icon> addIconShare(ArrayList<Icon> icons) {
        icons.add(new Icon(R.drawable.ic_facebook, R.drawable.ic_facebook, false, "Facebook"));
        icons.add(new Icon(R.drawable.ic_facebook_msg, R.drawable.ic_facebook_msg, false, "Messenger"));
        icons.add(new Icon(R.drawable.ic_instagram, R.drawable.ic_instagram, false, "Instagram"));
        icons.add(new Icon(R.drawable.ic_twitter, R.drawable.ic_twitter, false, "Twitter"));
        icons.add(new Icon(R.drawable.ic_telegram, R.drawable.ic_telegram, false, "Telegram"));
        icons.add(new Icon(R.drawable.ic_snapchat, R.drawable.ic_snapchat, false, "Snapchat"));
        icons.add(new Icon(R.drawable.ic_whats_app, R.drawable.ic_whats_app, false, "WhatsApp"));
        icons.add(new Icon(R.drawable.ic_more, R.drawable.ic_more, false, "More"));
        return icons;
    }

    public static void dataIconMFeature(ArrayList<Icon> icons) {
        icons.add(new Icon(R.drawable.ic_iconeditor, R.drawable.ic_iconeditor, false, "Editor"));
        icons.add(new Icon(R.drawable.ic_iconscrapbook, R.drawable.ic_iconscrapbook, false, "Scrapbook"));
        icons.add(new Icon(R.drawable.ic_iconquotes, R.drawable.ic_iconquotes, false, "Quotes"));
        icons.add(new Icon(R.drawable.ic_iconmycreative, R.drawable.ic_iconmycreative, false, "My Creative"));
        icons.add(new Icon(R.drawable.ic_iconshop, R.drawable.ic_iconshop, false, "Shop"));
    }
}
