package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.os.Parcel;
import android.os.Parcelable;

public class Palette implements Parcelable {
    private String color1;
    private String color2;
    private String color3;
    private String color4;
    private String color5;
    private String name;
    private boolean isPick;

    public Palette(String color1, String color2, String color3, String color4, String color5, boolean isPick) {
        this.color1 = color1;
        this.color2 = color2;
        this.color3 = color3;
        this.color4 = color4;
        this.color5 = color5;
        this.isPick = isPick;
    }

    protected Palette(Parcel in) {
        color1 = in.readString();
        color2 = in.readString();
        color3 = in.readString();
        color4 = in.readString();
        color5 = in.readString();
        name = in.readString();
        isPick = in.readByte() != 0;
    }

    public static final Creator<Palette> CREATOR = new Creator<Palette>() {
        @Override
        public Palette createFromParcel(Parcel in) {
            return new Palette(in);
        }

        @Override
        public Palette[] newArray(int size) {
            return new Palette[size];
        }
    };

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor1() {
        return color1;
    }

    public String getColor2() {
        return color2;
    }

    public String getColor3() {
        return color3;
    }

    public String getColor4() {
        return color4;
    }

    public String getColor5() {
        return color5;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(color1);
        dest.writeString(color2);
        dest.writeString(color3);
        dest.writeString(color4);
        dest.writeString(color5);
        dest.writeString(name);
        dest.writeByte((byte) (isPick ? 1 : 0));
    }
}
