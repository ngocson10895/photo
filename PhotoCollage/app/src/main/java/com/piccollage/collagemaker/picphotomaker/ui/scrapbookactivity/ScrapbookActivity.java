package com.piccollage.collagemaker.picphotomaker.ui.scrapbookactivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdSize;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.IconsAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityScrapbookPreviewBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.entity.Ratio;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.entity.TextOfUser;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.DrawableSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.Sticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.StickerView;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.TextSticker;
import com.piccollage.collagemaker.picphotomaker.ui.DialogExitFeature;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.PreviewActivity;
import com.piccollage.collagemaker.picphotomaker.ui.morefeature.AddTextFragment;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.scrapbookactivity.layout.ImageEntity;
import com.piccollage.collagemaker.picphotomaker.ui.scrapbookactivity.layout.PhotoView;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.DateTimeUtils;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.view.AddStickerView;
import com.piccollage.collagemaker.picphotomaker.utils.view.BackgroundView;
import com.piccollage.collagemaker.picphotomaker.utils.view.RatioView;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradients;
import com.piccollage.collagemaker.picphotomaker.viewholder.Pattern;
import com.piccollage.collagemaker.picphotomaker.viewholder.Patterns;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dauroi.photoeditor.utils.PhotoUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: màn hình tính năng scrapbook
 * Des: user có thể kéo thả chỉnh sửa tùy ý theo ý muốn
 */
public class ScrapbookActivity extends BaseAppActivity implements AddTextFragment.ISendTextView {
    private static final String TAG = "ScrapbookActivity";

    private ActivityScrapbookPreviewBinding binding;
    private ArrayList<Icon> iconFeature;
    private List<Photo> photosPicked;
    private PhotoView photoView;
    private int photoWidth, photoHeight;
    private Bitmap bmBackground;
    private DialogLoading dialog;
    private DialogExitFeature dialogExitFeature;
    private IconsAdapter adapterMainFeature;
    private ImageView background;
    private ArrayList<StickerTheme> stickerThemes;
    private boolean isPreview;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityScrapbookPreviewBinding.inflate(getLayoutInflater());
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        stickerThemes = new ArrayList<>();
        loadAds();
        dialogExitFeature = new DialogExitFeature(this, ScrapbookActivity.this::discard);
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Scrapbook.NAME);

        dialog = new DialogLoading(this, DialogLoading.CREATE);
        iconFeature = new ArrayList<>();
        adapterMainFeature = new IconsAdapter(iconFeature, this, this::pickFeature, "");
        binding.rvMainFeature.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvMainFeature.setAdapter(adapterMainFeature);

        background = new ImageView(this);
        bmBackground = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.WHITE));
        background.setScaleType(ImageView.ScaleType.CENTER_CROP);
        background.setImageBitmap(bmBackground);

        photoView = new PhotoView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        binding.photoLayout.addView(background, params);
        binding.photoLayout.addView(photoView, params);

        binding.backgroundView.setListener(new BackgroundView.IBackgroundListener() {
            @Override
            public void previewBackground(int color) {
                background.setImageBitmap(ImageDecoder.drawableToBitmap(new ColorDrawable(color)));
            }

            @Override
            public void previewPattern(String path) {
                if (path.contains("storage")) {
                    Glide.with(ScrapbookActivity.this).load(path).centerCrop().error(R.drawable.sticker_default).into(background);
                } else
                    Glide.with(ScrapbookActivity.this).load(Uri.parse("file:///android_asset/background/" + path)).centerCrop().error(R.drawable.sticker_default).into(background);
            }

            @Override
            public void previewGradient(String stColor, String endColor) {
                background.setImageDrawable(new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{Color.parseColor(stColor), Color.parseColor(endColor)}));
            }

            @Override
            public void previewPalette(String c1, String c2, String c3, String c4, String c5) {

            }

            @Override
            public void jumpToShop(String type) {
                Intent intent = new Intent(ScrapbookActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, type);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction(Bitmap bm) {
                bmBackground = bm;
                background.setImageBitmap(bm);
                binding.tbScrapbookPre.setVisibility(View.VISIBLE);
            }

            @Override
            public void closeAction(Bitmap bm) {
                bmBackground = bm;
                background.setImageBitmap(bm);
                binding.tbScrapbookPre.setVisibility(View.VISIBLE);
            }
        });

        binding.addStickerView.setListener(new AddStickerView.IStickerViewListener() {
            @Override
            public void previewAction(StickerItem stickerItem) {
                add(stickerItem);
            }

            @Override
            public void jumpToShop() {
                Intent intent = new Intent(ScrapbookActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, Constant.STICKER);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction() {
                binding.tbScrapbookPre.setVisibility(View.VISIBLE);
                isPreview = false;
            }

            @Override
            public void backAction() {
                binding.tbScrapbookPre.setVisibility(View.VISIBLE);
                isPreview = false;
                binding.stickerView.removeCurrentSticker();
            }
        });

        binding.ratioView.setListener(new RatioView.IRatioViewListener() {
            @Override
            public void previewRatio(Ratio ratio) {
                ratioPick(ratio);
            }

            @Override
            public void doneAction(Ratio ratio) {
                ratioPick(ratio);
            }

            @Override
            public void cancelAction(Ratio ratio) {
                ratioPick(ratio);
            }
        });
    }

    private void loadAds() {
        Advertisement.loadBannerAdExit(this);
        Advertisement.showBannerAdHome(this, binding.adsLayout, AdSize.BANNER, new BannerAdsUtils.BannerAdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdFailed() {

            }
        }, () -> {

        });
        Advertisement.loadInterstitialAdInApp(this);
    }

    @Override
    public void buttonClick() {
        onIvSaveShareClicked();
        onBtnCloseClicked();
    }

    private void pickFeature(int i) {
        switch (i) {
            case 0:
                if (checkDoubleClick()) {
                    binding.tbScrapbookPre.setVisibility(View.GONE);
                    binding.backgroundView.show();
                }
                break;
            case 1:
                if (checkDoubleClick()) {
                    binding.addStickerView.setVisibility(View.GONE);
                    binding.addStickerView.show();
                }
                break;
            case 2:
                if (checkDoubleClick()) {
                    onIvAddTextClicked();
                }
                break;
            case 3:
                if (checkDoubleClick()) {
                    binding.ratioView.show();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getStickerData(EventItemShopSticker eventItemShopSticker) {
        stickerThemes.clear();
        stickerThemes.addAll(eventItemShopSticker.getStickerThemes());
    }

    public void initData() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        InitData.addFeatureIconScrap(iconFeature);
        adapterMainFeature.setIconArrayList(iconFeature);
        adapterMainFeature.notifyDataSetChanged();

        photosPicked = new ArrayList<>();
        ApplicationViewModel applicationViewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        if (getIntent() != null) {
            ArrayList<String> paths = getIntent().getStringArrayListExtra(PickPhotoActivity.PHOTOS_PICKED);
            for (String path : paths) {
                photosPicked.add(new Photo(path, 0));
            }
        } else photosPicked.add(new Photo("", 0));

        photoView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                photoWidth = photoView.getWidth();
                photoHeight = photoView.getHeight();
                new BuildLayout(ScrapbookActivity.this, 0.4f, false).execute();
                photoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        applicationViewModel.getDataStickers().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (!itemsDownloaded.isEmpty()) {
                    for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                        for (StickerTheme stickerTheme : stickerThemes) {
                            if (itemDownloaded.getName().equals(stickerTheme.getNameTheme())) {
                                stickerTheme.setDownloaded(true);
                            }
                        }
                    }
                } else {
                    for (StickerTheme stickerTheme : stickerThemes) {
                        stickerTheme.setDownloaded(false);
                    }
                }
                binding.addStickerView.setMoreSticker(stickerThemes);
            }
        });

        applicationViewModel.getDataGradients().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Gradients> gradients = new ArrayList<>();
                gradients.add(new Gradients(Constant.DEFAULT, InitData.addGradients()));
                for (ItemDownloaded item : itemsDownloaded) {
                    List<Gradient> list = new ArrayList<>();
                    String[] code = item.getPath().split("_");
                    String[] nameItem = item.getNameItem().split("_");
                    for (int i = 0; i < code.length; i += 2) {
                        if (code[i].length() == 6) {
                            Gradient gra = new Gradient("#" + code[i], "#" + code[i + 1], false, item.getName());
                            list.add(gra);
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    gradients.add(new Gradients(item.getName(), list));
                }
                gradients.add(0, new Gradients(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataGradientsEx(gradients);
            }
        });

        ArrayList<Pattern> pattern = new ArrayList<>();
        for (String p : FileUtil.listAssetFiles("background", this)) {
            pattern.add(new Pattern(p, String.valueOf(FileUtil.listAssetFiles("background", this).indexOf(p)), Constant.DEFAULT, false));
        }
        applicationViewModel.getDataPatterns().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Patterns> patterns = new ArrayList<>();
                patterns.add(new Patterns(Constant.DEFAULT, pattern));
                for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                    ArrayList<String> paths = new ArrayList<>();
                    List<Pattern> list = new ArrayList<>();
                    FileUtil.getAllFiles(new File(itemDownloaded.getPath()), paths);
                    for (String path : paths) {
                        list.add(new Pattern(path, "", itemDownloaded.getName(), false));
                    }
                    String[] nameItem = itemDownloaded.getNameItem().split("_");
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    patterns.add(new Patterns(itemDownloaded.getName(), list));
                }
                patterns.add(0, new Patterns(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataPatternsEx(patterns);
            }
        });
    }

    // chỉnh sửa tỉ lệ ảnh thì phải chỉnh kích cỡ của ảnh bên trong ( hiện tại chưa lưu được vị trí của ảnh nên khi thay đổi ảnh sẽ về trung tâm )
    private void ratioPick(Ratio ratio) {
        ConstraintSet set = new ConstraintSet();
        set.clone(binding.clMain);
        set.setDimensionRatio(binding.clPhoto.getId(), ratio.getRatio());
        set.applyTo(binding.clMain);

        photoView = new PhotoView(this);
        binding.photoLayout.removeAllViews();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        binding.photoLayout.addView(background, params);
        binding.photoLayout.addView(photoView, params);

        photoView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                photoWidth = photoView.getWidth();
                photoHeight = photoView.getHeight();
                switch (ratio.getRatio()) {
                    case "1.01:1":
                    case "1.001:1":
                    case "1.0001:1":
                    case "1:1":
                        new BuildLayout(ScrapbookActivity.this, 0.4f, false).execute();
                        binding.adsLayout.setVisibility(View.VISIBLE);
                        break;
                    case "2:1":
                        new BuildLayout(ScrapbookActivity.this, 0.35f, false).execute();
                        binding.adsLayout.setVisibility(View.VISIBLE);
                        break;
                    case "3:1":
                        new BuildLayout(ScrapbookActivity.this, 0.2f, false).execute();
                        binding.adsLayout.setVisibility(View.VISIBLE);
                        break;
                    case "16:9":
                        new BuildLayout(ScrapbookActivity.this, 0.25f, false).execute();
                        binding.adsLayout.setVisibility(View.VISIBLE);
                        break;
                    case "9:16":
                        new BuildLayout(ScrapbookActivity.this, 0.5f, false).execute();
                        binding.adsLayout.setVisibility(View.INVISIBLE);
                        break;
                }
                photoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public void discard() {
        Intent intent = new Intent(this, PickPhotoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(PickPhotoActivity.MODE, Constant.SCRAPBOOK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    /**
     * Build Layout Main
     */
    private static class BuildLayout extends AsyncTask<Void, Void, ArrayList<ImageEntity>> {
        private WeakReference<ScrapbookActivity> weakReference;
        private float ratio;
        private boolean isIdle;

        BuildLayout(ScrapbookActivity activity, float ratio, boolean isIdle) {
            weakReference = new WeakReference<>(activity);
            this.ratio = ratio;
            this.isIdle = isIdle;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ScrapbookActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            activity.dialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<ImageEntity> aVoid) {
            super.onPostExecute(aVoid);
            ScrapbookActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            for (ImageEntity entity : aVoid) activity.photoView.addImageEntity(entity);
            activity.photoView.setIdle(isIdle);
            activity.binding.ratioView.setBuild(false);
            activity.dialog.dismiss();
        }

        @Override
        protected ArrayList<ImageEntity> doInBackground(Void... voids) {
            ScrapbookActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            ArrayList<ImageEntity> entities = new ArrayList<>();
            for (Photo photo : activity.photosPicked) {
                float angle = (float) (activity.photosPicked.indexOf(photo) * Math.PI / 10); // thay đổi góc nằm của item khi mới hiển thị

                Uri uri = Uri.fromFile(new File(photo.getPathPhoto()));
                ImageEntity entity = new ImageEntity(uri, activity.getResources());
                entity.setInitScaleFactor(ratio);
                entity.setSticker(false);
                entity.load(activity,
                        (float) (activity.photoWidth - entity.getWidth()) / 2,
                        (float) (activity.photoHeight - entity.getHeight()) / 2, angle);
                entities.add(entity);
            }
            return entities;
        }
    }

    // Save and share image
    public void saveAndShare() {
        if (checkDoubleClick()) {
            if (binding.stickerView.isShowBorder() && binding.stickerView.isShowIcons()) {
                binding.stickerView.setShowBorder(false);
                binding.stickerView.setShowIcons(false);
            }

            dialog.show();
            new Handler().postDelayed(() -> {
                Task task = new Task(this);
                task.execute();
            }, 2000);
        }
    }

    public void onIvAddTextClicked() {
        if (checkDoubleClick()) {
            AddTextFragment addTextFragment = new AddTextFragment();
            binding.rvMainFeature.setVisibility(View.GONE);
            binding.tbScrapbookPre.setVisibility(View.GONE);
            getSupportFragmentManager();
            addTextFragment.show(getSupportFragmentManager(), addTextFragment.getTag());
        }
    }

    @Override
    public void turnOff() {
        binding.rvMainFeature.setVisibility(View.VISIBLE);
        binding.tbScrapbookPre.setVisibility(View.VISIBLE);
    }

    // Add text for main layout
    @Override
    public void sendText(TextOfUser textOfUser) {
        binding.stickerView.setLocked(false);
        binding.stickerView.setConstrained(true);
        binding.stickerView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.stickerView.setShowBorder(true);
                binding.stickerView.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerClicked: ");
                binding.stickerView.swapLayers(binding.stickerView.getStickers().indexOf(sticker), binding.stickerView.getStickerCount() - 1);
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.stickerView.setShowBorder(false);
                    binding.stickerView.setShowIcons(false);
                } else {
                    binding.stickerView.setShowBorder(true);
                    binding.stickerView.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.stickerView.configDefaultIcons("text");

        TextSticker textSticker = new TextSticker(this, getResources().getDrawable(R.drawable.sticker_transparent_background_hor));
        if (!textOfUser.getContent().isEmpty()) {
            textSticker.setTextOfUser(textOfUser);
            textSticker.resizeText();
            binding.stickerView.addSticker(textSticker);
        }
    }

    // Add sticker for main layout
    public void add(StickerItem item) {
        binding.stickerView.setLocked(false);
        binding.stickerView.setConstrained(true);
        binding.stickerView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.stickerView.setShowBorder(true);
                binding.stickerView.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerClicked: ");
                binding.stickerView.swapLayers(binding.stickerView.getStickers().indexOf(sticker), binding.stickerView.getStickerCount() - 1);
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.stickerView.setShowBorder(false);
                    binding.stickerView.setShowIcons(false);
                } else {
                    binding.stickerView.setShowBorder(true);
                    binding.stickerView.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.stickerView.configDefaultIcons("sticker");
        Glide.with(this).load(item.getUrl()).centerCrop().into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable drawable, @Nullable Transition<? super Drawable> transition) {
                if (isPreview) {
                    binding.stickerView.removeCurrentSticker();
                } else {
                    isPreview = true;
                }
                binding.stickerView.addSticker(new DrawableSticker(drawable));
            }

            @Override
            public void onLoadCleared(@Nullable Drawable drawable) {

            }
        });
    }

    public void onBtnCloseClicked() {
        binding.btnClose.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                dialogExitFeature.show();
            }
        });
    }

    public void onIvSaveShareClicked() {
        binding.ivSaveShare.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Scrapbook.DONE);
            saveAndShare();
        });
    }

    /**
     * Task to export image
     */
    private static class Task extends AsyncTask<Void, Void, File> {
        WeakReference<ScrapbookActivity> weakReference;

        Task(ScrapbookActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            ScrapbookActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            if (new PermissionChecker(activity).isPermissionOK()) {
                PhotoUtils.addImageToGallery(file.getAbsolutePath(), activity);
                Toasty.success(activity, activity.getString(R.string.saving_done), Toasty.LENGTH_SHORT).show();
                Advertisement.showInterstitialAdInApp(activity, new InterstitialAdsPopupOthers.OnAdsListener() {
                    @Override
                    public void onAdsClose() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.SCRAPBOOK);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                    @Override
                    public void close() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.SCRAPBOOK);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                });
            }
            activity.dialog.dismiss();
        }

        @Override
        protected File doInBackground(Void... voids) {
            ScrapbookActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            String parentFilePath = FileUtil.getParentFileString("My Creative");
            File parentFile = new File(parentFilePath);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "doInBackground: loaded");
                } else Log.d(TAG, "doInBackground: error");
            }
            File photoFile = new File(parentFile, DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png"));
            try {
                Objects.requireNonNull(ImageUtils.getBitmapFromView(activity.binding.clPhoto)).compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(photoFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return photoFile;
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.backgroundView.getVisibility() == View.VISIBLE) {
            binding.backgroundView.backAction();
        } else if (binding.ratioView.getVisibility() == View.VISIBLE) {
            binding.ratioView.backAction();
        } else if (binding.addStickerView.getVisibility() == View.VISIBLE) {
            binding.addStickerView.backAction();
        } else {
            dialogExitFeature.show();
        }
    }
}
