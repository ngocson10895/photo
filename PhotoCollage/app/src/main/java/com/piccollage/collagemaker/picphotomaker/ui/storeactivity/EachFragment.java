package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ItemShopAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.api.APIServiceWrapper;
import com.piccollage.collagemaker.picphotomaker.base.BaseFragment;
import com.piccollage.collagemaker.picphotomaker.data.item.DataSpecShop;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentShopItemBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.homeactivity.DetailItemActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import dauroi.photoeditor.utils.NetworkUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: show item của từng theme
 * Des: show các item có thể tải về của từng theme
 */
public class EachFragment extends BaseFragment implements ItemShopAdapter.IDownload, DialogDownload.ICancel {
    private static final String TAG = EachFragment.class.getSimpleName();
    private static final String URI_PATH = "uri_path";
    private static final String THEME = "theme";

    private ItemShopAdapter itemShopAdapter;
    private ArrayList<ItemShop> itemShops;
    private String uriApi, theme;
    private ApplicationViewModel viewModel;
    private DialogDownload dialog;
    private DialogLoading dialogLoading;
    private File parentFile;
    private DownloadTask task;
    private SharedPrefsImpl sharedPrefs;
    private FragmentShopItemBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentShopItemBinding.inflate(getLayoutInflater());

        initView();
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Advertisement.loadInterstitialAdInApp(getContext());
    }

    public static Fragment newInstance(String uriPath, String theme) {
        EachFragment fragment = new EachFragment();
        Bundle args = new Bundle();
        args.putString(URI_PATH, uriPath);
        args.putString(THEME, theme);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        initData();
    }

    private void initData() {
        if (getContext() != null) {
            itemShops = new ArrayList<>();
            if (getArguments() != null) {
                uriApi = getArguments().getString(URI_PATH);
                theme = getArguments().getString(THEME);

                itemShopAdapter = new ItemShopAdapter(getContext(), itemShops, theme, this);
                binding.rvParent.setAdapter(itemShopAdapter);
                if (NetworkUtils.checkNetworkAvailable(getContext())) {
                    APIServiceWrapper.getInstance().loadAPIEachStore(uriApi, itemShops, theme, new APIServiceWrapper.ILoadEachStoreDone() {
                        @Override
                        public void done(ArrayList<ItemShop> itemShops) {
                            if (getActivity() != null) {
                                // kiểm tra xem các item nào đã download thì đổi ui
                                viewModel.getAllData().observe(getActivity(), itemsDownloaded -> {
                                    if (itemsDownloaded != null) {
                                        if (itemsDownloaded.isEmpty()) {
                                            for (ItemShop itemShop : itemShops)
                                                itemShop.setDownloaded(false);
                                        } else {
                                            for (ItemShop itemShop : itemShops) {
                                                for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                                                    if (itemDownloaded.getName().equals(itemShop.getNameItem()) && itemDownloaded.getTheme().equals(theme)) {
                                                        itemShop.setDownloaded(true);
                                                        break;
                                                    } else itemShop.setDownloaded(false);
                                                }
                                            }
                                        }
                                        itemShopAdapter.setItemShops(itemShops);
                                        itemShopAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }

                        @Override
                        public void error() {
                            Toasty.error(Objects.requireNonNull(getContext()), getResources().getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
                        }
                    }, getContext());
                }
            }

            sharedPrefs = new SharedPrefsImpl(getContext(), Constant.NAME_SHARE);
        }
    }

    private void initView() {
        if (getContext() != null) {
            Advertisement.loadBannerAdExit(getContext());
            dialogLoading = new DialogLoading(getContext(), DialogLoading.LOADING);
            dialog = new DialogDownload(getContext(), this);
        }
        binding.rvParent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rvParent.addItemDecoration(new BetweenSpacesItemDecoration(8, 0));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    /*
    tải xuống item. nếu item thuộc theme GRADIENT hoặc PALETTE thì phải delay 0.5s vì thực chất chỉ là string, không có item nào cả
     */
    private void downloadItem(int position) {
        if (getContext() != null) {
            if (NetworkUtils.checkNetworkAvailable(getContext())) {
                if (theme.equals(Constant.GRADIENT)) {
                    MyTrackingFireBase.trackingDownload(getContext(), theme);
                    if (getContext() != null) {
                        if (checkDoubleClick()) {
                            dialog.show();
                        }
                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            StringBuilder parent = new StringBuilder();
                            StringBuilder item = new StringBuilder();
                            for (DataSpecShop data : itemShops.get(position).getPathItems()) {
                                parent.append(data.getUrlThumb()).append("_");
                                item.append(data.getTitle()).append("_");
                            }
                            parentFile = FileUtil.getParentFile(getContext(), "ItemDownload/" + theme);
                            if (!parentFile.exists()) {
                                if (parentFile.mkdirs()) {
                                    Log.d(TAG, "createTask: loaded");
                                } else
                                    Log.d(TAG, "createTask: error");
                            }
                            File f = new File(parentFile.getAbsolutePath() + "/" + itemShops.get(position).getNameItem());
                            if (!f.exists()) {
                                if (f.mkdirs()) {
                                    Log.d(TAG, "createTask: loaded");
                                } else
                                    Log.d(TAG, "createTask: error");
                            }
                            parent.append(f.getAbsolutePath());
                            viewModel.insert(new ItemDownloaded(theme, parent.toString(), itemShops.get(position).getNameItem(), "20 KB", item.toString()));
                            dialog.dismiss();
                            Toasty.success(getContext(), getContext().getString(R.string.download_done), Toasty.LENGTH_SHORT).show();
                        }, 500);
                    }
                } else if (theme.equals(Constant.PALETTE)) {
                    MyTrackingFireBase.trackingDownload(getContext(), theme);
                    if (getContext() != null) {
                        dialog.show();
                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            StringBuilder parent = new StringBuilder();
                            StringBuilder item = new StringBuilder();
                            for (DataSpecShop data : itemShops.get(position).getPathItems()) {
                                parent.append(data.getUrlThumb()).append("_");
                                item.append(data.getTitle()).append("_");
                            }
                            parentFile = FileUtil.getParentFile(getContext(), "ItemDownload/" + theme);
                            if (!parentFile.exists()) {
                                if (parentFile.mkdirs()) {
                                    Log.d(TAG, "createTask: loaded");
                                } else
                                    Log.d(TAG, "createTask: error");
                            }
                            File f = new File(parentFile.getAbsolutePath() + "/" + itemShops.get(position).getNameItem());
                            if (!f.exists()) {
                                if (f.mkdirs()) {
                                    Log.d(TAG, "createTask: loaded");
                                } else
                                    Log.d(TAG, "createTask: error");
                            }
                            parent.append(f.getAbsolutePath());
                            viewModel.insert(new ItemDownloaded(theme, parent.toString(), itemShops.get(position).getNameItem(), "20 KB", item.toString()));
                            dialog.dismiss();
                            Toasty.success(getContext(), getContext().getString(R.string.download_done), Toasty.LENGTH_SHORT).show();
                        }, 500);
                    }
                } else {
                    createTask(itemShops.get(position).getUrlFile(), itemShops.get(position).getNameItem());
                    download(task, position);
                }
            } else {
                Toasty.error(getContext(), getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void download(int position) {
        if (checkDoubleClick()) {
            MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.Store.DOWNLOAD + theme);
            if (sharedPrefs.get(Constant.IS_GOT_PURCHASE, Boolean.class)) {
                if (itemShops != null) {
                    downloadItem(position);
                }
            } else {
                if (!itemShops.get(position).isPro()) {
                    Advertisement.showInterstitialAdInApp(getContext(), new InterstitialAdsPopupOthers.OnAdsListener() {
                        @Override
                        public void onAdsClose() {
                            downloadItem(position);
                            dialogLoading.dismiss();
                        }

                        @Override
                        public void showLoading() {
                            if (dialogLoading != null) dialogLoading.show();
                        }

                        @Override
                        public void hideLoading() {
                            if (dialogLoading != null) dialogLoading.dismiss();
                        }
                    }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                        @Override
                        public void close() {
                            downloadItem(position);
                            dialogLoading.dismiss();
                        }

                        @Override
                        public void showLoading() {
                            if (dialogLoading != null) dialogLoading.show();
                        }

                        @Override
                        public void hideLoading() {
                            if (dialogLoading != null) dialogLoading.dismiss();
                        }
                    });
                } else {
                    Intent intent = new Intent(getContext(), BuySubActivity.class);
                    startActivity(intent);
                }
            }
        }
    }

    @Override
    public void itemDownloaded(int pos) {
        Intent intent = new Intent(getContext(), DetailItemActivity.class);
        intent.putExtra("Downloaded", 1);
        intent.putExtra("Name", itemShopAdapter.getItemShops().get(pos).getNameItem());
        intent.putExtra("Zip", itemShopAdapter.getItemShops().get(pos).getUrlFile());
        intent.putExtra("Thumb", itemShopAdapter.getItemShops().get(pos).getPathItems().get(0).getUrlThumb());
        intent.putExtra("Preview", itemShopAdapter.getItemShops().get(pos).getUrlPreview());
        StringBuilder item = new StringBuilder();
        for (DataSpecShop data : itemShops.get(pos).getPathItems()) {
            item.append(data.getTitle()).append("_");
        }
        intent.putExtra("NameItems", item.toString());
        startActivity(intent);
    }

    @Override
    public void itemPro(int pos) {
        Intent intent = new Intent(getContext(), DetailItemActivity.class);
        intent.putExtra("Downloaded", 2);
        intent.putExtra("Name", itemShopAdapter.getItemShops().get(pos).getNameItem());
        intent.putExtra("Zip", itemShopAdapter.getItemShops().get(pos).getUrlFile());
        intent.putExtra("Thumb", itemShopAdapter.getItemShops().get(pos).getPathItems().get(0).getUrlThumb());
        intent.putExtra("Preview", itemShopAdapter.getItemShops().get(pos).getUrlPreview());
        StringBuilder item = new StringBuilder();
        for (DataSpecShop data : itemShops.get(pos).getPathItems()) {
            item.append(data.getTitle()).append("_");
        }
        intent.putExtra("NameItems", item.toString());
        startActivity(intent);
    }

    @Override
    public void item(int pos) {
        Intent intent = new Intent(getContext(), DetailItemActivity.class);
        intent.putExtra("Name", itemShopAdapter.getItemShops().get(pos).getNameItem());
        intent.putExtra("Zip", itemShopAdapter.getItemShops().get(pos).getUrlFile());
        intent.putExtra("Thumb", itemShopAdapter.getItemShops().get(pos).getPathItems().get(0).getUrlThumb());
        intent.putExtra("Preview", itemShopAdapter.getItemShops().get(pos).getUrlPreview());
        StringBuilder item = new StringBuilder();
        for (DataSpecShop data : itemShops.get(pos).getPathItems()) {
            item.append(data.getTitle()).append("_");
        }
        intent.putExtra("NameItems", item.toString());
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        APIServiceWrapper.clearTag(uriApi);
    }

    private void createTask(String urlZip, String nameZip) {
        if (getContext() != null) {
            parentFile = FileUtil.getParentFile(getContext(), "ItemDownload/" + theme);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "createTask: loaded");
                } else
                    Log.d(TAG, "createTask: error");
            }
            task = new DownloadTask.Builder(urlZip, parentFile).setFilename(nameZip).setMinIntervalMillisCallbackProcess(DownloadTask.Builder.DEFAULT_MIN_INTERVAL_MILLIS_CALLBACK_PROCESS)
                    .setPassIfAlreadyCompleted(DownloadTask.Builder.DEFAULT_PASS_IF_ALREADY_COMPLETED).build();
        }
    }

    // download các item thuộc theme FONT, STICKER, PATTERN
    private void download(DownloadTask task, int position) {
        task.enqueue(new DownloadListener4WithSpeed() {
            int size = 0;

            @Override
            public void taskStart(@NonNull DownloadTask task) {
                Log.d(TAG, "taskStart: ");
            }

            @Override
            public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {
                Log.d(TAG, "connectStart: ");
            }

            @Override
            public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {
                Log.d(TAG, "connectEnd: ");
            }

            @Override
            public void infoReady(@NonNull DownloadTask task, @NonNull BreakpointInfo info, boolean fromBreakpoint, @NonNull Listener4SpeedAssistExtend.Listener4SpeedModel model) {
                Log.d(TAG, "infoReady: ");
                size = (int) info.getTotalLength();
                if (dialog != null) {
                    dialog.show();
                    dialog.setInfo(getResources().getString(R.string.downloading), getResources().getString(R.string.wait_when_download));
                    dialog.setMax(size);
                }
            }

            @Override
            public void progressBlock(@NonNull DownloadTask task, int blockIndex, long currentBlockOffset, @NonNull SpeedCalculator blockSpeed) {
                Log.d(TAG, "progressBlock: ");
            }

            @Override
            public void progress(@NonNull DownloadTask task, long currentOffset, @NonNull SpeedCalculator taskSpeed) {
                Log.d(TAG, "progress: ");
                if (dialog != null) {
                    dialog.setCurrent(currentOffset);
                }
            }

            @Override
            public void blockEnd(@NonNull DownloadTask task, int blockIndex, BlockInfo info, @NonNull SpeedCalculator blockSpeed) {
                Log.d(TAG, "blockEnd: ");
            }

            @Override
            public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause, @NonNull SpeedCalculator taskSpeed) {
                Log.d(TAG, "taskEnd: ");
                if (cause == EndCause.COMPLETED) {
                    if (getContext() != null) {
                        StringBuilder item = new StringBuilder();
                        for (DataSpecShop data : itemShops.get(position).getPathItems()) {
                            item.append(data.getTitle()).append("_");
                        }
                        String childParentFile = parentFile.getAbsolutePath() + "/" + itemShops.get(position).getNameItem();
                        File childFile = new File(childParentFile + "unzip");

                        try {
                            InputStream inputStream = new FileInputStream(childParentFile);
                            if (!childFile.exists()) {
                                if (childFile.mkdirs()) {
                                    Log.d(TAG, "taskEnd: ");
                                }
                            }
                            FileUtil.unzip(inputStream, childFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (parentFile.isDirectory()) {
                            for (File child : parentFile.listFiles()) {
                                if (!child.isDirectory()) {
                                    if (child.delete()) {
                                        Log.d(TAG, "taskEnd: delete loaded");
                                    }
                                }
                            }
                        }
                        viewModel.insert(new ItemDownloaded(theme, childFile.getAbsolutePath(), itemShops.get(position).getNameItem()
                                , (float) ((size / 1024)) + " KB", item.toString()));
                        itemShopAdapter.notifyDataSetChanged();
                        MyTrackingFireBase.trackingScreen(getActivity(),
                                TrackingUtils.TrackingConstant.Store.DOWNLOAD_DONE + theme + " " + itemShops.get(position).getNameItem());
                        Toasty.success(getContext(), getContext().getString(R.string.download_done), Toasty.LENGTH_SHORT).show();
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                } else if (cause == EndCause.CANCELED) {
                    Toasty.normal(Objects.requireNonNull(getContext()), getString(R.string.cancel_completed), Toasty.LENGTH_SHORT).show();
                    MyTrackingFireBase.trackingScreen(getActivity(),
                            TrackingUtils.TrackingConstant.Store.DOWNLOAD_CANCEL);
                } else {
                    MyTrackingFireBase.trackingScreen(getActivity(),
                            TrackingUtils.TrackingConstant.Store.DOWNLOAD_ERROR);
                    Toasty.error(Objects.requireNonNull(getContext()), getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    @Override
    public void cancel() {
        if (task != null) {
            task.cancel();
        }
    }
}
