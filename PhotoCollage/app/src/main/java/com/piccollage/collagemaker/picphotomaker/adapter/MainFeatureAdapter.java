package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemIconShareBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;

import java.util.ArrayList;

/**
 * Main feature adapter ở màn hình home
 */
public class MainFeatureAdapter extends RecyclerView.Adapter<MainFeatureAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Icon> icons;
    private IMainFeatureListener iMainFeatureListener;

    public interface IMainFeatureListener {
        void pickMainF(int position);
    }

    public MainFeatureAdapter(Context context, ArrayList<Icon> icons, IMainFeatureListener iMainFeatureListener) {
        this.context = context;
        this.icons = icons;
        this.iMainFeatureListener = iMainFeatureListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_icon_share, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.binding.tvName.setText(icons.get(i).getName());
        Glide.with(context).load(context.getResources().getDrawable(icons.get(i).getIcon())).into(viewHolder.binding.iconShare);
        viewHolder.itemView.setOnClickListener(v -> iMainFeatureListener.pickMainF(i));
    }

    @Override
    public int getItemCount() {
        return icons.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemIconShareBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemIconShareBinding.bind(itemView);
        }
    }
}
