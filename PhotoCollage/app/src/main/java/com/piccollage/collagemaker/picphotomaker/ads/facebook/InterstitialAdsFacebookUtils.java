package com.piccollage.collagemaker.picphotomaker.ads.facebook;

import android.content.Context;
import android.util.Log;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyFirebaseConfig;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Changed by sean 03/03/2020 : Facebook Inters Ads
 */
public class InterstitialAdsFacebookUtils {
    private static final String TAG = "InterstitialAdsFacebook";
    private static InterstitialAdsFacebookUtils instances;
    private InterstitialAd interstitialAd;
    private IFbIntersAdsListener iFbIntersAdsListener;
    private boolean isPreLoadDone;

    public interface IFbIntersAdsListener {
        void close();

        void showLoading();

        void hideLoading();
    }

    public static InterstitialAdsFacebookUtils getInstance() {
        if (null == instances) {
            instances = new InterstitialAdsFacebookUtils();
        }
        return instances;
    }

    public void loadAdInApp(Context context) {
        isPreLoadDone = false;
        if (MyUtils.isAppPurchased()) {
            return;
        }
        String[] ads_id = logger.getFb_popup_openapp7(context);
        new InterstitialFacebookBuilder().loadAd(context, ads_id, 0);
    }

    public void showAdInApp(IFbIntersAdsListener iFbIntersAdsListener) {
        this.iFbIntersAdsListener = iFbIntersAdsListener;
        iFbIntersAdsListener.showLoading();
        if (MyUtils.isAppPurchased()) {
            iFbIntersAdsListener.hideLoading();
            iFbIntersAdsListener.close();
            return;
        }
        Observable.interval(0, 1, TimeUnit.SECONDS, AndroidSchedulers.mainThread()).
                doOnDispose(iFbIntersAdsListener::hideLoading).subscribe(new Observer<Long>() {
            private Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(Long aLong) {
                if (aLong >= MyFirebaseConfig.getKeyAdsTimeoutFeature()) {
                    disposable.dispose();
                    iFbIntersAdsListener.close();
                } else {
                    if (isPreLoadDone) {
                        if (interstitialAd != null && interstitialAd.isAdLoaded()) {
                            interstitialAd.show();
                        }
                    } else iFbIntersAdsListener.close();
                    disposable.dispose();
                }
            }

            @Override
            public void onError(Throwable e) {
                iFbIntersAdsListener.hideLoading();
                iFbIntersAdsListener.close();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public class InterstitialFacebookBuilder {
        public void loadAd(Context context, String[] ads_id, int count) {
            if (context == null) {
                isPreLoadDone = false;
                return;
            }
            if (ads_id == null || ads_id.length == 0 || count >= ads_id.length) {
                isPreLoadDone = false;
                return;
            }

            interstitialAd = new InterstitialAd(context, ads_id[count]);
            interstitialAd.setAdListener(new InterstitialAdListener() {
                @Override
                public void onInterstitialDisplayed(Ad ad) {
                    if (iFbIntersAdsListener != null) {
                        iFbIntersAdsListener.hideLoading();
                    }
                    Log.d(TAG, "onInterstitialDisplayed: ");
                }

                @Override
                public void onInterstitialDismissed(Ad ad) {
                    Log.d(TAG, "onInterstitialDismissed: ");
                    if (iFbIntersAdsListener != null) {
                        iFbIntersAdsListener.close();
                    }
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    loadAd(context, ads_id, count + 1);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    isPreLoadDone = true;
                }

                @Override
                public void onAdClicked(Ad ad) {
                    Log.d(TAG, "onAdClicked: ");
                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            interstitialAd.loadAd();
        }
    }
}
