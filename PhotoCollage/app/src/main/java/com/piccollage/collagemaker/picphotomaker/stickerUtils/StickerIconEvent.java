package com.piccollage.collagemaker.picphotomaker.stickerUtils;

import android.view.MotionEvent;

/**
 * Listener chứa tương tác của user với sticker view
 */
public interface StickerIconEvent {
  void onActionDown(StickerView stickerView, MotionEvent event);

  void onActionMove(StickerView stickerView, MotionEvent event);

  void onActionUp(StickerView stickerView, MotionEvent event);
}
