package com.piccollage.collagemaker.picphotomaker.ui.vipactivity.purchase;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.LocaleHelper;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingConstants;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingManager;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingUtils;

import java.util.List;
import java.util.Objects;

public abstract class BasePremiumActivity extends AppCompatActivity
        implements BillingManager.BillingUpdatesListener, PurchasesUpdatedListener {

    public abstract View getLayoutView();

    public abstract void onViewCreated();

    public abstract void setDataForViews(AppPurchases appPurchases);

    private Context mContext;
    private BillingManager mBillingManager;

    private long lastClickTime = 0;

    public boolean checkDoubleClick() {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return false;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        beforeSetContentView();
        setContentView(getLayoutView());
        onViewCreated();
        mContext = this;
        initBillingV3();
    }

    public void beforeSetContentView() {

    }

    private void initBillingV3() {
        mBillingManager = new BillingManager(this, this);
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {

    }

    @Override
    public void onBillingClientSetupFinished() {
        if (mBillingManager == null) {
            return;
        }
        getLifecycle().addObserver(new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            public void onResume() {
                PremiumHelper.getInstances().querySubscriptionDetailsAsync(mBillingManager, appPurchases -> setDataForViews(appPurchases));
            }
        });
    }

    public void onSubscriptionTrialFree(boolean isSale) {
        if (isSale) {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(1)).get(3),
                    BillingClient.SkuType.SUBS);
        } else {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(3)).get(3),
                    BillingClient.SkuType.SUBS);
        }
    }

    public void onSubscriptionMonthly(boolean isSale) {
        if (isSale) {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(1)).get(0),
                    BillingClient.SkuType.SUBS);
        } else {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(3)).get(0),
                    BillingClient.SkuType.SUBS);
        }
    }

    public void onSubscriptionMonthlyTrial(boolean isSale) {
        if (isSale) {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(1)).get(4),
                    BillingClient.SkuType.SUBS);
        } else {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(3)).get(4),
                    BillingClient.SkuType.SUBS);
        }
    }

    public void onSubscriptionMonths() {
//        mBillingManager.initiatePurchaseFlow(BillingUtils.mapSubscriptionGroup().get(groupSubscription).get(1),
//                BillingClient.SkuType.SUBS);
    }

    public void onSubscriptionYearly(boolean isSale) {
        if (isSale) {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(1)).get(2),
                    BillingClient.SkuType.SUBS);
        } else {
            mBillingManager.initiatePurchaseFlow(Objects.requireNonNull(BillingUtils.mapSubscriptionGroup().get(3)).get(2),
                    BillingClient.SkuType.SUBS);
        }
    }

    public void onPurchaseOnetimeProduct() {
        if (mBillingManager == null) {
            return;
        }
        mBillingManager.initiatePurchaseFlow(BillingConstants.SKU_PHOTOCOLLAGE002, BillingClient.SkuType.INAPP);
    }

    public void onRestorePurchase() {
        if (mBillingManager == null) {
            return;
        }
        mBillingManager.queryPurchases();
    }


    @Override
    public void onConsumeFinished(String token, int result) {
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        if (purchases != null && !purchases.isEmpty()) {
            BillingUtils.queryPurchaseFinish(mContext);
        }
    }

    @Override
    protected void onDestroy() {
        if (mBillingManager != null) {
            mBillingManager.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.setLocale(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }
}
