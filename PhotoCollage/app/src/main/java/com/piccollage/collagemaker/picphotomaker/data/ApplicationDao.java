package com.piccollage.collagemaker.picphotomaker.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;

import java.util.List;

/**
 * Create from Administrator
 * Purpose: chứa các câu lệnh truy vấn SQL
 * Des: chứa các câu lệnh truy vấn SQL
 */
@Dao
public interface ApplicationDao {

    // Data downloaded query
    @Insert
    void insertItemDownload(ItemDownloaded itemDownloaded); // insert item downloaded

    @Delete
    void deleteItemDownload(ItemDownloaded itemDownloaded); // delete item downloaded

    @Query("DELETE FROM item_downloaded ")
    void deleteAll(); // xóa toàn bộ db từ bảng item_download

    @Query("SELECT * FROM item_downloaded")
    LiveData<List<ItemDownloaded>> getAllItemsDownloaded();

    @Query("SELECT * FROM item_downloaded WHERE theme = 'Gradient'")
    LiveData<List<ItemDownloaded>> getItemsGradientDownloaded(); // lấy toàn bộ item có theme là gradient

    @Query("SELECT * FROM item_downloaded WHERE theme = 'Pattern'")
    LiveData<List<ItemDownloaded>> getItemsPatternDownloaded(); // lấy toàn bộ item có theme là pattern

    @Query("SELECT * FROM item_downloaded WHERE theme = 'Sticker'")
    LiveData<List<ItemDownloaded>> getItemsStickerDownloaded(); // lấy toàn bộ item có theme là sticker

    @Query("SELECT * FROM item_downloaded WHERE theme = 'Font'")
    LiveData<List<ItemDownloaded>> getItemsFontDownloaded(); // lấy toàn bộ item có theme là font

    @Query("SELECT * FROM item_downloaded WHERE theme = 'Palette'")
    LiveData<List<ItemDownloaded>> getItemsPaletteDownloaded(); // lấy toàn bộ item có theme là palette

    @Insert
    void insertPip(PipPicture pipPicture); // insert data pip vào db sau khi download

    @Query("SELECT * FROM pip_style")
    LiveData<List<PipPicture>> getPipPictures(); // get toàn bộ item trong bảng pip_style
}
