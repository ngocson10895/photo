package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.List;

/**
 * Photo Picked Adapter
 */
public class PhotoPickedAdapter extends RecyclerView.Adapter<PhotoPickedAdapter.ViewHolder> {
    private List<Photo> pathsPhotoPicked;
    private Context context;
    private IRemove iRemove;

    public PhotoPickedAdapter(List<Photo> pathsPhotoPicked, Context context, IRemove iRemove) {
        this.pathsPhotoPicked = pathsPhotoPicked;
        this.context = context;
        this.iRemove = iRemove;
    }

    public interface IRemove {
        void remove(Photo photo);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_photo_picked, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Glide.with(context).asBitmap().load(pathsPhotoPicked.get(i).getPathPhoto()).into(viewHolder.ivPhotoPicked);
        viewHolder.btnRemove.setOnClickListener(view -> iRemove.remove(pathsPhotoPicked.get(i)));
    }

    @Override
    public int getItemCount() {
        return pathsPhotoPicked.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPhotoPicked;
        ImageView btnRemove;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivPhotoPicked = itemView.findViewById(R.id.iv_photo_picked);
            btnRemove = itemView.findViewById(R.id.btn_remove);
        }
    }

    public void setPathsPhotoPicked(List<Photo> pathsPhotoPicked) {
        this.pathsPhotoPicked = pathsPhotoPicked;
        notifyDataSetChanged();
    }
}
