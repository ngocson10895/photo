package com.piccollage.collagemaker.picphotomaker.actions;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dauroi.com.imageprocessing.filter.ImageFilter;
import dauroi.photoeditor.config.ALog;
import dauroi.photoeditor.database.table.CropTable;
import dauroi.photoeditor.database.table.ItemPackageTable;
import dauroi.photoeditor.listener.ApplyFilterListener;
import dauroi.photoeditor.model.CropInfo;
import dauroi.photoeditor.model.ItemInfo;
import dauroi.photoeditor.utils.PhotoUtils;
import dauroi.photoeditor.utils.Utils;
import dauroi.photoeditor.view.CropImageView;
import dauroi.photoeditor.view.DrawableCropImageView;
import dauroi.photoeditor.view.DrawableCropImageView.OnDrawMaskListener;
import dauroi.photoeditor.view.MultiTouchHandler;

/**
 * Create from Administrator
 * Purpose: Action crop ảnh
 * Des: Chứa các function thực hiện action crop ảnh
 */
@SuppressLint({"UseSparseArrays", "NewApi"})
public class CropAction extends MaskAction implements OnTouchListener, OnDrawMaskListener {
    private static final String TAG = CropAction.class.getSimpleName();

    private View mDrawableCropLayout;
    private DrawableCropImageView mDrawableCropImageView;
    private ImageView mClearImageView;
    private CropImageView mRectangleCropMaskView;

    private MultiTouchHandler mTouchHandler;

    private Bundle mSavedInstanceSquareData;
    private Bundle mSavedInstanceCustomData;
    private Bundle mSavedInstanceDrawData;

    public CropAction(EditorActivity activity) {
        super(activity, ItemPackageTable.CROP_TYPE);
    }

    @Override
    protected void onInit() {
        super.onInit();
        mSelectedItemIndexes = new HashMap<>();
        mListViewPositions = new HashMap<>();
    }

    @Override
    public void saveInstanceState(Bundle bundle) {
        super.saveInstanceState(bundle);
        bundle.putParcelable("dauroi.photoeditor.actions.CropAction.mTouchHandler", mTouchHandler);
        if (mCurrentPosition == 0) {
            if (mSavedInstanceSquareData == null) {
                mSavedInstanceSquareData = new Bundle();
            }
            mRectangleCropMaskView.saveInstanceState(mSavedInstanceSquareData);
        } else if (mCurrentPosition == 1) {
            if (mSavedInstanceCustomData == null) {
                mSavedInstanceCustomData = new Bundle();
            }
            mRectangleCropMaskView.saveInstanceState(mSavedInstanceCustomData);
        } else if (mCurrentPosition == 2) {
            if (mSavedInstanceDrawData == null) {
                mSavedInstanceDrawData = new Bundle();
            }
            mDrawableCropImageView.saveInstanceState(mSavedInstanceDrawData);
        }
        bundle.putBundle("dauroi.photoeditor.actions.CropAction.mSavedInstanceSquareData", mSavedInstanceSquareData);
        bundle.putBundle("dauroi.photoeditor.actions.CropAction.mSavedInstanceCustomData", mSavedInstanceCustomData);
        bundle.putBundle("dauroi.photoeditor.actions.CropAction.mSavedInstanceDrawData", mSavedInstanceDrawData);
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        super.restoreInstanceState(bundle);
        ALog.d(TAG, "restoreInstanceState");
        MultiTouchHandler touchHandler = bundle.getParcelable("dauroi.photoeditor.actions.CropAction.mTouchHandler");
        if (touchHandler != null) {
            mTouchHandler = touchHandler;
        }

        mSavedInstanceSquareData = bundle.getBundle("dauroi.photoeditor.actions.CropAction.mSavedInstanceSquareData");
        mSavedInstanceCustomData = bundle.getBundle("dauroi.photoeditor.actions.CropAction.mSavedInstanceCustomData");
        mSavedInstanceDrawData = bundle.getBundle("dauroi.photoeditor.actions.CropAction.mSavedInstanceDrawData");
    }

    @SuppressLint({"ClickableViewAccessibility", "InflateParams"})
    @Override
    public View inflateMenuView() {
        mRootActionView = mLayoutInflater.inflate(R.layout.photo_editor_action_crop, null);
        // attach crop image view
        mRectangleCropMaskView = new CropImageView(activity);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT, Gravity.CENTER);
        mRectangleCropMaskView.setLayoutParams(params);
        mRectangleCropMaskView.setPaintMode(true);
        // drawable crop layout
        mDrawableCropLayout = mLayoutInflater.inflate(R.layout.photo_editor_crop_mask_draw, null);
        mDrawableCropImageView = mDrawableCropLayout.findViewById(R.id.drawbleCropView);
        mDrawableCropImageView.setOnDrawMaskListener(this);
        mClearImageView = mDrawableCropLayout.findViewById(R.id.clearImage);

        activity.getNormalImageView().setOnTouchListener(this);

        mClearImageView.setOnClickListener(v -> mDrawableCropImageView.clear());

        mCurrentPosition = DEFAULT_CROP_SELECTED_ITEM_INDEX;

        return mRootActionView;
    }

    @Override
    public String getActionName() {
        return "CropAction";
    }

    @Override
    public void attach() {
        super.attach();
        ALog.d(TAG, "attach");
        activity.getNormalImageView().setVisibility(View.VISIBLE);
        mMaskLayout.setVisibility(View.VISIBLE);
        activity.applyFilter(new ImageFilter());
        if (mTouchHandler != null) {
            pinchImage();
        } else {
            mTouchHandler = new MultiTouchHandler();
            mTouchHandler.setEnableRotation(true);
            initSourceImageView();
        }

        mMaskLayout.post(() -> activity.getImageProcessingView().setVisibility(View.GONE));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.attachMaskView(null);
        mDrawableCropImageView.clear();
        mMaskLayout.setVisibility(View.GONE);
        activity.getNormalImageView().setVisibility(View.GONE);
        activity.getImageProcessingView().setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResume() {
        super.onActivityResume();
        ALog.d(TAG, "onActivityResume");
    }

    // Action khi user click vào square view
    private void clickSquareView() {
        if (mSavedInstanceSquareData == null) {
            mRectangleCropMaskView.restoreInstanceState(mSavedInstanceSquareData);
        } else {
            mRectangleCropMaskView.setPaintMode(true);
            mRectangleCropMaskView.setBackgroundColor(Color.TRANSPARENT);

            final float width = activity.getPhotoViewWidth();
            final float height = activity.getPhotoViewHeight();
            float ratio = 1.0f;
            int size = (int) (Math.min(width, height));
            float clipWidth = size / 2.0f;
            float clipHeight = clipWidth / ratio;
            if (clipHeight > size / 2.0f) {
                clipHeight = size / 2.0f;
                clipWidth = size * ratio;
            }

            RectF rect = new RectF();
            rect.top = height / 2 - clipHeight / 2;
            rect.bottom = rect.top + clipHeight;
            rect.left = width / 2 - clipWidth / 2;
            rect.right = rect.left + clipWidth;

            mRectangleCropMaskView.setCropArea(rect);

            mRectangleCropMaskView.setRatio(ratio);
        }
    }

    // Action khi user click vào custom view
    private void clickCustomView() {
        if (mSavedInstanceCustomData != null) {
            mRectangleCropMaskView.restoreInstanceState(mSavedInstanceCustomData);
        } else {
            mRectangleCropMaskView.setPaintMode(true);
            mRectangleCropMaskView.setBackgroundColor(Color.TRANSPARENT);
            RectF rect = new RectF();
            final float width = activity.getPhotoViewWidth();
            final float height = activity.getPhotoViewHeight();
            rect.top = height / 3.0f;
            rect.bottom = 2 * height / 3.0f;
            rect.left = width / 3.0f;
            rect.right = 2 * width / 3.0f;
            mRectangleCropMaskView.setCropArea(rect);
            mRectangleCropMaskView.setRatio(CropImageView.CUSTOM_SIZE);
        }
    }

    private void initSourceImageView() {
        Matrix m = new Matrix();
        float ratio = activity.calculateScaleRatio();
        int[] thumbnail = activity.calculateThumbnailSize();
        m.postScale(1.0f / ratio, 1.0f / ratio, activity.getImageWidth() / 2.0f, activity.getImageHeight() / 2.0f);

        float dx = (activity.getImageWidth() - thumbnail[0]) / 2.0f;
        float dy = (activity.getImageHeight() - thumbnail[1]) / 2.0f;
        m.postTranslate(-dx, -dy);

        dx = (activity.getPhotoViewWidth() - thumbnail[0]) / 2.0f;
        dy = (activity.getPhotoViewHeight() - thumbnail[1]) / 2.0f;
        m.postTranslate(dx, dy);

        activity.getNormalImageView().setImageMatrix(m);
        mTouchHandler.setMatrix(m);
        mTouchHandler.setScale(ratio);
    }

    // reset touch handler
    public void reset() {
        mTouchHandler = null;
        mSavedInstanceCustomData = null;
        mSavedInstanceDrawData = null;
        mSavedInstanceSquareData = null;
    }

    @Override
    public void apply(final boolean finish) {
        if (!isAttached()) {
            return;
        }
        ApplyFilterTask task = new ApplyFilterTask(activity, new ApplyFilterListener() {
            String errMsg = null;

            @Override
            public void onFinishFiltering() {
                // reset touch handler
                mTouchHandler = null;
                activity.getNormalImageView().setImageBitmap(null);
                mSavedInstanceCustomData = null;
                mSavedInstanceDrawData = null;
                mSavedInstanceSquareData = null;
                mCurrentPosition = DEFAULT_CROP_SELECTED_ITEM_INDEX;
                for (ItemInfo itemInfo : mMenuItems) {
                    itemInfo.setSelected(false);
                }
                mMenuItems.get(0).setSelected(true);
                mCurrentPackageId = 0;
                mSelectedItemIndexes.clear();
                mListViewPositions.clear();
                mCurrentPackageFolder = null;

                if (finish) {
                    done();
                }

                if (activity.getRotationAction() != null) {
                    activity.getRotationAction().reset();
                }
            }

            @Override
            public Bitmap applyFilter() {
                Bitmap bm = null;
                try {
                    if (mCurrentPackageId == 0 && mCurrentPosition < 2) {
                        bm = rectangleCrop();
                    } else if (mCurrentPackageId == 0 && mCurrentPosition == 2) {
                        bm = cropDrawnImage();
                    } else {
                        bm = cropFrame(mCurrentPosition);
                    }
                } catch (OutOfMemoryError err) {
                    errMsg = err.getMessage();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return bm;
            }
        });

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private Bitmap cropDrawnImage() {
        return mDrawableCropImageView.cropImage(true);
    }

    /**
     * This function will recycle image in ImageProcessingView. So must set
     * result image to image processingview.
     *
     * @param position : vị trí ảnh crop
     * @return : bitmap của ảnh
     */
    private Bitmap cropFrame(int position) throws OutOfMemoryError {
        final ItemInfo info = mMenuItems.get(position);
        if (info.getShowingType() != ItemInfo.NORMAL_ITEM_TYPE) {
            return null;
        }
        // create normal mask image and normal image
        final float ratio = ((float) activity.getPhotoViewWidth()) / activity.getPhotoViewHeight();
        Bitmap normalImage = PhotoUtils.transparentPadding(activity.getImage(), ratio);
        if (normalImage != activity.getImage()) {
            if (activity.getImage() != null && !activity.getImage().isRecycled()) {
                activity.getImage().recycle();
            }
        }
        Bitmap mask = PhotoUtils.decodePNGImage(activity, ((CropInfo) info).getBackground());
        Bitmap normalMask = null;
        if (mask != null) {
            normalMask = PhotoUtils.transparentPadding(mask, ratio);
        }
        if (normalMask != mask) {
            if (!mask.isRecycled()) {
                mask.recycle();
            }
            System.gc();
        }

        if (normalMask != null) {
            mask = Bitmap.createScaledBitmap(normalMask, normalImage.getWidth(), normalImage.getHeight(), true);
            if (mask != normalMask) {
                if (!normalMask.isRecycled()) {
                    normalMask.recycle();
                }
                System.gc();
            }
        }

        Bitmap croppedImage = PhotoUtils.cropImage(normalImage, mask, mTouchHandler.getScaleMatrix());
        if (croppedImage != normalImage) {
            if (!normalImage.isRecycled()) {
                normalImage.recycle();
            }
            System.gc();
        }

        Bitmap result = PhotoUtils.cleanImage(croppedImage);
        if (result != croppedImage) {
            if (!croppedImage.isRecycled()) {
                croppedImage.recycle();
                System.gc();
            }
        }

        return result;
    }

    // Khi user click vào rectangle crop
    private Bitmap rectangleCrop() {
        final RectF cropArea = mRectangleCropMaskView.getCropArea();
        final float ratio = activity.calculateScaleRatio();
        final int[] thumbnailSize = activity.calculateThumbnailSize();
        final int dx = (activity.getPhotoViewWidth() - thumbnailSize[0]) / 2;
        final int dy = (activity.getPhotoViewHeight() - thumbnailSize[1]) / 2;
        final float left = Math.max((cropArea.left - dx) * ratio, 0);
        final float right = Math.min((cropArea.right - dx) * ratio, activity.getImageWidth());
        final float top = Math.max((cropArea.top - dy) * ratio, 0);
        final float bottom = Math.min((cropArea.bottom - dy) * ratio, activity.getImageHeight());
        return Bitmap.createBitmap(activity.getImage(), (int) left, (int) top, (int) (right - left),
                (int) (bottom - top));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (isAttached()) {
            if (event != null && mTouchHandler != null) {
                mTouchHandler.touch(event);
            }
            pinchImage();
            return true;
        } else {
            return false;
        }
    }

    private void pinchImage() {
        activity.getNormalImageView().setImageMatrix(mTouchHandler.getMatrix());
    }

    @Override
    public void onStartDrawing() {
        if (mClearImageView != null) {
            mClearImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFinishDrawing() {
        if (mClearImageView != null) {
            mClearImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected int getMaskLayoutRes() {
        return R.layout.photo_editor_mask_layout;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void selectNormalItem(int position) {
        if (!isAttached()) {
            return;
        }
        // save info
        final ItemInfo currentInfo = mMenuItems.get(mCurrentPosition);
        if (currentInfo.getShowingType() == ItemInfo.SQUARE_CROP_TYPE) {
            if (mSavedInstanceSquareData == null) {
                mSavedInstanceSquareData = new Bundle();
            }
            mRectangleCropMaskView.saveInstanceState(mSavedInstanceSquareData);
        } else if (currentInfo.getShowingType() == ItemInfo.CUSTOM_CROP_TYPE) {
            if (mSavedInstanceCustomData == null) {
                mSavedInstanceCustomData = new Bundle();
            }
            mRectangleCropMaskView.saveInstanceState(mSavedInstanceCustomData);
        } else if (currentInfo.getShowingType() == ItemInfo.DRAW_CROP_TYPE) {
            if (mSavedInstanceDrawData == null) {
                mSavedInstanceDrawData = new Bundle();
            }
            mDrawableCropImageView.saveInstanceState(mSavedInstanceDrawData);
        }

        activity.getNormalImageView().setImageBitmap(activity.getImage());
        final ItemInfo info = mMenuItems.get(position);
        if (info.getShowingType() == ItemInfo.SQUARE_CROP_TYPE) {
            activity.getNormalImageView().setImageMatrix(new Matrix());
            activity.getNormalImageView().setOnTouchListener(null);
            activity.getNormalImageView().setScaleType(ScaleType.FIT_CENTER);
            activity.attachMaskView(mRectangleCropMaskView);
            clickSquareView();
            mCurrentPosition = position;
        } else if (info.getShowingType() == ItemInfo.CUSTOM_CROP_TYPE) {
            activity.getNormalImageView().setImageMatrix(new Matrix());
            activity.getNormalImageView().setOnTouchListener(null);
            activity.getNormalImageView().setScaleType(ScaleType.FIT_CENTER);
            activity.attachMaskView(mRectangleCropMaskView);
            clickCustomView();
            mCurrentPosition = position;
        } else if (info.getShowingType() == ItemInfo.DRAW_CROP_TYPE) {
            activity.getNormalImageView().setImageMatrix(new Matrix());
            activity.getNormalImageView().setOnTouchListener(null);
            activity.getNormalImageView().setScaleType(ScaleType.FIT_CENTER);
            if (mSavedInstanceDrawData != null) {
                mDrawableCropImageView.restoreInstanceState(mSavedInstanceDrawData);
                mDrawableCropImageView.setFingerDrawingMode(true);
            }
            mDrawableCropImageView.setBitmap(activity.getImage());
            activity.attachMaskView(mDrawableCropLayout);
            mCurrentPosition = position;
        } else if (info.getShowingType() == ItemInfo.NORMAL_ITEM_TYPE) {
            activity.attachMaskView(mMaskLayout);
            // recycle old bg
            Drawable drawable = mImageMaskView.getBackground();
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bd = (BitmapDrawable) drawable;
                Bitmap bm = bd.getBitmap();
                if (bm != null && !bm.isRecycled()) {
                    mImageMaskView.setBackgroundColor(Color.TRANSPARENT);
                    bm.recycle();
                    System.gc();
                }
            }

            String bgPath = ((CropInfo) info).getForeground();
            Bitmap bg = PhotoUtils.decodePNGImage(activity, bgPath);
            if (bg != null) {
                adjustImageMaskLayout(bg.getWidth(), bg.getHeight());
            }
            mImageMaskView.setBackground(new BitmapDrawable(activity.getResources(), bg));

            activity.getNormalImageView().setOnTouchListener(this);
            activity.getNormalImageView().setScaleType(ScaleType.MATRIX);
            activity.getNormalImageView().post(() -> {
                if (mTouchHandler != null)
                    activity.getNormalImageView().setImageMatrix(mTouchHandler.getMatrix());
            });

            mCurrentPosition = position;
        }
    }

    // load data từ db. Các item crop ảnh
    @Override
    protected List<? extends ItemInfo> loadNormalItems(long packageId, String packageFolder) {
        CropTable cropTable = new CropTable(activity);
        List<CropInfo> cropInfos = cropTable.getAllRows(packageId);
        if (packageFolder != null && packageFolder.length() > 0) {
            final String baseFolder = Utils.CROP_FOLDER.concat("/").concat(packageFolder).concat("/");
            for (CropInfo info : cropInfos) {
                info.setForeground(baseFolder.concat(info.getForeground()));
                info.setThumbnail(baseFolder.concat(info.getThumbnail()));
                if (info.getSelectedThumbnail() != null && info.getSelectedThumbnail().length() > 0)
                    info.setSelectedThumbnail(baseFolder.concat(info.getSelectedThumbnail()));
                info.setBackground(baseFolder.concat(info.getBackground()));
            }
        }

        List<ItemInfo> itemInfos = new ArrayList<>();
        if (packageId < 1) {
            final ItemInfo squareItem = new ItemInfo();
            squareItem.setTitle(activity.getString(R.string.photo_editor_square));
            squareItem.setThumbnail(PhotoUtils.DRAWABLE_PREFIX + R.drawable.ic_crop_ic_square);
            squareItem.setSelectedThumbnail(PhotoUtils.DRAWABLE_PREFIX + R.drawable.ic_crop_ic_square_selected);
            squareItem.setShowingType(ItemInfo.SQUARE_CROP_TYPE);
            itemInfos.add(0, squareItem);

            final ItemInfo customItem = new ItemInfo();
            customItem.setTitle(activity.getString(R.string.photo_editor_custom));
            customItem.setThumbnail(PhotoUtils.DRAWABLE_PREFIX + R.drawable.ic_crop_ic_square_custom);
            customItem.setSelectedThumbnail(PhotoUtils.DRAWABLE_PREFIX + R.drawable.ic_crop_ic_square_custom_selected);
            customItem.setShowingType(ItemInfo.CUSTOM_CROP_TYPE);
            itemInfos.add(1, customItem);

            final ItemInfo drawItem = new ItemInfo();
            drawItem.setTitle(activity.getString(R.string.photo_editor_draw));
            drawItem.setThumbnail(PhotoUtils.DRAWABLE_PREFIX + R.drawable.ic_crop_ic_draw);
            drawItem.setSelectedThumbnail(PhotoUtils.DRAWABLE_PREFIX + R.drawable.ic_crop_ic_draw_selected);
            drawItem.setShowingType(ItemInfo.DRAW_CROP_TYPE);
            itemInfos.add(2, drawItem);
        }

        itemInfos.addAll(cropInfos);
        return itemInfos;
    }
}
