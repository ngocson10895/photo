package com.piccollage.collagemaker.picphotomaker.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogRateBinding;
import com.piccollage.collagemaker.picphotomaker.ui.homeactivity.HomeActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

/**
 * Create from Administrator
 * Purpose: Dialog rate app
 * Des: điều hường user tới store để rate app
 */
public class DialogRate extends BaseDialog {
    private Activity context;
    private DialogRateBinding binding;

    DialogRate(@NonNull Activity context) {
        super(context);
        this.context = context;
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogRate.NAME);
    }

    @Override
    public void beforeSetContentView() {
        binding = DialogRateBinding.inflate(getLayoutInflater());
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = ViewGroup.LayoutParams.WRAP_CONTENT;
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initView() {
        binding.tvRate.setOnClickListener(v -> {
            dismiss();
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.URL_APP)));
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogRate.RATE);
        });
        binding.tvSkip.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogRate.SKIP);
            dismiss();
            Intent intent = new Intent(context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
            context.overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        });
    }
}
