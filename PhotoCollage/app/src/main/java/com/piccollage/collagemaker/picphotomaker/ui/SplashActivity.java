package com.piccollage.collagemaker.picphotomaker.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.android.billingclient.api.Purchase;
import com.google.android.gms.corebase.lg;
import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.InterstitialAdsOpenUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyFirebaseConfig;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.ui.homeactivity.HomeActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingManager;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: màn hình splash của app
 * Des: để load trước 1 số libs và db
 */
public class SplashActivity extends AppCompatActivity implements BillingManager.BillingUpdatesListener {
    private PermissionChecker checker;
    private ApplicationViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);
        fullScreen();

        Advertisement.initSdk(this); // đăng ký khởi tạo logger
        NativeAdAdapter.loadAds(this, logger.getAdmob_native_tophome3(this));
        InterstitialAdsOpenUtils.getInstances().loadAdIfNeeded(); // load trước quảng cáo open app
        Advertisement.showBannerAdOther(this, null, true, null); // load trước banner

        if (!isTaskRoot()) {
            if (getIntent().hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(getIntent().getAction())) {
                lg.logs("SplashActivity onCreate SplashActivity is not the root.  Finishing Activity instead of launching.");
                finish();
                return;
            }
        }
        new BillingManager(this, this);

        checker = new PermissionChecker(this); // check permission
        checker.setiPermissionListener(new PermissionChecker.IPermissionListener() {
            @Override
            public void doneAll() {
                directToMainActivity(MyFirebaseConfig.getKeySplashDelay());
            }

            @Override
            public void cancel() {
                finish();
            }
        });

        // nếu có đủ permission thì cho vào app sau 3s ( có thể chỉnh trên firebase config)
        if (checker.isPermissionOK()) {
            directToMainActivity(MyFirebaseConfig.getKeySplashDelay()); // MyFirebaseConfig.getKeySplashDelay());
        } else {
            Toasty.warning(this, R.string.need_permission, Toasty.LENGTH_SHORT).show();
        }

        // load data default chỉ lần đầu tiên tải app
        if (sharedPrefs.get("PreloadData", Boolean.class)) {
            preloadData();
            sharedPrefs.put("PreloadData", false);
        }
    }

    /*
    load data pip default và lưu vào trong database
    VD: tên thư mục: 1#pipstyle31 - pip style loại 1 ( sử dụng 1 ảnh)
    trong 1 thư mục bảo gồm: 2 + loại ảnh. VD: loại 1 thì thư mục có 3 ảnh. loại 2 thì thư mục có 4 ảnh
    trong đó bao gồm: 1 ảnh preview, 1 ảnh foreground và n ảnh masks ( n = type)
    tọa độ của ảnh mask được lưu trong tên của ảnh được ngăn cách bởi dấu "_".
    VD: 0_30_0_153_167: 0 = 0+1: type, 30 = 30+1: tên của pip, 0: vị trí của ảnh mask, 0x = 153, 0y = 167
     */
    private void preloadData() {
        for (String s : FileUtil.listAssetFiles("pip", this)) {
            PipPicture pipPicture = new PipPicture();
            pipPicture.setType(Integer.parseInt(s.split("#")[0]));
            for (String s1 : FileUtil.listAssetFiles("pip/" + s, this)) {
                if (s1.contains("preview"))
                    pipPicture.setPathPreview("file:///android_asset/pip/" + s + "/" + s1);
                else if (s1.contains("fg"))
                    pipPicture.setForeGround(s + "/" + s1);
            }
            pipPicture.setId(FileUtil.listAssetFiles("pip", this).indexOf(s));
            pipPicture.setPathPreviewOnline("file:///android_asset/pip/" + s);
            viewModel.insertPip(pipPicture);
        }
    }

    /*
    cho kiểm tra quyền của user. nếu user cấp quyền thì cho vào app. nếu không thì exit app
    ( làm theo follow viva video )
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        checker.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    // full screen splash
    private void fullScreen() {
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void directToMainActivity(long delayedTime) {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            finish();
        }, delayedTime);
    }

    @Override
    public void onBillingClientSetupFinished() {
    }

    @Override
    public void onConsumeFinished(String token, int result) {
    }

    @Override
    public void onPurchasesUpdated(List<Purchase> purchases) {
        if (purchases != null)
            lg.logs("onPurchasesUpdated" + purchases.size());
        // check in app purchase của user
        if (purchases != null && !purchases.isEmpty()) {
            BillingUtils.verifyPurchase(this, true);
        } else {
            BillingUtils.verifyPurchase(this, false);
        }
    }
}
