package com.piccollage.collagemaker.picphotomaker.eventbus;

import com.piccollage.collagemaker.picphotomaker.entity.PipTheme;

import java.util.ArrayList;

/**
 * event khi load pip từ api
 */
public class EventPipApi {
    private ArrayList<PipTheme> pipThemes;

    public EventPipApi(ArrayList<PipTheme> pipThemes) {
        this.pipThemes = pipThemes;
    }

    public ArrayList<PipTheme> getPipThemes() {
        return pipThemes;
    }
}
