package com.piccollage.collagemaker.picphotomaker.utils.uui;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * custom linear layout manager to disable scroll or not
 */
public class ObservableLinearManager extends LinearLayoutManager {
    // true if we can scroll (not locked)
    // false if we cannot scroll (locked)
    private boolean mScrollable = false;

    public void setScrollingEnabled(boolean enabled) {
        mScrollable = enabled;
    }

    public ObservableLinearManager(Context context) {
        super(context);
    }

    public ObservableLinearManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public ObservableLinearManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean canScrollHorizontally() {
        if (getOrientation() == LinearLayoutManager.HORIZONTAL){
            return mScrollable && super.canScrollVertically();
        } else return false;
    }

    @Override
    public boolean canScrollVertically() {
        if (getOrientation() == LinearLayoutManager.VERTICAL){
            return mScrollable && super.canScrollVertically();
        } else return false;
    }
}
