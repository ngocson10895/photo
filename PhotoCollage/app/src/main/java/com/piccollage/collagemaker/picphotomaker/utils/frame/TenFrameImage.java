package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.PointF;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.List;

/**
 * Ten Frame Image
 */
public class TenFrameImage {
    public static void collage_10_8(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.2f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.2f, 0, 1, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.2f, 0.2f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.2f, 0.2f, 0.5f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.2f, 0.8f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.2f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.8f, 0.2f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.8f, 0.5f, 1, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0, 0.8f, 0.8f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.8f, 0.8f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_7(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.2f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.2f, 0, 1, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.2f, 0.2f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.2f, 0.2f, 0.6f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.6f, 0.2f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.2f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.2f, 0.5f, 0.6f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6f, 0.5f, 1, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0, 0.8f, 0.6f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.6f, 0.8f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_6(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.25f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.25f, 0.25f, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 0.75f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.25f, 0.5f, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 0.75f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.75f, 0.25f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.5f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_5(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.2f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.2f, 0.25f, 0.8f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.8f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.2f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.2f, 0.5f, 0.8f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.8f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.5f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_4(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 0.8f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.8f, 0, 1, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.2f, 0.8f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.8f, 0.2f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 0.8f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.8f, 0.5f, 1, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.5f, 0.8f, 0.8f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.8f, 0.8f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_3(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.7f, 0.3f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.7f, 0, 1, 0.3f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.3f, 0.7f, 0.7f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.7f, 0.3f, 0.9f, 0.7f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.9f, 0.3f, 1, 0.7f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.7f, 0.3f, 0.9f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.9f, 0.3f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3f, 0.7f, 0.7f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.7f, 0.7f, 1, 0.9f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.7f, 0.9f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_2(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.2f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.2f, 0.5f, 0.5f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 0.8f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.8f, 0.5f, 1, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.8f, 0.2f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.2f, 0.8f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.5f, 0.8f, 0.8f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.8f, 0.8f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_1(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.75f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.25f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.25f, 0.3333f, 0.5f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.5f, 0.3333f, 0.75f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.75f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.25f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.25f, 0.6666f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.75f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_10_0(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.2f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.2f, 0, 0.8f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.8f, 0, 1, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.2f, 0.2f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.8f, 0.2f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.2f, 0.8f, 0.8f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.8f, 0.8f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.8f, 0.2f, 1, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.2f, 0.2f, 0.8f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //tenth frame
        if (position == 9) {
            photosPicked.get(position).bound.set(0.2f, 0.5f, 0.8f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }
}
