package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogDeleteStoreBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.io.File;
import java.util.Objects;

/**
 * Create from Administrator
 * Purpose: dialog delete từng item trong màn hình manage
 * Des: để user có thể xóa từng item
 */
public class DialogDelete extends BaseDialog {
    private IReload iReload;
    private ApplicationViewModel viewModel;
    private String name, theme, url, fileSize, itemName;
    private int id;
    private FragmentActivity fragmentActivity;
    private File file;
    private DialogDeleteStoreBinding binding;

    public interface IReload {
        void reload();
    }

    DialogDelete(@NonNull FragmentActivity activity, IReload iReload) {
        super(activity);
        this.fragmentActivity = activity;
        this.iReload = iReload;
    }

    @Override
    public void beforeSetContentView() {
        binding = DialogDeleteStoreBinding.inflate(getLayoutInflater());
    }

    @Override
    public void initView() {
        viewModel = new ViewModelProvider(fragmentActivity).get(ApplicationViewModel.class);
        onViewClicked();
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    void setInfo(String name, String theme, int id, String url, String fileSize, String itemName) {
        this.name = name;
        this.theme = theme;
        this.url = url;
        this.fileSize = fileSize;
        this.itemName = itemName;
        this.id = id;
    }

    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.9);
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogDelete.NAME);
    }

    public void onViewClicked() {
        binding.btnCancel.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogDelete.CANCEL);
            dismiss();
        });
        binding.btnDelete.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogDelete.DELETE);
            deleteItem();
            dismiss();
        });
    }

    /**
     * Delete item
     */
    private void deleteItem() {
        ItemDownloaded item = new ItemDownloaded(theme, url, name, fileSize, itemName);
        if (theme.equals(Constant.PALETTE) || theme.equals(Constant.GRADIENT)) {
            String[] code = url.split("_");
            if (code[code.length - 1].length() > 6) {
                file = new File(code[code.length - 1]);
            }
        } else
            file = new File(url);
        FileUtil.deleteFile(file); // xóa toàn bộ file tải về
        item.setId(id);
        viewModel.delete(item); // xóa trong db
        iReload.reload();
    }
}
