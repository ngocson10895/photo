package com.piccollage.collagemaker.picphotomaker.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.EachFragment;

import java.util.List;

/**
 * adapter cho viewpager ở màn hình shop
 */
public class PagerAdapterShop extends FragmentStatePagerAdapter {
    private List<String> listTitleShop, listUrlEachApi;

    public PagerAdapterShop(FragmentManager fm, List<String> listTitleShop, List<String> listUrlEachApi) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.listTitleShop = listTitleShop;
        this.listUrlEachApi = listUrlEachApi;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return EachFragment.newInstance(listUrlEachApi.get(position), listTitleShop.get(position));
    }

    @Override
    public int getCount() {
        return listTitleShop.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return listTitleShop.get(position);
    }
}
