package com.piccollage.collagemaker.picphotomaker.stickerUtils;

/**
 * Event Flip Vertically
 */
public class FlipVerticallyEvent extends AbstractFlipEvent {

  @Override
  @StickerView.Flip protected int getFlipDirection() {
    return StickerView.FLIP_VERTICALLY;
  }
}
