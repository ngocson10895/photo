package com.piccollage.collagemaker.picphotomaker.ads.facebook;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;

public class BannerAdsFacebookUtils {
    private static final String TAG = "BannerAdsFacebookUtils";
    private static BannerAdsFacebookUtils instances;

    public static BannerAdsFacebookUtils getInstances() {
        if (null == instances) {
            instances = new BannerAdsFacebookUtils();
        }
        return instances;
    }

    public void loadBannerAdHome(Context context, ViewGroup container, AdSize adSize, IFacebookAdsListener iFacebookAdsListener) {
        String[] banner_id = logger.getFb_banner_home0(context);
        new AdBuilder().loadAd(context, container, adSize, banner_id, 0, iFacebookAdsListener);
    }

    public void loadBannerAdExitApp(Context context) {
        String[] banner_id = logger.getFb_banner_exit2(context);
        new AdBuilder().preLoadExitAd(context, AdSize.RECTANGLE_HEIGHT_250, banner_id, 0);
    }

    public void runBannerAdExitApp(ViewGroup viewGroup) {
        new AdBuilder().addMyBannerView(viewGroup, ConstantAds.BANNER_FACEBOOK_AD_EXIT);
    }

    public void loadBannerAdOthers(Context context, ViewGroup container, IFacebookAdsListener iFacebookAdsListener) {
        String[] banner_id = logger.getFb_banner_other1(context);
        new AdBuilder().loadAd(context, container, AdSize.BANNER_HEIGHT_50, banner_id, 0, iFacebookAdsListener);
    }

    public void loadBannerAdLock(Context context, ViewGroup container, IFacebookAdsListener iFacebookAdsListener) {
        String[] banner_id = logger.getFb_banner_other1(context);
        new AdBuilder().loadAd(context, container, AdSize.BANNER_HEIGHT_50, banner_id, 0, iFacebookAdsListener);
    }

    public interface IFacebookAdsListener {
        void loadSuccess();
    }

    public static class AdBuilder {
        void preLoadExitAd(Context context, AdSize adSize, String[] banner_id, int count) {
            if (context == null) {
                return;
            }
            if (banner_id == null || banner_id.length == 0 || count >= banner_id.length) {
                return;
            }
            AdView adView = new AdView(context, banner_id[count], adSize);
            adView.setAdListener(new AdListener() {
                @Override
                public void onError(Ad ad, AdError adError) {
                    Log.d(TAG, "onError: ");
                    preLoadExitAd(context, adSize, banner_id, count + 1);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    Log.d(TAG, "onAdLoaded: ");
                    ConstantAds.BANNER_FACEBOOK_AD_EXIT = adView;
                }

                @Override
                public void onAdClicked(Ad ad) {

                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            adView.loadAd();
        }

        public void loadAd(Context context, ViewGroup container, AdSize adSize, String[] banner_id, int count, IFacebookAdsListener iFacebookAdsListener) {
            if (context == null) {
                return;
            }
            if (banner_id == null || banner_id.length == 0 || count >= banner_id.length) {
                return;
            }
            AdView adView = new AdView(context, banner_id[count], adSize);

            adView.setAdListener(new AdListener() {
                @Override
                public void onError(Ad ad, AdError adError) {
                    Log.d(TAG, "onError: ");
                    loadAd(context, container, adSize, banner_id, count + 1, iFacebookAdsListener);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    if (container != null) {
                        addMyBannerView(container, adView);
                        iFacebookAdsListener.loadSuccess();
                    }
                }

                @Override
                public void onAdClicked(Ad ad) {

                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            adView.loadAd();
        }

        private void addMyBannerView(ViewGroup container, View view) {
            if (container == null || view == null) {
                return;
            }
            if (MyUtils.isAppPurchased()) {
                return;
            }
            try {
                if (view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeAllViews();
                }
                container.setVisibility(View.VISIBLE);
                container.removeAllViews();
                container.addView(view);
            } catch (Exception ignored) {
            }
        }
    }
}
