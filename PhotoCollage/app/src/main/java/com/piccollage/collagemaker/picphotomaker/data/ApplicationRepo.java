package com.piccollage.collagemaker.picphotomaker.data;

import android.app.Application;

import androidx.lifecycle.LiveData;

import android.os.AsyncTask;

import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.entity.Quote;

import java.util.List;

/**
 * nơi tập trung các thao tác với db
 */
public class ApplicationRepo {
    private ApplicationDao applicationDao;
    private LiveData<List<ItemDownloaded>> allItemsDownloaded;
    private LiveData<List<ItemDownloaded>> dataGradients;
    private LiveData<List<ItemDownloaded>> dataPatterns;
    private LiveData<List<ItemDownloaded>> dataStickers;
    private LiveData<List<ItemDownloaded>> dataFonts;
    private LiveData<List<ItemDownloaded>> dataPalettes;
    private LiveData<List<PipPicture>> pipPictures;

    public ApplicationRepo(Application application) {
        ApplicationDatabase database = ApplicationDatabase.getInstance(application);
        applicationDao = database.applicationDao();
        allItemsDownloaded = applicationDao.getAllItemsDownloaded();
        dataGradients = applicationDao.getItemsGradientDownloaded();
        dataPatterns = applicationDao.getItemsPatternDownloaded();
        dataStickers = applicationDao.getItemsStickerDownloaded();
        dataFonts = applicationDao.getItemsFontDownloaded();
        dataPalettes = applicationDao.getItemsPaletteDownloaded();
        pipPictures = applicationDao.getPipPictures();
    }

    public LiveData<List<PipPicture>> getPipPictures() {
        return pipPictures;
    }

    public void insertPIP(PipPicture pipPicture) {
        new InsertPIP(applicationDao).execute(pipPicture);
    }

    private static class InsertPIP extends AsyncTask<PipPicture, Void, Void> {

        private ApplicationDao applicationDao;

        InsertPIP(ApplicationDao applicationDao) {
            this.applicationDao = applicationDao;
        }

        @Override
        protected Void doInBackground(PipPicture... pipPictures) {
            applicationDao.insertPip(pipPictures[0]);
            return null;
        }
    }

    public void insertItem(ItemDownloaded itemDownloaded) {
        new Insert(applicationDao).execute(itemDownloaded);
    }

    public void deleteItem(ItemDownloaded itemDownloaded) {
        new Delete(applicationDao).execute(itemDownloaded);
    }

    public void deleteAll() {
        new DeleteAll(applicationDao).execute();
    }

    public LiveData<List<ItemDownloaded>> getAllData() {
        return allItemsDownloaded;
    }

    private static class Insert extends AsyncTask<ItemDownloaded, Void, Void> {

        private ApplicationDao applicationDao;

        Insert(ApplicationDao applicationDao) {
            this.applicationDao = applicationDao;
        }

        @Override
        protected Void doInBackground(ItemDownloaded... itemsDownloaded) {
            applicationDao.insertItemDownload(itemsDownloaded[0]);
            return null;
        }
    }

    private static class Delete extends AsyncTask<ItemDownloaded, Void, Void> {

        private ApplicationDao applicationDao;

        Delete(ApplicationDao applicationDao) {
            this.applicationDao = applicationDao;
        }

        @Override
        protected Void doInBackground(ItemDownloaded... itemsDownloaded) {
            applicationDao.deleteItemDownload(itemsDownloaded[0]);
            return null;
        }
    }

    private static class DeleteAll extends AsyncTask<Void, Void, Void> {

        private ApplicationDao applicationDao;

        DeleteAll(ApplicationDao applicationDao) {
            this.applicationDao = applicationDao;
        }

        @Override
        protected Void doInBackground(Void... aVoid) {
            applicationDao.deleteAll();
            return null;
        }
    }

    public LiveData<List<ItemDownloaded>> getDataGradients() {
        return dataGradients;
    }

    public LiveData<List<ItemDownloaded>> getDataPatterns() {
        return dataPatterns;
    }

    public LiveData<List<ItemDownloaded>> getDataStickers() {
        return dataStickers;
    }

    public LiveData<List<ItemDownloaded>> getDataFonts() {
        return dataFonts;
    }

    public LiveData<List<ItemDownloaded>> getDataPalettes() {
        return dataPalettes;
    }
}
