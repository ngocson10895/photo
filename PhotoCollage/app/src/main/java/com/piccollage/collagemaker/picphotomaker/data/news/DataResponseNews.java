package com.piccollage.collagemaker.picphotomaker.data.news;

import java.util.List;

/**
 * Data response news for home
 */
public class DataResponseNews {
    private List<DataNews> data;
    private String urlRoot;

    public List<DataNews> getData() {
        return data;
    }

    public String getUrlRoot() {
        return urlRoot;
    }
}
