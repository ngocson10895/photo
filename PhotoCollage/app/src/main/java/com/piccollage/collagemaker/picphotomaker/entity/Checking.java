package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Checking implements Parcelable {
    private String ip;
    private String hostname;
    private String city;
    private String region;
    private String country;
    private String loc;
    private String org;
    private String timezone;
    private String readme;

    protected Checking(Parcel in) {
        ip = in.readString();
        hostname = in.readString();
        city = in.readString();
        region = in.readString();
        country = in.readString();
        loc = in.readString();
        org = in.readString();
        timezone = in.readString();
        readme = in.readString();
    }

    public static final Creator<Checking> CREATOR = new Creator<Checking>() {
        @Override
        public Checking createFromParcel(Parcel in) {
            return new Checking(in);
        }

        @Override
        public Checking[] newArray(int size) {
            return new Checking[size];
        }
    };

    public String getIp() {
        return ip;
    }

    public String getHostname() {
        return hostname;
    }

    public String getCity() {
        return city;
    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public String getLoc() {
        return loc;
    }

    public String getOrg() {
        return org;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getReadme() {
        return readme;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ip);
        dest.writeString(hostname);
        dest.writeString(city);
        dest.writeString(region);
        dest.writeString(country);
        dest.writeString(loc);
        dest.writeString(org);
        dest.writeString(timezone);
        dest.writeString(readme);
    }
}
