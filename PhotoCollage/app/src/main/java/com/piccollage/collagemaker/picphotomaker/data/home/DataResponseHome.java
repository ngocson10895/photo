package com.piccollage.collagemaker.picphotomaker.data.home;

import java.util.List;

/**
 * hold data from api for home
 */
public class DataResponseHome {
    public List<DataHome> data;
}
