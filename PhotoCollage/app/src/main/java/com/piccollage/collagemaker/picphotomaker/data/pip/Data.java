package com.piccollage.collagemaker.picphotomaker.data.pip;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * Data Pip
 */
public class Data {
    private List<DataPIP> data;
    private String urlRoot;

    public List<DataPIP> getData() {
        return data;
    }

    public String getUrlRoot() {
        return urlRoot;
    }

    @NonNull
    @Override
    public String toString() {
        return urlRoot + " " + getData().size();
    }
}
