package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.os.Parcel;
import android.os.Parcelable;

public class Pattern implements Parcelable {
    private String path;
    private String name;
    private String theme;
    private boolean isPick;

    public Pattern(String path, String name, String theme, boolean isPick) {
        this.path = path;
        this.name = name;
        this.theme = theme;
        this.isPick = isPick;
    }

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public String getTheme() {
        return theme;
    }

    protected Pattern(Parcel in) {
        path = in.readString();
        name = in.readString();
        theme = in.readString();
        isPick = in.readByte() != 0;
    }

    public static final Creator<Pattern> CREATOR = new Creator<Pattern>() {
        @Override
        public Pattern createFromParcel(Parcel in) {
            return new Pattern(in);
        }

        @Override
        public Pattern[] newArray(int size) {
            return new Pattern[size];
        }
    };

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeString(name);
        dest.writeString(theme);
        dest.writeByte((byte) (isPick ? 1 : 0));
    }
}
