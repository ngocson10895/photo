package com.piccollage.collagemaker.picphotomaker.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.piccollage.collagemaker.picphotomaker.entity.PipTheme;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer.PipStyleFragment;

import java.util.ArrayList;

/**
 * adapter cho viewpager ở màn hình pip style
 */
public class PagerAdapterPIP extends FragmentStatePagerAdapter {
    private ArrayList<PipTheme> pipThemes;

    public PagerAdapterPIP(@NonNull FragmentManager fm, ArrayList<PipTheme> pipThemes) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.pipThemes = pipThemes;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return PipStyleFragment.newInstance(pipThemes.get(position));
    }

    @Override
    public int getCount() {
        return pipThemes.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return pipThemes.get(position).getName();
    }

    // Phải có nếu muốn reset all data ở các page khác
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
