package com.piccollage.collagemaker.picphotomaker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemListShopBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import java.util.List;

/**
 * Item shop adapter
 */
public class ItemShopAdapter extends RecyclerView.Adapter<ItemShopAdapter.ViewHolder> {
    private Context context;
    private List<ItemShop> itemShops;
    private String type;
    private IDownload iDownload;
    private SharedPrefsImpl sharedPrefs;

    public interface IDownload {
        void download(int position);

        void itemDownloaded(int pos);

        void itemPro(int pos);

        void item(int pos);
    }

    public ItemShopAdapter(Context context, List<ItemShop> itemShops, String type, IDownload iDownload) {
        this.context = context;
        this.itemShops = itemShops;
        this.iDownload = iDownload;
        this.type = type;
        sharedPrefs = new SharedPrefsImpl(context, Constant.NAME_SHARE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_shop, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (type.equals(Constant.STICKER)) {
            viewHolder.binding.rvItem.setVisibility(View.GONE);
            viewHolder.binding.ivThumb.setVisibility(View.VISIBLE);
            viewHolder.binding.tvName.setText(itemShops.get(i).getPathItems().get(0).getTitle());
            Glide.with(context).load(itemShops.get(i).getPathItems().get(0).getUrlThumb())
                    .error(R.drawable.img_default)
                    .placeholder(R.drawable.img_default)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.binding.ivThumb);
            if (itemShops.get(i).isDownloaded()) {
                viewHolder.itemView.setOnClickListener(v -> iDownload.itemDownloaded(viewHolder.getAdapterPosition()));
            } else if (itemShops.get(i).isPro()) {
                viewHolder.itemView.setOnClickListener(v -> iDownload.itemPro(viewHolder.getAdapterPosition()));
            } else {
                viewHolder.itemView.setOnClickListener(v -> iDownload.item(viewHolder.getAdapterPosition()));
            }
        } else {
            ItemShopDetailAdapter itemShopAdapter = new ItemShopDetailAdapter(context, itemShops.get(i).getPathItems(), type);
            viewHolder.binding.tvName.setText(itemShops.get(i).getNameItem());
            viewHolder.binding.rvItem.setAdapter(itemShopAdapter);
        }
        viewHolder.binding.pro.setVisibility((itemShops.get(i).isPro()) ? View.VISIBLE : View.INVISIBLE);
        viewHolder.binding.free.setVisibility((itemShops.get(i).isPro()) ? View.INVISIBLE : View.VISIBLE);
        if (!sharedPrefs.get(Constant.IS_GOT_PURCHASE, Boolean.class)) {
            viewHolder.binding.tvFree.setVisibility((!itemShops.get(i).isPro()) ? View.VISIBLE : View.INVISIBLE);
        } else {
            viewHolder.binding.tvFree.setVisibility(View.INVISIBLE);
            viewHolder.binding.pro.setVisibility(View.INVISIBLE);
        }
        viewHolder.binding.btnDownload.setVisibility((itemShops.get(i).isPro()) ? View.VISIBLE : View.INVISIBLE);
        if (itemShops.get(i).isDownloaded()) {
            viewHolder.binding.btnDone.setVisibility(View.VISIBLE);
            viewHolder.binding.tvFree.setVisibility(View.INVISIBLE);
            viewHolder.binding.btnDownload.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.binding.btnDone.setVisibility(View.INVISIBLE);
            viewHolder.binding.btnDownload.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return itemShops.size();
    }

    public void setItemShops(List<ItemShop> itemShops) {
        this.itemShops = itemShops;
    }

    public List<ItemShop> getItemShops() {
        return itemShops;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemListShopBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemListShopBinding.bind(itemView);

            binding.rvItem.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            binding.rvItem.addItemDecoration(new BetweenSpacesItemDecoration(0, 12));

            binding.btnDownload.setOnClickListener(v -> iDownload.download(getAdapterPosition()));
            binding.tvFree.setOnClickListener(v -> iDownload.download(getAdapterPosition()));
        }
    }
}
