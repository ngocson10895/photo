package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class PipTheme implements Parcelable {
    private String name;
    private ArrayList<PipPicture> pipPictures = new ArrayList<>();

    public PipTheme() {
    }

    protected PipTheme(Parcel in) {
        name = in.readString();
    }

    public static final Creator<PipTheme> CREATOR = new Creator<PipTheme>() {
        @Override
        public PipTheme createFromParcel(Parcel in) {
            return new PipTheme(in);
        }

        @Override
        public PipTheme[] newArray(int size) {
            return new PipTheme[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<PipPicture> getPipPictures() {
        return pipPictures;
    }

    public void setPipPictures(ArrayList<PipPicture> pipPictures) {
        this.pipPictures = pipPictures;
    }

    @NonNull
    @Override
    public String toString() {
        return name + " " + pipPictures.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
