package com.piccollage.collagemaker.picphotomaker.stickerUtils;

/**
 * Event Flip Horizontally
 */
public class FlipHorizontallyEvent extends AbstractFlipEvent {

  @Override
  @StickerView.Flip protected int getFlipDirection() {
    return StickerView.FLIP_HORIZONTALLY;
  }
}
