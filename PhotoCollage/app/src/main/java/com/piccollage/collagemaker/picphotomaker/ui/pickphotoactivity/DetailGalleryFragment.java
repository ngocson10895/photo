package com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.piccollage.collagemaker.picphotomaker.adapter.PhotoAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.base.BaseFragment;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentDetailGalleryBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventReload;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Create from Administrator
 * Purpose: detail từng gallery
 * Des: thể hiện ảnh của từng gallery
 */
public class DetailGalleryFragment extends BaseFragment implements PhotoAdapter.IContractListener {
    private static final String TAG = "DetailGalleryFragment";
    private static final String PHOTOS = "photos";
    private static final String NAME_GALLERY = "name";
    private static final String TYPE = "type";
    private static final String PHOTO_PICKED = "photo_picked";

    public interface IDetailGalleryContract {
        void pick(Photo photo);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IDetailGalleryContract) {
            iDetailGalleryContract = (IDetailGalleryContract) context;
        } else {
            Log.d(TAG, "onAttach: must implement this interface");
        }
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iDetailGalleryContract = null;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void resetAdapter(EventReload event) {
        adapter.notifyDataSetChanged();
    }

    private List<Photo> photos;
    private List<Photo> photosUrl;
    private PhotoAdapter adapter;
    private int type;
    private String name;
    private IDetailGalleryContract iDetailGalleryContract;
    private FragmentDetailGalleryBinding binding;

    public static DetailGalleryFragment newInstance(ArrayList<Photo> photos, String name, int type, ArrayList<Photo> photosPicked) {
        DetailGalleryFragment fragment = new DetailGalleryFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PHOTOS, photos);
        args.putString(NAME_GALLERY, name);
        args.putInt(TYPE, type);
        args.putParcelableArrayList(PHOTO_PICKED, photosPicked);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailGalleryBinding.inflate(getLayoutInflater());

        initView();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    private void initData() {
        if (getArguments() != null) {
            ArrayList<Photo> photosPicked = getArguments().getParcelableArrayList(PHOTO_PICKED);
            if (photosPicked != null && !photosPicked.isEmpty()) {
                photos.addAll(photosUrl);
                for (Object o : photos) {
                    for (Photo pp : photosPicked) {
                        if (o instanceof Photo) {
                            String path = ((Photo) o).getPathPhoto();
                            if (path != null && path.equals(pp.getPathPhoto())) {
                                photos.set(photos.indexOf(o), pp);
                                break;
                            }
                        }
                    }
                }
                adapter.setListPhotos(photos);
                adapter.notifyDataSetChanged();
            } else {
                photos.addAll(photosUrl);
                adapter.setListPhotos(photos);
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void initView() {
        if (getContext() != null) {
            Advertisement.showBannerAdOther(getContext(), binding.adsLayoutD);
            photosUrl = new ArrayList<>();
            photos = new ArrayList<>();

            if (getArguments() != null) {
                photosUrl.addAll(Objects.requireNonNull(getArguments().getParcelableArrayList(PHOTOS)));
                name = getArguments().getString(NAME_GALLERY);
                type = getArguments().getInt(TYPE);
            }
            if (type == Constant.PICK_MORE) {
                // kiểu pick nhiều lần
                adapter = new PhotoAdapter(photos, getContext(), this, Constant.PICK_MORE);
            } else {
                // chỉ pick 1 lần
                adapter = new PhotoAdapter(photos, getContext(), this, Constant.PICK_ONE);
            }

            if (getContext() != null) {
                binding.rvPhotos.setLayoutManager(new GridLayoutManager(getContext(), 4));
                binding.rvPhotos.setAdapter(adapter);
            }
            binding.tvTitle.setText(name);
            onViewClicked();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void pick(Photo photo) {
        iDetailGalleryContract.pick(photo);
    }

    @Override
    public void pickOne(Photo photo) {
        iDetailGalleryContract.pick(photo);
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> getParentFragmentManager().popBackStack());
    }
}
