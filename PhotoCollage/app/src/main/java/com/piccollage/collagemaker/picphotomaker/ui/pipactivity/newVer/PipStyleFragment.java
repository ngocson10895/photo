package com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;

import com.piccollage.collagemaker.picphotomaker.adapter.PIPPictureAdapter;
import com.piccollage.collagemaker.picphotomaker.base.BaseFragment;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentPipStyleBinding;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.entity.PipTheme;
import com.piccollage.collagemaker.picphotomaker.networking.NetworkChangeReceiver;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ItemOffsetDecoration;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ObservableGridManager;

import java.util.ArrayList;

/**
 * Create from Administrator
 * Purpose: Fragment for pip style
 * Des: fragment cho từng loại pip style trong viewpager
 */
public class PipStyleFragment extends BaseFragment {
    private static final String TAG = "PipStyleFragment";
    private static final String PIP_THEME = "pip_theme";

    private PIPPictureAdapter adapter;
    private NetworkChangeReceiver networkChangeReceiver;
    private FragmentPipStyleBinding binding;

    public static PipStyleFragment newInstance(PipTheme pipTheme) {
        PipStyleFragment fragment = new PipStyleFragment();
        Bundle args = new Bundle();
        args.putParcelable(PIP_THEME, pipTheme);
        fragment.setArguments(args);
        return fragment;
    }

    public void onViewClicked() {
        binding.btnGoToSetting.setOnClickListener(v -> {
            if (getActivity() != null)
                getActivity().startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        });
    }

    public interface IPipStyleListener {
        void pickStyle(PipPicture pipPicture);
    }

    private IPipStyleListener iPipStyleListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof IPipStyleListener) {
            iPipStyleListener = (IPipStyleListener) context;
        } else Log.d(TAG, "onAttach: must implement this interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        iPipStyleListener = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    private void initData() {
        if (getArguments() != null) {
            PipTheme pipTheme = getArguments().getParcelable(PIP_THEME);
            if (pipTheme != null) {
                adapter.setNewData(pipTheme.getPipPictures());
                adapter.setOnItemClickListener((adapter1, view, position) -> {
                    if (checkDoubleClick()) {
                        iPipStyleListener.pickStyle(adapter.getData().get(position));
                    }
                });
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPipStyleBinding.inflate(getLayoutInflater());
        initView();
        return binding.getRoot();
    }

    private void initView() {
        if (getContext() != null) {
            adapter = new PIPPictureAdapter(new ArrayList<>(), getContext());
            binding.rvPipStyle.setLayoutManager(new ObservableGridManager(getContext(), 3));
            binding.rvPipStyle.addItemDecoration(new ItemOffsetDecoration(8, getContext()));
            binding.rvPipStyle.setAdapter(adapter);

            networkChangeReceiver = new NetworkChangeReceiver();
            if (getActivity() != null) {
                getActivity().registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                networkChangeReceiver.setListener(new NetworkChangeReceiver.NetworkStateReceiverListener() {
                    @Override
                    public void onNetworkAvailable() {
                        binding.notInternet.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNetworkUnavailable() {
                        binding.notInternet.setVisibility(View.VISIBLE);
                    }
                });
            }

            onViewClicked();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getActivity() != null) getActivity().unregisterReceiver(networkChangeReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
