package com.piccollage.collagemaker.picphotomaker.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogExitBinding;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.util.Objects;

/**
 * Create from Administrator
 * Purpose: dialog back lại từ các chức năng chính
 * Des: hiển thị khi user back lại từ 1 chức năng để hỏi user có muốn hủy hay không
 */
public class DialogExitFeature extends BaseDialog {
    private Context context;
    private IContractDialogExit iContractDialogExit;
    private DialogExitBinding binding;

    @Override
    public void beforeSetContentView() {
        binding = DialogExitBinding.inflate(getLayoutInflater());
    }

    public interface IContractDialogExit {
        void discard();
    }

    public DialogExitFeature(@NonNull Context context, IContractDialogExit iContractDialogExit) {
        super(context);
        this.context = context;
        this.iContractDialogExit = iContractDialogExit;
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogExitFeature.NAME);
    }

    @Override
    public void initView() {
        Advertisement.showBannerAdExit(getContext(), binding.adsLayout);
        binding.tvSave.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(context, TrackingUtils.TrackingConstant.DialogExitFeature.OK);
            dismiss();
            iContractDialogExit.discard();
        });
        binding.tvCancel.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(context, TrackingUtils.TrackingConstant.DialogExitFeature.CANCEL);
            dismiss();
        });
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    // custom dialog
    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.85);
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
