package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.StickerAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentStickerBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventStickerPick;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.DialogDownload;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ItemOffsetDecoration;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class StickerFragment extends Fragment implements DialogDownload.ICancel {
    private static final String TAG = "StickerFragment";
    private static final String STICKER_THEME = "sticker_theme";

    private FragmentStickerBinding binding;
    private StickerAdapter stickerAdapter;
    private StickerTheme stickerTheme;
    private DialogDownload dialog;
    private ApplicationViewModel viewModel;
    private DownloadTask downloadTask;
    private DialogLoading dialogLoading;

    public static StickerFragment newInstance(StickerTheme stickerThemes) {
        StickerFragment fragment = new StickerFragment();
        Bundle args = new Bundle();
        args.putParcelable(STICKER_THEME, stickerThemes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        initData();
    }

    private void initData() {
        if (getArguments() != null) {
            stickerTheme = getArguments().getParcelable(STICKER_THEME);
            if (stickerTheme != null) {
                stickerAdapter.setNewData(stickerTheme.getStickers());
                stickerAdapter.setOnItemClickListener((adapter, view, position) -> {
                    if (stickerTheme.isDownloaded()) {
                        EventBus.getDefault().postSticky(new EventStickerPick(stickerAdapter.getItem(position)));
                    }
                });

                if (stickerTheme.isPro() && getContext() != null) {
                    binding.content.setText(getContext().getString(R.string.up_to_pro_version_to_download_it));
                    binding.content.setTextSize(12);
                    binding.viewAdsToDownload.setBackground(getContext().getResources().getDrawable(R.drawable.btn_go_to_pro));
                    binding.icon.setImageResource(R.drawable.ic_icpromenuleft_white);
                }
                if (stickerTheme.isDownloaded()) {
                    binding.viewAdsToDownload.setVisibility(View.INVISIBLE);
                } else {
                    binding.viewAdsToDownload.setVisibility(View.VISIBLE);
                    binding.viewAdsToDownload.setOnClickListener(v -> {
                        if (MyUtils.isAppPurchased()) download();
                        else {
                            if (stickerTheme.isPro()) {
                                if (getActivity() != null) {
                                    Intent intent = new Intent(getActivity(), BuySubActivity.class);
                                    startActivity(intent);
                                    getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                            } else
                                Advertisement.showInterstitialAdInApp(getContext(), new InterstitialAdsPopupOthers.OnAdsListener() {
                                    @Override
                                    public void onAdsClose() {
                                        download();
                                    }

                                    @Override
                                    public void showLoading() {
                                        dialogLoading.show();
                                    }

                                    @Override
                                    public void hideLoading() {
                                        dialogLoading.dismiss();
                                    }
                                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                                    @Override
                                    public void close() {
                                        download();
                                    }

                                    @Override
                                    public void showLoading() {
                                        dialogLoading.show();
                                    }

                                    @Override
                                    public void hideLoading() {
                                        dialogLoading.dismiss();
                                    }
                                });
                        }
                    });
                }
            }
        }
    }

    private void download() {
        if (getContext() != null) {
            File parentFile = FileUtil.getParentFile(getContext(), "ItemDownload/" + Constant.STICKER);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "createTask: loaded");
                } else
                    Log.d(TAG, "createTask: error");
            }
            downloadTask = new DownloadTask.Builder(stickerTheme.getUrlZip(), parentFile)
                    .setFilename(stickerTheme.getNameTheme()).setMinIntervalMillisCallbackProcess(16)
                    .setPassIfAlreadyCompleted(true).build();
            downloadTask.enqueue(new DownloadListener4WithSpeed() {
                int size = 0;

                @Override
                public void taskStart(@NonNull DownloadTask task) {

                }

                @Override
                public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {

                }

                @Override
                public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {

                }

                @Override
                public void infoReady(@NonNull DownloadTask task, @NonNull BreakpointInfo info, boolean fromBreakpoint, @NonNull Listener4SpeedAssistExtend.Listener4SpeedModel model) {
                    size = (int) info.getTotalLength();
                    if (dialog != null) {
                        dialog.show();
                        dialog.setInfo(getResources().getString(R.string.downloading), getResources().getString(R.string.wait_when_download));
                        dialog.setMax(size);
                    }
                }

                @Override
                public void progressBlock(@NonNull DownloadTask task, int blockIndex, long currentBlockOffset, @NonNull SpeedCalculator blockSpeed) {

                }

                @Override
                public void progress(@NonNull DownloadTask task, long currentOffset, @NonNull SpeedCalculator taskSpeed) {
                    if (dialog != null) {
                        dialog.setCurrent(currentOffset);
                    }
                }

                @Override
                public void blockEnd(@NonNull DownloadTask task, int blockIndex, BlockInfo info, @NonNull SpeedCalculator blockSpeed) {

                }

                @Override
                public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause, @NonNull SpeedCalculator taskSpeed) {
                    if (cause == EndCause.COMPLETED) {
                        String childParentFile = parentFile.getAbsolutePath() + "/" + stickerTheme.getNameTheme();
                        File childFile = new File(childParentFile + "unzip");

                        try {
                            InputStream inputStream = new FileInputStream(childParentFile);
                            if (!childFile.exists()) {
                                if (childFile.mkdirs()) {
                                    Log.d(TAG, "taskEnd: ");
                                }
                            }
                            FileUtil.unzip(inputStream, childFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (parentFile.isDirectory()) {
                            for (File child : parentFile.listFiles()) {
                                if (!child.isDirectory()) {
                                    if (child.delete()) {
                                        Log.d(TAG, "taskEnd: delete loaded");
                                    }
                                }
                            }
                        }
                        viewModel.insert(new ItemDownloaded(Constant.STICKER, childFile.getAbsolutePath(), stickerTheme.getNameTheme()
                                , (float) ((size / 1024)) + " KB", ""));
                        if (getContext() != null) {
                            Toasty.success(getContext(), getString(R.string.download_done), Toasty.LENGTH_SHORT).show();
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    } else if (cause == EndCause.CANCELED) {
                        if (getContext() != null) {
                            Toasty.normal(getContext(), getString(R.string.cancel_completed), Toasty.LENGTH_SHORT).show();
                        }
                    } else {
                        if (getContext() != null) {
                            Toasty.error(getContext(), getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                }
            });
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentStickerBinding.inflate(getLayoutInflater());
        initView();
        return binding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        Advertisement.loadInterstitialAdInApp(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        Advertisement.loadInterstitialAdInApp(getContext());
    }

    private void initView() {
        if (getContext() != null) {
            stickerAdapter = new StickerAdapter(new ArrayList<>(), getContext());
            binding.rvSticker.setLayoutManager(new GridLayoutManager(getContext(), 5));
            binding.rvSticker.addItemDecoration(new ItemOffsetDecoration(8, getContext()));
            binding.rvSticker.setAdapter(stickerAdapter);

            dialog = new DialogDownload(getContext(), this);
            dialogLoading = new DialogLoading(getContext(), DialogLoading.LOADING);
        }
    }

    @Override
    public void cancel() {
        downloadTask.cancel();
    }
}
