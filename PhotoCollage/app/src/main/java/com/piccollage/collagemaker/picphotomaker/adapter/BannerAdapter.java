package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemBanner1Binding;
import com.piccollage.collagemaker.picphotomaker.entity.BannerOfHome;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;

import java.util.List;

/**
 * Banner Adapter ở màn hình Home
 */
public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {
    private List<BannerOfHome> listBannerOfHome;
    private Context context;
    private IReloadNetwork iReloadNetwork;

    public BannerAdapter(List<BannerOfHome> listBannerOfHome, Context context) {
        this.listBannerOfHome = listBannerOfHome;
        this.context = context;
    }

    public void setiReloadNetwork(IReloadNetwork iReloadNetwork) {
        this.iReloadNetwork = iReloadNetwork;
    }

    public interface IReloadNetwork {
        void goToStore();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_banner_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        if (context != null) {
            holder.binding.img.setVisibility(View.VISIBLE);
            holder.binding.tvTitle.setVisibility(View.VISIBLE);
            holder.binding.tvCaption.setVisibility(View.VISIBLE);
            Glide.with(MyApplication.getInstance()).load(listBannerOfHome.get(position % listBannerOfHome.size()).getUri()).
                    error(context.getResources().getDrawable(R.drawable.introsticker1)).
                    placeholder(context.getResources().getDrawable(R.drawable.introsticker1)).
                    diskCacheStrategy(DiskCacheStrategy.ALL).
                    into(holder.binding.img);
            if (listBannerOfHome.get(position % listBannerOfHome.size()).getTitle().contains("#")) {
                String[] st = listBannerOfHome.get(position % listBannerOfHome.size()).getTitle().split("#");
                holder.binding.tvTitle.setText(st[0]);
                holder.binding.tvCaption.setText(st[1]);
            }
            holder.itemView.setOnClickListener(v -> iReloadNetwork.goToStore());
        }
    }

    public void setListBannerOfHome(List<BannerOfHome> listBannerOfHome) {
        this.listBannerOfHome = listBannerOfHome;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return 2001;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemBanner1Binding binding;
        
        ViewHolder(View itemView) {
            super(itemView);
            binding = ItemBanner1Binding.bind(itemView);
        }
    }
}
