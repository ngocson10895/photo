package com.piccollage.collagemaker.picphotomaker.ui.editoractivity;

public interface OnDoneActionsClickListener {
	void onDoneButtonClick();

	void onApplyButtonClick();
}
