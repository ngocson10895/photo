package com.piccollage.collagemaker.picphotomaker.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;

/**
 * base dialog
 */
public abstract class BaseDialog extends Dialog {

    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        beforeSetContentView();
        setContentView(getLayoutView());
        customDialog();
        initView();
        initData();
    }

    public abstract View getLayoutView();

    public abstract void customDialog();

    public void beforeSetContentView(){

    }

    public void initData() {

    }

    public void initView() {

    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
