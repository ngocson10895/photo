package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class StickerTheme implements Parcelable {
    private String nameTheme;
    private ArrayList<StickerItem> stickers;
    private boolean isDownloaded;
    private String urlZip;
    private boolean isPro;

    public StickerTheme(String nameTheme, ArrayList<StickerItem> stickers, boolean isDownloaded, String urlZip, boolean isPro) {
        this.nameTheme = nameTheme;
        this.stickers = stickers;
        this.isDownloaded = isDownloaded;
        this.urlZip = urlZip;
        this.isPro = isPro;
    }

    protected StickerTheme(Parcel in) {
        nameTheme = in.readString();
        stickers = in.createTypedArrayList(StickerItem.CREATOR);
        isDownloaded = in.readByte() != 0;
        urlZip = in.readString();
        isPro = in.readByte() != 0;
    }

    public static final Creator<StickerTheme> CREATOR = new Creator<StickerTheme>() {
        @Override
        public StickerTheme createFromParcel(Parcel in) {
            return new StickerTheme(in);
        }

        @Override
        public StickerTheme[] newArray(int size) {
            return new StickerTheme[size];
        }
    };

    public boolean isPro() {
        return isPro;
    }

    public String getNameTheme() {
        return nameTheme;
    }

    public ArrayList<StickerItem> getStickers() {
        return stickers;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }

    public String getUrlZip() {
        return urlZip;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameTheme);
        dest.writeTypedList(stickers);
        dest.writeByte((byte) (isDownloaded ? 1 : 0));
        dest.writeString(urlZip);
        dest.writeByte((byte) (isPro ? 1 : 0));
    }
}
