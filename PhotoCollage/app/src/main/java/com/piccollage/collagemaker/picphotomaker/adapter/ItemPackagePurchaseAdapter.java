package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemPackageDownloadedBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Package;
import com.piccollage.collagemaker.picphotomaker.utils.ColorUtil;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;

import java.util.ArrayList;

/**
 * adapter item package
 */
public class ItemPackagePurchaseAdapter extends RecyclerView.Adapter<ItemPackagePurchaseAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Package> packages;
    private IContractPackage iDeletePackage;

    public interface IContractPackage {
        void deletePackage(int position);

        void detailPackage(int position);
    }

    public ItemPackagePurchaseAdapter(Context context, ArrayList<Package> packages) {
        this.context = context;
        this.packages = packages;
    }

    public void setiContractPackage(IContractPackage iDeletePackage) {
        this.iDeletePackage = iDeletePackage;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_package_downloaded, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        switch (packages.get(i).getTheme()) {
            case Constant.GRADIENT:
                viewHolder.binding.ivFirstText.setVisibility(View.INVISIBLE);
                ColorUtil.loadGradientToImageOvalGradient(packages.get(i).getPathFirstImage(), context
                        , viewHolder.binding.ivFirstImage, 160);
                break;
            case Constant.FONT:
                viewHolder.binding.ivFirstImage.setVisibility(View.INVISIBLE);
                viewHolder.binding.ivFirstText.setVisibility(View.VISIBLE);
                Typeface typeface = TextUtils.loadTypeface(context, packages.get(i).getPathFirstImage());
                viewHolder.binding.ivFirstText.setTypeface(typeface);
                break;
            case Constant.PALETTE:
                viewHolder.binding.ivFirstImage.setVisibility(View.INVISIBLE);
                viewHolder.binding.ivFirstText.setVisibility(View.INVISIBLE);
                viewHolder.binding.ivFirstPalette.setVisibility(View.VISIBLE);
                String[] colors = packages.get(i).getPathFirstImage().split("_");
                viewHolder.binding.iv1.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[0])));
                viewHolder.binding.iv2.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[1])));
                viewHolder.binding.iv3.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[2])));
                viewHolder.binding.iv4.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[3])));
                viewHolder.binding.iv5.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[4])));
                break;
            default:
                viewHolder.binding.ivFirstImage.setVisibility(View.VISIBLE);
                viewHolder.binding.ivFirstText.setVisibility(View.INVISIBLE);
                viewHolder.binding.ivFirstPalette.setVisibility(View.INVISIBLE);
                Glide.with(context).asBitmap().
                        load(packages.get(i).getPathFirstImage()).
                        error(R.mipmap.thumbs).
                        apply(RequestOptions.circleCropTransform()).into(viewHolder.binding.ivFirstImage);
                break;
        }
        viewHolder.binding.tvTitle.setText(packages.get(i).getTitle().split("_")[0]);
        viewHolder.binding.tvCapacity.setText(packages.get(i).getCapacity());
        if (packages.get(i).isEdit()) {
            viewHolder.binding.ivRemovePack.setVisibility(View.VISIBLE);
        } else viewHolder.binding.ivRemovePack.setVisibility(View.GONE);
        viewHolder.binding.ivRemovePack.setOnClickListener(v -> iDeletePackage.deletePackage(i));
        viewHolder.itemView.setOnClickListener(v -> iDeletePackage.detailPackage(i));

        if (i == 0) {
            viewHolder.binding.tvHeader.setVisibility(View.VISIBLE);
            viewHolder.binding.tvHeader.setText(packages.get(0).getDateTaken());
        } else {
            if (packages.get(i).getDateTaken().equals(packages.get(i - 1).getDateTaken())) {
                viewHolder.binding.tvHeader.setVisibility(View.GONE);
            } else {
                viewHolder.binding.tvHeader.setVisibility(View.VISIBLE);
                viewHolder.binding.tvHeader.setText(packages.get(i).getDateTaken());
            }
        }
    }

    @Override
    public int getItemCount() {
        return packages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemPackageDownloadedBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemPackageDownloadedBinding.bind(itemView);
        }
    }

    public void setPackages(ArrayList<Package> packages) {
        this.packages = packages;
    }
}
