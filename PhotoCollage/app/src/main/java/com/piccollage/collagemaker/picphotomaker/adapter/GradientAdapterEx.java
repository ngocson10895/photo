package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.GradientChildViewHolder;
import com.piccollage.collagemaker.picphotomaker.viewholder.GradientViewHolder;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradients;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class GradientAdapterEx extends ExpandableRecyclerViewAdapter<GradientViewHolder, GradientChildViewHolder> {
    private IPickGradient iPickGradient;
    private Context context;
    private List<? extends ExpandableGroup> mGroups;

    public interface IPickGradient {
        void pick(Gradient gradient, String title);

        void pickMore();
    }

    public GradientAdapterEx(List<? extends ExpandableGroup> groups, Context context) {
        super(groups);
        this.context = context;
        mGroups = groups;
    }

    public void setiPickGradient(IPickGradient iPickGradient) {
        this.iPickGradient = iPickGradient;
    }

    @Override
    public GradientViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gradient_parent, parent, false);
        return new GradientViewHolder(view, context);
    }

    @Override
    public GradientChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gradient, parent, false);
        return new GradientChildViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(GradientChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Gradient gradient = (Gradient) group.getItems().get(childIndex);
        holder.ivChose.setVisibility((gradient.isPick()) ? View.VISIBLE : View.INVISIBLE);
        holder.setGradient(gradient.getStColor(), gradient.getEndColor(), gradient.getName());
        holder.itemView.setOnClickListener(v -> {
            for (ExpandableGroup expandableGroup : getGroups()){
                for (Object obj : expandableGroup.getItems()){
                    Gradient gra = (Gradient) obj;
                    gra.setPick(false);
                }
            }
            gradient.setPick(true);
            notifyDataSetChanged();
            iPickGradient.pick(gradient, group.getTitle());
        });
    }

    @Override
    public void onBindGroupViewHolder(GradientViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setTheme(group);
        if (!group.getTitle().equals(Constant.MORE)) {
            if (isGroupExpanded(flatPosition)) {
                holder.ivBack.setVisibility(View.VISIBLE);
            } else {
                holder.ivBack.setVisibility(View.GONE);
                Glide.with(context).load(holder.firstImage).centerInside().into(holder.imageView);
                holder.tvTitle.setVisibility(View.VISIBLE);
            }
        } else {
            Glide.with(context).load(R.drawable.ic_itemnone).centerCrop().into(holder.imageView);
            holder.ivBack.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (group.getTitle().equals(Constant.MORE)) {
                iPickGradient.pickMore();
            } else {
                toggleGroup(holder.getAdapterPosition());
                if (isGroupExpanded(holder.getAdapterPosition())) {
                    holder.ivBack.setVisibility(View.VISIBLE);
                } else {
                    holder.ivBack.setVisibility(View.GONE);
                    Glide.with(context).load(holder.firstImage).centerInside().into(holder.imageView);
                    holder.tvTitle.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    // Expand only group user picking
    @Override
    public void onGroupExpanded(int positionStart, int itemCount) {
        if (itemCount > 0) {
            int groupIndex = expandableList.getUnflattenedPosition(positionStart).groupPos;
            notifyItemRangeInserted(positionStart, itemCount);
            for (ExpandableGroup grp : mGroups) {
                if (grp != mGroups.get(groupIndex)) {
                    if (isGroupExpanded(grp)) {
                        toggleGroup(grp);
                        notifyItemChanged(mGroups.indexOf(grp));
                    }
                }
            }
        }
    }
}
