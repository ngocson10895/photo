package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.content.Intent;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ThemeAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityManagePackageBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Theme;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Create from Administrator
 * Purpose: quản lý các package của các item cụ thể
 * Des:
 */
public class PackageThemeActivity extends BaseAppActivity {
    private ThemeAdapter themeAdapter;
    private ArrayList<Theme> themes;
    private ApplicationViewModel applicationViewModel;
    private ActivityManagePackageBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityManagePackageBinding.inflate(getLayoutInflater());
    }

    @Override
    public void initView() {
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.GONE);
        }
        binding.adsLayout.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayout, false, new BannerAdsUtils.BannerAdListener() {
            @Override
            public void onAdLoaded() {
                binding.viewPro.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAdFailed() {

            }
        }));
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PackageTheme.NAME);
        applicationViewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        themes = new ArrayList<>();
        themeAdapter = new ThemeAdapter(themes, this);
        binding.rvTheme.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.rvTheme.addItemDecoration(new BetweenSpacesItemDecoration(8, 0));
        binding.rvTheme.setAdapter(themeAdapter);
        themeAdapter.setOnItemClickListener((adapter, view, position) -> {
            Intent intent = new Intent(this, PackageItemActivity.class);
            intent.putExtra("Type", themeAdapter.getData().get(position).getName());
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            finish();
        });
        applicationViewModel.getAllData().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null && itemsDownloaded.isEmpty()) {
                binding.clNotPack.setVisibility(View.VISIBLE);
            } else binding.clNotPack.setVisibility(View.INVISIBLE);
        });
    }

    @Override
    public void buttonClick() {
        onCloseClicked();
        onGoToShopClicked();
        onViewClicked();
    }

    @Override
    public void initData() {
        File file = FileUtil.getParentFile(this, "ItemDownload");
        if (file != null && file.exists()) {
            if (file.listFiles() != null && file.listFiles().length > 0) {
                for (File f : file.listFiles()) {
                    if (f.isDirectory()) {
                        Theme t = new Theme();
                        t.setName(f.getName());
                        t.setNumberOfItem(f.listFiles().length);
                        switch (f.getName()) {
                            case Constant.STICKER:
                            case Constant.PATTERN:
                            case Constant.FONT: {
                                ArrayList<File> fl = new ArrayList<>(Arrays.asList(f.listFiles()));
                                if (!fl.isEmpty()) {
                                    // sắp xếp các item theo ngày tải về. sau đó đảo ngược lại thì item mới nhất sẽ lên đầu
                                    Collections.sort(fl, (o1, o2) -> Long.compare(FileUtil.getDateModifier(o1.getAbsolutePath()), FileUtil.getDateModifier(o2.getAbsolutePath())));
                                    Collections.reverse(fl);
                                    t.setUrlFirstItem(fl.get(0).listFiles()[0].getAbsolutePath());
                                    themes.add(t);
                                }
                                break;
                            }
                            case Constant.GRADIENT: {
                                ArrayList<File> fl = new ArrayList<>(Arrays.asList(f.listFiles()));
                                if (!fl.isEmpty()) {
                                    Collections.sort(fl, (o1, o2) -> Long.compare(FileUtil.getDateModifier(o1.getAbsolutePath()), FileUtil.getDateModifier(o2.getAbsolutePath())));
                                    Collections.reverse(fl);

                                    applicationViewModel.getDataGradients().observe(this, itemsDownloaded -> {
                                        if (itemsDownloaded != null && !itemsDownloaded.isEmpty()) {
                                            String[] colors = itemsDownloaded.get(itemsDownloaded.size() - 1).getPath().split("_");
                                            t.setUrlFirstItem(colors[0] + "_" + colors[1]);
                                            themes.add(t);
                                            themeAdapter.setNewData(themes);
                                        }
                                    });
                                }
                                break;
                            }
                            case Constant.PALETTE: {
                                ArrayList<File> fl = new ArrayList<>(Arrays.asList(f.listFiles()));
                                if (!fl.isEmpty()) {
                                    Collections.sort(fl, (o1, o2) -> Long.compare(FileUtil.getDateModifier(o1.getAbsolutePath()), FileUtil.getDateModifier(o2.getAbsolutePath())));
                                    Collections.reverse(fl);

                                    applicationViewModel.getDataPalettes().observe(this, itemsDownloaded -> {
                                        if (itemsDownloaded != null && !itemsDownloaded.isEmpty()) {
                                            String[] colors = itemsDownloaded.get(itemsDownloaded.size() - 1).getPath().split("_");
                                            t.setUrlFirstItem(colors[0] + "_" + colors[1] + "_" + colors[2] + "_" + colors[3] + "_" + colors[4]);
                                            themes.add(t);
                                            themeAdapter.setNewData(themes);
                                        }
                                    });
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void onCloseClicked() {
        binding.ivClose.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PackageTheme.BACK);
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }

    public void onGoToShopClicked() {
        binding.btnGoToStore.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                Intent intent = new Intent(this, StoreActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }

    public void onViewClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
