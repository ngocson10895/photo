package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.Matrix;
import android.graphics.Path;

/**
 * Frame Image Utils
 */
public class FrameImageUtils {

    public static Path[] createTwoHeartItem() {
        Path[] result = new Path[2];
        Path t = new Path();
        t.moveTo(297.3F, 550.87F);
        t.cubicTo(283.52F, 535.43F, 249.13F, 505.34F, 220.86F, 483.99F);
        t.cubicTo(137.12F, 420.75F, 125.72F, 411.6F, 91.72F, 380.29F);
        t.cubicTo(29.03F, 322.57F, 2.41F, 264.58F, 2.5F, 185.95F);
        t.cubicTo(2.55F, 147.57F, 5.17F, 132.78F, 15.91F, 110.15F);
        t.cubicTo(34.15F, 71.77F, 61.01F, 43.24F, 95.36F, 25.8F);
        t.cubicTo(119.69F, 13.44F, 131.68F, 7.95F, 172.3F, 7.73F);
        t.cubicTo(214.8F, 7.49F, 223.74F, 12.45F, 248.74F, 26.18F);
        t.cubicTo(279.16F, 42.9F, 310.48F, 78.62F, 316.95F, 103.99F);
        t.lineTo(320.95F, 119.66F);
        result[0] = t;
        t = new Path();
        t.moveTo(320.95F, 119.66F);
        t.lineTo(330.81F, 98.08F);
        t.cubicTo(386.53F, -23.89F, 564.41F, -22.07F, 626.31F, 101.11F);
        t.cubicTo(645.95F, 140.19F, 648.11F, 223.62F, 630.69F, 270.62F);
        t.cubicTo(607.98F, 331.93F, 565.31F, 378.67F, 466.69F, 450.3F);
        t.cubicTo(402.01F, 497.27F, 328.8F, 568.35F, 323.71F, 578.33F);
        t.cubicTo(317.79F, 589.92F, 323.42F, 580.14F, 297.3F, 550.87F);
        result[1] = t;
        return result;
    }

    static Path createHeartItem(float top, float size) {
        Path path = new Path();
        path.moveTo(top, top + size / 4);
        path.quadTo(top, top, top + size / 4, top);
        path.quadTo(top + size / 2, top, top + size / 2, top + size / 4);
        path.quadTo(top + size / 2, top, top + size * 3 / 4, top);
        path.quadTo(top + size, top, top + size, top + size / 4);
        path.quadTo(top + size, top + size / 2, top + size * 3 / 4, top + size * 3 / 4);
        path.lineTo(top + size / 2, top + size);
        path.lineTo(top + size / 4, top + size * 3 / 4);
        path.quadTo(top, top + size / 2, top, top + size / 4);
        return path;
    }

    public static Path createFatHeartItem() {
        Path path = new Path();
        path.moveTo(75, 40);
        path.cubicTo(75, 37, 70, 25, 50, 25);
        path.cubicTo(20, 25, 20, 62.5f, 20, 62.5f);
        path.cubicTo(20, 80, 40, 102, 75, 120);
        path.cubicTo(110, 102, 130, 80, 130, 62.5f);
        path.cubicTo(130, 62.5f, 130, 25, 100, 25);
        path.cubicTo(85, 25, 75, 37, 75, 40);
        Matrix m = new Matrix();
        m.postTranslate(-20, -25);
        path.transform(m);
        return path;
    }

    public static Path createHeartItem() {
        Path path = new Path();
        path.moveTo(256.0F, -7.47F);
        path.lineTo(225.07F, 20.69F);
        path.cubicTo(115.2F, 120.32F, 42.67F, 186.24F, 42.67F, 266.67F);
        path.cubicTo(42.67F, 332.59F, 94.29F, 384.0F, 160.0F, 384.0F);
        path.cubicTo(197.12F, 384.0F, 232.75F, 366.72F, 256.0F, 339.63F);
        path.cubicTo(279.25F, 366.72F, 314.88F, 384.0F, 352.0F, 384.0F);
        path.cubicTo(417.71F, 384.0F, 469.33F, 332.59F, 469.33F, 266.67F);
        path.cubicTo(469.33F, 186.24F, 396.8F, 120.32F, 286.93F, 20.69F);
        path.lineTo(256.0F, -7.47F);
        Matrix m = new Matrix();
        m.preScale(1, -1);
        m.postTranslate(-42, 384);
        path.transform(m);
        return path;
    }

}
