package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.content.Intent;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ItemPackagePurchaseAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityPackagePurchaseBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.Package;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.DateTimeUtils;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Create from Administrator
 * Purpose: màn hình hiển thị item downloaded của từng theme
 * Des: quản lý item downloaded từng theme của user
 */
public class PackageItemActivity extends BaseAppActivity implements DialogDelete.IReload {
    private ActivityPackagePurchaseBinding binding;
    private boolean isEdit = false;
    private ArrayList<Package> packages;
    private ItemPackagePurchaseAdapter adapter;
    private DialogDelete dialogDelete;
    private String theme;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityPackagePurchaseBinding.inflate(getLayoutInflater());
    }

    public void initData() {
        if (getIntent() != null) {
            theme = getIntent().getStringExtra("Type");
            binding.title.setText(theme);
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PackageItem.NAME);
        ApplicationViewModel viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        packages = new ArrayList<>();
        adapter = new ItemPackagePurchaseAdapter(this, packages);
        adapter.setiContractPackage(new ItemPackagePurchaseAdapter.IContractPackage() {
            @Override
            public void deletePackage(int position) {
                if (checkDoubleClick()) {
                    MyTrackingFireBase.trackingScreen(PackageItemActivity.this, TrackingUtils.TrackingConstant.PackageItem.WANT_DELETE_ITEM);
                    dialogDelete = new DialogDelete(PackageItemActivity.this, PackageItemActivity.this);
                    dialogDelete.show();
                    dialogDelete.setInfo(packages.get(position).getTitle(),
                            packages.get(position).getTheme(), packages.get(position).getId(),
                            packages.get(position).getPath(), packages.get(position).getCapacity(), packages.get(position).getNameItem());
                }
            }

            @Override
            public void detailPackage(int position) {
                if (checkDoubleClick()) {
                    Intent intent = new Intent(PackageItemActivity.this, DetailItemActivity.class);
                    intent.putExtra(DetailItemActivity.TITLE, packages.get(position).getTitle());
                    intent.putExtra(DetailItemActivity.CAPACITY, packages.get(position).getCapacity());
                    intent.putExtra(DetailItemActivity.PATH_FIRST_IMAGE, packages.get(position).getPathFirstImage());
                    intent.putExtra(DetailItemActivity.THEME, packages.get(position).getTheme());
                    intent.putStringArrayListExtra(DetailItemActivity.URIS, (ArrayList<String>) packages.get(position).getUris());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            }
        });
        binding.rvItemPurchase.setAdapter(adapter);

        viewModel.getAllData().observe(this, itemsDownloaded -> {
            packages.clear();
            if (itemsDownloaded != null) {
                if (itemsDownloaded.isEmpty()) {
                    binding.etdDone.setVisibility(View.GONE);
                    binding.etdEdit.setVisibility(View.GONE);
                } else {
                    for (ItemDownloaded item : itemsDownloaded) {
                        ArrayList<String> uri = new ArrayList<>();
                        if (item.getTheme().equals(theme)) {
                            // Gradient
                            if (item.getTheme().equals(Constant.GRADIENT)) {
                                String[] colorEach = item.getPath().split("_");
                                for (int i = 0; i < colorEach.length; i += 2) {
                                    if (colorEach[i].length() == 6) {
                                        uri.add(colorEach[i] + "_" + colorEach[i + 1]);
                                    }
                                }
                                Package pack = new Package(item.getName(), item.getFileSize(),
                                        item.getPath(), uri.get(0), item.getTheme(), item.getId(), uri, item.getNameItem());
                                if (colorEach[colorEach.length - 1].length() > 6) {
                                    pack.setDateTaken(DateTimeUtils.toUTCDateString(FileUtil.getDateModifier(colorEach[colorEach.length - 1])));
                                } else pack.setDateTaken(getString(R.string.old_items));
                                if (isEdit) pack.setEdit(true);
                                else pack.setEdit(false);
                                packages.add(pack);
                            }
                            // Palette
                            else if (item.getTheme().equals(Constant.PALETTE)) {
                                String[] colorEach = item.getPath().split("_");
                                for (int i = 0; i < colorEach.length; i += 5) {
                                    if (colorEach[i].length() == 6) {
                                        uri.add(colorEach[i] + "_" + colorEach[i + 1] + "_" + colorEach[i + 2] + "_" + colorEach[i + 3] + "_" + colorEach[i + 4]);
                                    }
                                }
                                Package pack = new Package(item.getName(), item.getFileSize(),
                                        item.getPath(), uri.get(0), item.getTheme(), item.getId(), uri, item.getNameItem());
                                if (colorEach[colorEach.length - 1].length() > 6) {
                                    pack.setDateTaken(DateTimeUtils.toUTCDateString(FileUtil.getDateModifier(colorEach[colorEach.length - 1])));
                                } else pack.setDateTaken(getString(R.string.old_items));
                                if (isEdit) pack.setEdit(true);
                                else pack.setEdit(false);
                                packages.add(pack);
                            }
                            // Others
                            else {
                                FileUtil.getAllFiles(new File(item.getPath()), uri);
                                Package pack = new Package(item.getName(), item.getFileSize(),
                                        item.getPath(), uri.get(0), item.getTheme(), item.getId(), uri, item.getNameItem());
                                pack.setDateTaken(DateTimeUtils.toUTCDateString(FileUtil.getDateModifier(item.getPath())));
                                if (isEdit) pack.setEdit(true);
                                else pack.setEdit(false);
                                packages.add(pack);
                            }
                        }
                    }
                }
                Collections.reverse(packages);
                adapter.setPackages(packages);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void buttonClick() {
        onEtdDoneClicked();
        onEtdEditClicked();
        onViewClicked();
        onViewProClicked();
    }

    public void initView() {
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.INVISIBLE);
        }
        binding.adsLayoutP.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayoutP, false, new BannerAdsUtils.BannerAdListener() {
            @Override
            public void onAdLoaded() {
                binding.viewPro.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAdFailed() {

            }
        }));
        binding.rvItemPurchase.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    public void onEtdEditClicked() {
        binding.etdEdit.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                binding.etdEdit.setVisibility(View.INVISIBLE);
                binding.etdDone.setVisibility(View.VISIBLE);

                for (Package p : packages) {
                    p.setEdit(true);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void onEtdDoneClicked() {
        binding.etdDone.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                binding.etdEdit.setVisibility(View.VISIBLE);
                binding.etdDone.setVisibility(View.INVISIBLE);

                for (Package p : packages) {
                    p.setEdit(false);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onBackPressed() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PackageItem.BACK);
        Intent intent = new Intent(this, PackageThemeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }

    @Override
    public void reload() {
        isEdit = true;
        adapter.notifyDataSetChanged();
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.PackageItem.BACK);
                Intent intent = new Intent(this, PackageThemeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                finish();
            }
        });
    }

    public void onViewProClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
