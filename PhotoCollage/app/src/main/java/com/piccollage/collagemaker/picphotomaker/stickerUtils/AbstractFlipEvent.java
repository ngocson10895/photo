package com.piccollage.collagemaker.picphotomaker.stickerUtils;

import android.view.MotionEvent;

/**
 * Flip Event cho sticker view
 */
public abstract class AbstractFlipEvent implements StickerIconEvent {

  @Override
  public void onActionDown(StickerView stickerView, MotionEvent event) {

  }

  @Override
  public void onActionMove(StickerView stickerView, MotionEvent event) {

  }

  @Override
  public void onActionUp(StickerView stickerView, MotionEvent event) {
    stickerView.flipCurrentSticker(getFlipDirection());
  }

  @StickerView.Flip protected abstract int getFlipDirection();
}
