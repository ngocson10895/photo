package com.piccollage.collagemaker.picphotomaker.ui.storeactivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ItemDownloadedAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityDetailPackageBinding;
import com.piccollage.collagemaker.picphotomaker.ui.homeactivity.BottomUsing;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.ColorUtil;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.ItemOffsetDecoration;

import java.util.ArrayList;

/**
 * Create from Administrator
 * Purpose: hiển thị các gói bé đã download
 * Des: hiển thị các gói bé đã download
 */
public class DetailItemActivity extends BaseAppActivity {
    public static final String TAG = DetailItemActivity.class.getSimpleName();
    public static final String TITLE = "title";
    public static final String CAPACITY = "capacity";
    public static final String PATH_FIRST_IMAGE = "path_first_image";
    public static final String THEME = "theme";
    public static final String URIS = "uris";

    private ArrayList<String> uris;
    private String theme;
    private ActivityDetailPackageBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityDetailPackageBinding.inflate(getLayoutInflater());
    }

    @Override
    public void buttonClick() {
        onBackClicked();
        onViewClicked();
        onViewProClicked();
    }

    public void initData() {
        if (getIntent() != null) {
            String title = getIntent().getStringExtra(TITLE);
            theme = getIntent().getStringExtra(THEME);
            uris = getIntent().getStringArrayListExtra(URIS);
            if (title != null) {
                binding.tvTitle.setText(title.split("_")[0]);
            }
            if (getIntent().getStringExtra(CAPACITY) != null) {
                binding.tvCapacity.setText(getIntent().getStringExtra(CAPACITY));
            }
            if (theme != null) {
                switch (theme) {
                    case Constant.GRADIENT:
                        binding.ivFirstImage.setBackgroundColor(Color.WHITE);
                        ColorUtil.loadGradientToImageOvalGradient(getIntent().getStringExtra(PATH_FIRST_IMAGE), this, binding.ivFirstImage, 160);
                        break;
                    case Constant.FONT:
                        binding.ivText.setVisibility(View.VISIBLE);
                        Typeface typeface = TextUtils.loadTypeface(this, getIntent().getStringExtra(PATH_FIRST_IMAGE));
                        binding.ivText.setTypeface(typeface);
                        binding.ivFirstImage.setVisibility(View.INVISIBLE);
                        binding.ivFirstPalette.setVisibility(View.INVISIBLE);
                        break;
                    case Constant.PALETTE:
                        binding.ivFirstPalette.setVisibility(View.VISIBLE);
                        binding.ivFirstImage.setVisibility(View.INVISIBLE);
                        binding.ivText.setVisibility(View.INVISIBLE);
                        String[] colors = getIntent().getStringExtra(PATH_FIRST_IMAGE).split("_");
                        binding.iv1.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[0])));
                        binding.iv2.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[1])));
                        binding.iv3.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[2])));
                        binding.iv4.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[3])));
                        binding.iv5.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[4])));
                        break;
                    default:
                        Glide.with(this).load(getIntent().getStringExtra(PATH_FIRST_IMAGE))
                                .error(R.mipmap.thumbs).apply(RequestOptions.circleCropTransform())
                                .into(binding.ivFirstImage);
                        break;
                }
            }
        }
        loadItemPaths();
    }

    private void loadItemPaths() {
        ItemDownloadedAdapter adapter = new ItemDownloadedAdapter(this, uris, theme);
        binding.rvListItemDetail.setAdapter(adapter);
    }

    public void initView() {
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.GONE);
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItem.NAME);
        binding.adsLayoutPD.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayoutPD, false, new BannerAdsUtils.BannerAdListener() {
            @Override
            public void onAdLoaded() {
                binding.viewPro.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAdFailed() {

            }
        }));
        binding.rvListItemDetail.setLayoutManager(new GridLayoutManager(this, 4));
        binding.rvListItemDetail.addItemDecoration(new ItemOffsetDecoration(8, this));
    }

    public void onBackClicked() {
        binding.ivBack.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItem.BACK);
            finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        });
    }

    public void onViewClicked() {
        binding.btnUseNow.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                if (new PermissionChecker(this).isPermissionOK()) {
                    MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItem.USE);
                    BottomUsing bottomUsing = new BottomUsing();
                    bottomUsing.show(getSupportFragmentManager(), "BottomUsing");
                }
            }
        });
    }

    public void onViewProClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
