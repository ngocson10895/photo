package com.piccollage.collagemaker.picphotomaker.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.utils.view.StickerFragment;

import java.util.ArrayList;

/**
 * Adapter cho view pager ở phần add sticker
 */
public class PagerAdapterSticker extends FragmentStatePagerAdapter {
    private ArrayList<StickerTheme> stickerThemes;

    public PagerAdapterSticker(@NonNull FragmentManager fm, ArrayList<StickerTheme> stickerThemes) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.stickerThemes = stickerThemes;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return StickerFragment.newInstance(stickerThemes.get(position));
    }

    @Override
    public int getCount() {
        return stickerThemes.size();
    }

    public void setNewData(ArrayList<StickerTheme> stickerThemes) {
        this.stickerThemes = stickerThemes;
    }

    public ArrayList<StickerTheme> getStickerThemes() {
        return stickerThemes;
    }

    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
