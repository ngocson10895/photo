package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.PointF;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.HashMap;
import java.util.List;

/**
 * Nine Frame Layout
 */
public class NineFrameImage {
    public static void collage_9_11(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.2666f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.7519f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.2f, 0, 0.8f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8889f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.1111f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.7334f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0.2481f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.3333f, 0.3333f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.2666f, 0.3333f, 0.7334f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8572f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.1428f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.6666f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0.2f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.6666f, 0.4f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8333f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0.6666f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.2f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.6f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.1666f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_9_10(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.39645f, 0.39645f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.73881f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.6306f));
            photosPicked.get(position).pointList.add(new PointF(0.6306f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.73881f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.2929f, 0, 0.7071f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.60355f, 0, 1, 0.39645f);
            photosPicked.get(position).pointList.add(new PointF(0.26119f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.73881f));
            photosPicked.get(position).pointList.add(new PointF(0.3694f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.6306f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.75f, 0.2929f, 1, 0.7071f);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.75f));
            photosPicked.get(position).pointList.add(new PointF(0, 0.25f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.60355f, 0.60355f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.26199f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.3694f));
            photosPicked.get(position).pointList.add(new PointF(0.3694f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.26199f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 2));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.2929f, 0.75f, 0.7071f, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.60355f, 0.39645f, 1);
            photosPicked.get(position).pointList.add(new PointF(0.6306f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3694f));
            photosPicked.get(position).pointList.add(new PointF(0.73881f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.26199f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(2, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.2929f, 0.25f, 0.7071f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(1, 0.75f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.25f, 0.25f, 0.75f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0.2929f, 0));
            photosPicked.get(position).pointList.add(new PointF(0.7071f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.2929f));
            photosPicked.get(position).pointList.add(new PointF(1, 0.7071f));
            photosPicked.get(position).pointList.add(new PointF(0.7071f, 1));
            photosPicked.get(position).pointList.add(new PointF(0.2929f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.7071f));
            photosPicked.get(position).pointList.add(new PointF(0, 0.2929f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(5), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(6), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(7), new PointF(1, 1));
        }
    }

    public static void collage_9_9(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.3f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.6f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.3f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.6f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.3f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.4f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.7f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0.6f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.7f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.4f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.7f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.4f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.7f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0.6f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.3f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.4f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.3f, 0.3f, 0.7f, 0.7f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_8(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 0.75f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.5f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.3333f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3333f, 0.3333f, 0.6666f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.6666f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_7(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 0.75f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.5f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3333f, 0.6666f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.6666f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_6(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.2f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.2f, 0, 0.4f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.4f, 0, 0.6f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.6f, 0, 0.8f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.8f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.2f, 0.5f, 0.4f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.4f, 0.5f, 0.6f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6f, 0.5f, 0.8f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.8f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_5(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.3333f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.3333f, 0.25f, 0.6666f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.6666f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_4(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.25f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.25f, 0.3333f, 0.5f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.3333f, 0.75f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.75f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.25f, 0.6666f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.5f, 0.6666f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.75f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_3(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.2f, 0.4f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.4f, 0.2f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.8f, 0.4f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.4f, 0.8f, 0.8f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.8f, 0.6f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.8f, 0.2f, 1, 0.6f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.2f, 0, 0.6f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6f, 0, 1, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.2f, 0.2f, 0.8f, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_2(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3333f, 0.75f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.6666f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_1(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.3333f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.3333f, 0.3333f, 0.6666f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3333f, 0.6666f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.6666f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_9_0(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.75f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.25f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.25f, 0.25f, 0.75f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.75f, 0.25f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.25f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.25f, 0.75f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //ninth frame
        if (position == 8) {
            photosPicked.get(position).bound.set(0.75f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }
}
