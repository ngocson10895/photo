package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.PointF;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.HashMap;
import java.util.List;

/**
 * Four Frame Image
 */
public class FourFrameImage {
    public static void collage_4_25(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_4_24(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 0.3f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.6667f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.2f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3333f));
            photosPicked.get(position).pointList.add(new PointF(1, 0.6667f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.4f, 1, 0.8f);
            photosPicked.get(position).pointList.add(new PointF(0, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.75f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.7f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3333f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_23(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.6f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.8333f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.6f));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.3f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.2857f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.2f));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_22(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.4f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.1666f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.2f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.6f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.8333f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_21(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.25f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.25f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.75f));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_20(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.75f, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.25f, 0.3333f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.6667f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_19(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_4_18(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_4_17(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_16(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.3333f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_15(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6667f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.6667f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_14(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.3333f, 0.6667f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.6667f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_13(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.6667f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6667f, 0.3333f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_12(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_11(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.5f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_10(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_9(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.3333f, 0.5f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.6666f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_8(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.4f, 0.4f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.4f, 0, 1, 0.4f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6f, 0.4f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.4f, 0.6f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_7(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0.6667f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_6(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6667f, 0.6667f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.3333f, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(2, 2));
        }
    }

    public static void collage_4_5(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.5f));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.6667f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0.3333f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.5f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(4), new PointF(1, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.6667f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_4(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.5f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.1666f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.4444f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.2f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5555f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0.25f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
    }

    public static void collage_4_3(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.51f, 0.675f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(0.653f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.8f));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0, 1, 0.4646f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3375f));
            photosPicked.get(position).pointList.add(new PointF(0.23125f, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3333f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_2(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.6667f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0.75f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.6667f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 2));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.6667f);
            photosPicked.get(position).pointList.add(new PointF(0.3333f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 0.75f));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(1, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.3333f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0.25f, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.3333f));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0.25f));
            photosPicked.get(position).pointList.add(new PointF(0.6667f, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(3), new PointF(2, 2));
        }
    }

    public static void collage_4_1(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_4_0(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //four frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }
}
