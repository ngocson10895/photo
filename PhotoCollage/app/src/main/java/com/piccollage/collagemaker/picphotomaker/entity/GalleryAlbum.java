package com.piccollage.collagemaker.picphotomaker.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Album
 */
public class GalleryAlbum {
    private String albumName;
    private String takenDate;
    private long idAlbum;
    private List<Photo> listImage = new ArrayList<>();

    public GalleryAlbum(String albumName, String takenDate, long idAlbum) {
        this.albumName = albumName;
        this.takenDate = takenDate;
        this.idAlbum = idAlbum;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getTakenDate() {
        return takenDate;
    }

    public long getIdAlbum() {
        return idAlbum;
    }

    public int getQuantityOfImage() {
        return listImage.size();
    }

    public List<Photo> getListImage() {
        return listImage;
    }

    public void setListImage(List<Photo> listImage) {
        this.listImage = listImage;
    }
}
