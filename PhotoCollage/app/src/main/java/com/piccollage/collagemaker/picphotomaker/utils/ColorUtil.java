package com.piccollage.collagemaker.picphotomaker.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.piccollage.collagemaker.picphotomaker.R;

/**
 * Color Helper
 */
public class ColorUtil {
    // load image gradient from string to image view with custom size
    public static void loadGradientToImageOvalGradient(String gradient, Context context, ImageView imageView, int size) {
        if (gradient != null) {
            String[] colors = gradient.split("_");
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR
                    , new int[]{Color.parseColor("#" + colors[0]), Color.parseColor("#" + colors[1])});
            Glide.with(context).
                    load(gradientDrawable).
                    error(R.mipmap.thumbs).
                    override(size, size).
                    apply(RequestOptions.circleCropTransform()).into(imageView);
        }
    }
}
