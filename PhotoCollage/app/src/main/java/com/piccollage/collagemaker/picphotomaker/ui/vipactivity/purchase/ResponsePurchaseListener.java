package com.piccollage.collagemaker.picphotomaker.ui.vipactivity.purchase;

public interface ResponsePurchaseListener {
    void onResponsePurchase(AppPurchases appPurchases);
}
