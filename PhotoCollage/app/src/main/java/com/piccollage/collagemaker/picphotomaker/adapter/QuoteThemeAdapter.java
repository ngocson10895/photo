package com.piccollage.collagemaker.picphotomaker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemThemeQuoteBinding;

import java.util.ArrayList;

/**
 * Adapter quote theme
 */
public class QuoteThemeAdapter extends RecyclerView.Adapter<QuoteThemeAdapter.ViewHolder> {
    private ArrayList<String> themes;
    private Context context;
    private IPickThemeQuote iPickThemeQuote;
    private int initPosition = 0;

    public interface IPickThemeQuote {
        void pickTheme(String string);
    }

    public QuoteThemeAdapter(ArrayList<String> themes, Context context, IPickThemeQuote iPickThemeQuote) {
        this.themes = themes;
        this.context = context;
        this.iPickThemeQuote = iPickThemeQuote;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_theme_quote, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (initPosition == i)
            viewHolder.binding.tvTheme.setTextColor(context.getResources().getColor(R.color.pink));
        else viewHolder.binding.tvTheme.setTextColor(context.getResources().getColor(R.color.gray));
        viewHolder.binding.tvTheme.setText("#" + themes.get(i));
        viewHolder.itemView.setOnClickListener(v -> {
            iPickThemeQuote.pickTheme(themes.get(viewHolder.getAdapterPosition()));
            if (initPosition != viewHolder.getAdapterPosition()) {
                initPosition = viewHolder.getAdapterPosition();
                notifyDataSetChanged();
                viewHolder.binding.tvTheme.setTextColor(context.getResources().getColor(R.color.pink));
            }
        });
    }

    @Override
    public int getItemCount() {
        return themes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemThemeQuoteBinding binding;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemThemeQuoteBinding.bind(itemView);
        }
    }

    public void setThemes(ArrayList<String> themes) {
        this.themes = themes;
    }
}
