package com.piccollage.collagemaker.picphotomaker.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * File Helper
 */
public class FileUtil {
    private static final String TAG = "FileUtil";

    // save file to storage
    public static File getParentFile(@NonNull Context context, String nameFile) {
        final File externalSaveDir = context.getExternalFilesDir(nameFile);
        if (externalSaveDir == null) {
            return context.getCacheDir();
        } else {
            return externalSaveDir;
        }
    }

    // create parent file in external storage
    public static String getParentFileString(String nameFile) {
        return Environment.getExternalStorageDirectory().getAbsolutePath().concat("/Photo Collage/").concat("/" + nameFile + "/");
    }

    // unzip the zip file
    public static void unzip(InputStream in, File targetDir) throws IOException {
        ZipInputStream zipIn = new ZipInputStream(in);
        ZipEntry zipEntry;
        byte[] b = new byte[8192];
        while ((zipEntry = zipIn.getNextEntry()) != null) {
            File file = new File(targetDir, zipEntry.getName());
            String canonicalPath = file.getCanonicalPath();
            try {
                if (!canonicalPath.startsWith(targetDir.toString())) {
                    //TODO  SecurityException
                    break;
                } else {
                    if (zipEntry.isDirectory()) {
                        if (file.mkdirs()) {
                            Log.d(TAG, "unzip: ");
                        }
                    } else {
                        File parent = file.getParentFile();
                        if (!parent.exists()) {
                            if (parent.mkdirs()) {
                                Log.d(TAG, "unzip: ");
                            }
                        }
                        FileOutputStream fos = new FileOutputStream(file);
                        int r;
                        while ((r = zipIn.read(b)) != -1) {
                            fos.write(b, 0, r);
                        }
                        fos.close();
                    }
                }
            } catch (SecurityException e) {
                // plugin.getLogger().log(Level.SEVERE, configFile.getPath().concat(” read permission denied.“), e);
                e.printStackTrace();
                break;
            }
            // Finish unzipping…
        }
    }

    // delete all file in storage
    public static void deleteFile(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteFile(child);
            }
        }
        if (fileOrDirectory.delete()) {
            Log.d(TAG, "deleteEffectFile: Delete");
        } else {
            Log.d(TAG, "deleteEffectFile: Can not Delete");
        }
    }

    // get all file in directory
    public static void getAllFiles(File fileOrDirectory, ArrayList<String> listUri) {
        if (fileOrDirectory.isDirectory()) {
            if (fileOrDirectory.listFiles() != null) {
                for (File child : fileOrDirectory.listFiles()) {
                    if (child.getName().equals("__MACOSX")) {
                        continue;
                    }
                    getAllFiles(child, listUri);
                    listUri.add(child.toString());
                }
            } else {
                Log.d(TAG, "getAllFiles: list file null");
            }
        }
    }

    // get all path in assets
    public static List<String> listAssetFiles(String path, Context context) {
        List<String> listFrame = new ArrayList<>();
        listFrame.add("");
        try {
            String[] fileList = context.getAssets().list(path);
            if (fileList == null) {
                Log.d(TAG, "listAssetFiles: null");
            } else {
                listFrame.clear();
                listFrame.addAll(Arrays.asList(fileList));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listFrame;
    }

    // get last modified value of file
    public static long getDateModifier(String path) {
        return new File(path).lastModified();
    }

    public static String fileToMD5(String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filePath);
            byte[] buffer = new byte[1024];
            MessageDigest digest = MessageDigest.getInstance("MD5");
            int numRead = 0;
            while (numRead != -1) {
                numRead = inputStream.read(buffer);
                if (numRead > 0) {
                    digest.update(buffer, 0, numRead);
                }
            }
            byte[] md5Bytes = digest.digest();
            return convertHashToString(md5Bytes);
        } catch (Exception ignored) {
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    Log.e(TAG, "file to md5 failed", e);
                }
            }
        }
    }

    private static String convertHashToString(byte[] md5Bytes) {
        StringBuilder buf = new StringBuilder();
        for (byte md5Byte : md5Bytes) {
            buf.append(Integer.toString((md5Byte & 0xff) + 0x100, 16).substring(1));
        }
        return buf.toString().toUpperCase();
    }
}
