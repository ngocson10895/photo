package com.piccollage.collagemaker.picphotomaker.ui.pipactivity.layout;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.DragEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;

import java.util.ArrayList;
import java.util.List;

import dauroi.photoeditor.utils.PhotoUtils;

/**
 * Create from Administrator
 * Purpose: Template layout chứa ảnh của user
 * Des: chứa ảnh của user và thực hiện thao tác như chon, drag and drop
 */
@SuppressLint("ViewConstructor")
public class TemplateLayout extends RelativeLayout implements PhotoItemLayout.OnImageClickListener {

    private Context context;
    private List<Photo> listPhotoPicked;
    private List<PhotoItemLayout> itemLayoutList;
    private List<HiddenLayout> hiddenLayouts;
    private Bitmap template, background;
    private IClickImage iClickImage;
    private int count = 0;
    private ImageView ivTemplate;

    public interface IClickImage {
        void clickImage(int index, List<Photo> photosPicked);
    }

    OnDragListener onDragListener = (v, event) -> {
        int dragEvent = event.getAction();
        switch (dragEvent) {
            case DragEvent.ACTION_DRAG_ENTERED:

            case DragEvent.ACTION_DRAG_EXITED:
                break;

            case DragEvent.ACTION_DROP:
                PhotoItemLayout target = (PhotoItemLayout) v;
                PhotoItemLayout dragged = (PhotoItemLayout) event.getLocalState();
                String targetPath = "", draggedPath = "";
                if (target.getPhotoItem() != null)
                    targetPath = target.getPhotoItem().getPathPhoto();
                if (dragged.getPhotoItem() != null)
                    draggedPath = dragged.getPhotoItem().getPathPhoto();
                if (targetPath == null) targetPath = "";
                if (draggedPath == null) draggedPath = "";
                if (!targetPath.equals(draggedPath))
                    target.swapImage(dragged);
                break;
        }
        return true;
    };

    public TemplateLayout(Context context, List<Photo> photosPicked, Bitmap template, IClickImage iClickImage) {
        super(context);
        listPhotoPicked = photosPicked;
        this.context = context;
        this.template = template;
        itemLayoutList = new ArrayList<>();
        setLayerType(LAYER_TYPE_HARDWARE, null);
        this.iClickImage = iClickImage;
        hiddenLayouts = new ArrayList<>();
    }

    public void buildTemplateLayout(int containerWidth, int containerHeight, String imageBg, String type) {
        if (template != null) {
            background = ImageDecoder.decodeFileToBitmap(imageBg);
            setBackground(new BitmapDrawable(getContext().getResources(), PhotoUtils.blurImage(background, 10)));

            itemLayoutList.clear();
            float mInternalScaleRatio = 1.0f / PhotoUtils.calculateScaleRatio(template.getWidth(), template.getHeight(), containerWidth, containerHeight);
            for (Photo photo : listPhotoPicked) {
                itemLayoutList.add(addPhotoItemView(photo, mInternalScaleRatio, listPhotoPicked.indexOf(photo), type));
            }

            ivTemplate = new ImageView(context);
            if (listPhotoPicked.get(0).template.contains("unzip") && listPhotoPicked.get(0).template.contains(".webp")) {
                ivTemplate.setBackground(new BitmapDrawable(getResources(), PhotoUtils.decodePNGImage(context, listPhotoPicked.get(0).template)));
            } else if (listPhotoPicked.get(0).template.contains(".webp")) {
                ivTemplate.setBackground(new BitmapDrawable(getResources(),
                        ImageDecoder.getBitmapFromAssets("pip/" + listPhotoPicked.get(0).template, getContext())));
            }
            LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            addView(ivTemplate, params);
        }
    }

    private PhotoItemLayout addPhotoItemView(Photo item, float internalScale, int index, String type) {
        if (item == null || item.maskPath == null) {
            return null;
        }
        final PhotoItemLayout imageView = new PhotoItemLayout(getContext(), item, index, type);
        final float viewWidth = internalScale * imageView.getMaskImage().getWidth();
        final float viewHeight = internalScale * imageView.getMaskImage().getHeight();
        imageView.build(viewWidth, viewHeight, (float) 1);
        imageView.setOnImageClickListener(this);
        imageView.setOnDragListener(onDragListener);

        HiddenLayout hiddenLayout = new HiddenLayout(getContext(), item, type);
        hiddenLayout.build(viewWidth, viewHeight, (float) 1);
        hiddenLayout.setVisibility(INVISIBLE);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) viewWidth, (int) viewHeight);
        params.leftMargin = (int) (internalScale * item.x);
        params.topMargin = (int) (internalScale * item.y);
        addView(imageView, params);
        addView(hiddenLayout, params);
        hiddenLayouts.add(hiddenLayout);
        return imageView;
    }

    @Override
    public void onLongClickImage(PhotoItemLayout view) {
        if (listPhotoPicked.size() > 1) {
            view.setTag("x=" + view.getPhotoItem().x + ",y=" + view.getPhotoItem().y + ",path=" + view.getPhotoItem().getPathPhoto());
            ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());
            String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
            ClipData dragData = new ClipData(view.getTag().toString(), mimeTypes, item);
            DragShadowBuilder myShadow = new DragShadowBuilder(view);
            view.startDrag(dragData, myShadow, view, 0);
        }
    }

    @Override
    public void clickImage(int index) {
        if (count == 0) {
            for (HiddenLayout photo : hiddenLayouts) {
                if (hiddenLayouts.indexOf(photo) == index) {
                    photo.setVisibility(INVISIBLE);
                } else
                    photo.setVisibility(VISIBLE);
            }
            for (PhotoItemLayout photoItemLayout : itemLayoutList) {
                photoItemLayout.setmEnableTouch(false);
                photoItemLayout.setClick(true);
            }
            iClickImage.clickImage(index, listPhotoPicked);
        }
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setNotIdle(boolean idle) {
        for (PhotoItemLayout photoItemLayout : itemLayoutList) {
            photoItemLayout.setmEnableTouch(idle);
        }
    }

    public void hideHiddenLayout(){
        for (HiddenLayout hiddenLayout : hiddenLayouts){
            hiddenLayout.setVisibility(INVISIBLE);
        }
    }

    public void changeImage(int index, Photo photoUpdate) {
        itemLayoutList.get(index).changeImage(photoUpdate.getPathPhoto());
    }

    public void changeBackground(Photo photo) {
        if (photo != null) {
            background = ImageDecoder.decodeFileToBitmap(photo.getPathPhoto());
            setBackground(new BitmapDrawable(getContext().getResources(), PhotoUtils.blurImage(background, 10)));
        }
    }
}
