package com.piccollage.collagemaker.picphotomaker.ui.vipactivity.purchase;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.google.android.gms.common.util.CollectionUtils;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingConstants;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingManager;
import com.piccollage.collagemaker.picphotomaker.utils.billing.BillingUtils;

import java.util.List;

public class PremiumHelper {
    public static PremiumHelper instances;
    private AppPurchases mAppPurchases;

    public static PremiumHelper getInstances() {
        if (null == instances) {
            instances = new PremiumHelper();
        }
        return instances;
    }

    public void querySubscriptionDetailsAsync(BillingManager billingManager, ResponsePurchaseListener listener) {
        mAppPurchases = new AppPurchases();
        List<String> subscriptions3 = BillingUtils.mapSubscriptionGroup().get(3);
        billingManager.querySkuDetailsAsync(BillingClient.SkuType.SUBS, subscriptions3, (responseCode, skuDetailsList) -> {
            if (CollectionUtils.isEmpty(skuDetailsList)) return;
            calculatorPrice(skuDetailsList);
            updateAppPurchaseDetails(mAppPurchases, listener);
        });

        List<String> subscriptions1 = BillingUtils.mapSubscriptionGroup().get(1);
        billingManager.querySkuDetailsAsync(BillingClient.SkuType.SUBS, subscriptions1, (responseCode, skuDetailsList) -> {
            if (CollectionUtils.isEmpty(skuDetailsList)) return;
            calculatorPrice2(skuDetailsList);
            updateAppPurchaseDetails(mAppPurchases, listener);
        });

        billingManager.querySkuDetailsAsync(BillingClient.SkuType.INAPP, BillingUtils.getSkuListOneTime(), (responseCode, skuDetailsList) -> {
            if (CollectionUtils.isEmpty(skuDetailsList)) return;
            for (SkuDetails skuDetails : skuDetailsList) {
                if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SKU_PHOTOCOLLAGE002)) {
                    mAppPurchases.priceProduct = skuDetails.getPrice();
                    updateAppPurchaseDetails(mAppPurchases, listener);
                }
            }
        });
    }

    private void calculatorPrice(List<SkuDetails> skuDetailsList) {
        for (SkuDetails skuDetails : skuDetailsList) {
            if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_MONTHLY_002)
                    || skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_MONTHLY_003)) {
                mAppPurchases.priceSubscriptionMonthly3 = skuDetails.getPrice();
                mAppPurchases.valueSubMonthly3 = skuDetails.getPriceAmountMicros();
            }

            if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_YEARLY_ID_002)
                    || skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_YEARLY_ID_003)) {
                mAppPurchases.priceSubscriptionYearly3 = skuDetails.getPrice();
                mAppPurchases.valueSubYearly3 = skuDetails.getPriceAmountMicros();
            }

//            mAppPurchases.savePriceMonthly = "Save" + " " + Math.round(((mAppPurchases.valueSubMonthly * 4) -
//                    mAppPurchases.valueSubMoths) / (mAppPurchases.valueSubMonthly * 4) * 100) + "%";
//            mAppPurchases.savePriceYearly = Math.round(((mAppPurchases.valueSubMonthly * 12) -
//                    mAppPurchases.valueSubYearly) / (mAppPurchases.valueSubMonthly * 12) * 100) + "%";
        }
    }

    private void calculatorPrice2(List<SkuDetails> skuDetailsList) {
        for (SkuDetails skuDetails : skuDetailsList) {
            if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_MONTHLY_001)) {
                mAppPurchases.priceSubscriptionMonthly1 = skuDetails.getPrice();
                mAppPurchases.valueSubMonthly1 = skuDetails.getPriceAmountMicros();
            }

            if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_YEARLY_TRIAL_ID_001)) {
                mAppPurchases.priceSubscriptionYearly1 = skuDetails.getPrice();
                mAppPurchases.valueSubYearly1 = skuDetails.getPriceAmountMicros();
            }

            if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_YEARLY_TRIAL_ID_003)) {
                mAppPurchases.priceSubscriptionYearly3 = skuDetails.getPrice();
                mAppPurchases.valueSubYearly3 = skuDetails.getPriceAmountMicros();
            }
            if (skuDetails.getSku().equalsIgnoreCase(BillingConstants.SUBSCRIPTION_MONTHLY_003)) {
                mAppPurchases.priceSubscriptionMonthly3 = skuDetails.getPrice();
                mAppPurchases.valueSubMonthly3 = skuDetails.getPriceAmountMicros();
            }

            if (mAppPurchases.valueSubMonthly1 != 0.0 && mAppPurchases.valueSubMonthly3 != 0.0) {
                mAppPurchases.savePriceMonthly13 = 100 - Math.round(mAppPurchases.valueSubMonthly1 / mAppPurchases.valueSubMonthly3 * 100) + "%";
            } else mAppPurchases.savePriceMonthly13 = "";
            if (mAppPurchases.valueSubYearly1 != 0.0 && mAppPurchases.valueSubYearly3 != 0.0) {
                mAppPurchases.savePriceYearly13 = 100 - Math.round(mAppPurchases.valueSubYearly1 / mAppPurchases.valueSubYearly3 * 100) + "%";
            } else mAppPurchases.savePriceYearly13 = "";

//            mAppPurchases.savePriceMonthly = "Save" + " " + Math.round(((mAppPurchases.valueSubMonthly * 4) -
//                    mAppPurchases.valueSubMoths) / (mAppPurchases.valueSubMonthly * 4) * 100) + "%";
//            mAppPurchases.savePriceYearly = Math.round(((mAppPurchases.valueSubMonthly * 12) -
//                    mAppPurchases.valueSubYearly) / (mAppPurchases.valueSubMonthly * 12) * 100) + "%";

        }
    }

    public void updateAppPurchaseDetails(AppPurchases appPurchases, ResponsePurchaseListener listener) {
        if (listener != null) listener.onResponsePurchase(appPurchases);
    }
}
