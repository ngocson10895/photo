package com.piccollage.collagemaker.picphotomaker.ads.adsS;

import android.content.Context;
import android.os.Bundle;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyFirebaseConfig;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.LogUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Changed by sean 02/03/2020: Cache and load ads. After 0s thì load lần tiếp theo
 */
public class InterstitialAdsPopupOthers {
    private static InterstitialAdsPopupOthers instance;
    private static InterstitialAd interstitialAdInApp;
    private boolean isPreloadSuccess;
    private long timeStartShowAds = 0;
    private OnAdsListener onAdsListener;

    public interface OnAdsListener {
        void onAdsClose();

        void showLoading();

        void hideLoading();
    }

    private InterstitialAdsPopupOthers() {
        MobileAds.initialize(MyApplication.getInstance());
        MobileAds.setAppMuted(true);
    }

    public static InterstitialAdsPopupOthers getInstance() {
        if (instance == null) {
            instance = new InterstitialAdsPopupOthers();
        }
        return instance;
    }

    // TODO: 30/12/2019 load inters pops up in app
    public void loadAd(Context context) {
        isPreloadSuccess = false;
        if (MyUtils.isAppPurchased()) {
            return;
        }
        loadAd(context, 0, logger.getAdmob_popup_inapp8(context));
    }

    private void loadAd(Context context, int count, String[] interAdsInApp) {
        MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOthers.REQUEST);
        if (count >= interAdsInApp.length || MyUtils.isAppPurchased() || interAdsInApp.length == 0 || context == null) {
            isPreloadSuccess = false;
            return;
        }
        interstitialAdInApp = new InterstitialAd(context);
        if (LogUtils.isForceDebug) {
            interstitialAdInApp.setAdUnitId(interAdsInApp[count]);
        } else interstitialAdInApp.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

        interstitialAdInApp.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOthers.FAIL + i);
                loadAd(context, count + 1, interAdsInApp);
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                onAdsListener.onAdsClose();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOthers.LOADED);
                isPreloadSuccess = true;
                timeStartShowAds = System.currentTimeMillis();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.IntersOthers.SHOW);
            }
        });
        interstitialAdInApp.loadAd(getAdRequest());
    }

    public void showAdsNow(OnAdsListener onAdsListener, Context context) {
        this.onAdsListener = onAdsListener;
        if (System.currentTimeMillis() - timeStartShowAds < MyFirebaseConfig.getKeyAdsDelayAfterOpen()) { // 02/03/2020: set time out cho mỗi lần load ads
            onAdsListener.onAdsClose();
            return;
        }
        if (context == null || MyUtils.isAppPurchased()) {
            onAdsListener.onAdsClose();
            return;
        }

        if (interstitialAdInApp == null) {
            loadAd(context);
            return;
        }

        Observable.interval(0, 0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .doOnDispose(onAdsListener::hideLoading)
                .subscribe(new Observer<Long>() {
                    private Disposable openAppDisposable;

                    @Override
                    public void onSubscribe(Disposable d) {
                        openAppDisposable = d;
                        timeStartShowAds = System.currentTimeMillis();
                        onAdsListener.showLoading();
                    }

                    @Override
                    public void onNext(Long count) {
                        if (count >= MyFirebaseConfig.getKeyAdsTimeoutFeature()) {
                            openAppDisposable.dispose();
                            onAdsListener.onAdsClose();
                        } else {
                            if (isPreloadSuccess) {
                                if (isAdLoaded()) {
                                    interstitialAdInApp.show();
                                }
                            } else onAdsListener.onAdsClose();
                            openAppDisposable.dispose();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        onAdsListener.hideLoading();
                        onAdsListener.onAdsClose();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    private boolean isAdLoaded() {
        return interstitialAdInApp != null && interstitialAdInApp.isLoaded();
    }

    private AdRequest getAdRequest() {
        return new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, new Bundle())
                .build();
    }
}
