package com.piccollage.collagemaker.picphotomaker.ads.facebook;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;

import java.util.ArrayList;
import java.util.List;

public class NativeAdsFacebookUtils {
    private static final String TAG = "NativeAdsFacebookUtils";
    private static NativeAdsFacebookUtils instance;

    public static NativeAdsFacebookUtils getInstances() {
        if (null == instance) {
            instance = new NativeAdsFacebookUtils();
        }
        return instance;
    }

    public void loadNativeAdHomePage(Context context, int resourceId) {
        String[] ads_native_id = logger.getFb_page13(context);
        new AdBuilder().preLoadAd(context, ads_native_id, resourceId, 0);
    }

    public void loadNativeAdHomeTop(Context context, ViewGroup viewGroup, int resourceId) {
        String[] ads_native_id = logger.getFb_native_tophome3(context);
        new AdBuilder().loadAd(context, viewGroup, ads_native_id, resourceId, 0);
    }

    public void loadNativeAdHomeBottom(Context context, ViewGroup viewGroup, int resourceId) {
        String[] ads_native_id = logger.getFb_native_bottomhome4(context);
        new AdBuilder().loadAd(context, viewGroup, ads_native_id, resourceId, 0);
    }

    public void loadNativeAdSettings(Context context, ViewGroup viewGroup, int resourceId) {
        String[] ads_native_id = logger.getFb_native_setting5(context);
        new AdBuilder().loadAd(context, viewGroup, ads_native_id, resourceId, 0);
    }

    public void loadNativeAdNews(Context context, ViewGroup viewGroup, int resourceId) {
        String[] ads_native_id = logger.getFb_new12(context);
        new AdBuilder().loadAd(context, viewGroup, ads_native_id, resourceId, 0);
    }

    public class AdBuilder {
        public void preLoadAd(Context context, String [] ads_native_id, int resourceId, int countNative){
            if (countNative >= ads_native_id.length) {
                return;
            }
            NativeAd nativeAd = new NativeAd(context, ads_native_id[countNative]);
            nativeAd.setAdListener(new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) {
                    Log.d(TAG, "onMediaDownloaded: ");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    if (adError.getErrorCode() == 6001) {
                        return;
                    }
                    Log.d(TAG, "onError: ");
                    preLoadAd(context, ads_native_id, resourceId, countNative + 1);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    if (nativeAd != ad) {
                        return;
                    }
                    inflateAd(nativeAd, context, null, resourceId);
                }

                @Override
                public void onAdClicked(Ad ad) {

                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            // Request an ad
            nativeAd.loadAd();
        }

        public void loadAd(Context context, ViewGroup viewGroup, String[] ads_native_id, int resourceId, int countNative) {
            if (countNative >= ads_native_id.length) {
                return;
            }
            NativeAd nativeAd = new NativeAd(context, ads_native_id[countNative]);
            nativeAd.setAdListener(new NativeAdListener() {
                @Override
                public void onMediaDownloaded(Ad ad) {

                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    if (adError.getErrorCode() == 6001) {
                        return;
                    }
                    loadAd(context, viewGroup, ads_native_id, resourceId, countNative + 1);
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    if (nativeAd != ad) {
                        return;
                    }
                    inflateAd(nativeAd, context, viewGroup, resourceId);
                }

                @Override
                public void onAdClicked(Ad ad) {

                }

                @Override
                public void onLoggingImpression(Ad ad) {

                }
            });
            // Request an ad
            nativeAd.loadAd();
        }

        private void inflateAd(NativeAd nativeAd, Context context, ViewGroup viewGroup, int layoutResourceId) {
            if (context == null) {
                return;
            }

            NativeAdLayout nativeAdLayout = new NativeAdLayout(context);
            nativeAd.unregisterView();
            LayoutInflater inflater = LayoutInflater.from(context);
            LinearLayout adView = (LinearLayout) inflater.inflate(layoutResourceId, nativeAdLayout, false);
            // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
            nativeAdLayout.addView(adView);
            // Add the AdOptionsView
            LinearLayout adChoicesContainer = adView.findViewById(R.id.ad_choices_container);
            if (adChoicesContainer != null) {
                AdOptionsView adOptionsView = new AdOptionsView(context, nativeAd, nativeAdLayout);
                adChoicesContainer.removeAllViews();
                adChoicesContainer.addView(adOptionsView, 0);
            }

            // Create native UI using the ad metadata.
            ImageView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
            TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
            MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
            TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
            TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
            TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
            TextView nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

            // Set the Text.
            if (nativeAdTitle != null) {
                nativeAdTitle.setText(nativeAd.getAdvertiserName());
            }
            if (nativeAdBody != null) {
                nativeAdBody.setText(nativeAd.getAdBodyText());
            }
            if (nativeAdSocialContext != null) {
                nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
            }
            if (nativeAdCallToAction != null) {
                nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
                nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
            }

            if (sponsoredLabel != null) {
                sponsoredLabel.setText(nativeAd.getSponsoredTranslation());
            }

            // Create a list of clickable views
            List<View> clickableViews = new ArrayList<>();
            clickableViews.add(nativeAdTitle);
            clickableViews.add(nativeAdCallToAction);

            // Register the Title and CTA button to listen for clicks.
            if (nativeAdIcon != null) {
                nativeAd.registerViewForInteraction(
                        adView,
                        nativeAdMedia,
                        nativeAdIcon,
                        clickableViews);
            }
            if (viewGroup != null) {
                addNativeAdViewToContainer(viewGroup, nativeAdLayout, context);
            } else {
                ConstantAds.NATIVE_AD_FACEBOOK = nativeAdLayout;
            }
        }

        private void addNativeAdViewToContainer(final ViewGroup container, final NativeAdLayout adView, Context context) {
            if (container == null || adView == null || context == null) {
                return;
            }
            if (MyUtils.isAppPurchased()) {
                return;
            }
            try {
                if (adView.getParent() != null) {
                    if (adView.getParent() == container) {
                        return;
                    }
                    ((ViewGroup) adView.getParent()).removeAllViews();
                }
                container.setVisibility(View.VISIBLE);
                container.removeAllViews();
                container.addView(adView);
            } catch (Exception e) {
            }
        }

    }
}
