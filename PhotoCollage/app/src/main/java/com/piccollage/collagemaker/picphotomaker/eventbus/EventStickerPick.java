package com.piccollage.collagemaker.picphotomaker.eventbus;

import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;

/**
 * event sticker pick
 */
public class EventStickerPick {
    private StickerItem sticker;

    public EventStickerPick(StickerItem sticker) {
        this.sticker = sticker;
    }

    public StickerItem getSticker() {
        return sticker;
    }
}
