package com.piccollage.collagemaker.picphotomaker.ads.adsnew;

public interface InterstitialAdUtilsListener {

    void showLoading();

    void hideLoading();

    void hideAds();
}
