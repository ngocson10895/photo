package com.piccollage.collagemaker.picphotomaker.entity;

public class Language {
    private String language;
    private boolean isSelected;

    public Language(String language, boolean isSelected) {
        this.language = language;
        this.isSelected = isSelected;
    }

    public String getLanguage() {
        return language;
    }

    public boolean isSelected() {
        return isSelected;
    }

}
