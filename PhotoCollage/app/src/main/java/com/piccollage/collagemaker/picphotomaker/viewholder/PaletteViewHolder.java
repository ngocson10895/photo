package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class PaletteViewHolder extends GroupViewHolder {
    public ImageView iv1, iv2, iv3, iv4, iv5;
    public TextView tvName;
    public ImageView ivMore, ivBack;

    public PaletteViewHolder(View itemView) {
        super(itemView);
        tvName = itemView.findViewById(R.id.ivName);
        iv1 = itemView.findViewById(R.id.iv1);
        iv2 = itemView.findViewById(R.id.iv2);
        iv3 = itemView.findViewById(R.id.iv3);
        iv4 = itemView.findViewById(R.id.iv4);
        iv5 = itemView.findViewById(R.id.iv5);
        ivMore = itemView.findViewById(R.id.ivMore);
        ivBack = itemView.findViewById(R.id.ivBack);
    }

    public void setTheme(ExpandableGroup palettes) {
        if (palettes instanceof Palettes) {
            if (palettes.getTitle().equals(Constant.MORE)) {
                ivMore.setVisibility(View.VISIBLE);
                iv1.setVisibility(View.INVISIBLE);
                iv2.setVisibility(View.INVISIBLE);
                iv3.setVisibility(View.INVISIBLE);
                iv4.setVisibility(View.INVISIBLE);
                iv5.setVisibility(View.INVISIBLE);
            } else {
                ivMore.setVisibility(View.INVISIBLE);
                Palette palette = (Palette) palettes.getItems().get(0);
                iv1.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor1())));
                iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                iv1.setVisibility(View.VISIBLE);
                iv2.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor2())));
                iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                iv2.setVisibility(View.VISIBLE);
                iv3.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor3())));
                iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                iv3.setVisibility(View.VISIBLE);
                iv4.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor4())));
                iv4.setScaleType(ImageView.ScaleType.CENTER_CROP);
                iv4.setVisibility(View.VISIBLE);
                iv5.setImageDrawable(new ColorDrawable(Color.parseColor(palette.getColor5())));
                iv5.setScaleType(ImageView.ScaleType.CENTER_CROP);
                iv5.setVisibility(View.VISIBLE);
                tvName.setText(palettes.getTitle());
            }
        }
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
    }
}
