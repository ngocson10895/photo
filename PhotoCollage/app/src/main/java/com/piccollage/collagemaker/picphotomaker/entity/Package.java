package com.piccollage.collagemaker.picphotomaker.entity;

import java.util.List;

public class Package {
    private String title;
    private String theme;
    private String capacity;
    private String pathFirstImage;
    private String path;
    private int id;
    private boolean edit;
    private List<String> uris;
    private String nameItem;
    private String dateTaken;

    public Package(String title, String capacity, String path, String pathFirstImage, String theme, int id, List<String> uris, String nameItem) {
        this.title = title;
        this.capacity = capacity;
        this.path = path;
        this.pathFirstImage = pathFirstImage;
        this.theme = theme;
        this.id = id;
        this.uris = uris;
        this.nameItem = nameItem;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }

    public String getTitle() {
        return title;
    }

    public String getCapacity() {
        return capacity;
    }

    public String getPathFirstImage() {
        return pathFirstImage;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public String getPath() {
        return path;
    }

    public String getTheme() {
        return theme;
    }

    public int getId() {
        return id;
    }

    public List<String> getUris() {
        return uris;
    }

    public String getNameItem() {
        return nameItem;
    }
}
