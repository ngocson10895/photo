package com.piccollage.collagemaker.picphotomaker.utils.billing;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.piccollage.collagemaker.picphotomaker.ui.SplashActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BillingUtils {

    public static void verifyPurchase(Context context, boolean isVerifyPurchase) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, Constant.NAME_SHARE);
        if (isVerifyPurchase) {
            sharedPrefs.put(Constant.IS_GOT_PURCHASE, true);
        } else {
            sharedPrefs.put(Constant.IS_GOT_PURCHASE, false);
        }
    }

    public static void queryPurchaseFinish(Context context) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, Constant.NAME_SHARE);
        sharedPrefs.put(Constant.IS_REALTIME_DATA_PURCHASE, true);
        sharedPrefs.put(Constant.IS_GOT_PURCHASE, true);
        new Handler().postDelayed(() -> restartApp(context), 1000);
    }

    private static void restartApp(Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
        Runtime.getRuntime().exit(0);
    }

    private static List<String> listSubscriptionFirst() {
        List<String> list = new ArrayList<>();
        list.add(BillingConstants.SUBSCRIPTION_MONTHLY_001);
        list.add(BillingConstants.SUBSCRIPTION_MONTH_ID_001);
        list.add(BillingConstants.SUBSCRIPTION_YEARLY_ID_001);
        list.add(BillingConstants.SUBSCRIPTION_YEARLY_TRIAL_ID_001);
        list.add(BillingConstants.SUBSCRIPTION_MONTH_TRIAL_ID_001);
        return list;
    }

    private static List<String> listSubscriptionSecond() {
        List<String> list = new ArrayList<>();
        list.add(BillingConstants.SUBSCRIPTION_MONTHLY_002);
        list.add(BillingConstants.SUBSCRIPTION_MONTH_ID_002);
        list.add(BillingConstants.SUBSCRIPTION_YEARLY_ID_002);
        list.add(BillingConstants.SUBSCRIPTION_YEARLY_TRIAL_ID_002);
        list.add(BillingConstants.SUBSCRIPTION_MONTH_TRAIL_ID_002);
        return list;
    }

    private static List<String> listSubscriptionThird() {
        List<String> list = new ArrayList<>();
        list.add(BillingConstants.SUBSCRIPTION_MONTHLY_003);
        list.add(BillingConstants.SUBSCRIPTION_MONTH_ID_003);
        list.add(BillingConstants.SUBSCRIPTION_YEARLY_ID_003);
        list.add(BillingConstants.SUBSCRIPTION_YEARLY_TRIAL_ID_003);
        list.add(BillingConstants.SUBSCRIPTION_MONTH_TRIAL_ID_003);
        return list;
    }

    public static List<String> getSkuListOneTime() {
        List<String> result = new ArrayList<>();
        result.add(BillingConstants.SKU_PHOTOCOLLAGE002);
        result.add(BillingConstants.SKU_REWARD_VIDEO);
        return result;
    }

    public static Map<Integer, List<String>> mapSubscriptionGroup() {
        Map<Integer, List<String>> mapMap = new HashMap<>();
        mapMap.put(1, listSubscriptionFirst());
        mapMap.put(2, listSubscriptionSecond());
        mapMap.put(3, listSubscriptionThird());
        return mapMap;
    }

}
