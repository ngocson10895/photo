package com.piccollage.collagemaker.picphotomaker.ui.scrapbookactivity.layout;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.DrawFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import android.util.Log;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;

/**
 * Image Entity. xử lý các tác vụ với từng ảnh
 */
public class ImageEntity extends MultiTouchEntity {
    private static final String TAG = "ImageEntity";
    private double mInitScaleFactor = 0.25;

    private transient Drawable mDrawable;

    private Uri mImageUri = null;
    private int mResourceId = -1;
    private boolean mDrawImageBorder = false;
    private int mBorderColor = Color.GREEN;
    private float mBorderSize = 3;
    private RectF mBoundRect = new RectF();
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private boolean mSticker = true;
    private GradientDrawable mGradientDrawable = new GradientDrawable(
            GradientDrawable.Orientation.TL_BR, new int[]{Color.TRANSPARENT,
            Color.GRAY});
    private boolean mDrawShadow = false;
    private int mShadowSize = 0;

    /**
     * Image Entity
     *
     * @param resourceId : resource ID
     * @param res        : resource
     */
    ImageEntity(int resourceId, Resources res) {
        super(res);
        mResourceId = resourceId;
        mImageUri = null;
        loadConfigs();
    }

    /**
     * Image Entity
     *
     * @param image : uri image
     * @param res   : resource
     */
    public ImageEntity(Uri image, Resources res) {
        super(res);
        mImageUri = image;
        mResourceId = -1;
        loadConfigs();
    }

    /**
     * Image Entity
     *
     * @param e   : image entity
     * @param res : resource
     */
    public ImageEntity(@NonNull ImageEntity e, Resources res) {
        super(res);
        mDrawable = e.mDrawable;
        mResourceId = e.mResourceId;
        mScaleX = e.mScaleX;
        mScaleY = e.mScaleY;
        mCenterX = e.mCenterX;
        mCenterY = e.mCenterY;
        mAngle = e.mAngle;
        mImageUri = e.mImageUri;
        loadConfigs();
    }

    /**
     * load Configs
     */
    private void loadConfigs() {
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mBorderSize);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
    }

    void setBorderColor(int color) {
        mBorderColor = color;
    }

    void setDrawImageBorder(boolean drawImageBorder) {
        mDrawImageBorder = drawImageBorder;
    }

    int getBorderColor() {
        return mBorderColor;
    }

    boolean isDrawImageBorder() {
        return mDrawImageBorder;
    }

    public void setSticker(boolean sticker) {
        mSticker = sticker;
    }

    public boolean isSticker() {
        return mSticker;
    }

    void setShadowSize(int shadowSize) {
        mShadowSize = shadowSize;
    }

    void setDrawShadow(boolean drawShadow) {
        mDrawShadow = drawShadow;
    }

    public void setInitScaleFactor(double initScaleFactor) {
        mInitScaleFactor = initScaleFactor;
    }

    void setBorderSize(float borderSize) {
        mBorderSize = borderSize;
        mPaint.setStrokeWidth(mBorderSize);
    }

    public Uri getImageUri() {
        return mImageUri;
    }

    public void setImageUri(Context context, Uri imageUri) {
        unload();
        mImageUri = imageUri;
        load(context);
    }

    public int getResourceId() {
        return mResourceId;
    }

    public Drawable getDrawable() {
        return mDrawable;
    }

    /**
     * draw on canvas
     *
     * @param canvas : canvas
     */
    @Override
    public void draw(Canvas canvas) {
        draw(canvas, 1);
    }

    /**
     * draw on canvas with scale
     *
     * @param canvas : canvas
     * @param scale  : number of scale
     */
    public void draw(@NonNull Canvas canvas, float scale) {
        canvas.save();
        if (mDrawable == null) {
            return;
        }

        if (mDrawable instanceof BitmapDrawable) {
            Bitmap bm = ((BitmapDrawable) mDrawable).getBitmap();
            if (bm == null || bm.isRecycled()) {
                return;
            }
        }

        float dx = scale * (mMaxX + mMinX) / 2;
        float dy = scale * (mMaxY + mMinY) / 2;

        mDrawable.setBounds((int) (scale * mMinX), (int) (scale * mMinY), (int) (scale * mMaxX), (int) (scale * mMaxY));

        canvas.translate(dx, dy);
        canvas.rotate(mAngle * 180.0f / (float) Math.PI);
        canvas.translate(-dx, -dy);

        if (mDrawShadow && !mSticker && mShadowSize > 1) {
            drawShadow(canvas, scale);
        }

        mDrawable.draw(canvas);
        canvas.restore();
    }

    /**
     * Draw shadow
     *
     * @param canvas : canvas
     * @param scale  : number of scale
     */
    private void drawShadow(Canvas canvas, float scale) {
        mGradientDrawable.setBounds((int) (scale * (mMinX + mShadowSize)), (int) (scale * (mMinY
                + mShadowSize)), (int) (scale * (mMaxX + mShadowSize)), (int) (scale * (mMaxY
                + mShadowSize)));
        mGradientDrawable.setCornerRadius(5);
        mGradientDrawable.draw(canvas);
    }

    /**
     * check null
     *
     * @return : true
     */
    public boolean isNull() {
        if (mImageUri != null || mResourceId > 0) {
            return false;
        }
        Log.d(TAG, "isNull() called");
        return true;
    }

    /**
     * Called by activity's onPause() method to free memory used for loading the images
     */
    @Override
    public void unload() {
        if (mDrawable instanceof BitmapDrawable) {
            Bitmap bm = ((BitmapDrawable) mDrawable).getBitmap();
            if (bm != null && !bm.isRecycled()) {
                bm.recycle();
            }
        }
        this.mDrawable = null;
    }

    /**
     * load
     *
     * @param context : context
     */
    @Override
    public void load(Context context) {
        Resources res = context.getResources();
        getMetrics(res);

        if (mDrawable == null) {
            mDrawable = createDrawableFromPrimaryInfo(context);
        }
        if (mDrawable == null) {
            if (mImageUri != null) {
                resetPrimaryInfo();
            }
            return;
        }

        mWidth = mDrawable.getIntrinsicWidth();
        mHeight = mDrawable.getIntrinsicHeight();
        setPos(mCenterX, mCenterY, mScaleX, mScaleY, mAngle);
    }

    /**
     * Called by activity's onResume() method to load the images
     */
    public void load(Context context, float startMidX, float startMidY,
                     float startAngle) {
        Resources res = context.getResources();
        getMetrics(res);

        mStartMidX = startMidX;
        mStartMidY = startMidY;
        if (mDrawable == null) {
            mDrawable = createDrawableFromPrimaryInfo(context);
        }

        if (mDrawable == null) {
            if (mImageUri != null) {
                resetPrimaryInfo();
            }
            return;
        }

        mWidth = mDrawable.getIntrinsicWidth();
        mHeight = mDrawable.getIntrinsicHeight();

        float centerX;
        float centerY;
        float scaleX;
        float scaleY;
        // float angle;
        if (mFirstLoad) {
            centerX = startMidX;
            centerY = startMidY;

            float scaleFactor = (float) (Math
                    .min(mDisplayWidth, mDisplayHeight)
                    / (float) Math.max(mWidth, mHeight) * mInitScaleFactor);
            scaleX = scaleY = scaleFactor;
            mAngle = startAngle;

            mFirstLoad = false;
        } else {
            centerX = mCenterX;
            centerY = mCenterY;
            scaleX = mScaleX;
            scaleY = mScaleY;
            // angle = mAngle;
        }
        setPos(centerX, centerY, scaleX, scaleY, mAngle);
    }

    @Override
    public void load(Context context, float startMidX, float startMidY) {
        load(context, startMidX, startMidY, 0);
    }

    /**
     * Create a Drawable with primary info
     *
     * @param context : context
     * @return : a drawable
     */
    private Drawable createDrawableFromPrimaryInfo(Context context) {
        Resources res = context.getResources();
        Drawable drawable = null;
        if (mImageUri != null) {
            drawable = ImageDecoder.decodeUriToDrawable(context, mImageUri);
        } else if (mResourceId > 0) {
            drawable = res.getDrawable(mResourceId);
        }
        return drawable;
    }

    /**
     * reset Primary info
     */
    private void resetPrimaryInfo() {
        mImageUri = null;
        mResourceId = -1;
    }

    /**
     * write object to parcel
     *
     * @param dest  : The Parcel in which the object should be written.
     * @param flags : Additional flags about how the object should be written
     */
    // parcelable
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeDouble(mInitScaleFactor);
        dest.writeParcelable(mImageUri, flags);
        dest.writeInt(mResourceId);
        dest.writeBooleanArray(new boolean[]{mDrawImageBorder, mSticker});
        dest.writeInt(mBorderColor);
        dest.writeFloat(mBorderSize);
        dest.writeParcelable(mBoundRect, flags);
        Log.d(TAG, "writeToParcel() called with: dest = [" + dest + "], flags = [" + flags + "]");
    }

    /**
     * generates instances of your Parcelable class from a Parcel
     */
    public static final Parcelable.Creator<ImageEntity> CREATOR = new Parcelable.Creator<ImageEntity>() {
        @Override
        public ImageEntity createFromParcel(Parcel in) {
            return new ImageEntity(in);
        }

        @Override
        public ImageEntity[] newArray(int size) {
            return new ImageEntity[size];
        }
    };

    /**
     * Read object from Parcel
     *
     * @param in : The parcel to read the point's coordinates ( never null )
     */
    @Override
    public void readFromParcel(Parcel in) {
        super.readFromParcel(in);
        mInitScaleFactor = in.readDouble();
        mImageUri = in.readParcelable(Uri.class.getClassLoader());
        mResourceId = in.readInt();
        boolean[] val = new boolean[2];
        in.readBooleanArray(val);
        mDrawImageBorder = val[0];
        mSticker = val[1];
        mBorderColor = in.readInt();
        mBorderSize = in.readFloat();
        mBoundRect = in.readParcelable(RectF.class.getClassLoader());
    }

    /**
     * Image entity
     *
     * @param in : Parcel
     */
    private ImageEntity(Parcel in) {
        readFromParcel(in);
    }
}
