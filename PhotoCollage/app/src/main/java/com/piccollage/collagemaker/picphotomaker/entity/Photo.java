package com.piccollage.collagemaker.picphotomaker.entity;

import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Photo
 */
public class Photo implements Parcelable{
    private String pathPhoto;
    private int timesPick;
    private long timeMf;

    public long getTimeMf() {
        return timeMf;
    }

    public void setTimeMf(long timeMf) {
        this.timeMf = timeMf;
    }

    public String maskPath;
    public String template;
    private static final int SHRINK_METHOD_DEFAULT = 0;
    public static final int SHRINK_METHOD_3_3 = 1;
    public static final int SHRINK_METHOD_USING_MAP = 2;
    public static final int SHRINK_METHOD_3_6 = 3;
    public static final int SHRINK_METHOD_3_8 = 4;
    public static final int SHRINK_METHOD_COMMON = 5;
    private static final int CORNER_METHOD_DEFAULT = 0;
    public static final int CORNER_METHOD_3_6 = 1;
    public static final int CORNER_METHOD_3_13 = 2;
    //Primary info
    //Using point list to construct view. All points and width, height are in [0, 1] range.
    public float x = 0;
    public float y = 0;
    public ArrayList<PointF> pointList = new ArrayList<>();
    public RectF bound = new RectF();
    //Using pathPhoto to create
    public Path path = null;
    public RectF pathRatioBound = null;
    public boolean pathInCenterHorizontal = false;
    public boolean pathInCenterVertical = false;
    public boolean pathAlignParentRight = false;
    public float pathScaleRatio = 1;
    public boolean fitBound = false;
    //other info
    public boolean hasBackground = false;
    public int shrinkMethod = SHRINK_METHOD_DEFAULT;
    public int cornerMethod = CORNER_METHOD_DEFAULT;
    public boolean disableShrink = false;
    public HashMap<PointF, PointF> shrinkMap;
    //Clear polygon or arc area
    public ArrayList<PointF> clearAreaPoints;
    //Clear an area using pathPhoto
    public Path clearPath = null;
    public RectF clearPathRatioBound = null;
    public boolean clearPathInCenterHorizontal = false;
    public boolean clearPathInCenterVertical = false;
    public float clearPathScaleRatio = 1;
    public boolean centerInClearBound = false;

    public Photo(String pathPhoto, int timesPick) {
        this.pathPhoto = pathPhoto;
        this.timesPick = timesPick;
    }

    protected Photo(Parcel in) {
        pathPhoto = in.readString();
        timesPick = in.readInt();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public void setPathPhoto(String pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }

    public int getTimesPick() {
        return timesPick;
    }

    public void setTimesPick(int timesPick) {
        this.timesPick = timesPick;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pathPhoto);
        dest.writeInt(timesPick);
    }
}
