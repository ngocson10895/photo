package com.piccollage.collagemaker.picphotomaker.ui.morefeature;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Layout;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ItemFeatureTextAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.PagerAdapterFont;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentAddTextBinding;
import com.piccollage.collagemaker.picphotomaker.entity.FontItem;
import com.piccollage.collagemaker.picphotomaker.entity.FontTheme;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.TextOfUser;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventFontPick;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopFont;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: thực hiện việc add text
 * Des: bao gồm các function để user có thể tùy chỉnh text và add text
 */
public class AddTextFragment extends DialogFragment implements SeekBar.OnSeekBarChangeListener {
    private static final String TAG = "AddTextFragment";
    private static final String TEXT_USER = "text_of_user";

    private ArrayList<FontTheme> listFont;
    private InputMethodManager inputMethodManager;
    private ItemFeatureTextAdapter addTextAdapter, customFeatureAdapter;
    private ISendTextView iSendTextView;
    private String fontPath = Constant.FONT_DEFAULT;
    private ApplicationViewModel viewModel;
    private Layout.Alignment alignment;
    private FragmentAddTextBinding binding;
    private PagerAdapterFont pagerAdapterFont;
    private int posPage = 0;
    private int countAlign = 0;
    private TextOfUser textOfUser;

    public interface ISendTextView {
        void turnOff();

        void sendText(TextOfUser textOfUser);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ISendTextView) {
            iSendTextView = (ISendTextView) context;
        } else {
            Log.d(TAG, "onAttach: must implement this interface");
        }
    }

    public void setBundle(TextOfUser textOfUser) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(TEXT_USER, textOfUser);
        setArguments(bundle);
    }

    // khi fragment detach thì sẽ gửi lại text of user
    @Override
    public void onDetach() {
        super.onDetach();
        if (iSendTextView != null) {
            if (textOfUser != null) {
                iSendTextView.sendText(textOfUser);
            }
            iSendTextView.turnOff();
        }
        iSendTextView = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAddTextBinding.inflate(inflater, container, false);
        Objects.requireNonNull(getDialog()).requestWindowFeature(Window.FEATURE_NO_TITLE);

        initView();
        return binding.getRoot();
    }

    private void initView() {
        if (getContext() != null) {
            byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
            for (int i = 0; i < dc.length; i++) {
                if (dc[i] != getContext().getPackageName().codePointAt(i)) {
                    if (getActivity() != null) getActivity().finish();
                }
            }
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewText.NAME);
            binding.adsLayoutT.post(() -> Advertisement.showBannerAdOther(getContext(), binding.adsLayoutT));
            binding.edtText.requestFocus();
            addTextAdapter = new ItemFeatureTextAdapter(new ArrayList<>(), getContext());
            binding.rvFeature.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            binding.rvFeature.addItemDecoration(new BetweenSpacesItemDecoration(0, 32));
            binding.rvFeature.setAdapter(addTextAdapter);
            addTextAdapter.setOnItemClickListener((adapter1, view, position) -> {
                for (Icon icon : addTextAdapter.getData()) {
                    icon.setPick(false);
                }
                addTextAdapter.getData().get(position).setPick(true);
                addTextAdapter.notifyDataSetChanged();
                posTextFeature(position);
            });

            customFeatureAdapter = new ItemFeatureTextAdapter(new ArrayList<>(), getContext());
            binding.rvCustomFeature.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            binding.rvCustomFeature.addItemDecoration(new BetweenSpacesItemDecoration(0, 16));
            binding.rvCustomFeature.setAdapter(customFeatureAdapter);
            customFeatureAdapter.setOnItemClickListener((adapter1, view, position) -> {
                for (Icon icon : customFeatureAdapter.getData()) {
                    icon.setPick(false);
                }
                customFeatureAdapter.getData().get(position).setPick(true);
                customFeatureAdapter.notifyDataSetChanged();
                posCustomTextPick(position);
            });

            binding.sbLetterSpacing.setOnSeekBarChangeListener(this);
            binding.sbLineSpacing.setOnSeekBarChangeListener(this);
            binding.sbShadow.setOnSeekBarChangeListener(this);
            binding.sbOpacity.setOnSeekBarChangeListener(this);

            binding.sbColorText.setHexColors(InitData.colors());
            binding.sbColorText.setListener((position, color) -> binding.edtText.setTextColor(color));

            binding.viewPagerFont.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabBarFont) {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    posPage = position;
                }
            });
            binding.tabBarFont.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(binding.viewPagerFont));
            listFont = new ArrayList<>();
            pagerAdapterFont = new PagerAdapterFont(getChildFragmentManager(), new ArrayList<>());
            binding.viewPagerFont.setAdapter(pagerAdapterFont);
            binding.tabBarFont.setupWithViewPager(binding.viewPagerFont);

            onBtnDoneClicked();
            onIvBackTextClicked();
            onGoToShop();
        }
    }

    private void onGoToShop() {
        binding.iconShop.setOnClickListener(v -> {
            if (getActivity() != null) {
                Intent intent = new Intent(getActivity(), StoreActivity.class);
                intent.putExtra(StoreActivity.VALUE, Constant.FONT);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
        Dialog dialog = getDialog();
        //Make dialog full screen with transparent background
        if (dialog != null && dialog.getWindow() != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getMoreFontData(EventItemShopFont event) {
        viewModel.getDataFonts().observe(getViewLifecycleOwner(), itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (!itemsDownloaded.isEmpty()) {
                    for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                        for (FontTheme fontTheme : event.getFontThemes()) {
                            if (itemDownloaded.getName().equals(fontTheme.getTheme())) {
                                fontTheme.setDownload(true);
                                File file = new File(itemDownloaded.getPath());
                                if (file.exists()) {
                                    for (int i = 0; i < file.listFiles().length; i++) {
                                        if (i < fontTheme.getFontItems().size() && !file.listFiles()[i].isDirectory()) {
                                            fontTheme.getFontItems().get(i).setmFontPath(file.listFiles()[i].getAbsolutePath());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    listFont.removeAll(event.getFontThemes());
                    listFont.addAll(event.getFontThemes());
                    pagerAdapterFont.setNewData(listFont);
                    pagerAdapterFont.notifyDataSetChanged();
                    new Handler().postDelayed(() -> Objects.requireNonNull(binding.tabBarFont.getTabAt(posPage)).select(), 100);
                    customTab();
                } else {
                    for (FontTheme fontTheme : event.getFontThemes()) {
                        fontTheme.setDownload(false);
                    }
                    listFont.removeAll(event.getFontThemes());
                    listFont.addAll(event.getFontThemes());
                    pagerAdapterFont.setNewData(listFont);
                    pagerAdapterFont.notifyDataSetChanged();
                    new Handler().postDelayed(() -> Objects.requireNonNull(binding.tabBarFont.getTabAt(posPage)).select(), 100);
                    customTab();
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        }
        initData();
    }

    private void initData() {
        if (getArguments() != null) {
            textOfUser = getArguments().getParcelable(TEXT_USER);
            if (textOfUser != null) {
                binding.edtText.setText(textOfUser.getContent());
                binding.edtText.setTextColor(textOfUser.getColorText());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    binding.edtText.setLetterSpacing(textOfUser.getLetterSpacing());
                }
                binding.edtText.setLineSpacing(textOfUser.getLineSpacing(), textOfUser.getLineSpacing());
                binding.edtText.setShadowLayer(textOfUser.getRadius(), textOfUser.getDx(), textOfUser.getDy(), textOfUser.getColorShadow());
                binding.edtText.setTypeface(textOfUser.getTypeface());
                binding.edtText.setSelection(textOfUser.getContent().length());

                binding.sbLetterSpacing.setProgress((int) (textOfUser.getLetterSpacing() * 30));
                binding.sbLineSpacing.setProgress((int) (textOfUser.getLineSpacing() * 10));
                binding.sbShadow.setProgress((int) textOfUser.getDx());
            }
        }

        addTextAdapter.setNewData(InitData.addTextFeature());
        customFeatureAdapter.setNewData(InitData.addTextCustomFeature());
        addFont();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void fontPick(EventFontPick event) {
        Typeface font = TextUtils.loadTypeface(getContext(), event.getFontItem().getFontPath());
        binding.edtText.setTypeface(font);
        fontPath = event.getFontItem().getFontPath();
    }

    private void addFont() {
        FontTheme fontTheme = new FontTheme(Constant.DEFAULT, new ArrayList<>(), true, "", false);
        ArrayList<FontItem> fontItems = (ArrayList<FontItem>) TextUtils.listAssetFiles("fonts", getContext());
        for (FontItem fontItem : fontItems) {
            if (fontItem.getFontPath().equals(fontPath)) {
                fontTheme.getFontItems().add(new FontItem(String.valueOf(fontItems.indexOf(fontItem)), fontItem.getFontPath(), true, Constant.DEFAULT));
            } else
                fontTheme.getFontItems().add(new FontItem(String.valueOf(fontItems.indexOf(fontItem)), fontItem.getFontPath(), false, Constant.DEFAULT));
        }
        listFont.add(fontTheme);
        pagerAdapterFont.setNewData(listFont);
        pagerAdapterFont.notifyDataSetChanged();
        customTab();
    }

    private void customTab() {
        for (int i = 0; i < binding.tabBarFont.getTabCount(); i++) {
            TabLayout.Tab tab = binding.tabBarFont.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(R.layout.custom_tab_text);
                if (tab.getCustomView() != null) {
                    TextView text = tab.getCustomView().findViewById(R.id.title);
                    ImageView icDownloaded = tab.getCustomView().findViewById(R.id.iconNotDownload);
                    text.setText(pagerAdapterFont.getPageTitle(i));
                    if (listFont.get(i).isDownload()) {
                        icDownloaded.setVisibility(View.INVISIBLE);
                    } else icDownloaded.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void posTextFeature(int position) {
        if (getView() != null) {
            switch (position) {
                case 0:
                    MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.TEXT);
                    binding.edtText.setEnabled(true);
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    binding.v.setVisibility(View.INVISIBLE);
                    binding.fontView.setVisibility(View.GONE);
                    binding.clCustom.setVisibility(View.GONE);
                    break;
                case 1:
                    MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.FONT);
                    binding.edtText.setEnabled(false);
                    inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    new Handler().postDelayed(() -> {
                        binding.v.setVisibility(View.INVISIBLE);
                        binding.fontView.setVisibility(View.VISIBLE);
                        binding.clCustom.setVisibility(View.INVISIBLE);
                    }, 300);
                    break;
                case 2:
                    MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.CUSTOM);
                    binding.edtText.setEnabled(false);
                    inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    new Handler().postDelayed(() -> {
                        binding.v.setVisibility(View.INVISIBLE);
                        binding.clCustom.setVisibility(View.VISIBLE);
                        binding.fontView.setVisibility(View.INVISIBLE);
                    }, 300);
                    break;
                default:
            }
        }
    }

    private void posCustomTextPick(int position) {
        switch (position) {
            case 0:
                MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.CHARACTER_SPACE);
                binding.sbLetterSpacing.setVisibility(View.VISIBLE);
                binding.sbLineSpacing.setVisibility(View.INVISIBLE);
                binding.sbOpacity.setVisibility(View.INVISIBLE);
                binding.sbShadow.setVisibility(View.INVISIBLE);
                break;
            case 1:
                MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.LINE_SPACE);
                binding.sbLetterSpacing.setVisibility(View.INVISIBLE);
                binding.sbLineSpacing.setVisibility(View.VISIBLE);
                binding.sbOpacity.setVisibility(View.INVISIBLE);
                binding.sbShadow.setVisibility(View.INVISIBLE);
                break;
            case 2:
                MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.SHADOW);
                binding.sbLetterSpacing.setVisibility(View.INVISIBLE);
                binding.sbLineSpacing.setVisibility(View.INVISIBLE);
                binding.sbOpacity.setVisibility(View.INVISIBLE);
                binding.sbShadow.setVisibility(View.VISIBLE);
                break;
            case 3:
                MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.OPACITY_COLOR);
                binding.sbLetterSpacing.setVisibility(View.INVISIBLE);
                binding.sbLineSpacing.setVisibility(View.INVISIBLE);
                binding.sbOpacity.setVisibility(View.VISIBLE);
                binding.sbShadow.setVisibility(View.INVISIBLE);
                break;
            case 4:
                if (getContext() != null) {
                    // text alignment không hỗ trợ cho android < 9
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.ViewText.ALIGNMENT);
                        ImageView icon = (ImageView) customFeatureAdapter.getViewByPosition(binding.rvCustomFeature, 4, R.id.iv_icon);
                        if (icon != null) {
                            countAlign++;
                            if (countAlign % 3 == 1) {
                                Glide.with(this).load(getContext().getDrawable(R.drawable.ic_icmenualigncenter)).into(icon);
                                binding.edtText.setGravity(Gravity.CENTER);
                                alignment = Layout.Alignment.ALIGN_CENTER;
                            } else if (countAlign % 3 == 2) {
                                Glide.with(this).load(getContext().getDrawable(R.drawable.ic_icmenualignright)).into(icon);
                                binding.edtText.setGravity(Gravity.END);
                                alignment = Layout.Alignment.ALIGN_RIGHT;
                            } else {
                                Glide.with(this).load(getContext().getDrawable(R.drawable.ic_icmenualignleft)).into(icon);
                                binding.edtText.setGravity(Gravity.START);
                                alignment = Layout.Alignment.ALIGN_LEFT;
                            }
                        }
                    } else {
                        Toasty.error(getContext(), getString(R.string.device_not_support), Toasty.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @SuppressLint("NewApi")
    private void onBtnDoneClicked() {
        binding.doneText.setOnClickListener(v1 -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewText.DONE);
            inputMethodManager.hideSoftInputFromWindow(binding.doneText.getWindowToken(), 0);
            textOfUser = new TextOfUser();
            textOfUser.setContent(binding.edtText.getText().toString());
            textOfUser.setColorText(binding.edtText.getCurrentTextColor());
            textOfUser.setColorShadow(binding.edtText.getShadowColor());
            textOfUser.setRadius(binding.edtText.getShadowRadius());
            // letter spacing không hỗ trợ cho android < 5
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textOfUser.setLetterSpacing(binding.edtText.getLetterSpacing());
            }
            textOfUser.setLineSpacing(binding.edtText.getLineSpacingMultiplier());
            textOfUser.setTypeface(binding.edtText.getTypeface());
            textOfUser.setDx(binding.edtText.getShadowDx());
            textOfUser.setDy(binding.edtText.getShadowDy());
            textOfUser.setAlignment(alignment);
            dismiss();
        });
    }

    private void onIvBackTextClicked() {
        binding.ivBackText.setOnClickListener(v1 -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewText.CANCEL);
            inputMethodManager.hideSoftInputFromWindow(binding.ivBackText.getWindowToken(), 0);
            dismiss();
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.sb_letter_spacing:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    binding.edtText.setLetterSpacing((float) progress / 30);
                } else {
                    if (getContext() != null) {
                        Toasty.error(getContext(), getString(R.string.device_not_support), Toasty.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.sb_line_spacing:
                binding.edtText.setLineSpacing(0, (float) progress / 10);
                break;
            case R.id.sb_shadow:
                binding.edtText.setShadowLayer(1, (float) progress, (float) progress, R.color.black);
                break;
            case R.id.sb_opacity:
                int red = Color.red(binding.edtText.getCurrentTextColor());
                int blue = Color.blue(binding.edtText.getCurrentTextColor());
                int green = Color.green(binding.edtText.getCurrentTextColor());
                binding.edtText.setTextColor(Color.argb(progress, red, green, blue));
                break;
            default:
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Log.d(TAG, "onStartTrackingTouch: ");
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.d(TAG, "onStopTrackingTouch: ");
    }
}
