package com.piccollage.collagemaker.picphotomaker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemGalleryBinding;
import com.piccollage.collagemaker.picphotomaker.entity.GalleryAlbum;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.viewholder.UnifiedNativeAdBannerViewHolder;

import java.util.List;

/**
 * Gallery adapter cho phần chọn ảnh có thêm ads native
 */
public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;
    private static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 1;

    private Context context;
    private List<Object> items;
    private IGalleryContract iGalleryContract;

    public interface IGalleryContract {
        void pickGallery(int position);
    }

    public GalleryAdapter(Context context, List<Object> items) {
        this.context = context;
        this.items = items;
    }

    public void setiGalleryContract(IGalleryContract iGalleryContract) {
        this.iGalleryContract = iGalleryContract;
    }

    public List<Object> getItems() {
        return items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        switch (i) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.Native.SHOW);
                View unifiedNativeLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ads_banner_gallery, parent, false);
                return new UnifiedNativeAdBannerViewHolder(unifiedNativeLayoutView);
            case MENU_ITEM_VIEW_TYPE:
                // Fall through.
            default:
                View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
                return new GalleryViewHolder(menuItemLayoutView);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        int viewType = getItemViewType(i);
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) items.get(i);
                NativeAdAdapter.populateUnifiedNativeAdViewBanner(nativeAd, ((UnifiedNativeAdBannerViewHolder) viewHolder).getAdView());
                break;
            case MENU_ITEM_VIEW_TYPE:
                // fall through
            default:
                GalleryViewHolder itemHolder = (GalleryViewHolder) viewHolder;
                GalleryAlbum galleryAlbum = (GalleryAlbum) items.get(i);
                itemHolder.binding.tvTitle.setText(galleryAlbum.getAlbumName());
                Glide.with(context).asBitmap().load(galleryAlbum.getListImage().get(0).getPathPhoto()).
                        centerCrop().apply(RequestOptions.circleCropTransform()).into(itemHolder.binding.ivFirstImage);
                itemHolder.binding.tvQuantity.setText(galleryAlbum.getListImage().size() + " " + context.getString(R.string.images));
                viewHolder.itemView.setOnClickListener(v -> iGalleryContract.pickGallery(i));
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object recyclerViewItem = items.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE;
        }
        return MENU_ITEM_VIEW_TYPE;
    }

    static class GalleryViewHolder extends RecyclerView.ViewHolder {
        ItemGalleryBinding binding;

        GalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemGalleryBinding.bind(itemView);
        }
    }

    public void setItems(List<Object> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
