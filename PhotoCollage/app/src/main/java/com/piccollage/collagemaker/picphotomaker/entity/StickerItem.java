package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class StickerItem implements Parcelable {
    private String nameTheme;
    private String url;

    public StickerItem(String nameTheme, String url) {
        this.nameTheme = nameTheme;
        this.url = url;
    }

    private StickerItem(Parcel in) {
        nameTheme = in.readString();
        url = in.readString();
    }

    public static final Creator<StickerItem> CREATOR = new Creator<StickerItem>() {
        @Override
        public StickerItem createFromParcel(Parcel in) {
            return new StickerItem(in);
        }

        @Override
        public StickerItem[] newArray(int size) {
            return new StickerItem[size];
        }
    };

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameTheme);
        dest.writeString(url);
    }
}
