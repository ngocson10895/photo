package com.piccollage.collagemaker.picphotomaker.base;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.entity.GalleryAlbum;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.DateTimeUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Base Fragment
 */
public class BaseFragment extends Fragment {
    private static final String TAG = "BaseFragment";

    private long lastClickTime = 0;

    // check time để tránh trường hợp user click liên tục trong 1 khoảng thời gian
    public boolean checkDoubleClick() {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return false;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        return true;
    }

    /**
     * Get list path from assets
     *
     * @param path : path
     * @return : list
     */
    public List<String> listAssetFiles(String path) {
        List<String> listFrame = new ArrayList<>();
        listFrame.add("");
        try {
            if (getContext() != null) {
                String[] fileList = getContext().getAssets().list(path);
                if (fileList == null) {
                    Log.d(TAG, "listAssetFiles: null");
                } else {
                    listFrame.clear();
                    listFrame.addAll(Arrays.asList(fileList));
                }
            } else {
                Log.d(TAG, "listAssetFiles: null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listFrame;
    }

    /**
     * Load Album in device
     */
    public interface AlbumListener {
        void onStart();

        void onFinish(List<GalleryAlbum> ab, long id);
    }

    private AlbumListener albumListener;

    protected void setListenerAlbum(AlbumListener abListener) {
        this.albumListener = abListener;
    }

    // Asyntask để load toàn bộ album ảnh trong device. Có thể sử dụng rxandroid để thay thế và tối ưu hiệu năng
    public static class LoadAlbumAsync extends AsyncTask<Void, Void, List<GalleryAlbum>> {
        private WeakReference<BaseFragment> weakReference;

        public LoadAlbumAsync(BaseFragment fragment) {
            weakReference = new WeakReference<>(fragment);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            BaseFragment fragment = weakReference.get();
            if (fragment == null || fragment.isDetached()) {
                return;
            }
            if (fragment.albumListener != null)
                fragment.albumListener.onStart();
        }

        @SuppressLint("UseSparseArrays")
        @Override
        protected List<GalleryAlbum> doInBackground(Void... voids) {
            BaseFragment fragment = weakReference.get();
            if (fragment == null || fragment.isDetached()) {
                return null;
            }
            final HashMap<Long, GalleryAlbum> map = new HashMap<>();
            // which image properties are we querying
            final String[] projection = new String[]{
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.BUCKET_ID,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.Media.DATE_TAKEN
            };

            // Get the base URI for the People table in the Contacts content provider.
            Uri uriImages = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            Cursor cursor = null;
            if (fragment.getActivity() != null) {
                try {
                    // Make the query.
                    ContentResolver contentResolver = fragment.getActivity().getContentResolver();
                    cursor = contentResolver.query(uriImages,
                            projection, // Which columns to return
                            "",         // Which rows to return (all rows)
                            null,       // Selection arguments (none)
                            MediaStore.Images.Media.DATE_TAKEN + " DESC"         // Ordering follow DESC
                    );

                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            // Get the field values
                            String bucketName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)); // name gallery
                            long date = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN)); // date gallery
                            String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)); // image path
                            long bucketId = cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_ID)); // id for bucket
                            // Do something with the values.
                            GalleryAlbum album = map.get(bucketId);
                            if (album == null) {
                                album = new GalleryAlbum(bucketName, DateTimeUtils.toUTCDateTimeString(new Date(date)), bucketId);
                                Photo p = new Photo(imagePath, 0);
                                album.getListImage().add(p);
                                map.put(bucketId, album);
                            } else {
                                Photo p = new Photo(imagePath, 0);
                                album.getListImage().add(p);
                            }
                        } while (cursor.moveToNext());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
            }
            return new ArrayList<>(map.values());
        }

        @Override
        protected void onPostExecute(List<GalleryAlbum> aVoid) {
            super.onPostExecute(aVoid);
            BaseFragment fragment = weakReference.get();
            if (fragment == null || fragment.isDetached()) {
                return;
            }
            long id = 0;
            for (GalleryAlbum album : aVoid) {
                if (album.getAlbumName() != null && album.getAlbumName().equals("My Creative")) {
                    id = album.getIdAlbum();
                    break;
                }
            }
            if (fragment.albumListener != null)
                fragment.albumListener.onFinish(aVoid, id);
        }
    }

    // add fragment
    protected void fragmentReplaces(@NonNull Fragment var2, String tag) {
        if (getActivity() != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.add(R.id.frGallery, var2);
            fragmentTransaction.addToBackStack(tag);
            fragmentTransaction.commit();
        }
    }
}
