package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemCollageTypeBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Frame;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;

import java.util.List;

/**
 * Collage Type Adapter
 */
public class CollageTypeAdapter extends BaseQuickAdapter<Frame, BaseViewHolder> {
    private Context context;

    public CollageTypeAdapter(@Nullable List<Frame> data, Context context) {
        super(R.layout.item_collage_type, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Frame item) {
        ItemCollageTypeBinding binding = ItemCollageTypeBinding.bind(helper.itemView);
        binding.ivFrame.setImageBitmap(ImageDecoder.getBitmapFromAssets("frame/" + item.getPath(), context));
        binding.v.setVisibility((item.isPick()) ? View.VISIBLE : View.INVISIBLE);
    }
}
