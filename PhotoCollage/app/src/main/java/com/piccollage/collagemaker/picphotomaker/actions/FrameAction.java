package com.piccollage.collagemaker.picphotomaker.actions;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

import java.util.List;

import dauroi.com.imageprocessing.ImageProcessor;
import dauroi.com.imageprocessing.filter.ImageFilter;
import dauroi.com.imageprocessing.filter.blend.SourceOverBlendFilter;
import dauroi.photoeditor.R;
import dauroi.photoeditor.database.table.ItemPackageTable;
import dauroi.photoeditor.database.table.ShadeTable;
import dauroi.photoeditor.listener.ApplyFilterListener;
import dauroi.photoeditor.model.ItemInfo;
import dauroi.photoeditor.model.ShadeInfo;
import dauroi.photoeditor.utils.PhotoUtils;
import dauroi.photoeditor.utils.Utils;

/**
 * Not use
 */
public class FrameAction extends MaskAction {
    FrameAction(EditorActivity activity) {
        super(activity, ItemPackageTable.FRAME_TYPE);
    }

    @Override
    public void saveInstanceState(Bundle bundle) {
        super.saveInstanceState(bundle);
        saveCurrentInfos(bundle, "dauroi.photoeditor.actions.FrameAction.mCurrentPosition",
                "dauroi.photoeditor.actions.FrameAction.mPackageId",
                "dauroi.photoeditor.actions.FrameAction.mCurrentPackageFolder");
        saveMaps(bundle, "dauroi.photoeditor.actions.FrameAction.mSelectedItemIndexes",
                "dauroi.photoeditor.actions.FrameAction.mListViewPositions");
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        super.restoreInstanceState(bundle);
        restoreCurrentInfos(bundle, "dauroi.photoeditor.actions.FrameAction.mCurrentPosition",
                "dauroi.photoeditor.actions.FrameAction.mPackageId",
                "dauroi.photoeditor.actions.FrameAction.mCurrentPackageFolder");
        restoreMaps(bundle, "dauroi.photoeditor.actions.FrameAction.mSelectedItemIndexes",
                "dauroi.photoeditor.actions.FrameAction.mListViewPositions");
    }

    @Override
    public void apply(final boolean finish) {
        if (!isAttached()) {
            return;
        }
        ApplyFilterTask task = new ApplyFilterTask(activity, new ApplyFilterListener() {

            @Override
            public void onFinishFiltering() {
                mImageMaskView.setBackgroundColor(Color.TRANSPARENT);
                mCurrentPosition = 0;
                mCurrentPackageId = 0;
                mCurrentPackageFolder = null;

                if (finish) {
                    done();
                }
            }

            @Override
            public Bitmap applyFilter() {
                Bitmap bm = null;
                try {
                    Drawable drawable = mImageMaskView.getBackground();
                    if (drawable instanceof BitmapDrawable) {
                        BitmapDrawable bd = (BitmapDrawable) drawable;
                        Bitmap frame = bd.getBitmap();
                        if (frame != null && !frame.isRecycled()) {
                            SourceOverBlendFilter filter = new SourceOverBlendFilter();
                            filter.setBitmap(frame);
                            bm = ImageProcessor.getFiltratedBitmap(activity.getImage(), filter);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return bm;
            }
        });

        task.execute();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.attachMaskView(null);
    }

    @SuppressLint("InflateParams")
    @Override
    public View inflateMenuView() {
        mRootActionView = mLayoutInflater.inflate(R.layout.photo_editor_action_frame, null);
        return mRootActionView;
    }

    @Override
    public void attach() {
        super.attach();
        mMaskLayout.setBackgroundColor(Color.TRANSPARENT);
        activity.attachMaskView(mMaskLayout);
        adjustImageMaskLayout();
        activity.applyFilter(new ImageFilter());
    }

    @Override
    public void onActivityResume() {
        super.onActivityResume();
        if (isAttached()) {
            activity.attachMaskView(mMaskLayout);
            activity.applyFilter(new ImageFilter());
        }
    }

    @Override
    protected int getMaskLayoutRes() {
        return R.layout.photo_editor_mask_layout;
    }

    protected String getShadeType() {
        return ShadeTable.FRAME_TYPE;
    }

    @SuppressLint("NewApi")
    @Override
    protected void selectNormalItem(int position) {
        final ItemInfo info = mMenuItems.get(position);
        // recycle old bg
        Drawable drawable = mImageMaskView.getBackground();
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bd = (BitmapDrawable) drawable;
            Bitmap bm = bd.getBitmap();
            if (bm != null && !bm.isRecycled()) {
                mImageMaskView.setBackgroundColor(Color.TRANSPARENT);
                bm.recycle();
                bm = null;
            }
        }

        Bitmap bg = PhotoUtils.decodePNGImage(activity, ((ShadeInfo) info).getForeground());
        mImageMaskView.setBackground(new BitmapDrawable(activity.getResources(), bg));
    }

    @Override
    protected List<? extends ItemInfo> loadNormalItems(long packageId, String packageFolder) {
        ShadeTable shadeTable = new ShadeTable(activity);
        List<ShadeInfo> frameInfos = shadeTable.getRows(packageId, getShadeType());
        if (packageFolder != null && packageFolder.length() > 0) {
            final String baseFolder = Utils.FRAME_FOLDER.concat("/").concat(packageFolder).concat("/");
            for (ShadeInfo info : frameInfos) {
                info.setForeground(baseFolder.concat(info.getForeground()));
                info.setThumbnail(baseFolder.concat(info.getThumbnail()));
            }
        }

        return frameInfos;
    }

    @Override
    public String getActionName() {
        return "FrameAction";
    }
}
