package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.data.item.DataSpecShop;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;

import java.util.ArrayList;

/**
 * Item shop detail adapter
 */
public class ItemShopDetailAdapter extends RecyclerView.Adapter<ItemShopDetailAdapter.ViewHolder> {
    private Context context;
    private ArrayList<DataSpecShop> pathImage;
    private String type;

    ItemShopDetailAdapter(Context context, ArrayList<DataSpecShop> pathImage, String type) {
        this.context = context;
        this.pathImage = pathImage;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (type.equals(Constant.GRADIENT)) {
            view = LayoutInflater.from(context).inflate(R.layout.item_shop_detail_gradient, viewGroup, false);
        } else if (type.equals(Constant.PALETTE)) {
            view = LayoutInflater.from(context).inflate(R.layout.item_shop_detail_pallete, viewGroup, false);
        } else
            view = LayoutInflater.from(context).inflate(R.layout.item_shop_detail, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        switch (type) {
            case Constant.GRADIENT:
                String[] colors = pathImage.get(i).getUrlThumb().split("_");
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR
                        , new int[]{Color.parseColor("#" + colors[0]), Color.parseColor("#" + colors[1])});
                if (viewHolder.ivItem != null) {
                    Glide.with(context).load(gradientDrawable)
                            .error(R.drawable.sticker_default)
                            .centerCrop()
                            .into(viewHolder.ivItem);
                }
                viewHolder.ivName.setTextColor(Color.WHITE);
                break;
            case Constant.PALETTE:
                String[] palettes = pathImage.get(i).getUrlThumb().split("_");

                if (viewHolder.iv1 != null) {
                    viewHolder.iv1.setImageDrawable(new ColorDrawable(Color.parseColor("#" + palettes[0])));
                }
                if (viewHolder.iv2 != null) {
                    viewHolder.iv2.setImageDrawable(new ColorDrawable(Color.parseColor("#" + palettes[1])));
                }
                if (viewHolder.iv3 != null) {
                    viewHolder.iv3.setImageDrawable(new ColorDrawable(Color.parseColor("#" + palettes[2])));
                }
                if (viewHolder.iv4 != null) {
                    viewHolder.iv4.setImageDrawable(new ColorDrawable(Color.parseColor("#" + palettes[3])));
                }
                if (viewHolder.iv5 != null) {
                    viewHolder.iv5.setImageDrawable(new ColorDrawable(Color.parseColor("#" + palettes[4])));
                }
                break;
            default:
                if (viewHolder.ivItem != null) {
                    Glide.with(context).load(pathImage.get(i).getUrlThumb())
                            .error(R.drawable.sticker_default)
                            .placeholder(R.drawable.sticker_default)
                            .diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop()
                            .into(viewHolder.ivItem);
                }
                break;
        }
        viewHolder.ivName.setText(pathImage.get(i).getTitle());
    }

    @Override
    public int getItemCount() {
        return pathImage.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        ImageView ivItem;
        TextView ivName;
        @Nullable
        ImageView iv1;
        @Nullable
        ImageView iv2;
        @Nullable
        ImageView iv3;
        @Nullable
        ImageView iv4;
        @Nullable
        ImageView iv5;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivItem = itemView.findViewById(R.id.iv_item);
            ivName = itemView.findViewById(R.id.ivName);
            iv1 = itemView.findViewById(R.id.iv1);
            iv2 = itemView.findViewById(R.id.iv2);
            iv3 = itemView.findViewById(R.id.iv3);
            iv4 = itemView.findViewById(R.id.iv4);
            iv5 = itemView.findViewById(R.id.iv5);
        }
    }
}
