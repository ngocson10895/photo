package com.piccollage.collagemaker.picphotomaker.viewholder;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class Patterns extends ExpandableGroup<Pattern> {
    private List<Pattern> items;

    public Patterns(String title, List<Pattern> items) {
        super(title, items);
        this.items = items;
    }

    @Override
    public String getTitle() {
        return super.getTitle();
    }

    @Override
    public List<Pattern> getItems() {
        return items;
    }
}
