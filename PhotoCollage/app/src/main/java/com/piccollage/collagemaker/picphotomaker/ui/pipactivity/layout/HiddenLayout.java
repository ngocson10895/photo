package com.piccollage.collagemaker.picphotomaker.ui.pipactivity.layout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.widget.AppCompatImageView;

import android.view.MotionEvent;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;

import dauroi.photoeditor.utils.PhotoUtils;

/**
 * Hidden layout để đánh dấu những ảnh không được chọn
 */
@SuppressLint("ViewConstructor")
public class HiddenLayout extends AppCompatImageView {

    private Bitmap mImage, mMaskImage;
    private Paint mPaint;
    private Matrix mImageMatrix, mScaleMatrix, mMaskMatrix, mScaleMaskMatrix;

    public HiddenLayout(Context context, Photo photo, String type) {
        super(context);

        mImage = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.parseColor("#95ffffff"))); // decode file image to bitmap
        if (type.equals("default")) {
            mMaskImage = ImageDecoder.getBitmapFromAssets("pip/" + photo.maskPath, getContext());
        } else
            mMaskImage = PhotoUtils.decodePNGImage(context, photo.maskPath);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG); // to paint a view
        mPaint.setFilterBitmap(true);

        setScaleType(ScaleType.MATRIX);
        setLayerType(LAYER_TYPE_SOFTWARE, mPaint);
        mImageMatrix = new Matrix();
        mScaleMatrix = new Matrix();
        mMaskMatrix = new Matrix();
        mScaleMaskMatrix = new Matrix();
    }

    public void build(final float viewWidth, final float viewHeight, final float scale) {
        if (mImage != null) {
            mImageMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(viewWidth, viewHeight, mImage.getWidth(), mImage.getHeight()));
            mScaleMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(scale * viewWidth, scale * viewHeight, mImage.getWidth(), mImage.getHeight()));
        }
        //mask
        if (mMaskImage != null) {
            mMaskMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(viewWidth, viewHeight, mMaskImage.getWidth(), mMaskImage.getHeight()));
            mScaleMaskMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(scale * viewWidth, scale * viewHeight, mMaskImage.getWidth(), mMaskImage.getHeight()));
        }

        invalidate();
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap image) {
        mImage = image;
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mImage, mImageMatrix, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(mMaskImage, mMaskMatrix, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }
}
