package com.piccollage.collagemaker.picphotomaker.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity(tableName = "item_downloaded")
public class ItemDownloaded {
    private String theme;
    private String path;
    private String name;
    private String fileSize;
    private String nameItem;

    @PrimaryKey(autoGenerate = true)
    private int id;

    public ItemDownloaded(String theme, String path, String name, String fileSize, String nameItem) {
        this.theme = theme;
        this.path = path;
        this.name = name;
        this.fileSize = fileSize;
        this.nameItem = nameItem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTheme() {
        return theme;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getNameItem() {
        return nameItem;
    }

    @NonNull
    @Override
    public String toString() {
        return theme + " " + path + " " + name + " " + nameItem;
    }
}
