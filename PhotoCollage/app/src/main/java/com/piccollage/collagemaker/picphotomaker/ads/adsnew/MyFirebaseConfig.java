package com.piccollage.collagemaker.picphotomaker.ads.adsnew;

import android.util.Log;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;

public class MyFirebaseConfig {

    private static FirebaseRemoteConfig firebaseRemoteConfig;

    private MyFirebaseConfig() {
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        if (firebaseRemoteConfig != null)
            firebaseRemoteConfig.setConfigSettingsAsync(configSettings);
    }

    public static MyFirebaseConfig getInstance() {
        MyFirebaseConfig remoteConfig = new MyFirebaseConfig();
        try {
            firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return remoteConfig;
    }

    public void fetch() {
        long cacheExpiration = 3600; // 1 hour in seconds.
        if (firebaseRemoteConfig == null) return;

        firebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                firebaseRemoteConfig.fetchAndActivate();

                long versionCode = firebaseRemoteConfig.getLong(Constant.KEY_VERSION_CODE);
                long countOpenAds = firebaseRemoteConfig.getLong(Constant.KEY_COUNT_OPEN_ADS);
                long timeDelay = firebaseRemoteConfig.getLong(Constant.KEY_TIME_DELAY_ADS);
                long eventComing = firebaseRemoteConfig.getLong(Constant.KEY_EVENT_COMING);
                long foreUpdate = firebaseRemoteConfig.getLong(Constant.KEY_FORCE_UPDATE);
                long splashDelay = firebaseRemoteConfig.getLong(Constant.KEY_SPLASH_DELAY);
                long adsTimeout = firebaseRemoteConfig.getLong(Constant.KEY_ADS_TIMEOUT);
                long changeAds = firebaseRemoteConfig.getLong(Constant.KEY_CHANGE_ADS);
                long adsDelay = firebaseRemoteConfig.getLong(Constant.KEY_ADS_DELAY_AFTER_OPEN);
                long changeAdsInters = firebaseRemoteConfig.getLong(Constant.KEY_CHANGE_ADS_INTERS);
                long adsTimeOutFeature = firebaseRemoteConfig.getLong(Constant.KEY_ADS_TIMEOUT_FEATURE);
                long keyTimeSale = firebaseRemoteConfig.getLong(Constant.KEY_TIME_SALE);
                Log.d("RemoteConfig", "onComplete: vc:" +
                        versionCode + " co:" + countOpenAds + " ti:" + timeDelay
                        + " ev:" + eventComing + " fc:" + foreUpdate + " sp:" + splashDelay +
                        " to: " + adsTimeout + " delay: " + adsDelay + " key change ads inter: " + changeAdsInters
                        + " key time sale: " + keyTimeSale);
                setKeyVersionCode(versionCode);
                setKeyCountOpenAds(countOpenAds);
                setKeyTimeDelayAds(timeDelay);
                setKeyEventComming(eventComing);
                setKeyForceUpdate(foreUpdate);
                setKeySplashDelay(splashDelay);
                setKeyAdsTimeOut(adsTimeout);
                setKeyChangeAds(changeAds);
                setKeyChangeAdsInters(changeAdsInters);
                setKeyAdsDelayAfterOpen(adsDelay);
                setKeyAdsTimeoutFeature(adsTimeOutFeature);
                setKeyTimeSale(keyTimeSale);
            }
        });
    }

    // 24/03/2020: key to reset time sale
    public static void setKeyTimeSale(long timeSale){
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_TIME_SALE, timeSale);
    }

    public static long getKeyTimeSale() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_TIME_SALE, 0);
    }

    public static void setKeyAdsTimeoutFeature(long adsDelayAfterOpen) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_ADS_TIMEOUT_FEATURE, adsDelayAfterOpen);
    }

    // 03/03/2020: key to set ads timeout of feature. Default value = 5
    public static long getKeyAdsTimeoutFeature() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_ADS_TIMEOUT_FEATURE, 5);
    }

    public static void setKeyChangeAdsInters(long adsDelayAfterOpen) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_ADS_DELAY_AFTER_OPEN, adsDelayAfterOpen);
    }

    // 03/03/2020: key to change ads inters. default value = 0
    public static long getKeyChangeAdsInters() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_ADS_DELAY_AFTER_OPEN, 0);
    }

    public static void setKeyAdsDelayAfterOpen(long adsDelayAfterOpen) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_ADS_DELAY_AFTER_OPEN, adsDelayAfterOpen);
    }

    // 03/03/2020: ads delay after open. Default = 0
    public static long getKeyAdsDelayAfterOpen() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_ADS_DELAY_AFTER_OPEN, 0);
    }

    public static void setKeyChangeAds(long changeAds) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_CHANGE_ADS, changeAds);
    }

    // 03/03/2020: key to change ads from admob to fb. Default value admob = 0
    public static long getKeyChangeAds() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_CHANGE_ADS, 0);
    }

    public static void setKeyVersionCode(long versionCode) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_VERSION_CODE, versionCode);
    }

    public static long getKeyVersionCode() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_VERSION_CODE, 0);
    }

    public static void setKeyEventComming(long eventComming) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_EVENT_COMING, eventComming);
    }

    public static long getKeyEventComming() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_EVENT_COMING, 0);
    }

    public static void setKeyCountOpenAds(long countOpenAds) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_COUNT_OPEN_ADS, countOpenAds);
    }

    public static long getKeyCountOpenAds() {
        return MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_COUNT_OPEN_ADS, 0);
    }

    public static void setKeyTimeDelayAds(long timeDelayAds) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_TIME_DELAY_ADS, timeDelayAds);
    }

    public static long getKeyTimeDelayAds() {
        long time = MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_TIME_DELAY_ADS, 60000);
        if (time <= 0) time = 60000;
        return time;
    }

    public static void setKeyForceUpdate(long forceUpdate) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_FORCE_UPDATE, forceUpdate);
    }

    public static long getKeyForceUpdate() {
        long forceUpdate = MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_FORCE_UPDATE, 1);
        if (forceUpdate <= 0) forceUpdate = 1;
        return forceUpdate;
    }


    public static void setKeySplashDelay(long splashDelay) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_SPLASH_DELAY, splashDelay);
    }

    public static long getKeySplashDelay() {
        long delaySplash = MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_SPLASH_DELAY, 3000);
        if (delaySplash <= 0) delaySplash = 3000;
        return delaySplash;
    }

    public static void setKeyAdsTimeOut(long timeOut) {
        MyUtils.putLongPref(MyApplication.getInstance(), Constant.KEY_ADS_TIMEOUT, timeOut);
    }

    public static long getKeyAdsTimeOut() {
        long adsTimeout = MyUtils.getLongPref(MyApplication.getInstance(), Constant.KEY_ADS_TIMEOUT, 10);
        if (adsTimeout <= 0) adsTimeout = 10;
        return adsTimeout;
    }
}
