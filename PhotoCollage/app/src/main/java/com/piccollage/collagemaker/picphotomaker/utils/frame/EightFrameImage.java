package com.piccollage.collagemaker.picphotomaker.utils.frame;

import android.graphics.PointF;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.HashMap;
import java.util.List;

/**
 * Eight Frame Image
 */
public class EightFrameImage {
    public static void collage_8_16(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(2, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(1, 2));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            //shrink map
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).shrinkMethod = Photo.SHRINK_METHOD_COMMON;
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).shrinkMap = new HashMap<>();
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(0), new PointF(1, 1));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(1), new PointF(1, 2));
            photosPicked.get(position).shrinkMap.put(photosPicked.get(position).pointList.get(2), new PointF(2, 1));
        }
    }

    public static void collage_8_15(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.6666f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.6666f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.3333f, 0.6666f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_14(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.3333f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.3333f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.3333f, 0.6666f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_13(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.3333f, 0.75f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_12(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.6666f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.3333f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.3333f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.6666f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3333f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_11(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.6666f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3333f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_10(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.5f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_9(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.3333f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.3333f, 0.25f, 0.6666f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_8(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.6666f, 0.7f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.7f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.6666f, 0.5f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_7(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3f, 0, 0.6f, 0.2f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.2f, 0.3f, 0.6f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.3f, 0.2f, 0.6f, 0.6f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.6f, 0.6f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.6f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_6(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.6f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.6f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.6f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.3f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.3f, 0.5f, 0.6f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.3f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.3f, 0.75f, 0.6f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_5(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 0.75f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.25f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.25f, 0.5f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.75f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_4(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.25f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.25f, 0.75f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0, 0.75f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.75f, 0, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.75f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_3(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.25f, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 1, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.5f, 0.75f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.75f, 0.75f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_2(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.3333f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.3333f, 0.5f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.6666f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_1(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.3333f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.3333f, 0, 0.6666f, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.6666f, 0, 1, 0.3333f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0, 0.3333f, 0.5f, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0.5f, 0.3333f, 1, 0.6666f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.6666f, 0.3333f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.3333f, 0.6666f, 0.6666f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.6666f, 0.6666f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }

    public static void collage_8_0(int position, List<Photo> photosPicked) {
        //first frame
        if (position == 0) {
            photosPicked.get(position).bound.set(0, 0, 0.25f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //second frame
        if (position == 1) {
            photosPicked.get(position).bound.set(0.25f, 0, 0.5f, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //third frame
        if (position == 2) {
            photosPicked.get(position).bound.set(0.5f, 0, 1, 0.25f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fourth frame
        if (position == 3) {
            photosPicked.get(position).bound.set(0.5f, 0.25f, 1, 0.5f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //fifth frame
        if (position == 4) {
            photosPicked.get(position).bound.set(0, 0.5f, 0.5f, 0.75f);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //sixth frame
        if (position == 5) {
            photosPicked.get(position).bound.set(0, 0.75f, 0.5f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //seventh frame
        if (position == 6) {
            photosPicked.get(position).bound.set(0.5f, 0.5f, 0.75f, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
        //eighth frame
        if (position == 7) {
            photosPicked.get(position).bound.set(0.75f, 0.5f, 1, 1);
            photosPicked.get(position).pointList.add(new PointF(0, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 0));
            photosPicked.get(position).pointList.add(new PointF(1, 1));
            photosPicked.get(position).pointList.add(new PointF(0, 1));
        }
    }
}
