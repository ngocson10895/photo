package com.piccollage.collagemaker.picphotomaker.ui.vipactivity.dialograting;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RatingBar;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.RateDialogActivityHighScoreBinding;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class RateDialog extends BaseDialog {
    /* renamed from: m */
    public static String f12357m = "PRE_SHARING_CLICKED_MORE_APP";

    /* renamed from: n */
    public static String f12358n = "PRE_SHARING_CLICKED_VOTE_APP_VALUE";

    /* renamed from: o */
    public static String f12359o = "PRE_SHARING_COUNT_RECORD";

    /* renamed from: p */
    public static String f12360p = "IS_ABLE_SHOW_RATE_ACTIVITY";

    /* renamed from: q */
    public static String f12361q = "PRE_SHARING_COUNT_NEWAPPS_OPENED";

    /* renamed from: r */
    public static String f12362r = "PRE_SHARING_CLICKED_MORE_APP_VALUE";

    /* renamed from: s */
    public static String f12363s = "RATE_US";

    /* renamed from: t */
    public static String f12364t = "IS_NEW_DIALOG_HIGH_SCORE";

    /* renamed from: u */
    public static String f12365u = "IS_NEW_DIALOG_HIGH_SCORE_FBMAILTO";

    /* renamed from: v */
    public static String f12366v = "IS_NEW_DIALOG_HIGH_SCORE_APPNAME";

    /* renamed from: a */
    RatingBar f12367a;

    /* renamed from: b */
    Button f12368b;

    /* renamed from: c */
    Button f12369c;

    /* renamed from: d */
    Button f12370d;

    /* renamed from: e */
    String f12371e;

    /* renamed from: f */
    String f12372f = "";

    /* renamed from: g */
    SharedPreferences f12373g;

    /* renamed from: h */
    SharedPreferences.Editor f12374h;

    /* renamed from: i */
    boolean f12375i = false;

    /* renamed from: j */
    boolean f12376j = false;

    /* renamed from: k */
    boolean f12377k = false;

    /* renamed from: l */
    int f12378l = 0;

    /* renamed from: w */
    Activity f12379w;

    /* renamed from: x */
    private boolean f12380x = false;

    public RateDialog(@NonNull Activity context) {
        super(context);
        this.f12379w = context;
    }

    private String string;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public void initView() {
        C3638a.m16907a(false);
        if (!string.equals("")) {
            this.f12377k = true;
//            String string = f12379w.getIntent().getExtras().getString("fbMailto");
            SharedPreferences.Editor edit = this.f12379w.getApplicationContext().getSharedPreferences(f12364t, 0).edit();
            edit.putString(f12365u, getString());
            edit.apply();
        }
        this.f12372f = f12379w.getApplicationContext().getPackageName();
        this.f12371e = "https://play.google.com/store/apps/details?id=" + this.f12372f;
        this.f12373g = f12379w.getApplicationContext().getSharedPreferences(f12357m, 0);
        this.f12375i = this.f12373g.getBoolean(f12358n, false);
        this.f12376j = this.f12373g.getBoolean(f12362r, false);
        this.f12374h = this.f12373g.edit();
        this.f12378l = this.f12373g.getInt(f12359o, 0);
        this.f12367a = findViewById(R.id.rating_5_stars);
        this.f12369c = findViewById(R.id.btn_rate);
        this.f12370d = findViewById(R.id.btn_later);
        this.f12368b = findViewById(R.id.btn_cancel);
        this.f12367a.setOnRatingBarChangeListener((ratingBar, f, z) -> {
            if (f > 4.0f) {
                try {
                    f12379w.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + f12372f)));
                } catch (ActivityNotFoundException e) {
                    f12379w.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + f12372f)));
                }
            }
            dismiss();
            m16902a();
        });
        this.f12368b.setOnClickListener(view -> {
            String str;
            if (!f12377k) {
                f12374h.putInt(f12359o, 6);
                f12374h.apply();
            }
            SharedPreferences sharedPreferences = f12379w.getSharedPreferences(f12364t, 0);
            String string = sharedPreferences.getString(f12365u, null);
            String string2 = sharedPreferences.getString(f12366v, null);
            String str2 = "";
            Log.e("btnNever", "onClick: fbMailTo: " + string);
            if (string != null) {
                if (string2 == null) {
                    str = f12379w.getResources().getString(R.string.title_fb_mail3);
                } else {
                    str = f12379w.getResources().getString(R.string.title_fb_mail3) + ": " + string2;
                }
                mo11425a(string, str);
            }
            dismiss();
            m16902a();
        });
        this.f12369c.setOnClickListener(view -> {
            if (!f12377k) {
                f12374h.putInt(f12359o, 6);
                f12374h.commit();
                try {
                    C3638a.m16905a(f12379w);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                f12379w.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + f12372f)));
            } catch (ActivityNotFoundException e2) {
                f12379w.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + f12372f)));
            }
            dismiss();
            m16902a();
        });
        this.f12370d.setOnClickListener(view -> {
            if (!f12377k) {
                f12374h.putBoolean(f12360p, false);
                f12374h.putInt(f12359o, -5);
                f12374h.commit();
            }
            dismiss();
            m16902a();
        });
    }

    private RateDialogActivityHighScoreBinding binding;

    @Override
    public void beforeSetContentView() {
        binding = RateDialogActivityHighScoreBinding.inflate(getLayoutInflater());
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.9);
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m16902a() {
        new Handler().postDelayed(() -> C3638a.m16907a(true), 250);
    }

    /* renamed from: a */
    public void mo11425a(String str, String str2) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + str));
        intent.putExtra(Intent.EXTRA_SUBJECT, str2);
        intent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            f12379w.startActivity(Intent.createChooser(intent, f12379w.getResources().getString(R.string.dislike)));
        } catch (ActivityNotFoundException e) {
            Toasty.error(f12379w.getApplicationContext(),
                    f12379w.getResources().getString(R.string.no_email_intent), Toasty.LENGTH_SHORT).show();
        }
    }
}
