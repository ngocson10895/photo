package com.piccollage.collagemaker.picphotomaker.data.item;

/**
 * data cho từng theme khác nhau của shop
 */
public class DataSpecShop {
    private String title;
    private String urlThumb;
    private String pathViewPerSticker;
    private int idBegin;
    private int idEnd;
    private String imgType;

    public DataSpecShop(String title, String urlThumb, String pathViewPerSticker, int idBegin, int idEnd, String imgType) {
        this.title = title;
        this.urlThumb = urlThumb;
        this.pathViewPerSticker = pathViewPerSticker;
        this.idBegin = idBegin;
        this.idEnd = idEnd;
        this.imgType = imgType;
    }

    public void setUrlThumb(String urlThumb) {
        this.urlThumb = urlThumb;
    }

    public String getTitle() {
        return title;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public String getPathViewPerSticker() {
        return pathViewPerSticker;
    }

    public int getIdBegin() {
        return idBegin;
    }

    public int getIdEnd() {
        return idEnd;
    }

    public String getImgType() {
        return imgType;
    }
}
