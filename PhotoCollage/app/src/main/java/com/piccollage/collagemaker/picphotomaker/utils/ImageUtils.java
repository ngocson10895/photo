package com.piccollage.collagemaker.picphotomaker.utils;

import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import androidx.exifinterface.media.ExifInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Image Utils
 */
public class ImageUtils {
    public static class MemoryInfo {
        long availMem = 0;
        long totalMem = 0;
    }

    private static final float MIN_OUTPUT_IMAGE_SIZE = 640.0f;

    public static MemoryInfo getMemoryInfo(Context context) {
        final MemoryInfo info = new MemoryInfo();
        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        info.availMem = memInfo.availMem;

        info.totalMem = memInfo.totalMem;
        return info;
    }

    public static float calculateOutputScaleFactor(int viewWidth, int viewHeight) {
        float ratio = Math.min(viewWidth, viewHeight) / MIN_OUTPUT_IMAGE_SIZE;
        if (ratio < 1 && ratio > 0) {
            ratio = 1.0f / ratio;
        } else {
            ratio = 1;
        }
        return ratio;
    }

    // rotate image bitmap
    static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    // get matrix to draw image in center view
    public static Matrix createMatrixToDrawImageInCenterView(final float viewWidth, final float viewHeight, final float imageWidth, final float imageHeight) {
        final float ratioWidth = viewWidth / imageWidth;
        final float ratioHeight = viewHeight / imageHeight;
        final float ratio = Math.max(ratioWidth, ratioHeight);
        final float dx = (viewWidth - imageWidth) / 2.0f;
        final float dy = (viewHeight - imageHeight) / 2.0f;
        Matrix result = new Matrix();
        result.postTranslate(dx, dy);
        result.postScale(ratio, ratio, viewWidth / 2, viewHeight / 2);
        return result;
    }

    public static long getUsedMemorySize() {

        long freeSize = 0L;
        long totalSize = 0L;
        long usedSize = -1L;
        try {
            Runtime info = Runtime.getRuntime();
            freeSize = info.freeMemory();
            totalSize = info.totalMemory();
            usedSize = totalSize - freeSize;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usedSize;

    }

    public static void recycleView(View iv) {
        if (iv == null) {
            return;
        }

        Drawable background = iv.getBackground();
        iv.setBackgroundColor(Color.TRANSPARENT);

        if (background instanceof BitmapDrawable) {
            Bitmap bm = ((BitmapDrawable) background).getBitmap();
            if (bm != null && !bm.isRecycled()) {
                bm.recycle();
            }
        }
    }

    public static void recycleImageView(ImageView iv) {
        if (iv == null) {
            return;
        }

        Drawable background = iv.getBackground();
        Drawable d = iv.getDrawable();
        iv.setBackgroundColor(Color.TRANSPARENT);
        iv.setImageBitmap(null);

        if (background instanceof BitmapDrawable) {
            Bitmap bm = ((BitmapDrawable) background).getBitmap();
            if (bm != null && !bm.isRecycled()) {
                bm.recycle();
            }
        }

        if (d instanceof BitmapDrawable) {
            Bitmap bm = ((BitmapDrawable) d).getBitmap();
            if (bm != null && !bm.isRecycled()) {
                bm.recycle();
            }
        }
    }

    // get size of bitmap image
    public static long getSizeInBytes(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    // get bitmap from view
    public static Bitmap getBitmapFromView(View view) {
        try {
            //Define a bitmap with the same size as the view
            Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.ARGB_8888);
            //Bind a canvas to it
            Canvas canvas = new Canvas(returnedBitmap);
            //Get the view's background
            Drawable bgDrawable = view.getBackground();
            if (bgDrawable != null)
                //has background drawable, then draw it on the canvas
                bgDrawable.draw(canvas);
            else
                //does not have background drawable, then draw white background on the canvas
                canvas.drawColor(Color.WHITE);
            // draw the view on the canvas
            view.draw(canvas);
            //return the bitmap
            return returnedBitmap;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
            return null;
        }
    }

    // take screen of view to jpeg image
    public static void takeScreen(View view, final String outputImagePath) throws OutOfMemoryError {
        try {
            Bitmap bitmap = getBitmapFromView(view);
            File imageFile = new File(outputImagePath);
            imageFile.getParentFile().mkdirs();
            OutputStream fout;

            fout = new FileOutputStream(imageFile);
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
            }
            fout.flush();
            fout.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            System.gc();
            throw err;
        }
    }

    // save bitmap to image png
    public static void saveBitmap(Bitmap bitmap, final String path) {
        OutputStream fout = null;
        try {
            fout = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fout != null) {
                    fout.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // get orientation of image by exif info
    private static int getOrientationFromExif(String imagePath) {
        int orientation = -1;
        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    orientation = 270;

                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    orientation = 180;

                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    orientation = 90;

                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    orientation = 0;

                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return orientation;
    }

    // get orientation of image from media store
    private static int getOrientationFromMediaStore(Context context, Uri imageUri) {
        if (imageUri == null) {
            return -1;
        }

        String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
        Cursor cursor = context.getContentResolver().query(imageUri,
                projection, null, null, null);

        int orientation = -1;
        if (cursor != null && cursor.moveToFirst()) {
            orientation = cursor.getInt(0);
            cursor.close();
        }

        return orientation;
    }

    // make blur bitmap
    public static Bitmap fastBlur(Bitmap sentBitmap, int radius) {

        // Stack Blur v1.0 from
        // http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
        //
        // Java Author: Mario Klingemann <mario at quasimondo.com>
        // http://incubator.quasimondo.com
        // created Feburary 29, 2004
        // Android port : Yahel Bouaziz <yahel at kayenko.com>
        // http://www.kayenko.com
        // ported april 5th, 2012
        //

        // This is a compromise between Gaussian Blur and Box blur
        // It creates much better looking blurs than Box Blur, but is
        // 7x faster than my Gaussian Blur implementation.
        //
        // I called it Stack Blur because this describes best how this
        // filter works internally: it creates a kind of moving stack
        // of colors whilst scanning through the image. Thereby it
        // just has to add one new block of color to the right side
        // of the stack and remove the leftmost color. The remaining
        // colors on the topmost layer of the stack are either added on
        // or reduced by one, depending on if they are on the right or
        // on the left side of the stack.
        //
        // If you are using this algorithm in your code please add
        // the following line:
        //
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        if (bitmap.isRecycled())
            return null;
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int[] r = new int[wh];
        int[] g = new int[wh];
        int[] b = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int[] vmin = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int[] dv = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16)
                        | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    public static Bitmap getCircularArea(Bitmap bitmap, int circleX, int circleY, int radius) {
        Bitmap output = Bitmap.createBitmap(2 * radius, 2 * radius, Bitmap.Config.ARGB_8888);

        Rect rect = new Rect();
        rect.top = circleY - radius;
        rect.bottom = rect.top + 2 * radius;
        rect.left = circleX - radius;
        rect.right = rect.left + 2 * radius;

        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle((float) output.getWidth() / 2, (float) output.getHeight() / 2,
                radius, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, new Rect(0, 0, output.getWidth(),
                output.getHeight()), paint);

        return output;
    }

    public static Bitmap focus(Bitmap src, Bitmap filtratedBitmap, Rect rect, boolean isCircle) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        Bitmap focusBitmap;
        Bitmap area = Bitmap.createBitmap(rect.right - rect.left, rect.bottom - rect.top, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(area);
        canvas.drawBitmap(src, rect,
                new Rect(0, 0, area.getWidth(), area.getHeight()), paint);

        if (isCircle) {
            focusBitmap = getCircularBitmap(area);
            area.recycle();
        } else {
            focusBitmap = area;
        }

        Bitmap result = Bitmap.createBitmap(filtratedBitmap.getWidth(),
                filtratedBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(result);
        canvas.drawBitmap(focusBitmap, new Rect(0, 0, focusBitmap.getWidth(),
                focusBitmap.getHeight()), rect, paint);

        focusBitmap.recycle();

        return result;
    }

    private static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = (float) bitmap.getHeight() / 2;
        } else {
            r = (float) bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle((float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2, r,
                paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public Uri addImageAsApplication(ContentResolver contentresolver, String s, long l, String s1, String s2, Bitmap bitmap, byte abyte0[]) {
        FileOutputStream fileoutputstream;
        String s3;
        s3 = s1 + "/" + s2;
        File file1;
        boolean flag;
        File file = new File(s1);
        try {
            if (!file.exists()) {
                file.mkdirs();
            }
            file1 = new File(s1, s2);
            flag = file1.createNewFile();
            fileoutputstream = null;
            if (flag) {
                FileOutputStream fileoutputstream1 = new FileOutputStream(file1);
                if (bitmap == null) {
                    fileoutputstream1.write(abyte0);
                    fileoutputstream = fileoutputstream1;
                } else {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileoutputstream1);
                    fileoutputstream = fileoutputstream1;
                }
            }
            ContentValues contentvalues;
            if (fileoutputstream != null) {
                try {
                    fileoutputstream.close();
                } catch (Throwable ignored) {
                }
            }
            contentvalues = new ContentValues(7);
            contentvalues.put("title", s);
            contentvalues.put("_display_name", s2);
            contentvalues.put("datetaken", l);
            contentvalues.put("mime_type", "");
            contentvalues.put("_data", s3);
            return contentresolver.insert(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentvalues);
        } catch (Exception e) {
            return null;
        }
    }
}
