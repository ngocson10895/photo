package com.piccollage.collagemaker.picphotomaker.networking;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Create from Administrator
 * Purpose: theo dõi trạng thái internet
 * Des: tạo broad cast receiver để theo dõi trạng thái internet
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    public interface NetworkStateReceiverListener {
        void onNetworkAvailable();

        void onNetworkUnavailable();
    }

    private NetworkStateReceiverListener listener;

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        if (listener != null) {
            if (NetworkUtils.getConnectivityStatus(context)) {
                listener.onNetworkAvailable();
            } else {
                listener.onNetworkUnavailable();
            }
        }
    }

    public void setListener(NetworkStateReceiverListener listener) {
        this.listener = listener;
    }
}
