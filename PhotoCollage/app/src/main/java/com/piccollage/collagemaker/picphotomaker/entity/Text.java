package com.piccollage.collagemaker.picphotomaker.entity;

import android.graphics.Typeface;

public class Text {
    private String content;
    private int color;
    private Typeface font;
    private int position;

    public Text(String content, int color, Typeface font) {
        this.content = content;
        this.color = color;
        this.font = font;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Typeface getFont() {
        return font;
    }

    public void setFont(Typeface font) {
        this.font = font;
    }
}
