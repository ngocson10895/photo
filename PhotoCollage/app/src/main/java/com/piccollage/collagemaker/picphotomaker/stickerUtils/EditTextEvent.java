package com.piccollage.collagemaker.picphotomaker.stickerUtils;

import android.view.MotionEvent;

/**
 * Event edit text
 */
public class EditTextEvent implements StickerIconEvent {

    @Override
    public void onActionDown(StickerView stickerView, MotionEvent event) {

    }

    @Override
    public void onActionMove(StickerView stickerView, MotionEvent event) {

    }

    @Override
    public void onActionUp(StickerView stickerView, MotionEvent event) {
        TextSticker textSticker = (TextSticker) stickerView.getCurrentSticker();
        if (textSticker != null) {
            stickerView.editText(textSticker.getTextOfUser());
        }
    }
}
