package com.piccollage.collagemaker.picphotomaker.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;

import com.piccollage.collagemaker.picphotomaker.entity.FontItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Text Utils
 */
public class TextUtils {
    private static final String TAG = "TextUtils";
    private static final String ASSET_PREFIX = "assets://";

    /**
     * Get list font item from assets
     *
     * @param path : path
     * @return : list
     */
    public static List<FontItem> listAssetFiles(String path, Context context) {
        List<FontItem> listFontItem = new ArrayList<>();
        try {
            if (context != null) {
                String[] fileList = context.getAssets().list(path);
                if (fileList == null) {
                    Log.d(TAG, "listAssetFiles: null");
                } else {
                    for (String pathFont : fileList) {
                        listFontItem.add(new FontItem(pathFont.substring(0, pathFont.length() - 4),
                                ASSET_PREFIX.concat(path).concat("/").concat(pathFont), false,
                                Constant.DEFAULT));
                    }
                }
            } else {
                Log.d(TAG, "listAssetFiles: null");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listFontItem;
    }

    /**
     * load type face
     *
     * @param path : path
     * @return : type face
     */
    public static Typeface loadTypeface(Context context, String path) {
        if (path == null || path.length() < 1) {
            return Typeface.DEFAULT;
        } else if (path.startsWith(ASSET_PREFIX)) {
            String assetPath = path.substring(ASSET_PREFIX.length());
            return Typeface.createFromAsset(context.getAssets(), assetPath);
        } else {
            File file = new File(path);
            if (!file.isFile()) {
                return Typeface.DEFAULT;
            } else
                return Typeface.createFromFile(file);
        }
    }

    /**
     * find Paragraph Size
     *
     * @param paint        : to draw
     * @param originalText : init text
     * @param sentences    : sentences
     * @return : array int include pWidth and pHeight
     */
    public static int[] findParagraphSize(final Paint paint,
                                          final String originalText, final String[] sentences) {
        final Rect textBound = new Rect();
        paint.getTextBounds(originalText, 0, originalText.length(), textBound);
        final int textHeight = textBound.height();
        paint.getTextBounds("m", 0, 1, textBound);
        final int delta = (int) (0.5 * textBound.height() + textHeight);
        final int pHeight = textHeight + (sentences.length - 1) * delta;
        int pWidth = 0, tmp = 0;
        for (String str : sentences) {
            tmp = (int) (paint.measureText(str, 0, str.length()) + .5);
            if (tmp > pWidth) {
                pWidth = tmp;
            }
        }
        return new int[]{pWidth, pHeight};
    }

    /**
     * This function is optimized for drawing.
     *
     * @param canvas       : canvas
     * @param paint        : to draw
     * @param centerX      : center X
     * @param centerY      : center Y
     * @param originalText : original text
     * @param sentences    : sentences
     */
    public static void drawParagraph(final Canvas canvas, final Paint paint, final int centerX, final int centerY,
                                     final String originalText, final String[] sentences) {
        final Rect textBound = new Rect();
        paint.getTextBounds(originalText, 0, originalText.length(), textBound);
        final int textHeight = textBound.height();
        paint.getTextBounds("m", 0, 1, textBound);
        final int unitHeight = (int) (0.5 * textBound.height());
        final int delta = (int) (0.5 * textBound.height() + textHeight);
        final int pHeight = textHeight + (sentences.length - 1) * delta;
        int pWidth = 0, tmp = 0;
        for (String str : sentences) {
            tmp = (int) (paint.measureText(str, 0, str.length()) + .5);
            if (tmp > pWidth) {
                pWidth = tmp;
            }
        }

        final int top = centerY - pHeight / 2;
        final int left = centerX;
        //Draw every sentences
        int y = top + textHeight;
        int type = containHighCharacter(originalText);
        if (type == 1) {
            y = y - unitHeight;
        }
        for (String str : sentences) {
            canvas.drawText(str, left, y, paint);
            y += delta;
        }
    }

    /**
     * ??????
     *
     * @param text : text
     * @return : 1 or 2 or 0
     */
    private static int containHighCharacter(String text) {
        boolean q = false;
        boolean t = false;
        for (int idx = 0; idx < text.length(); idx++) {
            char c = text.charAt(idx);
            if (c == 'q' || c == 'y' || c == 'p' || c == 'g' || c == 'j') {
                q = true;
            } else if (c == 't' || c == 'd' || c == 'f' || c == 'h' || c == 'k' || c == 'l' || c == 'b') {
                t = true;
            }
        }
        if (q) {
            return 1;
        } else if (t) {
            return 2;
        } else {
            return 0;
        }
    }
}
