package com.piccollage.collagemaker.picphotomaker.ui;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogExitAppBinding;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.dialograting.RateDialog;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.util.Objects;

/**
 * Create from Administrator
 * Purpose: dialog exit app
 * Des: hiển thị khi user muốn thoát app.
 */
public class DialogExitApp extends BaseDialog {
    private Activity context;
    private DialogExitAppBinding binding;

    public DialogExitApp(@NonNull Activity context) {
        super(context);
        this.context = context;
    }

    @Override
    public void beforeSetContentView() {
        binding = DialogExitAppBinding.inflate(getLayoutInflater());
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogExitApp.NAME);
    }

    @Override
    public void initView() {
        binding.adsLayout.post(() -> Advertisement.showBannerAdExit(getContext(), binding.adsLayout));
        binding.btnRateUs.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogExitApp.RATE_US);
            dismiss();
            RateDialog rateDialog = new RateDialog(context);
            rateDialog.setString(Constant.SHARE_EMAIL);
            rateDialog.show();
        });
        binding.btnYes.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogExitApp.OK);
            dismiss();
            new Handler().postDelayed(() -> context.finish(), 300); // thoát app sau 0.3s
        });
        binding.btnNo.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogExitApp.CANCEL);
            dismiss();
        });
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    /**
     * custom dialog
     */
    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.85);
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
