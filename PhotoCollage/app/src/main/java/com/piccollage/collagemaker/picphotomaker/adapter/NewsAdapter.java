package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.data.news.DataNews;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemNewsBinding;

import java.util.ArrayList;

/**
 * Item news ở màn hình home
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private Context context;
    private ArrayList<DataNews> news;
    private IPickNews iPickNews;

    public NewsAdapter(Context context, ArrayList<DataNews> news, IPickNews iPickNews) {
        this.context = context;
        this.news = news;
        this.iPickNews = iPickNews;
    }

    public interface IPickNews {
        void pickNews(int position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(context).asBitmap().load(news.get(position).getThumbMagazine()).into(holder.binding.ivThumbs);
        holder.binding.tvTitle.setText(news.get(position).getTitleMagazine());
        holder.binding.tvDes.setText(news.get(position).getDesMagazine());
        holder.itemView.setOnClickListener(v -> iPickNews.pickNews(position));
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemNewsBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemNewsBinding.bind(itemView);
        }
    }

    public void setNews(ArrayList<DataNews> news) {
        this.news = news;
        notifyDataSetChanged();
    }
}
