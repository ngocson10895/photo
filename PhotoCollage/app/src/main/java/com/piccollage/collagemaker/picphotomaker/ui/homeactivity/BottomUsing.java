package com.piccollage.collagemaker.picphotomaker.ui.homeactivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.BottomUsingBinding;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer.PipStyleActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.util.Objects;

/**
 * Create from Administrator
 * Purpose: bottom dialog
 * Des: show feature cho use lựa chọn
 */
public class BottomUsing extends BottomSheetDialogFragment {
    private BottomUsingBinding binding;
    private PermissionChecker checker;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        if (getContext() != null) {
            checker = new PermissionChecker((AppCompatActivity) getActivity());
            View view = View.inflate(getContext(), R.layout.bottom_using, null);
            binding = BottomUsingBinding.bind(view);
            dialog.setContentView(view);
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();

            if (behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).addBottomSheetCallback(mBottomSheetBehaviorCallback);
            }
            onViewClicked();
        }
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        super.show(manager, tag);
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.NAME);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void onViewClicked() {
        binding.ivClose.setOnClickListener(v -> {
            dismiss();
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.CLOSE);
        });
        binding.clCollage.setOnClickListener(v -> {
            if (checker.isPermissionOK()) {
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.ACTION + "Collage");
                dismiss();
                Intent intent = new Intent(getActivity(), PickPhotoActivity.class);
                intent.putExtra(PickPhotoActivity.MODE, Constant.COLLAGE);
                startActivity(intent);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        binding.clPip.setOnClickListener(v -> {
            if (checker.isPermissionOK()) {
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.ACTION + "Pip");
                dismiss();
                Intent intent1 = new Intent(getActivity(), PipStyleActivity.class);
                startActivity(intent1);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        binding.clEditor.setOnClickListener(v -> {
            if (checker.isPermissionOK()) {
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.ACTION + "Editor");
                dismiss();
                Intent intent2 = new Intent(getActivity(), PickPhotoActivity.class);
                intent2.putExtra(PickPhotoActivity.MODE, Constant.EDITOR);
                startActivity(intent2);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        binding.clScrap.setOnClickListener(v -> {
            if (checker.isPermissionOK()) {
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.ACTION + "Scrapbook");
                dismiss();
                Intent intent3 = new Intent(getActivity(), PickPhotoActivity.class);
                intent3.putExtra(PickPhotoActivity.MODE, Constant.SCRAPBOOK);
                startActivity(intent3);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        binding.clShop.setOnClickListener(v -> {
            if (checker.isPermissionOK()) {
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.BottomUsing.ACTION + "Shop");
                dismiss();
                Intent intent4 = new Intent(getActivity(), StoreActivity.class);
                intent4.putExtra(StoreActivity.VALUE, Constant.STICKER);
                startActivity(intent4);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
    }
}
