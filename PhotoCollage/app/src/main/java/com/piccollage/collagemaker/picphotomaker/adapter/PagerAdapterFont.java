package com.piccollage.collagemaker.picphotomaker.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.piccollage.collagemaker.picphotomaker.entity.FontTheme;
import com.piccollage.collagemaker.picphotomaker.utils.view.FontFragment;

import java.util.ArrayList;

/**
 * adapter cho view pager ở view pick font
 */
public class PagerAdapterFont extends FragmentStatePagerAdapter {
    private ArrayList<FontTheme> fontThemes;

    public PagerAdapterFont(@NonNull FragmentManager fm, ArrayList<FontTheme> fontThemes) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fontThemes = fontThemes;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return FontFragment.newInstance(fontThemes.get(position));
    }

    @Override
    public int getCount() {
        return fontThemes.size();
    }

    public void setNewData(ArrayList<FontTheme> fontThemes) {
        this.fontThemes = fontThemes;
    }

    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fontThemes.get(position).getTheme();
    }
}
