package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Gradient implements Parcelable {
    private String stColor;
    private String endColor;
    private String name;
    private boolean isPick;
    private String theme;

    public Gradient(String stColor, String endColor, boolean isPick, String theme) {
        this.stColor = stColor;
        this.endColor = endColor;
        this.isPick = isPick;
        this.theme = theme;
    }

    protected Gradient(Parcel in) {
        stColor = in.readString();
        endColor = in.readString();
        name = in.readString();
        isPick = in.readByte() != 0;
        theme = in.readString();
    }

    public static final Creator<Gradient> CREATOR = new Creator<Gradient>() {
        @Override
        public Gradient createFromParcel(Parcel in) {
            return new Gradient(in);
        }

        @Override
        public Gradient[] newArray(int size) {
            return new Gradient[size];
        }
    };

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStColor() {
        return stColor;
    }

    public String getEndColor() {
        return endColor;
    }

    public String getName() {
        return name;
    }

    public String getTheme() {
        return theme;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stColor);
        dest.writeString(endColor);
        dest.writeString(name);
        dest.writeByte((byte) (isPick ? 1 : 0));
        dest.writeString(theme);
    }

    @NonNull
    @Override
    public String toString() {
        return stColor + " " + endColor + " " + name + " " + theme;
    }
}
