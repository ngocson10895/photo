package com.piccollage.collagemaker.picphotomaker.actions;

import android.os.Bundle;
import android.view.View;

import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.OnDoneActionsClickListener;

/**
 * Create from Administrator
 * Purpose: Base Action cho các action trong editor
 * Des: chứa các function để thực hiện cho chức năng filter
 */
public abstract class BaseAction implements OnDoneActionsClickListener {
    protected EditorActivity activity;
    View mRootActionView;
    private boolean mAttached = false;

    BaseAction(EditorActivity activity) {
        this.activity = activity;
        onInit();
        mRootActionView = inflateMenuView();
    }

    private void logToFacebook() {
        Bundle parameters = new Bundle();
        parameters.putString("actionName", getActionName());
    }

    protected void onInit() {

    }

    // thực hiện action hiện tại của user
    public void attach() {
        logToFacebook();
        activity.setDoneActionsClickListener(this);
        if (mRootActionView != null) {
            BaseAction action = activity.getCurrentAction();
            if (action != null) {
                action.mAttached = false;
            }

            activity.attachBottomMenu(mRootActionView);
            activity.setCurrentAction(this);
            mAttached = true;
        }
    }

    public void onDetach() {

    }

    boolean isAttached() {
        return mAttached;
    }

    public void done() {

    }

    public abstract void apply(final boolean finish);

    /**
     * Create root action view and find child widgets
     */
    public abstract View inflateMenuView();

    public abstract String getActionName();

    /**
     * Should be call to save instance state
     *
     * @param bundle : data to save
     */
    public void saveInstanceState(Bundle bundle) {
        bundle.putBoolean("dauroi.photoeditor.actions.".concat(getActionName()).concat(".mAttached"), mAttached);
    }

    /**
     * Should be call before calling attach() method if has saved instance
     * state.
     *
     * @param bundle : data to save
     */
    public void restoreInstanceState(Bundle bundle) {
        mAttached = bundle.getBoolean("dauroi.photoeditor.actions.".concat(getActionName()).concat(".mAttached"), mAttached);
    }

    public void onActivityResume() {

    }

    // Không sử dụng
    @Override
    public void onDoneButtonClick() {
        apply(true);
    }

    // apply action button
    @Override
    public void onApplyButtonClick() {
        apply(false);
    }

    void onClicked() {

    }
}
