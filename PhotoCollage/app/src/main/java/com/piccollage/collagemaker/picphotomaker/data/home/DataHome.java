package com.piccollage.collagemaker.picphotomaker.data.home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * data for home activity
 */
public class DataHome implements Parcelable {
    private String effectName;
    private int effectId;
    private String urlThumb;
    private String urlRequest;

    private DataHome(Parcel in) {
        effectName = in.readString();
        effectId = in.readInt();
        urlThumb = in.readString();
        urlRequest = in.readString();
    }

    public static final Creator<DataHome> CREATOR = new Creator<DataHome>() {
        @Override
        public DataHome createFromParcel(Parcel in) {
            return new DataHome(in);
        }

        @Override
        public DataHome[] newArray(int size) {
            return new DataHome[size];
        }
    };

    public String getEffectName() {
        return effectName;
    }

    public int getEffectId() {
        return effectId;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public String getUrlRequest() {
        return urlRequest;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(effectName);
        dest.writeInt(effectId);
        dest.writeString(urlThumb);
        dest.writeString(urlRequest);
    }
}
