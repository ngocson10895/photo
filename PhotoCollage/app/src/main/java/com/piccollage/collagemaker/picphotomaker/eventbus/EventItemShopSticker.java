package com.piccollage.collagemaker.picphotomaker.eventbus;

import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;

import java.util.ArrayList;

/**
 * event khi load sticker từ api
 */
public class EventItemShopSticker {
    private ArrayList<StickerTheme> stickerThemes;

    public EventItemShopSticker(ArrayList<StickerTheme> stickerThemes) {
        this.stickerThemes = stickerThemes;
    }

    public ArrayList<StickerTheme> getStickerThemes() {
        return stickerThemes;
    }
}
