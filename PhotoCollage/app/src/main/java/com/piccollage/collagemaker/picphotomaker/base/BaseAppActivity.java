package com.piccollage.collagemaker.picphotomaker.base;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.networking.NetworkChangeReceiver;
import com.piccollage.collagemaker.picphotomaker.ui.DetailNewsActivity;

/**
 * Base Activity
 */
public abstract class BaseAppActivity extends AppCompatActivity {
    private long lastClickTime = 0;
    private NetworkChangeReceiver networkChangeReceiver;

    // check double click để tránh trường hợp user click liên tục trong 1 khoảng thời gian
    public boolean checkDoubleClick() {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return false;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        return true;
    }

    // add fragment
    public void fragmentTrans(@IdRes int var1, @NonNull Fragment var2) {
        if (!isFinishing() && !isDestroyed()) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(var1, var2);
            fragmentTransaction.commit();
        }
    }

    // cần có để thay đổi ngôn ngữ của activity
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.setLocale(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpFullScreen();
        beforeSetContentView();
        setContentView(getLayoutView());
        initView();
        buttonClick();
        initData();

        checkNetWork();
    }

    // make activity full screen
    private void setUpFullScreen() {
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    // create broadcast receiver để check internet
    private void checkNetWork() {
        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        networkChangeReceiver.setListener(new NetworkChangeReceiver.NetworkStateReceiverListener() {
            @Override
            public void onNetworkAvailable() {
                onHasNetwork();
            }

            @Override
            public void onNetworkUnavailable() {
                notHasNetwork();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkChangeReceiver);
    }

    // nếu có mạng thì lập tức thực hiện
    public void onHasNetwork() {

    }

    // set up smt trước khi set content view
    public void beforeSetContentView() {

    }

    public void buttonClick() {

    }

    // nếu không có mạng thì lập tức thực hiện
    public void notHasNetwork() {

    }

    public abstract View getLayoutView();

    public void initView() {

    }

    public void initData() {
    }

    @Override
    public void onBackPressed() {
        if (this instanceof DetailNewsActivity) {
            super.onBackPressed();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right); // set animation khi chuyển activity
        }
    }
}