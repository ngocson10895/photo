package com.piccollage.collagemaker.picphotomaker.actions;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

import dauroi.photoeditor.listener.ApplyFilterListener;
import dauroi.photoeditor.view.OrientationImageView;

/**
 * Create from Administrator
 * Purpose: Rotation action
 * Des: thực hiện hành động xoay ảnh cho user
 */
public class RotationAction extends BaseAction {
    private View mOrientationLayout;
    private OrientationImageView mOrientationImageView;
    private boolean mRestoreOldState = false;
    private boolean mFirstAttached = false;
    private boolean mFirstApplied = false;

    public RotationAction(EditorActivity activity) {
        super(activity);
    }

    @Override
    public void saveInstanceState(Bundle bundle) {
        super.saveInstanceState(bundle);
        bundle.putBoolean("dauroi.photoeditor.actions.RotationAction.mFirstAttached", mFirstAttached);
        mOrientationImageView.saveInstanceState(bundle);
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        super.restoreInstanceState(bundle);
        mFirstAttached = bundle.getBoolean("dauroi.photoeditor.actions.RotationAction.mFirstAttached", mFirstAttached);
        if (mFirstAttached) {
            mOrientationImageView.restoreInstanceState(bundle);
            mRestoreOldState = true;
        } else {
            mRestoreOldState = false;
        }
    }

    public void reset() {
        mRestoreOldState = false;
        mFirstAttached = false;
    }

    @Override
    public void apply(final boolean finish) {
        if (!isAttached()) {
            return;
        }
        ApplyFilterTask task = new ApplyFilterTask(activity, new ApplyFilterListener() {

            @Override
            public void onFinishFiltering() {
                reset();
                if (mOrientationImageView.getAngle() % 180 != 0) {
                    if (activity.getCropAction() != null) {
                        activity.getCropAction().reset();
                    }
                }
                if (!mFirstApplied) {
                    Bitmap bm = Bitmap.createBitmap(activity.getImageWidth(), activity.getImageHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bm);
                    canvas.drawBitmap(activity.getImage(), 0, 0, new Paint());
                    activity.setImage(bm, true);
                    mFirstApplied = true;
                }

                if (finish) {
                    done();
                }
            }

            @Override
            public Bitmap applyFilter() {
                return mOrientationImageView.applyTransform();
            }
        });

        task.execute();
    }

    @SuppressLint("InflateParams")
    @Override
    public View inflateMenuView() {
        LayoutInflater inflater = LayoutInflater.from(activity);
        mRootActionView = inflater.inflate(R.layout.photo_editor_action_rotation, null);
        View mRotateLeftView = mRootActionView.findViewById(R.id.leftView);
        mRotateLeftView.setOnClickListener(v -> {
            rotateLeft();
            onClicked();
        });

        View mRotateRightView = mRootActionView.findViewById(R.id.rightView);
        mRotateRightView.setOnClickListener(v -> {
            rotateRight();
            onClicked();
        });

        View mFlipHorView = mRootActionView.findViewById(R.id.horView);
        mFlipHorView.setOnClickListener(v -> {
            flipHor();
            onClicked();
        });

        View mFlipVerView = mRootActionView.findViewById(R.id.verView);
        mFlipVerView.setOnClickListener(v -> {
            flipVer();
            onClicked();
        });
        // orientation view
        mOrientationLayout = inflater.inflate(R.layout.photo_editor_orientation, null);
        mOrientationImageView = mOrientationLayout.findViewById(R.id.orientationView);

        return mRootActionView;
    }

    /**
     * Called after restoring instance state
     */
    @Override
    public void attach() {
        super.attach();
        activity.attachMaskView(mOrientationLayout);
        if (mRestoreOldState || mFirstAttached) {
            //if (mRestoreOldState) {
            mOrientationImageView.setImage(activity.getImage());
            // }
            // mOrientationImageView.invalidate();
            mRestoreOldState = false;
        } else {
            mOrientationImageView.init(activity.getImage(), activity.getPhotoViewWidth(), activity.getPhotoViewHeight());
        }

        mOrientationImageView.post(() -> activity.getImageProcessingView().setVisibility(View.GONE));

        mFirstAttached = true;
    }

    @Override
    public void onActivityResume() {
        super.onActivityResume();
        if (isAttached() && mFirstAttached) {
            activity.attachMaskView(mOrientationLayout);
            mOrientationImageView.invalidate();
            mOrientationImageView.post(() -> activity.getImageProcessingView().setVisibility(View.GONE));
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.attachMaskView(null);
        activity.getImageProcessingView().setVisibility(View.VISIBLE);
    }

    private void rotateLeft() {
        mOrientationImageView.rotate(-90);
    }

    private void rotateRight() {
        mOrientationImageView.rotate(90);
    }

    private void flipVer() {
        mOrientationImageView.flip(OrientationImageView.FLIP_VERTICAL);
    }

    private void flipHor() {
        mOrientationImageView.flip(OrientationImageView.FLIP_HORIZONTAL);
    }

    @Override
    public String getActionName() {
        return "RotationAction";
    }
}
