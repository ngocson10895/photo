package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.piccollage.collagemaker.picphotomaker.R;

public class UnifiedNativeAdBannerViewHolder extends RecyclerView.ViewHolder {
    private UnifiedNativeAdView adView;

    public UnifiedNativeAdBannerViewHolder(@NonNull View itemView) {
        super(itemView);
        adView = itemView.findViewById(R.id.ad_view);

        // Register the view used for each individual asset.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
    }

    public UnifiedNativeAdView getAdView() {
        return adView;
    }
}
