package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemPhotoBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

import java.util.List;

import es.dmoral.toasty.Toasty;

/**
 * Photo Adapter
 */
public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Photo> listPhotos;
    private Context context;
    private IContractListener iContractListener;
    private int typePick;
    private SharedPrefsImpl sharedPrefs;
    private int quantity;

    public PhotoAdapter(List<Photo> listPhotos, Context context, IContractListener iContractListener, int typePick) {
        this.listPhotos = listPhotos;
        this.context = context;
        this.iContractListener = iContractListener;
        this.typePick = typePick;
        sharedPrefs = new SharedPrefsImpl(context, Constant.NAME_SHARE);
    }

    // to EachFragment
    public interface IContractListener {
        void pick(Photo photo);

        void pickOne(Photo photo);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_photo, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder holder = (ViewHolder) viewHolder;
        Photo photo = listPhotos.get(i);

        Glide.with(context).asBitmap().
                load(photo.getPathPhoto())
                .placeholder(R.mipmap.thumbs)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.thumbs)
                .into(holder.binding.ivPhoto);

        if (photo.getTimesPick() != 0) {
            holder.binding.tvPick.setVisibility(View.VISIBLE);
            holder.binding.tvPick.setText(String.valueOf(photo.getTimesPick()));
        } else {
            holder.binding.tvPick.setVisibility(View.INVISIBLE);
        }
        if (typePick == 2) {
            viewHolder.itemView.setOnClickListener(view -> {
                quantity = sharedPrefs.get(Constant.QUANTITY_PICK_PHOTO, Integer.class);
                // TODO: 17/06/2019 Multiple Pick Item
                if (quantity < sharedPrefs.get(Constant.QUANTITY_PICK_PHOTO_MAX, Integer.class)) {
                    quantity += 1;
                    sharedPrefs.put(Constant.QUANTITY_PICK_PHOTO, quantity);
                    photo.setTimesPick(photo.getTimesPick() + 1);
                    holder.binding.tvPick.setVisibility(View.VISIBLE);
                    holder.binding.tvPick.setText(String.valueOf(photo.getTimesPick()));
                    iContractListener.pick(photo);
                } else {
                    Toasty.warning(context, context.getString(R.string.u_should_chose)
                            + sharedPrefs.get(Constant.QUANTITY_PICK_PHOTO_MAX, Integer.class) + context.getString(R.string.images),
                            Toasty.LENGTH_SHORT).show();
                }
            });
        } else if (typePick == 1) {
            viewHolder.itemView.setOnClickListener(view -> {
                // TODO: 17/06/2019 Single Item Pick
                iContractListener.pickOne(photo);
            });
        }
    }

    @Override
    public int getItemCount() {
        return listPhotos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ItemPhotoBinding binding;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemPhotoBinding.bind(itemView);
        }
    }

    public void setListPhotos(List<Photo> listPhotos) {
        this.listPhotos = listPhotos;
    }

}