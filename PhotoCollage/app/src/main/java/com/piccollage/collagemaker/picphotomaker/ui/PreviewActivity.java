package com.piccollage.collagemaker.picphotomaker.ui;

import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import android.view.View;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.BuildConfig;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.IconShareAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityResultPreviewBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.ui.homeactivity.HomeActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer.PipStyleActivity;
import com.piccollage.collagemaker.picphotomaker.ui.quotesfeature.PickQuotesActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import java.io.File;
import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: màn hình hiển thị ảnh của user sau khi edit
 * Des: màn hình hiển thị kết quả, user có thể chia sẻ ảnh hoặc recreate ảnh mới
 */
public class PreviewActivity extends BaseAppActivity implements IconShareAdapter.IPickShare {
    public static final String COUNT = "count_done";

    private String pathImage, from;
    private DialogRate dialogRateFragment;
    private SharedPrefsImpl sharedPrefs;
    private int count;
    private ActivityResultPreviewBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityResultPreviewBinding.inflate(getLayoutInflater());
    }

    public void initData() {
        if (getIntent() != null) {
            pathImage = getIntent().getStringExtra(Constant.IMAGE_PATH);
            Glide.with(this).asBitmap().load(pathImage).into(binding.ivImage);
            from = getIntent().getStringExtra(Constant.VALUE_INTENT);
        }
        count = sharedPrefs.get(COUNT, Integer.class);
    }

    public void initView() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Preview.NAME);
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.GONE);
        }
        binding.adView.post(() -> Advertisement.showBannerAdOther(this, binding.adView, false, new BannerAdsUtils.BannerAdListener() {
            @Override
            public void onAdLoaded() {
                binding.viewPro.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailed() {

            }
        }));

        // check re-up app. nếu sai package thì finish
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);
        dialogRateFragment = new DialogRate(this);

        ArrayList<Icon> icons = new ArrayList<>();
        IconShareAdapter iconsAdapter = new IconShareAdapter(this, InitData.addIconShare(icons), this);
        binding.rvIconShare.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvIconShare.addItemDecoration(new BetweenSpacesItemDecoration(0, 16));
        binding.rvIconShare.setAdapter(iconsAdapter);
    }

    // share qua nhiều ứng dụng trong device
    private void share() {
        Uri photoURI = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider",
                new File(pathImage));
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/png");
        share.putExtra(Intent.EXTRA_STREAM, photoURI);
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(share, getString(R.string.photo_editor_share_image)));
    }

    /*
    tính số lần user vào màn hình preview. cứ 3 lần sẽ hiện dialog rate app 1 lần
     */
    public void onBtnDoneClicked() {
        count++;
        sharedPrefs.put(COUNT, count);
        if (sharedPrefs.get(COUNT, Integer.class) % 3 == 0) {
            dialogRateFragment.show();
        } else {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
    }

    @Override
    public void buttonClick() {
        onCloseClicked();
        onViewClicked();
        onViewProClicked();
        onRecreateClicked();
    }

    // recreate ảnh. Chia ra nhiều trường hợp vì nhiều follow dùng chung màn hình này
    private void onRecreateClicked() {
        binding.btnRecreate.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Preview.RECREATE_IMAGE);
            if (from != null) {
                switch (from) {
                    case Constant.QUOTE:
                        Intent intent = new Intent(this, PickQuotesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                        break;
                    case Constant.PIP:
                        Intent intent1 = new Intent(this, PipStyleActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                        break;
                    default:
                        Intent intent2 = new Intent(this, PickPhotoActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent2.putExtra(PickPhotoActivity.MODE, from);
                        startActivity(intent2);
                        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                }
            }
        });
    }

    public void onCloseClicked() {
        binding.ivClose.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Preview.BACK_TO_FEATURE);
                setResult(RESULT_OK);
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }

    @Override
    public void onBackPressed() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Preview.BACK_TO_FEATURE);
        setResult(RESULT_OK);
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    public void pickShare(int position) {
        shareEachApp(position);
    }

    private void shareEachApp(int position) {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Preview.SHARE);
        switch (position) {
            case 0:
                shareApp("com.facebook.katana");
                break;
            case 1:
                shareApp("com.facebook.orca");
                break;
            case 2:
                shareApp("com.instagram.android");
                break;
            case 3:
                shareApp("com.twitter.android");
                break;
            case 4:
                shareApp("org.telegram.messenger");
                break;
            case 5:
                shareApp("com.snapchat.android");
                break;
            case 6:
                shareApp("com.whatsapp");
                break;
            case 7:
                share();
                break;
            default:
        }
    }

    // share ảnh qua nhiều app như Fb, instagram, whatsapp, ...
    private void shareApp(String packageApp) {
        try {
            Uri photoURI = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    new File(pathImage));
            Intent intentFace = new Intent(Intent.ACTION_SEND);
            intentFace.setType("image/*");
            intentFace.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intentFace.putExtra(Intent.EXTRA_STREAM, photoURI);
            intentFace.setPackage(packageApp);
            startActivity(Intent.createChooser(intentFace, getString(R.string.photo_editor_share_image)));
        } catch (Exception e3) {
            Toasty.error(this, getString(R.string.not_install_app_share), Toasty.LENGTH_LONG).show();
        }
    }

    public void onViewClicked() {
        binding.btnDone.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Preview.DONE);
                onBtnDoneClicked();
            }
        });
    }

    public void onViewProClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
