package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.content.Context;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class PatternViewHolder extends GroupViewHolder {
    public ImageView imageView, ivBack;
    public TextView tvTitle;

    public PatternViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.ivPattern);
        tvTitle = itemView.findViewById(R.id.tvName);
        ivBack = itemView.findViewById(R.id.ivBack);
    }

    public void setTheme(ExpandableGroup patterns, Context context) {
        if (patterns instanceof Patterns) {
            if (patterns.getTitle().equals(Constant.MORE)) {
                Glide.with(context).load(R.drawable.ic_itemnone).centerCrop().into(imageView);
            } else if (patterns.getTitle().equals(Constant.DEFAULT)) {
                Pattern pattern = (Pattern) patterns.getItems().get(0);
                imageView.setImageBitmap(ImageDecoder.getBitmapFromAssets("background/" + pattern.getPath(), context));
            } else {
                Pattern pattern = (Pattern) patterns.getItems().get(0);
                Glide.with(context).load(pattern.getPath()).centerCrop().error(R.drawable.sticker_default).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
            }
            tvTitle.setText(patterns.getTitle());
        }
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
    }
}
