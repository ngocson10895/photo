package com.piccollage.collagemaker.picphotomaker.entity;

/**
 * BannerOfHome
 */
public class BannerOfHome {
    private String uri;
    private int icon;
    private String title;

    public BannerOfHome(String uri, String title) {
        this.uri = uri;
        this.title = title;
    }

    public BannerOfHome(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getUri() {
        return uri;
    }
}
