package com.piccollage.collagemaker.picphotomaker.actions;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

import java.util.List;

import dauroi.com.imageprocessing.filter.ImageFilter;
import dauroi.photoeditor.colorpicker.ColorPickerDialog;
import dauroi.photoeditor.listener.ApplyFilterListener;
import dauroi.photoeditor.model.ItemInfo;
import dauroi.photoeditor.utils.DialogUtils;
import dauroi.photoeditor.utils.DialogUtils.OnSelectDrawEffectListener;
import dauroi.photoeditor.utils.DialogUtils.OnSelectPaintSizeListener;
import dauroi.photoeditor.view.FingerPaintView;

/**
 * Create from Administrator
 * Purpose: Action vẽ khi user chọn
 * Des: Chứa các function cho user thực hiện quá trình vẽ
 */
public class DrawAction extends MaskAction implements ColorPickerDialog.OnColorChangedListener,
        OnSelectPaintSizeListener, OnSelectDrawEffectListener {
    private ImageView mEraseThumbnailView;
    private TextView mEraseNameView;
    private Dialog mSelectPaintSizeDialog;
    private ColorPickerDialog mColorPickerDialog;
    private int mCurrentColor = Color.WHITE;

    public DrawAction(EditorActivity activity) {
        super(activity);
    }

    @Override
    public void saveInstanceState(Bundle bundle) {
        super.saveInstanceState(bundle);
        bundle.putInt("dauroi.photoeditor.actions.DrawAction.mCurrentColor", mCurrentColor);
        ((FingerPaintView) mImageMaskView).saveInstanceState(bundle);
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        super.restoreInstanceState(bundle);
        mCurrentColor = bundle.getInt("dauroi.photoeditor.actions.DrawAction.mCurrentColor", mCurrentColor);
        ((FingerPaintView) mImageMaskView).restoreInstanceState(bundle);
    }

    @Override
    public void onDoneButtonClick() {
        apply(true);
    }

    @Override
    public void onApplyButtonClick() {
        apply(false);
    }

    public void apply(final boolean finish) {
        if (!isAttached()) {
            return;
        }

        ApplyFilterTask task = new ApplyFilterTask(activity, new ApplyFilterListener() {

            @Override
            public Bitmap applyFilter() {
                float ratio = activity.calculateScaleRatio();
                return ((FingerPaintView) mImageMaskView).drawImage(activity.getImage(), ratio);
            }

            @Override
            public void onFinishFiltering() {
                mCurrentColor = Color.WHITE;
                if (finish) {
                    done();
                }
            }

        });

        task.execute();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.attachMaskView(null);
        ((FingerPaintView) mImageMaskView).clear();
    }

    @Override
    public void attach() {
        super.attach();
        activity.attachMaskView(mMaskLayout);
        adjustImageMaskLayout();
        activity.applyFilter(new ImageFilter());
    }

    @SuppressLint("InflateParams")
    @Override
    public View inflateMenuView() {
        mRootActionView = mLayoutInflater.inflate(R.layout.photo_editor_action_draw, null);
        mSelectPaintSizeDialog = DialogUtils.createPreviewDrawingDialog(activity, this, false);

        View mColorView = mRootActionView.findViewById(R.id.colorView);
        mColorView.setOnClickListener(v -> {
            resetEraseButton();
            if (mColorPickerDialog == null) {
                mColorPickerDialog = new ColorPickerDialog(activity, mCurrentColor);
                mColorPickerDialog.setOnColorChangedListener(DrawAction.this);
            }

            mColorPickerDialog.setOldColor(mCurrentColor);
            if (!mColorPickerDialog.isShowing()) {
                mColorPickerDialog.show();
            }
        });

        View mSizeView = mRootActionView.findViewById(R.id.sizeView);
        mSizeView.setOnClickListener(v -> {
            resetEraseButton();
            if (!mSelectPaintSizeDialog.isShowing()) {
                mSelectPaintSizeDialog.show();
            }
        });

        View mEraseView = mRootActionView.findViewById(R.id.eraseView);
        mEraseThumbnailView = mRootActionView.findViewById(R.id.eraseThumbnailView);
        mEraseNameView = mRootActionView.findViewById(R.id.eraseNameView);
        mEraseView.setOnClickListener(v -> {
            if (((FingerPaintView) mImageMaskView).getEffect() != FingerPaintView.DRAW_EFFECT_ERASE) {
                ((FingerPaintView) mImageMaskView).setPaintEffect(FingerPaintView.DRAW_EFFECT_ERASE);
                mEraseThumbnailView.setImageResource(R.drawable.ic_draw_ic_eraser_selected);
                mEraseNameView.setTextColor(activity.getResources().getColor(R.color.pink));
            } else {
                resetEraseButton();
            }
        });

        View mClearView = mRootActionView.findViewById(R.id.clearView);
        mClearView.setOnClickListener(v -> {
            resetEraseButton();
            ((FingerPaintView) mImageMaskView).clear();
        });

        return mRootActionView;
    }

    private void resetEraseButton() {
        ((FingerPaintView) mImageMaskView).setPaintEffect(FingerPaintView.DRAW_EFFECT_NORMAL);
        mEraseThumbnailView.setImageResource(R.drawable.ic_draw_ic_eraser);
        mEraseNameView.setTextColor(activity.getResources().getColor(R.color.black));
    }

    @Override
    public void onColorChanged(int color) {
        mCurrentColor = color;
        ((FingerPaintView) mImageMaskView).setPaintColor(color);
    }

    @Override
    public void onSelectPaintSize(float size) {
        ((FingerPaintView) mImageMaskView).setPaintSize(size);
    }

    @Override
    public void onSelectEffect(int effect) {
        ((FingerPaintView) mImageMaskView).setPaintEffect(effect);
    }

    @Override
    protected int getMaskLayoutRes() {
        return R.layout.photo_editor_finger_paint_layout;
    }

    @Override
    protected void selectNormalItem(int position) {

    }

    @Override
    protected List<? extends ItemInfo> loadNormalItems(long packageId, String packageFolder) {
        return null;
    }

    @Override
    public String getActionName() {
        return "DrawAction";
    }
}
