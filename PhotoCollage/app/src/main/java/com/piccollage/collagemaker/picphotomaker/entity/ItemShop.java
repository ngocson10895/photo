package com.piccollage.collagemaker.picphotomaker.entity;

import com.piccollage.collagemaker.picphotomaker.data.item.DataSpecShop;

import java.util.ArrayList;

public class ItemShop {
    private String nameItem;
    private boolean downloaded;
    private boolean pro;
    private ArrayList<DataSpecShop> pathItems;
    private String urlFile;
    private String urlPreview;
    private String urlRoot;

    public ItemShop(String nameItem, boolean downloaded, String pro, ArrayList<DataSpecShop> pathItems, String urlFile, String urlPreview, String urlRoot) {
        this.nameItem = nameItem;
        this.downloaded = downloaded;
        this.pro = pro.contentEquals("true");
        this.pathItems = pathItems;
        this.urlFile = urlFile;
        this.urlPreview = urlPreview;
        this.urlRoot = urlRoot;
    }

    public String getUrlRoot() {
        return urlRoot;
    }

    public String getNameItem() {
        return nameItem;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public boolean isPro() {
        return pro;
    }

    public ArrayList<DataSpecShop> getPathItems() {
        return pathItems;
    }

    public String getUrlFile() {
        return urlFile;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public String getUrlPreview() {
        return urlPreview;
    }
}
