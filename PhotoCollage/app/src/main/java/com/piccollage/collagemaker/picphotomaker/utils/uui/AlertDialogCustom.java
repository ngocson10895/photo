package com.piccollage.collagemaker.picphotomaker.utils.uui;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;

import java.util.Objects;

import cn.pedant.SweetAlert.SweetAlertDialog;

// Custom alert dialog with animation
public class AlertDialogCustom extends SweetAlertDialog {

    public interface IAlertDialogListener {
        void okClick();

        void cancelClick();
    }

    private IAlertDialogListener iAlertDialogListener;

    public void setiAlertDialogListener(IAlertDialogListener iAlertDialogListener) {
        this.iAlertDialogListener = iAlertDialogListener;
    }

    public AlertDialogCustom(Context context, int alertType) {
        super(context, alertType);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customDialog();
    }

    private void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.9);
                int height = ViewGroup.LayoutParams.WRAP_CONTENT;
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setUp() {
        setTitleText(MyApplication.getContext().getString(R.string.warning_internet))
                .setContentText(MyApplication.getContext().getString(R.string.turn_on_internet))
                .setConfirmButton(MyApplication.getContext().getString(R.string.ok), sweetAlertDialog -> iAlertDialogListener.okClick())
                .setCancelButton(MyApplication.getContext().getString(R.string.cancel), SweetAlertDialog::dismiss);
    }

    public void setUpFeature() {
        setTitleText(MyApplication.getContext().getString(R.string.warning_internet))
                .setContentText(MyApplication.getContext().getString(R.string.feature_turn_on_internet))
                .setConfirmButton(MyApplication.getContext().getString(R.string.ok), sweetAlertDialog -> iAlertDialogListener.okClick())
                .setCancelButton(MyApplication.getContext().getString(R.string.cancel), sweetAlertDialog -> {
                    dismiss();
                    iAlertDialogListener.cancelClick();
                });
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
