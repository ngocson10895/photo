package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class GradientChildViewHolder extends ChildViewHolder {
    public ImageView imageView, ivChose;
    public TextView textView;

    public GradientChildViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.iv_gradient);
        textView = itemView.findViewById(R.id.tvName);
        ivChose = itemView.findViewById(R.id.ivChose);
    }

    public void setGradient(String stColor, String endColor, String name){
        GradientDrawable drawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{Color.parseColor(stColor), Color.parseColor(endColor)});
        imageView.setImageDrawable(drawable);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        textView.setText(name);
    }
}
