package com.piccollage.collagemaker.picphotomaker.data.item;

import java.util.List;

/**
 * Data response item shop
 */
public class DataResponseItem {
    private List<DataItem> data;
    private String urlRoot;
    private String urlSticker;
    private String urlFont;

    public List<DataItem> getData() {
        return data;
    }

    public String getUrlRoot() {
        return urlRoot;
    }

    public String getUrlSticker() {
        return urlSticker;
    }

    public String getUrlFont() {
        return urlFont;
    }
}
