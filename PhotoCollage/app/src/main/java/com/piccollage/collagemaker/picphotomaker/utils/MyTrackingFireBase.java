package com.piccollage.collagemaker.picphotomaker.utils;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.piccollage.collagemaker.picphotomaker.entity.Checking;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;

public class MyTrackingFireBase {

    public static void subscribeEvent(Context context, String event) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle params = new Bundle();
        params.putString("subscribe", event);
        mFirebaseAnalytics.logEvent(event, params);
    }

    public static void sendScreen(AppCompatActivity activity, String screenName) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        mFirebaseAnalytics.setCurrentScreen(activity, screenName, activity.getTitle().toString());
    }

    public static void eventAction(Context context, String eventAction) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle params = new Bundle();
        params.putString("content", eventAction);
        mFirebaseAnalytics.logEvent(eventAction, params);
    }

    public static void setUserProperty(Context context, String params, String property) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        mFirebaseAnalytics.setUserProperty(params, property);
    }

    public static void trackingNumberOfGallery(Context context, String event, int number, int total) {
        try {
            Bundle bundle = new Bundle();
            bundle.putInt(TrackingUtils.TrackingConstant.PickPhoto.NUMBER_GALLERY, number);
            bundle.putInt(TrackingUtils.TrackingConstant.PickPhoto.TOTAL_IMAGES, total);
            FirebaseAnalytics.getInstance(context).logEvent(event, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackingEventNew(String event) {
        try {
            Bundle bundle = new Bundle();
            FirebaseAnalytics.getInstance(MyApplication.getInstance()).logEvent(event, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackingDownload(Context context, String event) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString("THEME", event);
            FirebaseAnalytics.getInstance(context).logEvent("DOWNLOADED", bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackingCountry(Context context, Checking checking) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString("CITY", checking.getCity());
            bundle.putString("IP", checking.getIp());
            bundle.putString("HOSTNAME", checking.getHostname());
            bundle.putString("REGION", checking.getRegion());
            bundle.putString("COUNTRY", checking.getCountry());
            bundle.putString("LOC", checking.getLoc());
            bundle.putString("ORG", checking.getOrg());
            bundle.putString("TIMEZONE", checking.getTimezone());
            FirebaseAnalytics.getInstance(context).logEvent("CHECKING", bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void trackingScreen(Context context, String event) {
        try {
            Bundle bundle = new Bundle();
            FirebaseAnalytics.getInstance(context).logEvent(event, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
