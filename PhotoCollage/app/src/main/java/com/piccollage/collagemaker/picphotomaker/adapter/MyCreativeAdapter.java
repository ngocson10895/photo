package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.List;

/**
 * Item adapter trong màn hình my creative
 */
public class MyCreativeAdapter extends RecyclerView.Adapter<MyCreativeAdapter.ViewHolder> {
    private List<Photo> myCreativePaths;
    private Context context;
    private IChoseImage choseImage;

    public interface IChoseImage {
        void image(int position);
    }

    public MyCreativeAdapter(List<Photo> myCreativePaths, Context context, IChoseImage image) {
        this.myCreativePaths = myCreativePaths;
        this.context = context;
        this.choseImage = image;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_frame, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Glide.with(context).load(myCreativePaths.get(i).getPathPhoto()).into(viewHolder.ivMyCreative);
        viewHolder.ivMyCreative.setOnClickListener(v -> choseImage.image(i));
    }

    @Override
    public int getItemCount() {
        return myCreativePaths.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivMyCreative;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivMyCreative = itemView.findViewById(R.id.iv_frame);
        }
    }

    public void setMyCreativePaths(List<Photo> myCreativePaths) {
        this.myCreativePaths = myCreativePaths;
    }

    public List<Photo> getMyCreativePaths() {
        return myCreativePaths;
    }
}
