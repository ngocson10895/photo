package com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityChangeImageBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.ui.collageactivity.CollageActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.PipActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: màn hình change image
 * Des: để thay đổi ảnh từ các tính năng pip và collage
 */
public class ChangeImageActivity extends BaseAppActivity implements DetailGalleryFragment.IDetailGalleryContract {

    private ActivityChangeImageBinding binding;
    private Photo photoUpdate;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityChangeImageBinding.inflate(getLayoutInflater());
    }

    public void initData() {
        if (getIntent() != null) {
            Photo photo = getIntent().getParcelableExtra(CollageActivity.PHOTO);
            Glide.with(this).load(photo.getPathPhoto())
                    .placeholder(getResources().getDrawable(R.drawable.sticker_default))
                    .centerCrop().into(binding.ivOldImage);

            if (getIntent().getStringExtra(PipActivity.CHANGE_BG).equals(PipActivity.CHANGE_BG)) {
                binding.tvAddPhoto.setText(getString(R.string.change_image_background));
            } else binding.tvAddPhoto.setText(getResources().getString(R.string.change_photo));
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear(); // phải clear nếu không user có quá nhiều ảnh sẽ bị lỗi OOM
    }

    @Override
    public void buttonClick() {
        onViewClicked();
    }

    @Override
    public void onHasNetwork() {
        loadAds();
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        fragmentTrans(R.id.frGallery, GalleryAlbumFragment.newInstance(Constant.PICK_ONE, new ArrayList<>()));
    }

    private void loadAds() {
        Advertisement.showBannerAdOther(this, null, true, null);
    }

    public void onViewClicked() {
        binding.ivOk1.setOnClickListener(v -> {
            if (photoUpdate != null) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.ChangeImage.DONE);
                Intent data = new Intent();
                data.putExtra(CollageActivity.PHOTO, photoUpdate);
                setResult(AppCompatActivity.RESULT_OK, data);
                finish();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            } else {
                Toasty.warning(this, getString(R.string.have_to_pick), Toasty.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void pick(Photo photo) {
        photoUpdate = photo;
        Glide.with(this).asBitmap().load(photo.getPathPhoto()).
                placeholder(getResources().getDrawable(R.drawable.sticker_default)).centerCrop().into(binding.ivNewImage);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }
}
