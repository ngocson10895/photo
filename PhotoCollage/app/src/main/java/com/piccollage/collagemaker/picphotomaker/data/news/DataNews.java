package com.piccollage.collagemaker.picphotomaker.data.news;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * data news for home
 */
public class DataNews implements Parcelable {
    private int idMagazine;
    private String function;
    private String titleMagazine;
    private String desMagazine;
    private String thumbMagazine;
    private String contentHtml;

    private DataNews(Parcel in) {
        idMagazine = in.readInt();
        function = in.readString();
        titleMagazine = in.readString();
        desMagazine = in.readString();
        thumbMagazine = in.readString();
        contentHtml = in.readString();
    }

    public static final Creator<DataNews> CREATOR = new Creator<DataNews>() {
        @Override
        public DataNews createFromParcel(Parcel in) {
            return new DataNews(in);
        }

        @Override
        public DataNews[] newArray(int size) {
            return new DataNews[size];
        }
    };

    public int getIdMagazine() {
        return idMagazine;
    }

    public String getFunction() {
        return function;
    }

    public String getTitleMagazine() {
        return titleMagazine;
    }

    public String getDesMagazine() {
        return desMagazine;
    }

    public void setThumbMagazine(String urlRoot) {
        thumbMagazine = urlRoot + thumbMagazine;
    }

    public String getThumbMagazine() {
        return thumbMagazine;
    }

    public String getContentHtml() {
        return contentHtml;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idMagazine);
        dest.writeString(function);
        dest.writeString(titleMagazine);
        dest.writeString(desMagazine);
        dest.writeString(thumbMagazine);
        dest.writeString(contentHtml);
    }
}
