package com.piccollage.collagemaker.picphotomaker.entity;

public class Icon {
    private int iconPicked;
    private int icon;
    private boolean pick;
    private String name;

    public Icon(int iconPicked, int icon, boolean pick, String name) {
        this.iconPicked = iconPicked;
        this.icon = icon;
        this.pick = pick;
        this.name = name;
    }

    public int getIconPicked() {
        return iconPicked;
    }

    public int getIcon() {
        return icon;
    }

    public boolean isPick() {
        return pick;
    }

    public void setPick(boolean pick) {
        this.pick = pick;
    }

    public String getName() {
        return name;
    }
}
