package com.piccollage.collagemaker.picphotomaker.actions;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

import dauroi.photoeditor.R;
import dauroi.photoeditor.config.ALog;

/**
 * Create from Administrator
 * Purpose: Tạo mask action để hỗ trợ 1 số action
 * Des: Khi sử dụng các action như draw, crop cần tạo 1 mask để show action cho user
 */
public abstract class MaskAction extends PackageAction {
    private static final String TAG = MaskAction.class.getSimpleName();

    LayoutInflater mLayoutInflater;

    View mMaskLayout;
    private View mTopExtraView;
    private View mLeftExtraView;
    private View mRightExtraView;
    private View mBottomExtraView;
    View mImageMaskView;

    MaskAction(EditorActivity activity) {
        super(activity, null);
    }

    MaskAction(EditorActivity activity, String packageType) {
        super(activity, packageType);
    }

    @Override
    protected void onInit() {
        super.onInit();
        // image crop masks
        mLayoutInflater = LayoutInflater.from(activity);
        mMaskLayout = mLayoutInflater.inflate(getMaskLayoutRes(), null);
        mTopExtraView = mMaskLayout.findViewById(R.id.topView);
        mLeftExtraView = mMaskLayout.findViewById(R.id.leftView);
        mRightExtraView = mMaskLayout.findViewById(R.id.rightView);
        mBottomExtraView = mMaskLayout.findViewById(R.id.bottomView);
        mImageMaskView = mMaskLayout.findViewById(R.id.maskView);
    }

    void adjustImageMaskLayout() {
        adjustImageMaskLayout(activity.getImageWidth(), activity.getImageHeight());
    }

    void adjustImageMaskLayout(int maskImageWidth, int maskImageHeight) {
        final int[] size = activity.calculateThumbnailSize(maskImageWidth, maskImageHeight);
        final int dx = (activity.getPhotoViewWidth() - size[0]) / 2;
        final int dy = (activity.getPhotoViewHeight() - size[1]) / 2;
        if (dx <= 0) {
            mLeftExtraView.setVisibility(View.GONE);
            mRightExtraView.setVisibility(View.GONE);
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dx,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            mLeftExtraView.setLayoutParams(params);
            mRightExtraView.setLayoutParams(params);
            mLeftExtraView.setVisibility(View.VISIBLE);
            mRightExtraView.setVisibility(View.VISIBLE);
        }

        if (dy <= 0) {
            mTopExtraView.setVisibility(View.GONE);
            mBottomExtraView.setVisibility(View.GONE);
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    dy);
            mTopExtraView.setLayoutParams(params);
            mBottomExtraView.setLayoutParams(params);
            mTopExtraView.setVisibility(View.VISIBLE);
            mBottomExtraView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onActivityResume() {
        super.onActivityResume();
        ALog.d(TAG, "onActivityResume");
        if (isAttached()) {
            ALog.d(TAG, "mActivity.attachMaskView");
            activity.attachMaskView(mMaskLayout);
        }
    }

    abstract protected int getMaskLayoutRes();
}
