package com.piccollage.collagemaker.picphotomaker.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.view.View;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.news.DataNews;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityDetailNewsBinding;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

/**
 * Create from Administrator
 * Purpose: Show những thông tin mới qua api
 * Des: hiển thị cho user những thông tin mới như cách sử dụng các tính năng đc trả qua api
 */
public class DetailNewsActivity extends BaseAppActivity implements Html.ImageGetter {
    public static final String HTML = "html";

    private DataNews content;
    private ActivityDetailNewsBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityDetailNewsBinding.inflate(getLayoutInflater());
    }

    public void initData() {
        if (getIntent() != null) {
            if (MyUtils.isAppPurchased()) {
                binding.viewPro.setVisibility(View.INVISIBLE);
            }
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DialogNews.NAME);

            binding.adsLayout.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayout, false, new BannerAdsUtils.BannerAdListener() {
                @Override
                public void onAdLoaded() {
                    binding.viewPro.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAdFailed() {

                }
            }));
            content = getIntent().getParcelableExtra(HTML);
            Spannable html; // sử dụng để load html và add vào text
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                html = (Spannable) Html.fromHtml(content.getContentHtml(), Html.FROM_HTML_MODE_LEGACY, this, null);
            } else {
                html = (Spannable) Html.fromHtml(content.getContentHtml(), this, null);
            }
            binding.tvHtml.setText(html);
            Glide.with(this).asBitmap().load(content.getThumbMagazine()).into(binding.ivThumbs);
            binding.tvTitle.setText(content.getTitleMagazine());
            binding.tvDes.setText(content.getDesMagazine());
        }
    }

    public void onBackClicked() {
        binding.ivBack.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DialogNews.BACK);
            finish();
        });
    }

    // load image trong html
    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.mipmap.thumbs);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage(this).execute(source, d);

        return d;
    }

    @Override
    public void buttonClick() {
        onViewClicked();
        onViewProClicked();
        onBackClicked();
    }

    public void onViewClicked() {
        binding.clTry.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                if (new PermissionChecker(this).isPermissionOK()) {
                    if (content.getFunction().contains("COLLAGE")) {
                        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DialogNews.TRY);
                        Intent intent = new Intent(this, PickPhotoActivity.class);
                        intent.putExtra(PickPhotoActivity.MODE, Constant.COLLAGE);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        finish();
                    }
                }
            }
        });
    }

    public void onViewProClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }

    // sử dụng để chuyển image trong html sang drawable
    private static class LoadImage extends AsyncTask<Object, Void, Bitmap> {
        private WeakReference<DetailNewsActivity> weakReference;
        private LevelListDrawable mDrawable;

        LoadImage(DetailNewsActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            DetailNewsActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (IOException e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            DetailNewsActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(activity.getResources(), bitmap);
                mDrawable.addLevel(1, 1, d);
                if (WidthHeightScreen.getScreenWidthInPixels(activity) < 1080 && WidthHeightScreen.getScreenHeightInPixels(activity) < 2000) {
                    mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                } else {
                    mDrawable.setBounds(0, 0, (int) (bitmap.getWidth() * 2.5), (int) (bitmap.getHeight() * 2.5));
                }
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = activity.binding.tvHtml.getText();
                activity.binding.tvHtml.setText(t);
            }
        }
    }
}
