package com.piccollage.collagemaker.picphotomaker.viewholder;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class Palettes extends ExpandableGroup<Palette> {
    private List<Palette> items;

    public Palettes(String title, List<Palette> items) {
        super(title, items);
        this.items = items;
    }

    @Override
    public String getTitle() {
        return super.getTitle();
    }

    @Override
    public List<Palette> getItems() {
        return items;
    }
}
