package com.piccollage.collagemaker.picphotomaker.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.piccollage.collagemaker.picphotomaker.R;

import java.util.ArrayList;
import java.util.List;

/**
 * check permission for app
 */
public class PermissionChecker {
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private AppCompatActivity mActivity;
    private IPermissionListener iPermissionListener;

    public interface IPermissionListener{
        void doneAll();

        void cancel();
    }

    public PermissionChecker(AppCompatActivity activity) {
        mActivity = activity;
    }

    public void setiPermissionListener(IPermissionListener iPermissionListener) {
        this.iPermissionListener = iPermissionListener;
    }

    /**
     * Check that all given permissions have been granted by verifying that each entry in the
     * given array is of the value {@link PackageManager#PERMISSION_GRANTED}.
     *
     * @see AppCompatActivity#onRequestPermissionsResult(int, String[], int[])
     */
    private boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked.
        if (grantResults.length < 1) {
            return false;
        }

        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public boolean isPermissionOK() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkPermission();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean checkPermission() {
        boolean ret = true;

        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("Write external storage");
        }
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("Read external storage");
        }

        if (permissionsNeeded.size() > 0) {
            // Need Rationale
            String message = mActivity.getString(R.string.mess_warning);
            // Check for Rationale Option
            if (!mActivity.shouldShowRequestPermissionRationale(permissionsList.get(0))) {
                showMessageOKCancel(message,
                        (dialog, which) -> mActivity.requestPermissions(permissionsList.toArray(new String[0]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS));
            } else {
                mActivity.requestPermissions(permissionsList.toArray(new String[0]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            ret = false;
        }

        return ret;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mActivity)
                .setMessage(message)
                .setPositiveButton(mActivity.getString(R.string.done), okListener)
                .setNegativeButton(mActivity.getString(R.string.cancel), (dialog, which) -> iPermissionListener.cancel())
                .create()
                .show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        boolean ret = true;
        if (mActivity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            ret = false;
        }
        return ret;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (verifyPermissions(grantResults)) {
                // all permissions granted
                iPermissionListener.doneAll();
            } else {
                // some permissions denied
                iPermissionListener.cancel();
            }
        }
    }
}
