package com.piccollage.collagemaker.picphotomaker.utils.frame;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;

import java.util.List;

/**
 * Frame Utils: determine frame was choose to transform its photo
 */
public class FrameUtils {
    public static void createFrame(String pathFrame, List<Photo> items) {
        switch (pathFrame) {
            case "frame/01_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    OneFrameImage.collage_1_0(i, items);
                }
                break;
            case "frame/02_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_0(i, items);
                }
                break;
            case "frame/02_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_1(i, items);
                }
                break;
            case "frame/02_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_2(i, items);
                }
                break;
            case "frame/02_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_3(i, items);
                }
                break;
            case "frame/02_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_4(i, items);
                }
                break;
            case "frame/02_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_5(i, items);
                }
                break;
            case "frame/02_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_6(i, items);
                }
                break;
            case "frame/02_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_7(i, items);
                }
                break;
            case "frame/02_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_8(i, items);
                }
                break;
            case "frame/02_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_9(i, items);
                }
                break;
            case "frame/02_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_10(i, items);
                }
                break;
            case "frame/02_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TwoFrameImage.collage_2_11(i, items);
                }
                break;
            case "frame/03_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_0(i, items);
                }
                break;
            case "frame/03_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_1(i, items);
                }
                break;
            case "frame/03_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_2(i, items);
                }
                break;
            case "frame/03_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_3(i, items);
                }
                break;
            case "frame/03_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_4(i, items);
                }
                break;
            case "frame/03_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_5(i, items);
                }
                break;
            case "frame/03_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_6(i, items);
                }
                break;
            case "frame/03_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_7(i, items);
                }
                break;
            case "frame/03_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_8(i, items);
                }
                break;
            case "frame/03_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_9(i, items);
                }
                break;
            case "frame/03_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_10(i, items);
                }
                break;
            case "frame/03_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_11(i, items);
                }
                break;
            case "frame/03_12.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_12(i, items);
                }
                break;
            case "frame/03_13.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_13(i, items);
                }
                break;
            case "frame/03_14.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_14(i, items);
                }
                break;
            case "frame/03_15.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_15(i, items);
                }
                break;
            case "frame/03_16.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_16(i, items);
                }
                break;
            case "frame/03_17.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_17(i, items);
                }
                break;
            case "frame/03_18.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_18(i, items);
                }
                break;
            case "frame/03_19.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_19(i, items);
                }
                break;
            case "frame/03_20.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_20(i, items);
                }
                break;
            case "frame/03_21.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_21(i, items);
                }
                break;
            case "frame/03_22.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_22(i, items);
                }
                break;
            case "frame/03_23.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_23(i, items);
                }
                break;
            case "frame/03_24.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_24(i, items);
                }
                break;
            case "frame/03_25.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_25(i, items);
                }
                break;
            case "frame/03_26.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_26(i, items);
                }
                break;
            case "frame/03_27.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_27(i, items);
                }
                break;
            case "frame/03_28.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_28(i, items);
                }
                break;
            case "frame/03_29.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_29(i, items);
                }
                break;
            case "frame/03_30.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_30(i, items);
                }
                break;
            case "frame/03_31.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_31(i, items);
                }
                break;
            case "frame/03_32.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_32(i, items);
                }
                break;
            case "frame/03_33.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_33(i, items);
                }
                break;
            case "frame/03_34.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_34(i, items);
                }
                break;
            case "frame/03_35.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_35(i, items);
                }
                break;
            case "frame/03_36.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_36(i, items);
                }
                break;
            case "frame/03_37.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_37(i, items);
                }
                break;
            case "frame/03_38.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_38(i, items);
                }
                break;
            case "frame/03_39.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_39(i, items);
                }
                break;
            case "frame/03_40.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_40(i, items);
                }
                break;
            case "frame/03_41.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_41(i, items);
                }
                break;
            case "frame/03_42.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_42(i, items);
                }
                break;
            case "frame/03_43.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_43(i, items);
                }
                break;
            case "frame/03_44.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_44(i, items);
                }
                break;
            case "frame/03_45.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_45(i, items);
                }
                break;
            case "frame/03_46.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_46(i, items);
                }
                break;
            case "frame/03_47.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    ThreeFrameImage.collage_3_47(i, items);
                }
                break;
            case "frame/04_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_0(i, items);
                }
                break;
            case "frame/04_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_1(i, items);
                }
                break;
            case "frame/04_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_2(i, items);
                }
                break;
//            case "frame/04_3.png":
//                for (int i = 0; i < items.size(); i++) {
//                    items.get(i).pointList.clear();
//                    FourFrameImage.collage_4_3(i, items);
//                }
//                break;
            case "frame/04_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_4(i, items);
                }
                break;
            case "frame/04_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_5(i, items);
                }
                break;
            case "frame/04_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_6(i, items);
                }
                break;
            case "frame/04_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_7(i, items);
                }
                break;
            case "frame/04_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_8(i, items);
                }
                break;
            case "frame/04_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_9(i, items);
                }
                break;
            case "frame/04_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_10(i, items);
                }
                break;
            case "frame/04_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_11(i, items);
                }
                break;
            case "frame/04_12.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_12(i, items);
                }
                break;
            case "frame/04_13.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_13(i, items);
                }
                break;
            case "frame/04_14.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_14(i, items);
                }
                break;
            case "frame/04_15.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_15(i, items);
                }
                break;
            case "frame/04_16.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_16(i, items);
                }
                break;
            case "frame/04_17.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_17(i, items);
                }
                break;
            case "frame/04_18.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_18(i, items);
                }
                break;
            case "frame/04_19.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_19(i, items);
                }
                break;
            case "frame/04_20.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_20(i, items);
                }
                break;
            case "frame/04_21.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_21(i, items);
                }
                break;
            case "frame/04_22.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_22(i, items);
                }
                break;
            case "frame/04_23.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_23(i, items);
                }
                break;
            case "frame/04_24.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_24(i, items);
                }
                break;
            case "frame/04_25.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FourFrameImage.collage_4_25(i, items);
                }
                break;
            case "frame/05_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_0(i, items);
                }
                break;
            case "frame/05_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_1(i, items);
                }
                break;
            case "frame/05_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_2(i, items);
                }
                break;
            case "frame/05_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_3(i, items);
                }
                break;
            case "frame/05_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_4(i, items);
                }
                break;
            case "frame/05_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_5(i, items);
                }
                break;
            case "frame/05_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_6(i, items);
                }
                break;
            case "frame/05_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_7(i, items);
                }
                break;
            case "frame/05_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_8(i, items);
                }
                break;
            case "frame/05_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_9(i, items);
                }
                break;
            case "frame/05_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_10(i, items);
                }
                break;
            case "frame/05_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_11(i, items);
                }
                break;
            case "frame/05_12.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_12(i, items);
                }
                break;
            case "frame/05_13.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_13(i, items);
                }
                break;
            case "frame/05_14.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_14(i, items);
                }
                break;
            case "frame/05_15.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_15(i, items);
                }
                break;
            case "frame/05_16.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_16(i, items);
                }
                break;
            case "frame/05_17.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_17(i, items);
                }
                break;
            case "frame/05_18.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_18(i, items);
                }
                break;
            case "frame/05_19.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_19(i, items);
                }
                break;
            case "frame/05_20.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_20(i, items);
                }
                break;
            case "frame/05_21.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_21(i, items);
                }
                break;
            case "frame/05_22.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_22(i, items);
                }
                break;
            case "frame/05_23.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_23(i, items);
                }
                break;
            case "frame/05_24.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_24(i, items);
                }
                break;
            case "frame/05_25.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_25(i, items);
                }
                break;
            case "frame/05_26.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_26(i, items);
                }
                break;
            case "frame/05_27.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_27(i, items);
                }
                break;
            case "frame/05_28.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_28(i, items);
                }
                break;
            case "frame/05_29.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_29(i, items);
                }
                break;
            case "frame/05_30.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_30(i, items);
                }
                break;
            case "frame/05_31.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    FiveFrameImage.collage_5_31(i, items);
                }
                break;
            case "frame/06_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_0(i, items);
                }
                break;
            case "frame/06_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_1(i, items);
                }
                break;
            case "frame/06_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_2(i, items);
                }
                break;
            case "frame/06_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_3(i, items);
                }
                break;
            case "frame/06_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_4(i, items);
                }
                break;
            case "frame/06_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_5(i, items);
                }
                break;
            case "frame/06_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_6(i, items);
                }
                break;
            case "frame/06_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_7(i, items);
                }
                break;
            case "frame/06_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_8(i, items);
                }
                break;
            case "frame/06_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_9(i, items);
                }
                break;
            case "frame/06_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_10(i, items);
                }
                break;
            case "frame/06_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_11(i, items);
                }
                break;
            case "frame/06_12.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_12(i, items);
                }
                break;
            case "frame/06_13.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_13(i, items);
                }
                break;
            case "frame/06_14.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SixFrameImage.collage_6_14(i, items);
                }
                break;
            case "frame/07_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_0(i, items);
                }
                break;
            case "frame/07_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_1(i, items);
                }
                break;
            case "frame/07_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_2(i, items);
                }
                break;
            case "frame/07_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_3(i, items);
                }
                break;
            case "frame/07_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_4(i, items);
                }
                break;
            case "frame/07_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_5(i, items);
                }
                break;
            case "frame/07_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_6(i, items);
                }
                break;
            case "frame/07_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_7(i, items);
                }
                break;
            case "frame/07_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_8(i, items);
                }
                break;
            case "frame/07_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_9(i, items);
                }
                break;
            case "frame/07_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    SevenFrameImage.collage_7_10(i, items);
                }
                break;
            case "frame/08_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_0(i, items);
                }
                break;
            case "frame/08_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_1(i, items);
                }
                break;
            case "frame/08_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_2(i, items);
                }
                break;
            case "frame/08_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_3(i, items);
                }
                break;
            case "frame/08_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_4(i, items);
                }
                break;
            case "frame/08_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_5(i, items);
                }
                break;
            case "frame/08_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_6(i, items);
                }
                break;
            case "frame/08_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_7(i, items);
                }
                break;
            case "frame/08_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_8(i, items);
                }
                break;
            case "frame/08_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_9(i, items);
                }
                break;
            case "frame/08_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_10(i, items);
                }
                break;
            case "frame/08_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_11(i, items);
                }
                break;
            case "frame/08_12.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_12(i, items);
                }
                break;
            case "frame/08_13.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_13(i, items);
                }
                break;
            case "frame/08_14.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_14(i, items);
                }
                break;
            case "frame/08_15.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_15(i, items);
                }
                break;
            case "frame/08_16.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    EightFrameImage.collage_8_16(i, items);
                }
                break;
            case "frame/09_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_0(i, items);
                }
                break;
            case "frame/09_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_1(i, items);
                }
                break;
            case "frame/09_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_2(i, items);
                }
                break;
            case "frame/09_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_3(i, items);
                }
                break;
            case "frame/09_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_4(i, items);
                }
                break;
            case "frame/09_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_5(i, items);
                }
                break;
            case "frame/09_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_6(i, items);
                }
                break;
            case "frame/09_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_7(i, items);
                }
                break;
            case "frame/09_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_8(i, items);
                }
                break;
            case "frame/09_9.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_9(i, items);
                }
                break;
            case "frame/09_10.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_10(i, items);
                }
                break;
            case "frame/09_11.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    NineFrameImage.collage_9_11(i, items);
                }
                break;
            case "frame/10_0.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_0(i, items);
                }
                break;
            case "frame/10_1.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_1(i, items);
                }
                break;
            case "frame/10_2.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_2(i, items);
                }
                break;
            case "frame/10_3.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_3(i, items);
                }
                break;
            case "frame/10_4.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_4(i, items);
                }
                break;
            case "frame/10_5.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_5(i, items);
                }
                break;
            case "frame/10_6.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_6(i, items);
                }
                break;
            case "frame/10_7.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_7(i, items);
                }
                break;
            case "frame/10_8.png":
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).pointList.clear();
                    TenFrameImage.collage_10_8(i, items);
                }
                break;
            default:
        }
    }
}
