package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemIconShareBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;

import java.util.ArrayList;

/**
 * Adapter icon ở màn hình preview
 */
public class IconShareAdapter extends RecyclerView.Adapter<IconShareAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Icon> icons;
    private IPickShare iPickShare;

    public interface IPickShare {
        void pickShare(int position);
    }

    public IconShareAdapter(Context context, ArrayList<Icon> icons, IPickShare iPickShare) {
        this.context = context;
        this.icons = icons;
        this.iPickShare = iPickShare;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_icon_share, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(context).load(context.getResources().getDrawable(icons.get(position).getIcon())).into(holder.binding.iconShare);
        holder.binding.tvName.setText(icons.get(position).getName());
        holder.itemView.setOnClickListener(v -> iPickShare.pickShare(position));
    }

    @Override
    public int getItemCount() {
        return icons.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemIconShareBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemIconShareBinding.bind(itemView);
        }
    }
}
