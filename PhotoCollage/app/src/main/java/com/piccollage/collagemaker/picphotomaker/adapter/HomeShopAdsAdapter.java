package com.piccollage.collagemaker.picphotomaker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemHomeShopBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.viewholder.UnifiedNativeAdViewHolder;

import java.util.List;

/**
 * Adapter có thêm ads trong phần shop của home có thêm ads native
 */
public class HomeShopAdsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;
    private static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 1;

    private Context mContext;
    // The list of Native ads and menu items.
    private List<Object> mRecyclerViewItems;

    private IShopHomeListener iShopHomeListener;

    public interface IShopHomeListener {
        void pickItemShop(int pos);

        void pickItemShopPro(int pos);

        void itemDownloaded(int pos);
    }

    public HomeShopAdsAdapter(Context mContext, List<Object> mRecyclerViewItems) {
        this.mContext = mContext;
        this.mRecyclerViewItems = mRecyclerViewItems;
    }

    public void setiShopHomeListener(IShopHomeListener iShopHomeListener) {
        this.iShopHomeListener = iShopHomeListener;
    }

    public void setmRecyclerViewItems(List<Object> mRecyclerViewItems) {
        this.mRecyclerViewItems = mRecyclerViewItems;
        notifyDataSetChanged();
    }

    public List<Object> getmRecyclerViewItems() {
        return mRecyclerViewItems;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                MyTrackingFireBase.eventAction(MyApplication.getInstance(), TrackingUtils.TrackingConstant.Ads.Native.SHOW);
                View unifiedNativeLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ads_shop_home, parent, false);
                return new UnifiedNativeAdViewHolder(unifiedNativeLayoutView);
            case MENU_ITEM_VIEW_TYPE:
                // Fall through.
            default:
                View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_shop, parent, false);
                return new ItemViewHolder(menuItemLayoutView);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        int viewType = getItemViewType(i);
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) mRecyclerViewItems.get(i);
                NativeAdAdapter.populateUnifiedNativeAdViewHome(nativeAd, ((UnifiedNativeAdViewHolder) holder).getAdView());
                break;
            case MENU_ITEM_VIEW_TYPE:
                // fall through
            default:
                ItemViewHolder viewHolder = (ItemViewHolder) holder;
                ItemShop itemShop = (ItemShop) mRecyclerViewItems.get(i);

                Glide.with(mContext).load(itemShop.getPathItems().get(0).getUrlThumb())
                        .error(R.drawable.img_default)
                        .placeholder(R.drawable.img_default)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHolder.binding.ivThumb);
                viewHolder.binding.tvName.setText(itemShop.getPathItems().get(0).getTitle());
                viewHolder.binding.tvQuantity.setText(itemShop.getNameItem().split("_")[1] + " " + mContext.getString(R.string.items));
                if (itemShop.isPro())
                    viewHolder.itemView.setOnClickListener(v -> iShopHomeListener.pickItemShopPro(i));
                else viewHolder.itemView.setOnClickListener(v -> iShopHomeListener.pickItemShop(i));

                if (itemShop.isDownloaded()) {
                    viewHolder.binding.btnDone.setVisibility(View.VISIBLE);
                    viewHolder.binding.pro.setVisibility(View.INVISIBLE);
                    viewHolder.binding.free.setVisibility(View.INVISIBLE);
                    viewHolder.binding.btnDownload.setVisibility(View.INVISIBLE);
                    viewHolder.itemView.setOnClickListener(v -> iShopHomeListener.itemDownloaded(i));
                } else {
                    viewHolder.binding.btnDone.setVisibility(View.INVISIBLE);
                    if (MyUtils.isAppPurchased()) {
                        viewHolder.binding.btnDownload.setVisibility(View.VISIBLE);
                        viewHolder.binding.pro.setVisibility(View.INVISIBLE);
                        viewHolder.binding.free.setVisibility(View.INVISIBLE);
                    } else {
                        viewHolder.binding.btnDownload.setVisibility(View.INVISIBLE);
                        if (itemShop.isPro()) {
                            viewHolder.binding.pro.setVisibility(View.VISIBLE);
                            viewHolder.binding.free.setVisibility(View.INVISIBLE);
                        } else {
                            viewHolder.binding.pro.setVisibility(View.INVISIBLE);
                            viewHolder.binding.free.setVisibility(View.VISIBLE);
                        }
                    }
                }
        }
    }

    @Override
    public int getItemCount() {
        return mRecyclerViewItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object recyclerViewItem = mRecyclerViewItems.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE;
        }
        return MENU_ITEM_VIEW_TYPE;
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        ItemHomeShopBinding binding;

        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemHomeShopBinding.bind(itemView);
        }
    }
}
