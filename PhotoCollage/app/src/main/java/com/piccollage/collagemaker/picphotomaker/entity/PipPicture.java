package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity(tableName = "pip_style")
public class PipPicture implements Parcelable {
    private boolean isPro;
    private String pathPreview;
    private String pathPreviewOnline;
    private String foreGround;
    @Ignore
    private ArrayList<String> masks = new ArrayList<>();
    private int type;
    @Ignore
    private String linkToDownload;
    private int id;
    @Ignore
    private boolean isDownloaded;
    @Ignore
    private boolean isPick;

    @PrimaryKey(autoGenerate = true)
    private int idDB;

    public PipPicture() {
    }

    protected PipPicture(Parcel in) {
        isPro = in.readByte() != 0;
        pathPreview = in.readString();
        pathPreviewOnline = in.readString();
        foreGround = in.readString();
        masks = in.createStringArrayList();
        type = in.readInt();
        linkToDownload = in.readString();
        id = in.readInt();
        isDownloaded = in.readByte() != 0;
        isPick = in.readByte() != 0;
        idDB = in.readInt();
    }

    public static final Creator<PipPicture> CREATOR = new Creator<PipPicture>() {
        @Override
        public PipPicture createFromParcel(Parcel in) {
            return new PipPicture(in);
        }

        @Override
        public PipPicture[] newArray(int size) {
            return new PipPicture[size];
        }
    };

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public int getIdDB() {
        return idDB;
    }

    public void setIdDB(int idDB) {
        this.idDB = idDB;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPathPreview() {
        return pathPreview;
    }

    public void setPathPreview(String pathPreview) {
        this.pathPreview = pathPreview;
    }

    public String getForeGround() {
        return foreGround;
    }

    public String getPathPreviewOnline() {
        return pathPreviewOnline;
    }

    public void setPathPreviewOnline(String pathPreviewOnline) {
        this.pathPreviewOnline = pathPreviewOnline;
    }

    public void setForeGround(String foreGround) {
        this.foreGround = foreGround;
    }

    public boolean isPro() {
        return isPro;
    }

    public void setPro(boolean pro) {
        isPro = pro;
    }

    public ArrayList<String> getMasks() {
        return masks;
    }

    public String getLinkToDownload() {
        return linkToDownload;
    }

    public void setLinkToDownload(String linkToDownload) {
        this.linkToDownload = linkToDownload;
    }

    @NonNull
    @Override
    public String toString() {
        return pathPreview + " " + foreGround + " " + id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isPro ? 1 : 0));
        dest.writeString(pathPreview);
        dest.writeString(pathPreviewOnline);
        dest.writeString(foreGround);
        dest.writeStringList(masks);
        dest.writeInt(type);
        dest.writeString(linkToDownload);
        dest.writeInt(id);
        dest.writeByte((byte) (isDownloaded ? 1 : 0));
        dest.writeByte((byte) (isPick ? 1 : 0));
        dest.writeInt(idDB);
    }
}
