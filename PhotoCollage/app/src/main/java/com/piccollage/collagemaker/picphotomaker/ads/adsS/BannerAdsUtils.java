package com.piccollage.collagemaker.picphotomaker.ads.adsS;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.corebase.logger;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.ConstantAds;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.utils.LogUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

/**
 * Banner Google ads
 */
public class BannerAdsUtils {
    private static final String TAG = "BannerAdsUtils";
    private static BannerAdsUtils instances;

    public static BannerAdsUtils getInstances() {
        if (null == instances) {
            instances = new BannerAdsUtils();
        }
        return instances;
    }

    public void loadBannerAdExitApp(Context context) {
        String[] banner_id = logger.getAdmob_banner_exit2(context);
        new BannerAdBuilder().loadExitAd(context, banner_id, AdSize.MEDIUM_RECTANGLE, 0);
    }

    public void loadBannerAdSettings(Context context, ViewGroup container) {
        String[] banner_id = logger.getadmob_banner_other1(context);
        AdSize adSize = AdSize.BANNER;
        new BannerAdBuilder().loadAd(context, container, banner_id, adSize, 0, null);
    }

    // 19/03/2020: load ads banner other. chia ra preLoad hoặc không
    public void loadBannerAdOther(Context context, ViewGroup container, boolean isPreLoad, BannerAdListener bannerAdListener) {
        String[] banner_id = logger.getadmob_banner_other1(context);
        if (isPreLoad) {
            new BannerAdBuilder().loadOtherAd(context, banner_id, AdSize.BANNER, 0);
        } else
            new BannerAdBuilder().loadAd(context, container, banner_id, AdSize.BANNER, 0, bannerAdListener);
    }

    public void loadBannerAdHome(Context context, ViewGroup container, AdSize adSize, BannerAdListener bannerAdListener) {
        String[] banner_id = logger.getAdmob_banner_home0(context);
        new BannerAdBuilder().loadAd(context, container, banner_id, adSize, 0, bannerAdListener);
    }

    public interface BannerAdListener {
        void onAdLoaded();

        void onAdFailed();
    }

    public static class BannerAdBuilder {
        void loadExitAd(final Context context, final String[] banner_id, final AdSize adSize, final int count) {
            MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.REQUEST);
            if (context == null || MyUtils.isAppPurchased()) {
                return;
            }
            if (banner_id == null || banner_id.length == 0 || count >= banner_id.length) {
                return;
            }
            final String ads_id = banner_id[count];
            final AdView adView = new AdView(context);
//            if (adSize == AdSize.MEDIUM_RECTANGLE) {
                adView.setAdSize(adSize);
//            } else adView.setAdSize(getAdSize());

            if (LogUtils.isForceDebug) {
                adView.setAdUnitId(ads_id);
            } else adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.FAIL + i);
                    loadExitAd(context, banner_id, adSize, count + 1);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.LOADED);
                    ConstantAds.BANNER_GOOGLE_AD_EXIT = adView;
                }
            });
            adView.loadAd(getAdRequest());
        }

        void loadOtherAd(final Context context, final String[] banner_id, final AdSize adSize, final int count) {
            MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.REQUEST);
            if (context == null || MyUtils.isAppPurchased()) {
                return;
            }
            if (banner_id == null || banner_id.length == 0 || count >= banner_id.length) {
                return;
            }
            final String ads_id = banner_id[count];
            final AdView adView = new AdView(context);
//            if (adSize == AdSize.MEDIUM_RECTANGLE) {
                adView.setAdSize(adSize);
//            } else adView.setAdSize(getAdSize());

            if (LogUtils.isForceDebug) {
                adView.setAdUnitId(ads_id);
            } else adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.FAIL + i);
                    loadOtherAd(context, banner_id, adSize, count + 1);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.LOADED);
                    ConstantAds.BANNER_GOOGLE_AD_OTHER = adView;
                }
            });
            adView.loadAd(getAdRequest());
        }

        public void loadAd(final Context context, final ViewGroup viewGroup, final String[] banner_id, final AdSize adSize, final int count, final BannerAdListener listener) {
            MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.REQUEST);
            if (context == null || MyUtils.isAppPurchased()) {
                return;
            }
            if (banner_id == null || banner_id.length == 0 || count >= banner_id.length) {
                if (listener != null) listener.onAdFailed();
                return;
            }
            String ads_id = banner_id[count];
            final AdView adView = new AdView(context);
//            if (adSize == AdSize.MEDIUM_RECTANGLE) {
                adView.setAdSize(adSize);
//            } else adView.setAdSize(getAdSize());

            if (LogUtils.isForceDebug) {
                adView.setAdUnitId(ads_id);
            } else adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                    MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.FAIL + i);
                    loadAd(context, viewGroup, banner_id, adSize, count + 1, listener);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    MyTrackingFireBase.eventAction(context, TrackingUtils.TrackingConstant.Ads.Banner.LOADED);
                    if (listener != null) listener.onAdLoaded();
                    if (viewGroup != null) {
                        addMyBannerView(viewGroup, adView);
                    }
                }
            });
            adView.loadAd(getAdRequest());
        }

        private void addMyBannerView(ViewGroup container, AdView view) {
            if (container == null || view == null) {
                return;
            }
            try {
                if (view.getParent() != null) {
                    ((ViewGroup) view.getParent()).removeAllViews();
                }
                container.setVisibility(View.VISIBLE);
                container.removeAllViews();
                container.addView(view);
            } catch (Exception ignored) {
            }
        }

        private AdRequest getAdRequest() {
            return new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, new Bundle())
                    .build();
        }

        private AdSize getAdSize() {
            // Determine the screen width (less decorations) to use for the ad width.
            DisplayMetrics outMetrics = MyApplication.getContext().getResources().getDisplayMetrics();

            float density = outMetrics.density;

            float adWidthPixels = WidthHeightScreen.getScreenWidthInPixels(MyApplication.getContext());

            // If the ad hasn't been laid out, default to the full screen width.
            if (adWidthPixels == 0) {
                adWidthPixels = outMetrics.widthPixels;
            }

            int adWidth = (int) (adWidthPixels / density);

            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(MyApplication.getContext(), adWidth);
        }
    }
}
