package com.piccollage.collagemaker.picphotomaker.actions;

import android.graphics.Bitmap;

import com.piccollage.collagemaker.picphotomaker.ui.editoractivity.EditorActivity;

/**
 * Create from Administrator
 * Purpose: Tính năng làm mờ ảnh
 * Des: Thực hiện tính năng làm mờ ảnh
 */
public abstract class BlurAction extends BaseAction {
    static final int FAST_BLUR_RADIUS = 30; // độ mờ cho tính năng focus
    static final int FAST_BLUR_RADIUS_TOUCH_BLUR = 10; // độ mờ cho tính năng touch blur
    static final int EXCLUDE_BLURRED_SIZE = 30; // tăng kích thước blur

    Bitmap mBlurredImage;

    BlurAction(EditorActivity activity) {
        super(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        recycleImages();
    }

    // giải phóng bộ nhớ khi sử dụng ảnh blur
    void recycleImages() {
        if (mBlurredImage != null && !mBlurredImage.isRecycled()) {
            mBlurredImage.recycle();
            mBlurredImage = null;
        }
    }
}
