package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.SeekBar;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ItemFeatureAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.RatioIconAdapter;
import com.piccollage.collagemaker.picphotomaker.base.BaseView;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ViewMarginBinding;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

/**
 * Create from Administrator
 * Purpose: chỉnh sửa ảnh
 * Des: Thực hiện các tác vụ liên quan đến chỉnh sửa margin, shadow, corner, ratio
 */
public class ViewMargin extends BaseView implements SeekBar.OnSeekBarChangeListener {

    private ViewMarginBinding binding;
    private IMarginViewListener listener;
    private ItemFeatureAdapter itemFeatureAdapter;
    private int curFeature = 0, curRatio = 0, oldRatio = 0;
    private float curMargin = 2f, curCorner = 0f, curShadow = 0f;
    private float oldMargin = 2f, oldCorner = 0f, oldShadow = 0f;
    private RatioIconAdapter ratioIconAdapter;

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.sbCorner:
                curCorner = seekBar.getProgress();
                listener.previewAction(curMargin, seekBar.getProgress(), curShadow);
                break;
            case R.id.sbMargin:
                curMargin = seekBar.getProgress();
                listener.previewAction(seekBar.getProgress(), curCorner, curShadow);
                break;
            case R.id.sbShadow:
                curShadow = seekBar.getProgress();
                listener.previewAction(curMargin, curCorner, seekBar.getProgress());
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public interface IMarginViewListener extends IBaseViewListener {
        void previewAction(float margin, float corner, float shadow);

        void previewRatio(String ratio);
    }

    public ViewMargin(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData();
    }

    public void setListener(IMarginViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.NAME);
    }

    @Override
    public void initData() {
        itemFeatureAdapter = new ItemFeatureAdapter(InitData.addBorderIcon(), getContext());
        binding.rvFeature.setAdapter(itemFeatureAdapter);
        itemFeatureAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (curFeature != position) {
                int temp = curFeature;
                curFeature = position;
                itemFeatureAdapter.getData().get(temp).setPick(false);
                itemFeatureAdapter.getData().get(curFeature).setPick(true);
                itemFeatureAdapter.notifyItemChanged(temp);
                itemFeatureAdapter.notifyItemChanged(curFeature);
                pickFeature(position);
            }
        });

        ratioIconAdapter = new RatioIconAdapter(InitData.ratioData(), getContext());
        binding.rvRatios.setAdapter(ratioIconAdapter);
        ratioIconAdapter.setOnItemClickListener((adapter, view, position) -> {
            if (curRatio != position) {
                int temp = curRatio;
                curRatio = position;
                ratioIconAdapter.getData().get(temp).setPick(false);
                ratioIconAdapter.notifyItemChanged(temp);
                ratioIconAdapter.getData().get(curRatio).setPick(true);
                ratioIconAdapter.notifyItemChanged(curRatio);
                listener.previewRatio(ratioIconAdapter.getData().get(position).getRatio());
            }
        });
    }

    private void pickFeature(int position) {
        switch (position) {
            case 0:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.MARGIN);
                showMargin();
                break;
            case 1:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.CORNER);
                binding.sbCorner.setVisibility(VISIBLE);
                binding.sbMargin.setVisibility(INVISIBLE);
                binding.sbShadow.setVisibility(INVISIBLE);
                binding.rvRatios.setVisibility(INVISIBLE);
                break;
            case 2:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.RATIO);
                binding.rvRatios.setVisibility(VISIBLE);
                binding.sbMargin.setVisibility(INVISIBLE);
                binding.sbCorner.setVisibility(INVISIBLE);
                binding.sbShadow.setVisibility(INVISIBLE);
                break;
            case 3:
                MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.SHADOW);
                binding.sbShadow.setVisibility(VISIBLE);
                binding.sbMargin.setVisibility(INVISIBLE);
                binding.sbCorner.setVisibility(INVISIBLE);
                binding.rvRatios.setVisibility(INVISIBLE);
                break;
        }
    }

    private void showMargin() {
        itemFeatureAdapter.getData().get(curFeature).setPick(false);
        itemFeatureAdapter.notifyItemChanged(curFeature);
        curFeature = 0;
        itemFeatureAdapter.getData().get(0).setPick(true);
        itemFeatureAdapter.notifyItemChanged(curFeature);
        binding.sbMargin.setVisibility(VISIBLE);
        binding.sbShadow.setVisibility(INVISIBLE);
        binding.sbCorner.setVisibility(INVISIBLE);
        binding.rvRatios.setVisibility(INVISIBLE);
    }

    @Override
    protected void initView() {
        binding = ViewMarginBinding.bind(this);
        binding.sbCorner.setOnSeekBarChangeListener(this);
        binding.sbMargin.setOnSeekBarChangeListener(this);
        binding.sbShadow.setOnSeekBarChangeListener(this);

        binding.rvRatios.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvRatios.addItemDecoration(new BetweenSpacesItemDecoration(0, 16));

        binding.rvFeature.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvFeature.addItemDecoration(new BetweenSpacesItemDecoration(0, 16));
    }

    @Override
    public int getLayoutID() {
        return R.layout.view_margin;
    }

    @Override
    public void buttonClick() {
        onDoneViewClick();
        onCloseViewClick();
    }

    @Override
    public void backAction() {
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.CANCEL);
        if (curMargin != oldMargin || curShadow != oldShadow || curCorner != oldCorner || curRatio != oldRatio) {
            curShadow = oldShadow;
            curCorner = oldCorner;
            curMargin = oldMargin;
            binding.sbShadow.setProgress((int) curShadow);
            binding.sbMargin.setProgress((int) curMargin);
            binding.sbCorner.setProgress((int) curCorner);

            ratioIconAdapter.getData().get(curRatio).setPick(false);
            ratioIconAdapter.notifyItemChanged(curRatio);
            curRatio = oldRatio;
            ratioIconAdapter.getData().get(curRatio).setPick(true);
            ratioIconAdapter.notifyItemChanged(curRatio);
            listener.previewRatio(ratioIconAdapter.getData().get(curRatio).getRatio());
        }
        showMargin();
        hide();
        listener.backAction();
    }

    private void onDoneViewClick() {
        binding.ivDoneView.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewMargin.DONE);
            oldCorner = curCorner;
            oldMargin = curMargin;
            oldShadow = curShadow;
            oldRatio = curRatio;
            showMargin();
            listener.doneAction();
            hide();
        });
    }

    private void onCloseViewClick() {
        binding.ivHideView.setOnClickListener(v -> backAction());
    }
}
