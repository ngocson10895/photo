package com.piccollage.collagemaker.picphotomaker.entity;

public class FeatureOfBorder {
    private int iconPicked;
    private int icon;
    private boolean pick;

    public FeatureOfBorder(int iconPicked, int icon, boolean pick) {
        this.iconPicked = iconPicked;
        this.icon = icon;
        this.pick = pick;
    }

    public int getIconPicked() {
        return iconPicked;
    }

    public int getIcon() {
        return icon;
    }

    public boolean isPick() {
        return pick;
    }

    public void setPick(boolean pick) {
        this.pick = pick;
    }
}
