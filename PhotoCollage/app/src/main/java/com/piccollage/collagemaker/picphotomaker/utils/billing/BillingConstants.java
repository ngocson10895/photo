/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.piccollage.collagemaker.picphotomaker.utils.billing;

/**
 * Static fields and methods useful for billing
 */

public final class BillingConstants {

    public static final String SKU_REWARD_VIDEO = "android.test.reward";
    public static final String SKU_PHOTOCOLLAGE002 = "pipcollage.sub.001";
    public static final String SUBSCRIPTION_MONTHLY_001 = "pipcollage.sub.monthly.001";
    public static final String SUBSCRIPTION_MONTH_ID_001 = "pipcollage.sub.moths.001";
    public static final String SUBSCRIPTION_MONTH_TRIAL_ID_001 = "pipcollage.sub.monthlytrial.001";
    public static final String SUBSCRIPTION_YEARLY_ID_001 = "pipcollage.sub.yearly.001";
    public static final String SUBSCRIPTION_YEARLY_TRIAL_ID_001 = "pipcollage.sub.yearlytrial.001";

    public static final String SUBSCRIPTION_MONTHLY_002 = "pipcollage.sub.monthly.002";
    public static final String SUBSCRIPTION_MONTH_ID_002 = "pipcollage.sub.moths.002";
    public static final String SUBSCRIPTION_MONTH_TRAIL_ID_002 = "pipcollage.sub.monthlytrial.002";
    public static final String SUBSCRIPTION_YEARLY_ID_002 = "pipcollage.sub.yearly.002";
    public static final String SUBSCRIPTION_YEARLY_TRIAL_ID_002 = "pipcollage.sub.yearlytrial.002";

    public static final String SUBSCRIPTION_MONTHLY_003 = "pipcollage.sub.monthly.003";
    public static final String SUBSCRIPTION_MONTH_ID_003 = "pipcollage.sub.moths.003";
    public static final String SUBSCRIPTION_MONTH_TRIAL_ID_003 = "pipcollage.sub.monthlytrial.003";
    public static final String SUBSCRIPTION_YEARLY_ID_003 = "pipcollage.sub.yearly.003";
    public static final String SUBSCRIPTION_YEARLY_TRIAL_ID_003 = "pipcollage.sub.yearlytrial.003";

//    public static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnbfuHnQvnG" +
//            "cggKyRp/PdQ5Ot6VXJqGuF0ohqL5TtvihtsZYxxH7ZcbvXrzG8Z+IrPni0A8IIyeoQgtJ62pPCApK/Fu1We5IZN" +
//            "UUDY5/M+bvxMRoIhY+z9J07wO/higEsiIlXjETOabY+32xj5MyAmV7QoxkxMn3EsS34uzPbiny5itoK9OxLLisC" +
//            "34uwByL9t10oZggdwnTWwglVa9z//oTVSQr+XONJy31CVRtsCU6CoSOxX44sMEiVZPbtWhzHzx+nH5qLH14JbUW" +
//            "xCyFTbA4utVjOdDVvwXtwFseEc48QkzUNgNOxeXPRc/osA0W/I4j8tngJ+fscP/QyPnnp6wIDAQAB";

}

