package com.piccollage.collagemaker.picphotomaker.ui.settingactivity;

import android.content.Context;
import android.view.View;
import android.view.Window;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogPolicyBinding;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.util.Objects;

/**
 * Dialog policy của app
 */
public class DialogPolicy extends BaseDialog {
    private DialogPolicyBinding binding;

    public DialogPolicy(@NonNull Context context) {
        super(context);
    }

    @Override
    public void beforeSetContentView() {
        binding = DialogPolicyBinding.inflate(getLayoutInflater());
    }

    @Override
    public void initView() {
        binding.wvPolicy.loadUrl("https://avnsoftware.org/private-policy/"); // web view load url
        binding.wvPolicy.setWebViewClient(new WebViewClient());
        onViewClicked();
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.85);
                int height = (int) (WidthHeightScreen.getScreenHeightInPixels(Objects.requireNonNull(getContext())) * 0.85);
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> dismiss());
    }
}
