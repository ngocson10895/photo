package com.piccollage.collagemaker.picphotomaker.stickerUtils;

/**
 * Flip both direction ( Not use )
 */
public class FlipBothDirectionsEvent extends AbstractFlipEvent {

    @Override
    @StickerView.Flip
    protected int getFlipDirection() {
        return StickerView.FLIP_VERTICALLY | StickerView.FLIP_HORIZONTALLY;
    }
}
