package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.CollageTypeAdapter;
import com.piccollage.collagemaker.picphotomaker.adapter.PipTypeAdapter;
import com.piccollage.collagemaker.picphotomaker.base.BaseView;
import com.piccollage.collagemaker.picphotomaker.databinding.ViewTypeBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Frame;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

import java.util.ArrayList;

import dauroi.photoeditor.utils.NetworkUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: View show các type cho feature collage, pip
 * Des: chứa các thuộc tính để thực hiện việc chọn type của user trong feature pip và collage
 */
public class ViewType extends BaseView {
    private ViewTypeBinding binding;
    private ITypeViewCollageListener collageListener;
    private ITypeViewPipListener pipListener;
    private CollageTypeAdapter collageTypeAdapter;
    private PipTypeAdapter pipTypeAdapter;
    private ArrayList<Frame> listFrame;
    private int curPos = 0, oldPos = 0; // hai biến sử dụng để lưu trạng thái
    private boolean isBuild;
    private String type;

    // Listener cho Collage
    public interface ITypeViewCollageListener extends IBaseViewListener {
        void previewAction(String nameType, int pos);
    }

    // Listener cho PIP
    public interface ITypeViewPipListener extends IBaseViewListener {
        void previewAction(PipPicture pipPicture);

        void download(PipPicture pipPicture);
    }

    public void setBuild(boolean build) {
        isBuild = build;
    }

    public void setCollageListener(ITypeViewCollageListener collageListener) {
        this.collageListener = collageListener;
    }

    public void setPipListener(ITypeViewPipListener pipListener) {
        this.pipListener = pipListener;
    }

    public ViewType(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Retrieve attributes from xml
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewType);
        try {
            type = typedArray.getString(R.styleable.ViewType_type); // phải khai báo type trong view để nhận diện feature
        } finally {
            typedArray.recycle();
        }
        initData();
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewType.NAME);
    }

    @Override
    public void initData() {
        if (type.equals("Collage")) {
            listFrame = new ArrayList<>();
            collageTypeAdapter = new CollageTypeAdapter(listFrame, getContext());
            binding.rvType.setAdapter(collageTypeAdapter);
            collageTypeAdapter.setOnItemClickListener((adapter, view, position) -> {
                if (!isBuild) {
                    if (curPos != position) {
                        isBuild = true;
                        curPos = position;
                        reloadDataCollage(position);
                        collageListener.previewAction("frame/" + collageTypeAdapter.getData().get(position).getPath(), position);
                    }
                }
            });
        }
        if (type.equals("PIP")) {
            pipTypeAdapter = new PipTypeAdapter(new ArrayList<>(), getContext());
            binding.rvType.setAdapter(pipTypeAdapter);
            pipTypeAdapter.setOnItemClickListener((adapter, view, position) -> {
                if (!isBuild) {
                    if (curPos != position) {
                        isBuild = true;
                        curPos = position;
                        reloadDataPip(position);
                        if (pipTypeAdapter.getData().get(position).getPathPreview().contains("file:///android_asset/pip/")) {
                            pipListener.previewAction(pipTypeAdapter.getData().get(position)); // item mặc định
                        } else {
                            if (pipTypeAdapter.getData().get(position).isDownloaded()) {
                                pipListener.previewAction(pipTypeAdapter.getData().get(position)); // item đã download
                            } else {
                                if (NetworkUtils.checkNetworkAvailable(getContext())) {
                                    pipListener.download(pipTypeAdapter.getData().get(position)); // download item
                                } else {
                                    Toasty.error(getContext(), getContext().getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show(); // không có mạng
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    // cập nhật lại trạng thái khi user pick item collage
    private void reloadDataCollage(int pos) {
        for (Frame f : collageTypeAdapter.getData()) {
            f.setPick(false);
        }
        collageTypeAdapter.getData().get(pos).setPick(true);
        collageTypeAdapter.notifyDataSetChanged();
    }

    // cập nhật lại trạng thái khi user pick item pip
    private void reloadDataPip(int pos) {
        for (PipPicture pp : pipTypeAdapter.getData()) {
            pp.setPick(false);
        }
        pipTypeAdapter.getData().get(pos).setPick(true);
        pipTypeAdapter.notifyDataSetChanged();
    }

    @Override
    protected void initView() {
        binding = ViewTypeBinding.bind(this);
        binding.rvType.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvType.addItemDecoration(new BetweenSpacesItemDecoration(0, 8));
    }

    @Override
    public int getLayoutID() {
        return R.layout.view_type;
    }

    @Override
    public void buttonClick() {
        onDoneViewClicked();
        onHideViewClicked();
    }

    // Khi user cancel action hoặc back pressed thì sử dụng hàm này
    @Override
    public void backAction() {
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewType.CANCEL);
        if (curPos != oldPos && type.equals("Collage")) {
            curPos = oldPos;
            reloadDataCollage(curPos);
            binding.rvType.scrollToPosition(curPos);
            collageListener.previewAction("frame/" + collageTypeAdapter.getData().get(curPos).getPath(), curPos);
        }
        if (collageListener != null) collageListener.backAction();
        if (curPos != oldPos && type.equals("PIP")) {
            curPos = oldPos;
            reloadDataPip(curPos);
            binding.rvType.scrollToPosition(curPos);
            pipListener.previewAction(pipTypeAdapter.getData().get(curPos));
        }
        if (pipListener != null) pipListener.backAction();
        hide();
    }

    // khi user done action
    private void onDoneViewClicked() {
        binding.ivDoneView.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewType.DONE);
            oldPos = curPos;
            binding.rvType.scrollToPosition(oldPos);
            if (type.equals("Collage")) {
                collageListener.doneAction();
            }
            if (type.equals("PIP")) {
                pipListener.doneAction();
            }
            hide();
        });
    }

    private void onHideViewClicked() {
        binding.ivHideView.setOnClickListener(v -> backAction());
    }

    // Set data for collage feature
    public void setDataCollage(int numberOfPhoto) {
        binding.typeName.setText(getContext().getString(R.string.collage_style));
        for (String s : FileUtil.listAssetFiles("frame", getContext())) {
            int type = Integer.parseInt(s.split("_")[0]);
            if (numberOfPhoto == type) {
                listFrame.add(new Frame(s, type));
            }
        }
        listFrame.get(0).setPick(true);
        collageTypeAdapter.setNewData(listFrame);
        collageListener.previewAction("frame/" + collageTypeAdapter.getData().get(0).getPath(), 0);
    }

    // Set data for pip feature
    public void setDataPip(ArrayList<PipPicture> pipPictures) {
        binding.typeName.setText(getContext().getString(R.string.pip_style));
        pipTypeAdapter.setNewData(pipPictures);
    }

    // đặt trạng thái cho item đầu tiên khi user vào màn hình pip
    public void firstPick(PipPicture pipPicture) {
        for (PipPicture pp : pipTypeAdapter.getData()) {
            if (pp.getId() == pipPicture.getId()) {
                pp.setPick(true);
                binding.rvType.scrollToPosition(pipTypeAdapter.getData().indexOf(pp));
                curPos = oldPos = pipTypeAdapter.getData().indexOf(pp); // nếu item đầu tiên user pick # 0 thì phải set cho 2 vị trí = vị trí của item
            }
        }
        pipTypeAdapter.notifyDataSetChanged();
    }

    // Sau khi user download thành công item pip thì phải cập nhật lại trạng thái cho item đó
    public void updateItemDownload(PipPicture pipPicture) {
        for (PipPicture pp : pipTypeAdapter.getData()) {
            if (pp.getId() == pipPicture.getId()) pp.setDownloaded(true);
        }
        pipTypeAdapter.notifyDataSetChanged();
    }
}
