package com.piccollage.collagemaker.picphotomaker.data.item;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Data Item of Shop
 * Các tên field cần giống với các field trên file json giả về. Nếu muốn thay đổi thì phải có @SerializedName
 */
public class DataItem {
    private String eventName;
    private int eventId;
    private String urlThumb;
    private String isPro;
    private String urlZip;
    @SerializedName("content")
    private List<DataSpecShop> dataSpecShops;

    public String getEventName() {
        return eventName;
    }

    public int getEventId() {
        return eventId;
    }

    public String getUrlThumb() {
        return urlThumb;
    }

    public String getIsPro() {
        return isPro;
    }

    public String getUrlZip() {
        return urlZip;
    }

    public List<DataSpecShop> getDataSpecShops() {
        return dataSpecShops;
    }
}
