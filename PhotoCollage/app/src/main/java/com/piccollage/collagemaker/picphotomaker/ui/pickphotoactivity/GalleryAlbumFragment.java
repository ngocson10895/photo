package com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.GalleryAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseFragment;
import com.piccollage.collagemaker.picphotomaker.databinding.FragmentGalleryBinding;
import com.piccollage.collagemaker.picphotomaker.entity.GalleryAlbum;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Create from Administrator
 * Purpose: Show list gallery từ device của user
 * Des: Show list gallery từ device
 */
public class GalleryAlbumFragment extends BaseFragment {
    private static final String TAG = "GalleryAlbumFragment";
    private static final String TYPE = "type";
    private static final String PHOTO_PICKED = "photo_picked";

    private List<Object> galleryAlbums;
    private GalleryAdapter galleryAdapter;
    private DialogLoading dialogLoading;
    private FragmentGalleryBinding binding;

    public static GalleryAlbumFragment newInstance(int type, List<Photo> photosPicked) {
        GalleryAlbumFragment fragment = new GalleryAlbumFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE, type);
        args.putParcelableArrayList(PHOTO_PICKED, (ArrayList<? extends Parcelable>) photosPicked);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentGalleryBinding.inflate(getLayoutInflater());

        initView();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
    }

    private void initData() {
        if (getContext() != null) {
            dialogLoading = new DialogLoading(getContext(), DialogLoading.CREATE);
        }
        new LoadAlbumAsync(this).execute();
        setListenerAlbum(new AlbumListener() {
            @Override
            public void onStart() {
                dialogLoading.show();
            }

            @Override
            public void onFinish(List<GalleryAlbum> ab, long id) {
                if (ab != null && !ab.isEmpty()) {
                    GalleryAlbum myCreative;
                    for (GalleryAlbum album : ab) {
                        if (album.getIdAlbum() == id) {
                            myCreative = album;
                            ab.remove(album);
                            ab.add(0, myCreative);
                            break;
                        }
                    }
                    galleryAlbums.addAll(ab);
                    // create all album gallery và put nó lên đầu tiên
                    GalleryAlbum galleryAll = new GalleryAlbum("All Image", "", 0L);
                    ArrayList<Photo> allImages = new ArrayList<>();
                    for (GalleryAlbum album : ab) {
                        allImages.addAll(album.getListImage());
                    }
                    galleryAll.setListImage(allImages);
                    galleryAlbums.add(0, galleryAll);
                    insertAdsInMenuItems();
                    galleryAdapter.setItems(galleryAlbums);
                }
                dialogLoading.dismiss();
            }
        });
    }

    private void insertAdsInMenuItems() {
        if (galleryAlbums.size() <= 0) {
            return;
        }

        int offset = 4;
        int index = 4;
        if (NativeAdAdapter.getNativeAds() != null) {
            for (UnifiedNativeAd ad : NativeAdAdapter.getNativeAds()) {
                if (index <= galleryAlbums.size() - offset) {
                    galleryAlbums.add(index, ad);
                    index = index + offset;
                }
            }
        }
    }

    private void initView() {
        if (getContext() != null) {
            binding.adsLayoutG.post(() -> Advertisement.showBannerAdOther(getContext(), binding.adsLayoutG));

            galleryAlbums = new ArrayList<>();
            galleryAdapter = new GalleryAdapter(getContext(), galleryAlbums);
            binding.rvGallery.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            binding.rvGallery.setAdapter(galleryAdapter);
            galleryAdapter.setiGalleryContract(position -> {
                if (galleryAdapter.getItems().get(position) instanceof GalleryAlbum) {
                    GalleryAlbum galleryAlbum = (GalleryAlbum) galleryAlbums.get(position);
                    if (getArguments() != null) {
                        fragmentReplaces(DetailGalleryFragment.newInstance((ArrayList<Photo>) galleryAlbum.getListImage(),
                                galleryAlbum.getAlbumName(), getArguments().getInt(TYPE),
                                getArguments().getParcelableArrayList(PHOTO_PICKED)), TAG);
                    }
                }
            });
            onViewClicked();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> {
            if (getActivity() != null) {
                MyTrackingFireBase.trackingScreen(getActivity(), TrackingUtils.TrackingConstant.PickPhoto.BACK);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
    }
}
