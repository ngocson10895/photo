package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class GradientViewHolder extends GroupViewHolder {
    public ImageView imageView, ivBack;
    public TextView tvTitle;
    private Context context;
    public Drawable firstImage;

    public GradientViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        imageView = itemView.findViewById(R.id.iv_gradient);
        tvTitle = itemView.findViewById(R.id.tvName);
        ivBack = itemView.findViewById(R.id.ivBack);
    }

    public void setTheme(ExpandableGroup gradients) {
        if (gradients instanceof Gradients) {
            if (gradients.getTitle().equals(Constant.MORE)) {
                Glide.with(context).load(R.drawable.ic_itemnone).centerCrop().into(imageView);
            } else {
                Gradient gradient = (Gradient) gradients.getItems().get(0);
                firstImage = new GradientDrawable(GradientDrawable.Orientation.TL_BR,
                        new int[]{Color.parseColor(gradient.getStColor()), Color.parseColor(gradient.getEndColor())});
                imageView.setImageDrawable(firstImage);
            }
            tvTitle.setText(gradients.getTitle());
        }
    }

    @Override
    public void expand() {
        animateExpand();
    }

    @Override
    public void collapse() {
        animateCollapse();
    }

    private void animateExpand() {
        RotateAnimation rotate =
                new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
    }

    private void animateCollapse() {
        RotateAnimation rotate =
                new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(300);
        rotate.setFillAfter(true);
    }
}
