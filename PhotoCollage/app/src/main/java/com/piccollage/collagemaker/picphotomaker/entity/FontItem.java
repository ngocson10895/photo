package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

/**
 * Create from Administrator
 * Purpose: chứa info item font của theme
 * Des: các item font của theme
 */
public class FontItem implements Parcelable {
    private String mFontName;
    private String mFontPath;
    private String mFontTheme;
    private boolean isPick;

    public FontItem(String fontName, String fontPath, boolean isPick, String mFontTheme) {
        mFontName = fontName;
        mFontPath = fontPath;
        this.isPick = isPick;
        this.mFontTheme = mFontTheme;
    }

    protected FontItem(Parcel in) {
        mFontName = in.readString();
        mFontPath = in.readString();
        mFontTheme = in.readString();
        isPick = in.readByte() != 0;
    }

    public static final Creator<FontItem> CREATOR = new Creator<FontItem>() {
        @Override
        public FontItem createFromParcel(Parcel in) {
            return new FontItem(in);
        }

        @Override
        public FontItem[] newArray(int size) {
            return new FontItem[size];
        }
    };

    public String getmFontTheme() {
        return mFontTheme;
    }

    public void setmFontPath(String mFontPath) {
        this.mFontPath = mFontPath;
    }

    public String getFontName() {
        return mFontName;
    }

    public String getFontPath() {
        return mFontPath;
    }

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    @NonNull
    @Override
    public String toString() {
        return mFontName + " " + mFontPath + " " + mFontTheme;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFontName);
        dest.writeString(mFontPath);
        dest.writeString(mFontTheme);
        dest.writeByte((byte) (isPick ? 1 : 0));
    }
}
