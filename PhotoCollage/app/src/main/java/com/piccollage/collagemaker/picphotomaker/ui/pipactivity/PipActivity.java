package com.piccollage.collagemaker.picphotomaker.ui.pipactivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.ads.AdSize;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.IconsAdapter;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityPipPreviewBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.entity.TextOfUser;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventItemShopSticker;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventPipApi;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.DrawableSticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.Sticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.StickerView;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.TextSticker;
import com.piccollage.collagemaker.picphotomaker.ui.DialogExitFeature;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.PreviewActivity;
import com.piccollage.collagemaker.picphotomaker.ui.morefeature.AddTextFragment;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.ChangeImageActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.layout.TemplateLayout;
import com.piccollage.collagemaker.picphotomaker.ui.pipactivity.newVer.PipStyleActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;
import com.piccollage.collagemaker.picphotomaker.utils.view.AddStickerView;
import com.piccollage.collagemaker.picphotomaker.utils.view.BackgroundView;
import com.piccollage.collagemaker.picphotomaker.utils.view.MoreFeatureView;
import com.piccollage.collagemaker.picphotomaker.utils.view.ViewType;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradients;
import com.piccollage.collagemaker.picphotomaker.viewholder.Pattern;
import com.piccollage.collagemaker.picphotomaker.viewholder.Patterns;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dauroi.photoeditor.utils.DateTimeUtils;
import dauroi.photoeditor.utils.PhotoUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: màn hình chính để sử dụng tính năng pip
 * Des: user thao tác với màn hình này để tạo ảnh pip
 */
public class PipActivity extends BaseAppActivity implements TemplateLayout.IClickImage, AddTextFragment.ISendTextView {
    private static final String TAG = "PipActivity";
    public static final String PHOTO = "photo";
    public static final String CHANGE_BG = "change_bg";
    public static final int REQUEST_CODE_CHANGE_IMAGE = 997;
    public static final int REQUEST_CODE_CHANGE_BACKGROUND_IMAGE = 996;

    private ActivityPipPreviewBinding binding;
    private List<Photo> photosPicked;
    private IconsAdapter adapterMainFeature;
    private ArrayList<Icon> iconFeature;
    private int index;
    private TemplateLayout templateLayout;
    private DialogLoading dialog;
    private DialogExitFeature dialogExitFeature;
    private Bitmap bmBackground;
    private SharedPrefsImpl sharedPrefs;
    private Photo photoBg;
    private PipPicture pipPicture;
    private ApplicationViewModel applicationViewModel;
    private boolean isPreview;
    private int countFlip = 0;
    private ArrayList<StickerTheme> stickerThemes;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityPipPreviewBinding.inflate(getLayoutInflater());
    }

    @Override
    public void onHasNetwork() {
        loadAds();
    }

    public void initView() {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Pip.NAME);
        dialogExitFeature = new DialogExitFeature(this, PipActivity.this::discard);
        dialog = new DialogLoading(this, DialogLoading.CREATE);
        sharedPrefs = new SharedPrefsImpl(this, Constant.NAME_SHARE);
        iconFeature = new ArrayList<>();
        adapterMainFeature = new IconsAdapter(iconFeature, this, this::pickFeature, "");
        binding.rvMainFeature.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.rvMainFeature.setAdapter(adapterMainFeature);

        bmBackground = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.WHITE));
        binding.background.setImageBitmap(bmBackground);

        binding.typeView.setPipListener(new ViewType.ITypeViewPipListener() {
            @Override
            public void previewAction(PipPicture pipPicture) {
                new BuildLayout(photosPicked, PipActivity.this, true, 0, pipPicture).execute();
            }

            @Override
            public void download(PipPicture pipPicture) {
                downloadPIP(pipPicture);
            }

            @Override
            public void doneAction() {
                binding.tbPIPPre.setVisibility(View.VISIBLE);
            }

            @Override
            public void backAction() {
                binding.tbPIPPre.setVisibility(View.VISIBLE);
            }
        });

        binding.backgroundView.setListener(new BackgroundView.IBackgroundListener() {
            @Override
            public void previewBackground(int color) {
                binding.background.setImageBitmap(ImageDecoder.drawableToBitmap(new ColorDrawable(color)));
            }

            @Override
            public void previewPattern(String path) {
                if (path.contains("storage")) {
                    Glide.with(PipActivity.this).load(path).centerCrop().error(R.drawable.sticker_default).into(binding.background);
                } else
                    Glide.with(PipActivity.this).load(Uri.parse("file:///android_asset/background/" + path)).centerCrop().error(R.drawable.sticker_default).into(binding.background);
            }

            @Override
            public void previewGradient(String stColor, String endColor) {
                binding.background.setImageDrawable(new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{Color.parseColor(stColor), Color.parseColor(endColor)}));
            }

            @Override
            public void previewPalette(String c1, String c2, String c3, String c4, String c5) {

            }

            @Override
            public void jumpToShop(String type) {
                Intent intent = new Intent(PipActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, type);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction(Bitmap bm) {
                bmBackground = bm;
                binding.background.setImageBitmap(bm);
                binding.tbPIPPre.setVisibility(View.VISIBLE);
            }

            @Override
            public void closeAction(Bitmap bm) {
                bmBackground = bm;
                binding.background.setImageBitmap(bm);
                binding.tbPIPPre.setVisibility(View.VISIBLE);
            }
        });

        binding.addStickerView.setListener(new AddStickerView.IStickerViewListener() {
            @Override
            public void previewAction(StickerItem stickerItem) {
                add(stickerItem);
            }

            @Override
            public void jumpToShop() {
                Intent intent = new Intent(PipActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, Constant.STICKER);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction() {
                isPreview = false;
                binding.tbPIPPre.setVisibility(View.VISIBLE);
            }

            @Override
            public void backAction() {
                isPreview = false;
                binding.tbPIPPre.setVisibility(View.VISIBLE);
            }
        });

        binding.editView.setListener(new MoreFeatureView.IMoreFeatureListener() {
            @Override
            public void changeImage() {
                Intent intent = new Intent(PipActivity.this, ChangeImageActivity.class);
                intent.putExtra(PHOTO, photosPicked.get(index));
                intent.putExtra(PipActivity.CHANGE_BG, "");
                startActivityForResult(intent, REQUEST_CODE_CHANGE_IMAGE);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void flipImage() {
                countFlip++;
                if (countFlip % 2 == 0) {
                    templateLayout.setScaleX(1);
                } else if (countFlip % 2 == 1) {
                    templateLayout.setScaleX(-1);
                }
            }

            @Override
            public void doneAction() {
                binding.tbPIPPre.setVisibility(View.VISIBLE);
                binding.rvMainFeature.setVisibility(View.VISIBLE);
                templateLayout.hideHiddenLayout();
                templateLayout.setNotIdle(true);
            }

            @Override
            public void backAction() {
                binding.tbPIPPre.setVisibility(View.VISIBLE);
                binding.rvMainFeature.setVisibility(View.VISIBLE);
                templateLayout.hideHiddenLayout();
                templateLayout.setNotIdle(true);
            }
        });
    }

    // chứa các sự kiện click của button
    @Override
    public void buttonClick() {
        onIvSaveShareClicked();
        onBtnCloseClicked();
    }

    // load quảng cáo
    private void loadAds() {
        binding.adsLayout.post(() -> Advertisement.showBannerAdHome(this, binding.adsLayout, AdSize.BANNER,
                new BannerAdsUtils.BannerAdListener() {
                    @Override
                    public void onAdLoaded() {

                    }

                    @Override
                    public void onAdFailed() {

                    }
                }, () -> {

                }));
        Advertisement.loadBannerAdExit(this);
        Advertisement.loadInterstitialAdInApp(this);
    }

    // user thực hiện chọn 1 trong các chức năng chính
    private void pickFeature(int i) {
        switch (i) {
            case 0:
                if (checkDoubleClick()) {
                    binding.tbPIPPre.setVisibility(View.GONE);
                    binding.typeView.show();
                }
                break;
            case 1:
                if (checkDoubleClick()) {
                    binding.tbPIPPre.setVisibility(View.GONE);
                    binding.backgroundView.show();
                }
                break;
            case 2:
                if (checkDoubleClick()) {
                    binding.tbPIPPre.setVisibility(View.GONE);
                    binding.addStickerView.show();
                }
                break;
            case 3:
                if (checkDoubleClick()) {
                    AddTextFragment addTextFragment = new AddTextFragment();
                    binding.rvMainFeature.setVisibility(View.GONE);
                    binding.tbPIPPre.setVisibility(View.GONE);
                    addTextFragment.show(getSupportFragmentManager(), addTextFragment.getTag());
                }
                break;
            case 4:
                if (checkDoubleClick()) {
                    Intent intent = new Intent(this, ChangeImageActivity.class);
                    intent.putExtra(PHOTO, photoBg);
                    intent.putExtra(CHANGE_BG, CHANGE_BG);
                    startActivityForResult(intent, REQUEST_CODE_CHANGE_BACKGROUND_IMAGE);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
                break;
        }
    }

    // 18/03/2020: download pip when user pick style pip
    private void downloadPIP(PipPicture pipPicture) {
        DialogLoading dialogLoadingDownload = new DialogLoading(this, DialogLoading.PREPARING);
        File parentFile = FileUtil.getParentFile(this, "PIPStyleDownloaded");
        if (!parentFile.exists()) {
            if (parentFile.mkdirs()) {
                Log.d(TAG, "pickStyle: create folder");
            }
        }
        DownloadTask task = new DownloadTask.Builder(pipPicture.getLinkToDownload(), parentFile).setFilename(String.valueOf(pipPicture.getId()))
                .setMinIntervalMillisCallbackProcess(DownloadTask.Builder.DEFAULT_MIN_INTERVAL_MILLIS_CALLBACK_PROCESS)
                .setPassIfAlreadyCompleted(DownloadTask.Builder.DEFAULT_PASS_IF_ALREADY_COMPLETED).build();
        task.enqueue(new DownloadListener4WithSpeed() {
            @Override
            public void taskStart(@NonNull DownloadTask task) {
                dialogLoadingDownload.show();
            }

            @Override
            public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {

            }

            @Override
            public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {

            }

            @Override
            public void infoReady(@NonNull DownloadTask task, @NonNull BreakpointInfo info, boolean fromBreakpoint, @NonNull Listener4SpeedAssistExtend.Listener4SpeedModel model) {

            }

            @Override
            public void progressBlock(@NonNull DownloadTask task, int blockIndex, long currentBlockOffset, @NonNull SpeedCalculator blockSpeed) {

            }

            @Override
            public void progress(@NonNull DownloadTask task, long currentOffset, @NonNull SpeedCalculator taskSpeed) {

            }

            @Override
            public void blockEnd(@NonNull DownloadTask task, int blockIndex, BlockInfo info, @NonNull SpeedCalculator blockSpeed) {

            }

            @Override
            public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause, @NonNull SpeedCalculator taskSpeed) {
                if (cause == EndCause.COMPLETED) {
                    String childParentFile = parentFile.getAbsolutePath() + "/" + pipPicture.getId();
                    File childFile = new File(childParentFile + "unzip");

                    try {
                        InputStream inputStream = new FileInputStream(childParentFile);
                        if (!childFile.exists()) {
                            if (childFile.mkdirs()) {
                                Log.d(TAG, "taskEnd: ");
                            }
                        }
                        FileUtil.unzip(inputStream, childFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (parentFile.isDirectory()) {
                        if (parentFile.listFiles() != null) {
                            for (File child : parentFile.listFiles()) {
                                if (!child.isDirectory()) {
                                    if (child.delete()) {
                                        Log.d(TAG, "taskEnd: delete loaded");
                                    }
                                }
                            }
                        }
                    }
                    pipPicture.setPathPreviewOnline(childFile.getAbsolutePath());
                    if (childFile.listFiles() != null) {
                        for (File f : childFile.listFiles()) {
                            if (!f.isDirectory()) {
                                if (f.getName().contains("preview"))
                                    pipPicture.setPathPreview(f.getAbsolutePath());
                                else if (f.getName().contains("fg"))
                                    pipPicture.setForeGround(f.getAbsolutePath());
                                else {
                                    pipPicture.getMasks().add(f.getAbsolutePath());
                                }
                            }
                        }
                    }
                    pipPicture.setDownloaded(true);
                    binding.typeView.updateItemDownload(pipPicture);
                    applicationViewModel.insertPip(pipPicture);
                    dialogLoadingDownload.dismiss();
                    new BuildLayout(photosPicked, PipActivity.this, true, 0, pipPicture).execute();
                } else if (cause == EndCause.ERROR) {
                    dialogLoadingDownload.dismiss();
                    Toasty.error(PipActivity.this, getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
                } else {
                    dialogLoadingDownload.dismiss();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().removeStickyEvent(EventPipApi.class);
        EventBus.getDefault().unregister(this);
    }

    // 16/03/2020: get item from api
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getPIPApi(EventPipApi eventPipApi) {
        binding.typeView.setDataPip(eventPipApi.getPipThemes().get(photosPicked.size() - 1).getPipPictures());
        binding.typeView.firstPick(pipPicture);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getStickerData(EventItemShopSticker eventItemShopSticker) {
        stickerThemes.clear();
        stickerThemes.addAll(eventItemShopSticker.getStickerThemes());
    }

    public void initData() {
        applicationViewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        photosPicked = new ArrayList<>();

        InitData.addFeatureIconPIP(iconFeature);
        adapterMainFeature.setIconArrayList(iconFeature);

        if (getIntent() != null) {
            pipPicture = getIntent().getParcelableExtra(PickPhotoActivity.PIP_PICTURE);
            ArrayList<String> paths = getIntent().getStringArrayListExtra(PickPhotoActivity.PHOTOS_PICKED);
            for (String path : paths) {
                photosPicked.add(new Photo(path, 0));
            }
        } else photosPicked.add(new Photo("", 0));
        photoBg = photosPicked.get(0);

        if (sharedPrefs.get("acbqlkjn123", Boolean.class)) {
            // Container Layout will listen changes of view child
            binding.containerLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    new BuildLayout(photosPicked, PipActivity.this, true, 0, pipPicture).execute();
                    binding.containerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        } else {
            dialog.dismiss();
        }

        stickerThemes = new ArrayList<>();
        applicationViewModel.getDataStickers().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                if (!itemsDownloaded.isEmpty()) {
                    for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                        for (StickerTheme stickerTheme : stickerThemes) {
                            if (itemDownloaded.getName().equals(stickerTheme.getNameTheme())) {
                                stickerTheme.setDownloaded(true);
                            }
                        }
                    }
                } else {
                    for (StickerTheme stickerTheme : stickerThemes) {
                        stickerTheme.setDownloaded(false);
                    }
                }
                binding.addStickerView.setMoreSticker(stickerThemes);
            }
        });

        applicationViewModel.getDataGradients().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Gradients> gradients = new ArrayList<>();
                gradients.add(new Gradients(Constant.DEFAULT, InitData.addGradients()));
                for (ItemDownloaded item : itemsDownloaded) {
                    List<Gradient> list = new ArrayList<>();
                    String[] code = item.getPath().split("_");
                    String[] nameItem = item.getNameItem().split("_");
                    for (int i = 0; i < code.length; i += 2) {
                        if (code[i].length() == 6) {
                            Gradient gra = new Gradient("#" + code[i], "#" + code[i + 1], false, item.getName());
                            list.add(gra);
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    gradients.add(new Gradients(item.getName(), list));
                }
                gradients.add(0, new Gradients(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataGradientsEx(gradients);
            }
        });

        ArrayList<Pattern> pattern = new ArrayList<>();
        for (String p : FileUtil.listAssetFiles("background", this)) {
            pattern.add(new Pattern(p, String.valueOf(FileUtil.listAssetFiles("background", this).indexOf(p)), Constant.DEFAULT, false));
        }
        applicationViewModel.getDataPatterns().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Patterns> patterns = new ArrayList<>();
                patterns.add(new Patterns(Constant.DEFAULT, pattern));
                for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                    ArrayList<String> paths = new ArrayList<>();
                    List<Pattern> list = new ArrayList<>();
                    FileUtil.getAllFiles(new File(itemDownloaded.getPath()), paths);
                    for (String path : paths) {
                        list.add(new Pattern(path, "", itemDownloaded.getName(), false));
                    }
                    String[] nameItem = itemDownloaded.getNameItem().split("_");
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    patterns.add(new Patterns(itemDownloaded.getName(), list));
                }
                patterns.add(0, new Patterns(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataPatternsEx(patterns);
            }
        });
    }

    @Override
    public void clickImage(int index, List<Photo> photosPicked) {
        this.index = index;
        binding.tbPIPPre.setVisibility(View.GONE);
        binding.editView.show();
        binding.rvMainFeature.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHANGE_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    photosPicked.remove(photosPicked.get(index));
                    photosPicked.add(index, data.getParcelableExtra(PHOTO));
                    templateLayout.changeImage(index, data.getParcelableExtra(PHOTO));
                }
            }
        }
        if (requestCode == REQUEST_CODE_CHANGE_BACKGROUND_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    photoBg = data.getParcelableExtra(PHOTO);
                    templateLayout.changeBackground(data.getParcelableExtra(PHOTO));
                }
            }
        }
    }

    public void discard() {
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    /**
     * Build Layout Main
     */
    private static class BuildLayout extends AsyncTask<Void, Void, List<Photo>> {
        private List<Photo> photos;
        private WeakReference<PipActivity> weakReference;
        private boolean isNotIdle;
        private int count;
        private PipPicture pipPicture;

        BuildLayout(List<Photo> photos, PipActivity activity, boolean isNotIdle, int count, PipPicture pipPicture) {
            this.photos = photos;
            weakReference = new WeakReference<>(activity);
            this.isNotIdle = isNotIdle;
            this.count = count;
            this.pipPicture = pipPicture;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            PipActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            activity.dialog.show();
        }

        @Override
        protected void onPostExecute(List<Photo> photos) {
            super.onPostExecute(photos);
            PipActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            activity.buildLayoutMain(photos, pipPicture);
            activity.templateLayout.setNotIdle(isNotIdle);
            activity.templateLayout.setCount(count);
            activity.binding.typeView.setBuild(false);
        }

        @Override
        protected List<Photo> doInBackground(Void... voids) {
            PipActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            for (Photo photo : photos) {
                if (pipPicture.getForeGround() != null) {
                    /*
                    set up ảnh mask và ảnh foreground cho từng ảnh của user
                    set up vị trí ảnh của user
                     */
                    if (pipPicture.getForeGround().contains(".png")) {
                        if (pipPicture.getMasks().size() == photos.size()) {
                            photo.maskPath = pipPicture.getMasks().get(photos.indexOf(photo));
                            photo.template = pipPicture.getForeGround();
                            photo.x = Integer.parseInt(pipPicture.getMasks().get(photos.indexOf(photo)).replaceFirst(".png", "").split("_")[3]);
                            photo.y = Integer.parseInt(pipPicture.getMasks().get(photos.indexOf(photo)).replaceFirst(".png", "").split("_")[4]);
                        }
                    } else if (pipPicture.getForeGround().contains(".webp")) {
                        if (pipPicture.getMasks().size() == photos.size()) {
                            photo.maskPath = pipPicture.getMasks().get(photos.indexOf(photo));
                            photo.template = pipPicture.getForeGround();
                            photo.x = Integer.parseInt(pipPicture.getMasks().get(photos.indexOf(photo)).replaceFirst(".webp", "").split("_")[3]);
                            photo.y = Integer.parseInt(pipPicture.getMasks().get(photos.indexOf(photo)).replaceFirst(".webp", "").split("_")[4]);
                        }
                    }
                }
            }
            return new ArrayList<>(photos);
        }
    }

    // Build Frame Main
    private void buildLayoutMain(List<Photo> items, PipPicture changePP) {
        byte[] dc = Base64.decode("Y29tLnBpY2NvbGxhZ2UuY29sbGFnZW1ha2VyLnBpY3Bob3RvbWFrZXI=", 0);
        for (int i = 0; i < dc.length; i++) {
            if (dc[i] != getPackageName().codePointAt(i)) {
                finish();
            }
        }
        Bitmap template;
        if (changePP.getPathPreview().contains("file:///android_asset/pip/")) {
            template = ImageDecoder.getBitmapFromAssets("pip/" + items.get(0).template, this);
        } else
            template = PhotoUtils.decodePNGImage(this, items.get(0).template);

        templateLayout = new TemplateLayout(this, items, template, this);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(binding.containerLayout.getWidth(), binding.containerLayout.getHeight());
        if (changePP.getPathPreview().contains("file:///android_asset/pip/")) {
            templateLayout.buildTemplateLayout(binding.containerLayout.getWidth(), binding.containerLayout.getHeight(), photoBg.getPathPhoto(), "default");
        } else
            templateLayout.buildTemplateLayout(binding.containerLayout.getWidth(), binding.containerLayout.getHeight(), photoBg.getPathPhoto(), "");

        binding.containerLayout.removeAllViews();
        binding.containerLayout.addView(templateLayout, params);
        dialog.dismiss();
    }

    @Override
    public void turnOff() {
        binding.rvMainFeature.setVisibility(View.VISIBLE);
        binding.tbPIPPre.setVisibility(View.VISIBLE);
    }

    // Add text for layout main
    @Override
    public void sendText(TextOfUser textOfUser) {
        binding.stickerView.setLocked(false);
        binding.stickerView.setConstrained(true);
        binding.stickerView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.stickerView.setShowBorder(true);
                binding.stickerView.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                binding.stickerView.swapLayers(binding.stickerView.getStickers().indexOf(sticker), binding.stickerView.getStickerCount() - 1);
                Log.d(TAG, "onStickerClicked: ");
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.stickerView.setShowBorder(false);
                    binding.stickerView.setShowIcons(false);
                } else {
                    binding.stickerView.setShowBorder(true);
                    binding.stickerView.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.stickerView.configDefaultIcons("text");
        TextSticker textSticker = new TextSticker(this, getResources().getDrawable(R.drawable.sticker_transparent_background_hor));
        if (!textOfUser.getContent().isEmpty()) {
            textSticker.setTextOfUser(textOfUser);
            textSticker.resizeText();
            binding.stickerView.addSticker(textSticker);
        }
    }

    public void onBtnCloseClicked() {
        binding.btnClose.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                dialogExitFeature.show();
            }
        });
    }

    public void onIvSaveShareClicked() {
        binding.ivSaveShare.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Pip.DONE);
                save();
            }
        });
    }

    public void save() {
        if (binding.stickerView.isShowBorder() && binding.stickerView.isShowIcons()) {
            binding.stickerView.setShowBorder(false);
            binding.stickerView.setShowIcons(false);
        }
        dialog.show();
        new Handler().postDelayed(() -> {
            Task task = new Task(this);
            task.execute();
        }, 2000);
    }

    /**
     * Task to export image
     */
    private static class Task extends AsyncTask<Void, Void, File> {
        private WeakReference<PipActivity> weakReference;

        Task(PipActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            PipActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            if (new PermissionChecker(activity).isPermissionOK()) {
                PhotoUtils.addImageToGallery(file.getAbsolutePath(), activity);
                Toasty.success(activity, activity.getString(R.string.saving_done), Toasty.LENGTH_SHORT).show();
                Advertisement.showInterstitialAdInApp(activity, new InterstitialAdsPopupOthers.OnAdsListener() {
                    @Override
                    public void onAdsClose() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.PIP);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                    @Override
                    public void close() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.PIP);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                });
                activity.dialog.dismiss();
            }
        }

        @Override
        protected File doInBackground(Void... voids) {
            PipActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            Bitmap bitmap = ImageUtils.getBitmapFromView(activity.binding.clMain);
            String parentFilePath = FileUtil.getParentFileString("My Creative");
            File parentFile = new File(parentFilePath);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "doInBackground: ");
                }
            }
            File photoFile = new File(parentFile, DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png"));
            try {
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(photoFile));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return photoFile;
        }
    }

    // Add sticker for layout main
    public void add(StickerItem item) {
        binding.stickerView.setLocked(false);
        binding.stickerView.setConstrained(true);
        binding.stickerView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerAdded: ");
                binding.stickerView.setShowBorder(true);
                binding.stickerView.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {
                binding.stickerView.swapLayers(binding.stickerView.getStickers().indexOf(sticker), binding.stickerView.getStickerCount() - 1);
                Log.d(TAG, "onStickerClicked: ");
            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDeleted: ");
            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDragFinished: ");
            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerTouchedDown: ");
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.stickerView.setShowBorder(false);
                    binding.stickerView.setShowIcons(false);
                } else {
                    binding.stickerView.setShowBorder(true);
                    binding.stickerView.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerZoomFinished: ");
            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerFlipped: ");
            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                Log.d(TAG, "onStickerDoubleTapped: ");
            }
        });
        binding.stickerView.configDefaultIcons("sticker");
        Glide.with(this).load(item.getUrl()).centerCrop().into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable drawable, @Nullable Transition<? super Drawable> transition) {
                if (isPreview) {
                    binding.stickerView.removeCurrentSticker();
                } else {
                    isPreview = true;
                }
                binding.stickerView.addSticker(new DrawableSticker(drawable));
            }

            @Override
            public void onLoadCleared(@Nullable Drawable drawable) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (binding.typeView.getVisibility() == View.VISIBLE) {
            binding.typeView.backAction();
        } else if (binding.backgroundView.getVisibility() == View.VISIBLE) {
            binding.backgroundView.backAction();
        } else if (binding.addStickerView.getVisibility() == View.VISIBLE) {
            binding.addStickerView.backAction();
        } else if (binding.editView.getVisibility() == View.VISIBLE) {
            binding.editView.backAction();
            templateLayout.hideHiddenLayout();
            templateLayout.setNotIdle(true);
            binding.rvMainFeature.setVisibility(View.VISIBLE);
            binding.tbPIPPre.setVisibility(View.VISIBLE);
        } else {
            dialogExitFeature.show();
        }
    }
}
