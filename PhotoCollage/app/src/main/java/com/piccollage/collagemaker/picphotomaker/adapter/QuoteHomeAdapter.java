package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemQuoteHomeBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Quote;

import java.util.ArrayList;

/**
 * Adapter quote from api
 */
public class QuoteHomeAdapter extends RecyclerView.Adapter<QuoteHomeAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Quote> quotes;
    private IPickQuote iPickQuote;

    public interface IPickQuote {
        void pickQuote(Quote quote);
    }

    public QuoteHomeAdapter(Context context, ArrayList<Quote> quotes, IPickQuote iPickQuote) {
        this.context = context;
        this.quotes = quotes;
        this.iPickQuote = iPickQuote;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_quote_home, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.binding.tvQuote.setText(quotes.get(i).getQuote());
        Glide.with(context).asBitmap().load(quotes.get(i).getUrlThumbs())
                .diskCacheStrategy(DiskCacheStrategy.DATA).into(viewHolder.binding.ivBg);
        viewHolder.itemView.setOnClickListener(v -> iPickQuote.pickQuote(quotes.get(i)));
    }

    @Override
    public int getItemCount() {
        return quotes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemQuoteHomeBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemQuoteHomeBinding.bind(itemView);
        }
    }

    public void setQuotes(ArrayList<Quote> quotes) {
        this.quotes = quotes;
    }
}
