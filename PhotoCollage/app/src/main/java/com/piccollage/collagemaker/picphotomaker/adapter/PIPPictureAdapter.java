package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemPipPictureBinding;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;

import java.util.List;

/**
 * Pip picture adapter
 */
public class PIPPictureAdapter extends BaseQuickAdapter<PipPicture, BaseViewHolder> {
    private Context context;

    public PIPPictureAdapter(@Nullable List<PipPicture> data, Context context) {
        super(R.layout.item_pip_picture, data);
        this.context = context;
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, PipPicture item) {
        ItemPipPictureBinding binding = ItemPipPictureBinding.bind(helper.itemView);
        if (item.getPathPreview().contains("file:///android_asset/")) { // check nếu item từ assets
            binding.iconDownload.setVisibility(View.INVISIBLE);
            Glide.with(context).asBitmap().load(Uri.parse(item.getPathPreview()))
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_imgplaceholder)).into(binding.pipPreview);
        } else { // check item from server
            Glide.with(context).load(item.getPathPreview())
                    .placeholder(context.getResources().getDrawable(R.drawable.ic_imgplaceholder)).into(binding.pipPreview);
            if (item.isDownloaded()) {
                binding.iconDownload.setVisibility(View.INVISIBLE);
            } else binding.iconDownload.setVisibility(View.VISIBLE);
        }
    }
}
