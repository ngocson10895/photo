package com.piccollage.collagemaker.picphotomaker.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class PatternChildViewHolder extends ChildViewHolder {
    public ImageView imageView, ivChose;
    public TextView textView;

    public PatternChildViewHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.ivPattern);
        textView = itemView.findViewById(R.id.tvName);
        ivChose = itemView.findViewById(R.id.ivChose);
    }

    public void setPattern(String url, Context context, String name, String title) {
        if (title.equals(Constant.DEFAULT)) {
            imageView.setImageBitmap(ImageDecoder.getBitmapFromAssets("background/" + url, context));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            Glide.with(context).load(url).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.sticker_default).into(imageView);
        }
        textView.setText(name);
    }
}
