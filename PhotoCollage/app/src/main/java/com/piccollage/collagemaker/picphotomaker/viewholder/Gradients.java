package com.piccollage.collagemaker.picphotomaker.viewholder;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class Gradients extends ExpandableGroup<Gradient> {

    private List<Gradient> items;

    public Gradients(String title, List<Gradient> items) {
        super(title, items);
        this.items = items;
    }

    @Override
    public String getTitle() {
        return super.getTitle();
    }

    @Override
    public List<Gradient> getItems() {
        return items;
    }
}
