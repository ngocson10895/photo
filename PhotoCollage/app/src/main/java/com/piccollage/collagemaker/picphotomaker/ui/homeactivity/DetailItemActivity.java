package com.piccollage.collagemaker.picphotomaker.ui.homeactivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.breakpoint.BlockInfo;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.listener.DownloadListener4WithSpeed;
import com.liulishuo.okdownload.core.listener.assist.Listener4SpeedAssistExtend;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityDetailItemBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.DialogDownload;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: activity show detail item sticker shop
 * Des: thể hiện 1 số item trong file zip để user có thể trông thấy
 */
public class DetailItemActivity extends BaseAppActivity implements DialogDownload.ICancel {
    private static final String TAG = DetailItemActivity.class.getSimpleName();

    private String apiZip, name, nameItems;
    private File parentFile;
    private DownloadTask task;
    private ApplicationViewModel viewModel;
    private DialogDownload dialog;
    private DialogLoading dialogLoading;
    private ActivityDetailItemBinding binding;
    private BottomUsing bottomUsing;

    @Override
    public void initView() {
        Advertisement.loadBannerAdExit(this);
        Advertisement.showBannerAdOther(this, binding.adsLayout);

        dialog = new DialogDownload(this, this);

        viewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        dialogLoading = new DialogLoading(this, DialogLoading.CREATE);
        binding.animation.setAnimation(R.raw.loading);
        if (MyUtils.isAppPurchased()) {
            binding.icon.setVisibility(View.INVISIBLE);
            binding.tvUpToProVer.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Advertisement.loadInterstitialAdInApp(this);
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityDetailItemBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    @Override
    public void buttonClick() {
        onDownloadClicked();
        onUpToProClicked();
        onViewClicked();
    }

    @SuppressLint("SetTextI18n")
    public void initData() {
        if (getIntent() != null) {
            if (getIntent().getIntExtra("Downloaded", 0) == 1) {
                binding.btnUseNow.setText(getString(R.string.use_now));
                binding.btnUseNow.setBackground(getResources().getDrawable(R.drawable.btn_download_done));
            } else if (getIntent().getIntExtra("Downloaded", 0) == 2) {
                if (MyUtils.isAppPurchased()) {
                    binding.btnUseNow.setText(R.string.download_it_now);
                } else binding.btnUseNow.setText(R.string.up_to_pro_version_to_download_it);
            } else {
                if (!MyUtils.isAppPurchased())
                    binding.btnUseNow.setText(getString(R.string.watch_ads_to_download_free));
                else binding.btnUseNow.setText(R.string.download_it_now);
            }
            name = getIntent().getStringExtra("Name");
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItemShop.NAME + name);
            if (getIntent().getStringExtra("Name") != null) {
                binding.tvTitle.setText(getIntent().getStringExtra("Name").split("_")[0]);
            }
            if (getIntent().getStringExtra("Name").split("_")[1] != null) {
                binding.tvInfo.setText(getString(R.string.includes) + " " + getIntent().getStringExtra("Name").split("_")[1] + " " + getString(R.string.items));
            }
            Glide.with(this).load(getIntent().getStringExtra("Thumb")).error(R.drawable.img_default).into(binding.ivThumb);
            Glide.with(this).load(getIntent().getStringExtra("Preview")).error(R.drawable.img_default).addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    Log.d(TAG, "onLoadFailed: " + e);
                    binding.animation.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    binding.animation.setVisibility(View.GONE);
                    return false;
                }
            }).into(binding.ivDetail);
            apiZip = getIntent().getStringExtra("Zip");
            nameItems = getIntent().getStringExtra("NameItems");
        }
    }

    public void onViewClicked() {
        binding.ivBack.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItemShop.BACK);
                finish();
            }
        });
    }

    public void onDownloadClicked() {
        binding.btnUseNow.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                if (binding.btnUseNow.getText().equals(getString(R.string.use_now))) {
                    if (new PermissionChecker(this).isPermissionOK()) {
                        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItemShop.ACTION_USE);
                        BottomUsing bottomUsing = new BottomUsing();
                        bottomUsing.show(getSupportFragmentManager(), "BottomUsing");
                    }
                } else if (binding.btnUseNow.getText().equals(getString(R.string.up_to_pro_version_to_download_it))) {
                    Intent intent = new Intent(this, BuySubActivity.class);
                    intent.putExtra(PickPhotoActivity.MODE, Constant.COLLAGE);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else if (binding.btnUseNow.getText().equals(getString(R.string.download_it_now))) {
                    downloadItem();
                } else {
                    Advertisement.showInterstitialAdInApp(this, new InterstitialAdsPopupOthers.OnAdsListener() {
                        @Override
                        public void onAdsClose() {
                            downloadItem();
                            dialogLoading.dismiss();
                        }

                        @Override
                        public void showLoading() {
                            if (dialogLoading != null) dialogLoading.show();
                        }

                        @Override
                        public void hideLoading() {
                            if (dialogLoading != null) dialogLoading.dismiss();
                        }
                    }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                        @Override
                        public void close() {
                            downloadItem();
                            dialogLoading.dismiss();
                        }

                        @Override
                        public void showLoading() {
                            if (dialogLoading != null) dialogLoading.show();
                        }

                        @Override
                        public void hideLoading() {
                            if (dialogLoading != null) dialogLoading.dismiss();
                        }
                    });
                }
            }
        });
    }

    private void downloadItem() {
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.DetailItemShop.DOWNLOAD + name);
        createTask(apiZip, name);
        download(task);
    }

    // create download task
    private void createTask(String urlZip, String nameZip) {
        parentFile = FileUtil.getParentFile(this, "ItemDownload/" + Constant.STICKER);
        if (!parentFile.exists()) {
            if (parentFile.mkdirs()) {
                Log.d(TAG, "createTask: loaded");
            } else
                Log.d(TAG, "createTask: error");
        }
        task = new DownloadTask.Builder(urlZip, parentFile).setFilename(nameZip).setMinIntervalMillisCallbackProcess(16)
                .setPassIfAlreadyCompleted(true).build();
    }

    // download item đang xem
    private void download(DownloadTask task) {
        task.enqueue(new DownloadListener4WithSpeed() {
            int size = 0;

            @Override
            public void taskStart(@NonNull DownloadTask task) {
                Log.d(TAG, "taskStart: ");
            }

            @Override
            public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {
                Log.d(TAG, "connectStart: ");
            }

            @Override
            public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {
                Log.d(TAG, "connectEnd: ");
            }

            @Override
            public void infoReady(@NonNull DownloadTask task, @NonNull BreakpointInfo info, boolean fromBreakpoint, @NonNull Listener4SpeedAssistExtend.Listener4SpeedModel model) {
                Log.d(TAG, "infoReady: ");
                size = (int) info.getTotalLength();
                if (dialog != null) {
                    dialog.show();
                    dialog.setInfo(getResources().getString(R.string.downloading), getResources().getString(R.string.wait_when_download));
                    dialog.setMax(size);
                }
            }

            @Override
            public void progressBlock(@NonNull DownloadTask task, int blockIndex, long currentBlockOffset, @NonNull SpeedCalculator blockSpeed) {
                Log.d(TAG, "progressBlock: ");
            }

            @Override
            public void progress(@NonNull DownloadTask task, long currentOffset, @NonNull SpeedCalculator taskSpeed) {
                Log.d(TAG, "progress: ");
                if (dialog != null) {
                    dialog.setCurrent(currentOffset);
                }
            }

            @Override
            public void blockEnd(@NonNull DownloadTask task, int blockIndex, BlockInfo info, @NonNull SpeedCalculator blockSpeed) {
                Log.d(TAG, "blockEnd: ");
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause, @NonNull SpeedCalculator taskSpeed) {
                Log.d(TAG, "taskEnd: ");
                if (cause == EndCause.COMPLETED) {
                    MyTrackingFireBase.trackingDownload(DetailItemActivity.this, Constant.STICKER);

                    String childParentFile = parentFile.getAbsolutePath() + "/" + name;
                    File childFile = new File(childParentFile + "unzip");

                    try {
                        InputStream inputStream = new FileInputStream(childParentFile);
                        if (!childFile.exists()) {
                            if (childFile.mkdirs()) {
                                Log.d(TAG, "taskEnd: ");
                            }
                        }
                        FileUtil.unzip(inputStream, childFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (parentFile.isDirectory()) {
                        for (File child : parentFile.listFiles()) {
                            if (!child.isDirectory()) {
                                if (child.delete()) {
                                    Log.d(TAG, "taskEnd: delete loaded");
                                }
                            }
                        }
                    }
                    MyTrackingFireBase.trackingScreen(DetailItemActivity.this, TrackingUtils.TrackingConstant.DetailItemShop.DOWNLOAD_DONE + name);
                    viewModel.insert(new ItemDownloaded(Constant.STICKER, childFile.getAbsolutePath(), name
                            , (float) ((size / 1024)) + " KB", nameItems)); // download hoàn tất thì insert vào db
                    Toasty.success(DetailItemActivity.this, getString(R.string.download_done), Toasty.LENGTH_SHORT).show();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    getLifecycle().addObserver(new LifecycleObserver() {
                        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
                        void onResume() {
                            binding.btnUseNow.setText(getString(R.string.use_now));
                            binding.btnUseNow.setBackground(getResources().getDrawable(R.drawable.btn_download_done));
                        }
                    });
                    if (new PermissionChecker(DetailItemActivity.this).isPermissionOK()) {
                        bottomUsing = new BottomUsing();
                        if (!getSupportFragmentManager().isDestroyed()) {
                            bottomUsing.show(getSupportFragmentManager(), "BottomUsing");
                        }
                    }
                } else if (cause == EndCause.CANCELED) {
                    MyTrackingFireBase.trackingScreen(DetailItemActivity.this, TrackingUtils.TrackingConstant.DetailItemShop.DOWNLOAD_CANCEL + name);
                    Toasty.normal(DetailItemActivity.this, getString(R.string.cancel_completed), Toasty.LENGTH_SHORT).show();
                } else {
                    MyTrackingFireBase.trackingScreen(DetailItemActivity.this, TrackingUtils.TrackingConstant.DetailItemShop.DOWNLOAD_ERROR + name);
                    Toasty.error(DetailItemActivity.this, getString(R.string.please_check_your_internet_connection_and_try_again), Toasty.LENGTH_SHORT).show();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bottomUsing != null && bottomUsing.isVisible()) {
            bottomUsing.dismissAllowingStateLoss();
        }
    }

    @Override
    public void cancel() {
        if (task != null) task.cancel();
    }

    public void onUpToProClicked() {
        binding.tvUpToProVer.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }
}
