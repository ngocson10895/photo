package com.piccollage.collagemaker.picphotomaker.entity;

public class Theme {
    private String name;
    private int numberOfItem;
    private String urlFirstItem;

    public String getName() {
        return name;
    }

    public int getNumberOfItem() {
        return numberOfItem;
    }

    public String getUrlFirstItem() {
        return urlFirstItem;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfItem(int numberOfItem) {
        this.numberOfItem = numberOfItem;
    }

    public void setUrlFirstItem(String urlFirstItem) {
        this.urlFirstItem = urlFirstItem;
    }
}
