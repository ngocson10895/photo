package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.RatioIconAdapter;
import com.piccollage.collagemaker.picphotomaker.base.BaseView;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ViewRatioBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Ratio;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.BetweenSpacesItemDecoration;

/**
 * Create from Administrator
 * Purpose: ratio view cho scrapbook
 * Des: ratio view chỉnh sửa kích thước ảnh của scrapbook
 */
public class RatioView extends BaseView {
    private ViewRatioBinding binding;
    private RatioIconAdapter adapter;
    private IRatioViewListener listener;
    private int curPos = 0, oldPos = 0;
    private boolean isBuild;

    public RatioView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public interface IRatioViewListener {
        void previewRatio(Ratio ratio);

        void doneAction(Ratio ratio);

        void cancelAction(Ratio ratio);
    }

    @Override
    public void initData() {

    }

    public void setListener(IRatioViewListener listener) {
        this.listener = listener;
    }

    public void setBuild(boolean build) {
        isBuild = build;
    }

    @Override
    protected void initView() {
        binding = ViewRatioBinding.bind(this);
        adapter = new RatioIconAdapter(InitData.ratioData(), getContext());
        binding.rvRatio.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvRatio.addItemDecoration(new BetweenSpacesItemDecoration(0, 16));
        binding.rvRatio.setAdapter(adapter);
        adapter.setOnItemClickListener((adapter1, view, position) -> {
            if (!isBuild) {
                if (curPos != position) {
                    isBuild = true;
                    curPos = position;
                    reloadData(curPos);
                    listener.previewRatio(adapter.getData().get(position));
                }
            }
        });
    }

    private void reloadData(int pos) {
        for (Ratio ratio : adapter.getData()) {
            ratio.setPick(false);
        }
        adapter.getData().get(pos).setPick(true);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getLayoutID() {
        return R.layout.view_ratio;
    }

    @Override
    public void buttonClick() {
        onDoneClick();
        onHideClick();
    }

    private void onHideClick() {
        binding.ivHideView.setOnClickListener(v -> backAction());
    }

    private void onDoneClick() {
        binding.ivDoneView.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewRatio.DONE);
            if (curPos != oldPos) {
                oldPos = curPos;
                listener.doneAction(adapter.getData().get(curPos));
            }
            hide();
        });
    }

    @Override
    public void backAction() {
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewRatio.CANCEL);
        if (curPos != oldPos) {
            curPos = oldPos;
            listener.cancelAction(adapter.getData().get(oldPos));
        }
        hide();
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewRatio.NAME);
    }
}
