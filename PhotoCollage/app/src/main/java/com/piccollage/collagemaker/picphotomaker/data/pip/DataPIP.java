package com.piccollage.collagemaker.picphotomaker.data.pip;

import androidx.annotation.NonNull;

import java.util.List;

/**
 * data each pip
 */
public class DataPIP {
    private String eventName;
    private int eventId;
    private String sumImageNeedUse;
    private String path;
    private int idBegin;
    private int idEnd;
    private List<Content> content;

    public String getEventName() {
        return eventName;
    }

    public int getEventId() {
        return eventId;
    }

    public String getSumImageNeedUse() {
        return sumImageNeedUse;
    }

    public String getPath() {
        return path;
    }

    public int getIdBegin() {
        return idBegin;
    }

    public int getIdEnd() {
        return idEnd;
    }

    public List<Content> getContent() {
        return content;
    }

    @NonNull
    @Override
    public String toString() {
        return sumImageNeedUse + " " + path + " " + idBegin + " " + idEnd + " " + content.size();
    }
}
