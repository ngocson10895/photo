package com.piccollage.collagemaker.picphotomaker.ui.quotesfeature;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.Advertisement;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.base.BaseAppActivity;
import com.piccollage.collagemaker.picphotomaker.data.ApplicationViewModel;
import com.piccollage.collagemaker.picphotomaker.data.InitData;
import com.piccollage.collagemaker.picphotomaker.databinding.ActivityQuoteEditorBinding;
import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.entity.TextOfUser;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.Sticker;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.StickerView;
import com.piccollage.collagemaker.picphotomaker.stickerUtils.TextSticker;
import com.piccollage.collagemaker.picphotomaker.ui.DialogExitFeature;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.PreviewActivity;
import com.piccollage.collagemaker.picphotomaker.ui.morefeature.AddTextFragment;
import com.piccollage.collagemaker.picphotomaker.ui.pickphotoactivity.PickPhotoActivity;
import com.piccollage.collagemaker.picphotomaker.ui.storeactivity.StoreActivity;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.PermissionChecker;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.view.BackgroundView;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradient;
import com.piccollage.collagemaker.picphotomaker.viewholder.Gradients;
import com.piccollage.collagemaker.picphotomaker.viewholder.Palette;
import com.piccollage.collagemaker.picphotomaker.viewholder.Palettes;
import com.piccollage.collagemaker.picphotomaker.viewholder.Pattern;
import com.piccollage.collagemaker.picphotomaker.viewholder.Patterns;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import dauroi.photoeditor.utils.DateTimeUtils;
import dauroi.photoeditor.utils.PhotoUtils;
import es.dmoral.toasty.Toasty;

/**
 * Create from Administrator
 * Purpose: xử lý các tác vụ với màn hình edit quote
 * Des:
 */
public class QuoteEditorActivity extends BaseAppActivity implements AddTextFragment.ISendTextView {
    private static final String TAG = "QuoteEditorActivity";
    public static final String QUOTE = "quote";
    public static final String URL_QUOTE = "url";
    public static final int REQUEST_CODE_PICK_PHOTO = 999;

    private String quote;
    private DialogExitFeature dialogExitFeature;
    private DialogLoading dialogSave;
    private ActivityQuoteEditorBinding binding;

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void beforeSetContentView() {
        binding = ActivityQuoteEditorBinding.inflate(getLayoutInflater());
    }

    @SuppressLint("NewApi")
    public void initData() {
        ApplicationViewModel applicationViewModel = new ViewModelProvider(this).get(ApplicationViewModel.class);
        if (getIntent() != null) {
            quote = getIntent().getStringExtra(QUOTE);
            String urlThumbs = getIntent().getStringExtra(URL_QUOTE);
            Glide.with(this).asBitmap().load(urlThumbs).into(binding.ivBg);
        }
        binding.quoteView.configDefaultIcons("text");

        TextSticker textSticker = new TextSticker(this, getResources().getDrawable(R.drawable.sticker_transparent_background_ver));
        TextOfUser textOfUser = new TextOfUser();
        if (!quote.equals("")) {
            textOfUser.setContent(quote);
            textOfUser.setTypeface(TextUtils.loadTypeface(this, Constant.FONT_DEFAULT));
            textOfUser.setRadius(0);
            textOfUser.setColorText(Color.WHITE);
            textOfUser.setLineSpacing(1);
            textOfUser.setLetterSpacing(0);
            textSticker.setTextOfUser(textOfUser);
            textSticker.resizeText();
            binding.quoteView.addSticker(textSticker);
        }

        applicationViewModel.getDataGradients().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Gradients> gradients = new ArrayList<>();
                gradients.add(new Gradients(Constant.DEFAULT, InitData.addGradients()));
                for (ItemDownloaded item : itemsDownloaded) {
                    List<Gradient> list = new ArrayList<>();
                    String[] code = item.getPath().split("_");
                    String[] nameItem = item.getNameItem().split("_");
                    for (int i = 0; i < code.length; i += 2) {
                        if (code[i].length() == 6) {
                            Gradient gra = new Gradient("#" + code[i], "#" + code[i + 1], false, item.getName());
                            list.add(gra);
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    gradients.add(new Gradients(item.getName(), list));
                }
                gradients.add(0, new Gradients(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataGradientsEx(gradients);
            }
        });

        applicationViewModel.getDataPalettes().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Palettes> palettes = new ArrayList<>();
                palettes.add(new Palettes(Constant.DEFAULT, InitData.addPalettes()));
                for (ItemDownloaded item : itemsDownloaded) {
                    List<Palette> list = new ArrayList<>();
                    String[] code = item.getPath().split("_");
                    String[] nameItem = item.getNameItem().split("_");
                    for (int i = 0; i < code.length; i += 5) {
                        if (code[i].length() == 6) {
                            Palette pal = new Palette("#" + code[i], "#" + code[i + 1], "#" + code[i + 2], "#" + code[i + 3], "#" + code[i + 4], false);
                            list.add(pal);
                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    palettes.add(new Palettes(item.getName(), list));
                }
                palettes.add(0, new Palettes(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataPaletteEx(palettes);
            }
        });

        ArrayList<Pattern> pattern = new ArrayList<>();
        for (String p : FileUtil.listAssetFiles("background", this)) {
            pattern.add(new Pattern(p, String.valueOf(FileUtil.listAssetFiles("background", this).indexOf(p)), Constant.DEFAULT, false));
        }
        applicationViewModel.getDataPatterns().observe(this, itemsDownloaded -> {
            if (itemsDownloaded != null) {
                ArrayList<Patterns> patterns = new ArrayList<>();
                patterns.add(new Patterns(Constant.DEFAULT, pattern));
                for (ItemDownloaded itemDownloaded : itemsDownloaded) {
                    ArrayList<String> paths = new ArrayList<>();
                    List<Pattern> list = new ArrayList<>();
                    FileUtil.getAllFiles(new File(itemDownloaded.getPath()), paths);
                    for (String path : paths) {
                        list.add(new Pattern(path, "", itemDownloaded.getName(), false));
                    }
                    String[] nameItem = itemDownloaded.getNameItem().split("_");
                    for (int i = 0; i < list.size(); i++) {
                        if (i < nameItem.length) {
                            list.get(i).setName(nameItem[i]);
                        }
                    }
                    patterns.add(new Patterns(itemDownloaded.getName(), list));
                }
                patterns.add(0, new Patterns(Constant.MORE, new ArrayList<>()));
                binding.backgroundView.setDataPatternsEx(patterns);
            }
        });
    }

    private void posPalettePick(String c1, String c2, String c3, String c4, String c5) {
        binding.ivBg.setVisibility(View.INVISIBLE);
        binding.bgPaletteHor.setVisibility(View.VISIBLE);
        binding.iv1.setImageDrawable(new ColorDrawable(Color.parseColor(c1)));
        binding.iv2.setImageDrawable(new ColorDrawable(Color.parseColor(c2)));
        binding.iv3.setImageDrawable(new ColorDrawable(Color.parseColor(c3)));
        binding.iv4.setImageDrawable(new ColorDrawable(Color.parseColor(c4)));
        binding.iv5.setImageDrawable(new ColorDrawable(Color.parseColor(c5)));

        binding.iv6.setImageDrawable(new ColorDrawable(Color.parseColor(c1)));
        binding.iv7.setImageDrawable(new ColorDrawable(Color.parseColor(c2)));
        binding.iv8.setImageDrawable(new ColorDrawable(Color.parseColor(c3)));
        binding.iv9.setImageDrawable(new ColorDrawable(Color.parseColor(c4)));
        binding.iv10.setImageDrawable(new ColorDrawable(Color.parseColor(c5)));
    }

    @Override
    public void buttonClick() {
        onClChoseColorClicked();
        onClChosePhotoClicked();
        onIvCancelClicked();
        onIvDoneClicked();
        onIvRotateHorClicked();
        onIvRotateVerClicked();
    }

    public void initView() {
        loadAds();
        if (MyUtils.isAppPurchased()) {
            binding.viewPro.setVisibility(View.GONE);
        }
        onViewClicked();
        MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Quote.NAME);
        dialogExitFeature = new DialogExitFeature(this, QuoteEditorActivity.this::discard);
        dialogSave = new DialogLoading(this, DialogLoading.CREATE);

        binding.quoteView.setLocked(false);
        binding.quoteView.setConstrained(true);
        binding.quoteView.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            int clickCount = 1;

            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {
                binding.quoteView.setShowBorder(true);
                binding.quoteView.setShowIcons(true);
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {
                clickCount++;
                if (clickCount % 2 == 0) {
                    binding.quoteView.setShowBorder(false);
                    binding.quoteView.setShowIcons(false);
                } else {
                    binding.quoteView.setShowBorder(true);
                    binding.quoteView.setShowIcons(true);
                }
            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {

            }
        });

        binding.backgroundView.setListener(new BackgroundView.IBackgroundListener() {
            @Override
            public void previewBackground(int color) {
                binding.ivBg.setVisibility(View.VISIBLE);
                binding.clMain.setVisibility(View.INVISIBLE);
                binding.ivBg.setImageBitmap(ImageDecoder.drawableToBitmap(new ColorDrawable(color)));
            }

            @Override
            public void previewPattern(String path) {
                binding.ivBg.setVisibility(View.VISIBLE);
                binding.clMain.setVisibility(View.INVISIBLE);
                if (path.contains("storage")) {
                    Glide.with(QuoteEditorActivity.this).load(path).centerCrop().error(R.drawable.sticker_default).into(binding.ivBg);
                } else
                    Glide.with(QuoteEditorActivity.this).load(Uri.parse("file:///android_asset/background/" + path)).centerCrop().error(R.drawable.sticker_default).into(binding.ivBg);
            }

            @Override
            public void previewGradient(String stColor, String endColor) {
                binding.ivBg.setVisibility(View.VISIBLE);
                binding.clMain.setVisibility(View.INVISIBLE);
                binding.ivBg.setImageDrawable(new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{Color.parseColor(stColor), Color.parseColor(endColor)}));
            }

            @Override
            public void previewPalette(String c1, String c2, String c3, String c4, String c5) {
                binding.clMain.setVisibility(View.VISIBLE);
                binding.ivBg.setVisibility(View.INVISIBLE);
                posPalettePick(c1, c2, c3, c4, c5);
            }

            @Override
            public void jumpToShop(String type) {
                Intent intent = new Intent(QuoteEditorActivity.this, StoreActivity.class);
                intent.putExtra(Constant.VALUE_INTENT, type);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

            @Override
            public void doneAction(Bitmap bm) {
                clickDone();
            }

            @Override
            public void closeAction(Bitmap bm) {
                clickDone();
            }
        });
    }

    private void loadAds() {
        Advertisement.loadBannerAdExit(this);
        Advertisement.loadInterstitialAdInApp(this);
        binding.adsLayout.post(() -> Advertisement.showBannerAdOther(this, binding.adsLayout,
                false, new BannerAdsUtils.BannerAdListener() {
                    @Override
                    public void onAdLoaded() {
                        binding.viewPro.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAdFailed() {

                    }
                }));
    }

    public void onClChosePhotoClicked() {
        binding.clChosePhoto.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Quote.PHOTO_BG);
            Intent intent = new Intent(this, PickPhotoActivity.class);
            intent.putExtra(PickPhotoActivity.MODE, Constant.QUOTE);
            startActivityForResult(intent, REQUEST_CODE_PICK_PHOTO);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICK_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    binding.ivBg.setVisibility(View.VISIBLE);
                    Photo photo = data.getParcelableExtra(PickPhotoActivity.PHOTOS_PICKED);
                    binding.bgPaletteHor.setVisibility(View.INVISIBLE);
                    binding.bgPaletteVer.setVisibility(View.INVISIBLE);
                    Glide.with(this).asBitmap().load(photo.getPathPhoto()).into(binding.ivBg);
                }
            }
        }
    }

    public void onClChoseColorClicked() {
        binding.clChoseColor.setOnClickListener(v -> {
            binding.ivRotateHor.setVisibility(View.INVISIBLE);
            binding.ivRotateVer.setVisibility(View.INVISIBLE);
            binding.clChosePhoto.setVisibility(View.INVISIBLE);
            binding.backgroundView.show();
            binding.ivDone.setVisibility(View.INVISIBLE);
            binding.ivCancel.setVisibility(View.INVISIBLE);
        });
    }

    private void clickDone() {
        binding.ivDone.setVisibility(View.VISIBLE);
        binding.ivCancel.setVisibility(View.VISIBLE);
        binding.clChosePhoto.setVisibility(View.VISIBLE);
        binding.clChoseColor.setVisibility(View.VISIBLE);
        if (binding.bgPaletteHor.getVisibility() == View.VISIBLE || binding.bgPaletteVer.getVisibility() == View.VISIBLE) {
            binding.ivRotateHor.setVisibility(View.VISIBLE);
            binding.ivRotateVer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void turnOff() {

    }

    @SuppressLint("NewApi")
    @Override
    public void sendText(TextOfUser textOfUser) {
        binding.quoteView.configDefaultIcons("text");
        TextSticker textSticker = new TextSticker(this, getResources().getDrawable(R.drawable.sticker_transparent_background_ver));
        if (!textOfUser.getContent().equals("")) {
            textSticker.setTextOfUser(textOfUser);
            textSticker.resizeText();
            binding.quoteView.addSticker(textSticker);
        }
    }

    public void onIvCancelClicked() {
        binding.ivCancel.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                dialogExitFeature.show();
            }
        });
    }

    public void onIvDoneClicked() {
        binding.ivDone.setOnClickListener(v -> {
            if (checkDoubleClick()) {
                MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Quote.DONE);
                if (binding.quoteView.isShowBorder() && binding.quoteView.isShowIcons()) {
                    binding.quoteView.setShowBorder(false);
                    binding.quoteView.setShowIcons(false);
                }
                dialogSave.show();
                new Handler().postDelayed(() -> {
                    Task task = new Task(this);
                    task.execute();
                }, 2000);
            }
        });
    }

    public void discard() {
        finish();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public void onIvRotateHorClicked() {
        binding.ivRotateHor.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Quote.ROTATE_HOR);
            if (binding.bgPaletteVer.getVisibility() == View.VISIBLE) {
                binding.bgPaletteHor.setVisibility(View.VISIBLE);
                binding.bgPaletteVer.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void onIvRotateVerClicked() {
        binding.ivRotateVer.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(this, TrackingUtils.TrackingConstant.Quote.ROTATE_VER);
            if (binding.bgPaletteHor.getVisibility() == View.VISIBLE) {
                binding.bgPaletteHor.setVisibility(View.INVISIBLE);
                binding.bgPaletteVer.setVisibility(View.VISIBLE);
            }
        });
    }

    public void onViewClicked() {
        binding.viewPro.setOnClickListener(v -> {
            Intent intent = new Intent(this, BuySubActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });
    }

    /**
     * Task to export image
     */
    private static class Task extends AsyncTask<Void, Void, File> {
        private WeakReference<QuoteEditorActivity> weakReference;

        Task(QuoteEditorActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            QuoteEditorActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return;
            }
            if (new PermissionChecker(activity).isPermissionOK()) {
                PhotoUtils.addImageToGallery(file.getAbsolutePath(), activity);
                Toasty.success(activity, activity.getString(R.string.saving_done), Toasty.LENGTH_SHORT).show();
                Advertisement.showInterstitialAdInApp(activity, new InterstitialAdsPopupOthers.OnAdsListener() {
                    @Override
                    public void onAdsClose() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.QUOTE);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                }, new InterstitialAdsFacebookUtils.IFbIntersAdsListener() {
                    @Override
                    public void close() {
                        Intent intent = new Intent(activity, PreviewActivity.class);
                        intent.putExtra(Constant.IMAGE_PATH, file.getAbsolutePath());
                        intent.putExtra(Constant.VALUE_INTENT, Constant.QUOTE);
                        activity.startActivity(intent);
                        activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }

                    @Override
                    public void showLoading() {

                    }

                    @Override
                    public void hideLoading() {

                    }
                });
            }
            activity.dialogSave.dismiss();
        }

        @Override
        protected File doInBackground(Void... voids) {
            QuoteEditorActivity activity = weakReference.get();
            if (activity == null || activity.isDestroyed()) {
                return null;
            }
            Bitmap bitmap = ImageUtils.getBitmapFromView(activity.binding.clMain);
            String parentFilePath = FileUtil.getParentFileString("My Creative");
            File parentFile = new File(parentFilePath);
            if (!parentFile.exists()) {
                if (parentFile.mkdirs()) {
                    Log.d(TAG, "doInBackground: ");
                }
            }
            File photoFile = new File(parentFile, DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png"));
            try {
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(photoFile));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return photoFile;
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.backgroundView.getVisibility() == View.VISIBLE) {
            binding.backgroundView.hide();
            clickDone();
        } else {
            dialogExitFeature.show();
        }
    }
}
