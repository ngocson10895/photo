package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemFrameBinding;
import com.piccollage.collagemaker.picphotomaker.utils.ColorUtil;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;

import java.util.List;

/**
 * Adapter cho các item ở màn hình show các item downloaded
 */
public class ItemDownloadedAdapter extends RecyclerView.Adapter<ItemDownloadedAdapter.ViewHolder> {
    private Context context;
    private List<String> paths;
    private String theme;

    public ItemDownloadedAdapter(Context context, List<String> paths, String theme) {
        this.context = context;
        this.paths = paths;
        this.theme = theme;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_frame, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        switch (theme) {
            case Constant.GRADIENT: {
                String gradient = paths.get(i);
                ColorUtil.loadGradientToImageOvalGradient(gradient, context, viewHolder.binding.ivFrame, 160);
                break;
            }
            case Constant.FONT:
                Typeface typeface = TextUtils.loadTypeface(context, paths.get(i));
                viewHolder.binding.tvText.setTypeface(typeface);
                viewHolder.binding.tvText.setVisibility(View.VISIBLE);
                viewHolder.binding.ivFrame.setVisibility(View.INVISIBLE);
                viewHolder.binding.ivFirstPalette.setVisibility(View.INVISIBLE);
                break;
            case Constant.PALETTE: {
                viewHolder.binding.ivFirstPalette.setVisibility(View.VISIBLE);
                String[] colors = paths.get(i).split("_");
                viewHolder.binding.iv1.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[0])));
                viewHolder.binding.iv2.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[1])));
                viewHolder.binding.iv3.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[2])));
                viewHolder.binding.iv4.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[3])));
                viewHolder.binding.iv5.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[4])));
                viewHolder.binding.ivFrame.setVisibility(View.INVISIBLE);
                viewHolder.binding.tvText.setVisibility(View.INVISIBLE);
                break;
            }
            default:
                Glide.with(context).load(paths.get(i)).error(R.mipmap.thumbs).apply(RequestOptions.circleCropTransform())
                        .into(viewHolder.binding.ivFrame);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return paths.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemFrameBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemFrameBinding.bind(itemView);
        }
    }
}
