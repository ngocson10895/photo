package com.piccollage.collagemaker.picphotomaker.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemThemeBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Theme;
import com.piccollage.collagemaker.picphotomaker.utils.ColorUtil;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;

import java.util.ArrayList;

/**
 * Adapter theme để hiển thị các chủ đề như sticker, font, gradients,...
 */
public class ThemeAdapter extends BaseQuickAdapter<Theme, BaseViewHolder> {
    private Context context;

    public ThemeAdapter(@Nullable ArrayList<Theme> data, Context context) {
        super(R.layout.item_theme, data);
        this.context = context;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void convert(@NonNull BaseViewHolder helper, Theme item) {
        ItemThemeBinding binding = ItemThemeBinding.bind(helper.itemView);
        switch (item.getName()) {
            case Constant.STICKER:
            case Constant.PATTERN:
                binding.ivFirstPalette.setVisibility(View.INVISIBLE);
                binding.ivFirstText.setVisibility(View.INVISIBLE);
                Glide.with(context).load(item.getUrlFirstItem())
                        .error(R.mipmap.thumbs).apply(RequestOptions.circleCropTransform())
                        .into(binding.ivFirstItem);
                break;
            case Constant.FONT:
                binding.ivFirstPalette.setVisibility(View.INVISIBLE);
                binding.ivFirstText.setVisibility(View.VISIBLE);
                binding.ivFirstItem.setVisibility(View.INVISIBLE);
                Typeface typeface = TextUtils.loadTypeface(context, item.getUrlFirstItem());
                binding.ivFirstText.setTypeface(typeface);
                break;
            case Constant.GRADIENT:
                binding.ivFirstPalette.setVisibility(View.INVISIBLE);
                binding.ivFirstText.setVisibility(View.INVISIBLE);
                ColorUtil.loadGradientToImageOvalGradient(item.getUrlFirstItem(), context
                        , binding.ivFirstItem, 160);
                break;
            case Constant.PALETTE:
                String[] colors = item.getUrlFirstItem().split("_");
                binding.iv1.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[0])));
                binding.iv2.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[1])));
                binding.iv3.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[2])));
                binding.iv4.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[3])));
                binding.iv5.setImageDrawable(new ColorDrawable(Color.parseColor("#" + colors[4])));
                binding.ivFirstPalette.setVisibility(View.VISIBLE);
                break;
        }
        binding.tvName.setText(item.getName());
        binding.tvCapacity.setText(item.getNumberOfItem() + " packages");
    }
}
