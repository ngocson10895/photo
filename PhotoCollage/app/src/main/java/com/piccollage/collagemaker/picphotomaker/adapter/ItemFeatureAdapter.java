package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemFeatureBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;

import java.util.List;

/**
 * Adapter icon cho ở các view của màn hình
 */
public class ItemFeatureAdapter extends BaseQuickAdapter<Icon, BaseViewHolder> {
    private Context context;

    public ItemFeatureAdapter(@Nullable List<Icon> data, Context context) {
        super(R.layout.item_feature, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Icon item) {
        ItemFeatureBinding binding = ItemFeatureBinding.bind(helper.itemView);
        binding.name.setText(item.getName());
        if (item.getIcon() == 0) {
            binding.iconC.setVisibility(View.VISIBLE);
        } else {
            binding.iconC.setVisibility(View.INVISIBLE);
            if (item.isPick()) {
                Glide.with(context).load(context.getResources().getDrawable(item.getIconPicked())).into(binding.icon);
            } else
                Glide.with(context).load(context.getResources().getDrawable(item.getIcon())).into(binding.icon);
        }
    }
}
