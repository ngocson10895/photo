package com.piccollage.collagemaker.picphotomaker.utils.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.PagerAdapterSticker;
import com.piccollage.collagemaker.picphotomaker.base.BaseView;
import com.piccollage.collagemaker.picphotomaker.databinding.ViewAddStickerBinding;
import com.piccollage.collagemaker.picphotomaker.entity.StickerItem;
import com.piccollage.collagemaker.picphotomaker.entity.StickerTheme;
import com.piccollage.collagemaker.picphotomaker.eventbus.EventStickerPick;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.FileUtil;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

/**
 * View to add sticker
 */
public class AddStickerView extends BaseView {
    private ViewAddStickerBinding binding;
    private IStickerViewListener listener;
    private PagerAdapterSticker pagerAdapterSticker;
    private ArrayList<StickerTheme> stickerThemes;
    private SharedPrefsImpl sharedPrefs;
    private int posPage = 0;

    public interface IStickerViewListener extends IBaseViewListener {
        void previewAction(StickerItem stickerItem);

        void jumpToShop();
    }

    public AddStickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData();
    }

    public void setListener(IStickerViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void initData() {
        stickerThemes = new ArrayList<>();
        for (String s : FileUtil.listAssetFiles("sticker", getContext())) {
            StickerTheme stickerTheme = new StickerTheme(s, new ArrayList<>(), true, "", false);
            for (String s1 : FileUtil.listAssetFiles("sticker/" + s, getContext())) {
                StickerItem sticker = new StickerItem(s, "file:///android_asset/sticker/" + s + "/" + s1);
                stickerTheme.getStickers().add(sticker);
            }
            stickerThemes.add(stickerTheme);
        }
        if (sharedPrefs.get(Constant.STICKER, String.class) == null || sharedPrefs.get(Constant.STICKER, String.class).isEmpty()) {
            pagerAdapterSticker.setNewData(stickerThemes);
            pagerAdapterSticker.notifyDataSetChanged();
            customTab();
        }
    }

    @Override
    protected void initView() {
        binding = ViewAddStickerBinding.bind(this);
        sharedPrefs = new SharedPrefsImpl(getContext(), "dataApi");

        binding.viewPagerSticker.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabBarSticker) {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                posPage = position;
            }
        });
        binding.tabBarSticker.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(binding.viewPagerSticker));

        FragmentActivity fragmentActivity = (FragmentActivity) getContext();
        pagerAdapterSticker = new PagerAdapterSticker(fragmentActivity.getSupportFragmentManager(), new ArrayList<>());
        binding.viewPagerSticker.setAdapter(pagerAdapterSticker);
        binding.tabBarSticker.setupWithViewPager(binding.viewPagerSticker);
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewSticker.NAME);
        EventBus.getDefault().register(this);
    }

    @Override
    public void hide() {
        super.hide();
        EventBus.getDefault().removeStickyEvent(EventStickerPick.class);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getStickerPick(EventStickerPick event) {
        if (listener != null) listener.previewAction(event.getSticker());
    }

    private void customTab() {
        for (int i = 0; i < binding.tabBarSticker.getTabCount(); i++) {
            String url = pagerAdapterSticker.getStickerThemes().get(i).getStickers().get(0).getUrl();
            TabLayout.Tab tab = binding.tabBarSticker.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(R.layout.custom_tab_icon);
                if (tab.getCustomView() != null) {
                    ImageView icon = tab.getCustomView().findViewById(R.id.iconPre);
                    ImageView icDownloaded = tab.getCustomView().findViewById(R.id.iconNotDownload);
                    Glide.with(this).load(url).into(icon);
                    if (stickerThemes.get(i).isDownloaded()) {
                        icDownloaded.setVisibility(INVISIBLE);
                    } else icDownloaded.setVisibility(VISIBLE);
                }
            }
        }
    }

    public void setMoreSticker(ArrayList<StickerTheme> stickerThemes) {
        this.stickerThemes.removeAll(stickerThemes);
        this.stickerThemes.addAll(stickerThemes);
        pagerAdapterSticker.setNewData(this.stickerThemes);
        pagerAdapterSticker.notifyDataSetChanged();
        new Handler().postDelayed(() -> Objects.requireNonNull(binding.tabBarSticker.getTabAt(posPage)).select(), 100);
        customTab();
    }

    @Override
    public int getLayoutID() {
        return R.layout.view_add_sticker;
    }

    @Override
    public void buttonClick() {
        onHideViewClick();
        onDoneViewClick();
        onShopClick();
    }

    @Override
    public void backAction() {
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewSticker.CANCEL);
        listener.backAction();
        hide();
    }

    private void onHideViewClick() {
        binding.ivHideView.setOnClickListener(v -> backAction());
    }

    private void onDoneViewClick() {
        binding.ivDoneView.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.ViewSticker.DONE);
            listener.doneAction();
            hide();
        });
    }

    private void onShopClick() {
        binding.iconShop.setOnClickListener(v -> listener.jumpToShop());
    }
}
