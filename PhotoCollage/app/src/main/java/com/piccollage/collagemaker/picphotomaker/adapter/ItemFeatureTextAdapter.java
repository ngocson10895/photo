package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemFeatureTextBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;

import java.util.List;

/**
 * Adapter item text
 */
public class ItemFeatureTextAdapter extends BaseQuickAdapter<Icon, BaseViewHolder> {
    private Context context;

    public ItemFeatureTextAdapter(@Nullable List<Icon> data, Context context) {
        super(R.layout.item_feature_text, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Icon item) {
        ItemFeatureTextBinding binding = ItemFeatureTextBinding.bind(helper.itemView);
        if (item.isPick())
            binding.ivIcon.setImageDrawable(context.getResources().getDrawable(item.getIconPicked()));
        else binding.ivIcon.setImageDrawable(context.getResources().getDrawable(item.getIcon()));
        binding.tvName.setText(item.getName());
    }
}
