package com.piccollage.collagemaker.picphotomaker.entity;

public class Quote {
    private String quote;
    private String theme;
    private String urlThumbs;

    private int id;
    private boolean isPick;

    public Quote(String theme, String quote, String urlThumbs) {
        this.quote = quote;
        this.theme = theme;
        this.urlThumbs = urlThumbs;
    }

    public boolean isPick() {
        return isPick;
    }

    public void setPick(boolean pick) {
        isPick = pick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public String getTheme() {
        return theme;
    }

    public String getUrlThumbs() {
        return urlThumbs;
    }
}
