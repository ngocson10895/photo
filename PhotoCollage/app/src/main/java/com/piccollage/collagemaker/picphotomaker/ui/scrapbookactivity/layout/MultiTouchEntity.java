package com.piccollage.collagemaker.picphotomaker.ui.scrapbookactivity.layout;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Action Multi Touch Entity
 */
public abstract class MultiTouchEntity implements Parcelable {
    private static final String TAG = "MultiTouchEntity";
    boolean mFirstLoad = true;

    protected transient Paint mPaint = new Paint();

    // width/height of image
    int mWidth;
    int mHeight;

    // width/height of screen
    int mDisplayWidth;
    int mDisplayHeight;

    float mCenterX;
    float mCenterY;
    float mScaleX;
    float mScaleY;
    float mAngle;

    float mMinX;
    float mMaxX;
    float mMinY;
    float mMaxY;

    // area of the entity that can be scaled/rotated
    // using single touch (grows from bottom right)
    private final static int GRAB_AREA_SIZE = 40;
    private boolean mIsGrabAreaSelected = false;
    private boolean mIsLatestSelected = false;

    private float mGrabAreaX1;
    private float mGrabAreaY1;
    private float mGrabAreaX2;
    private float mGrabAreaY2;

    float mStartMidX;
    float mStartMidY;

    private static final int UI_MODE_ROTATE = 1;
    private static final int UI_MODE_ANISOTROPIC_SCALE = 2;
    private int mUIMode = UI_MODE_ROTATE;

    //detect touch
    private Matrix matrix = new Matrix();
    private float[] point = new float[2];
    private List<PointF> mappedPoints = new ArrayList<>();

    /**
     * Multi touch
     */
    MultiTouchEntity() {
        Log.d(TAG, "MultiTouchEntity() called");
    }

    /**
     * Multi touch with resource
     *
     * @param res : resource
     */
    MultiTouchEntity(Resources res) {
        getMetrics(res);
        Log.d(TAG, "MultiTouchEntity() called with: res = [" + res + "]");
    }

    /**
     * get metric of resource
     *
     * @param res : resource
     */
    void getMetrics(Resources res) {
        DisplayMetrics metrics = res.getDisplayMetrics();
        mDisplayWidth = (res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) ? Math
                .max(metrics.widthPixels, metrics.heightPixels) : Math.min(
                metrics.widthPixels, metrics.heightPixels);
        mDisplayHeight = (res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) ? Math
                .min(metrics.widthPixels, metrics.heightPixels) : Math.max(
                metrics.widthPixels, metrics.heightPixels);
        Log.d(TAG, "getMetrics() called with: res = [" + res + "]" + mDisplayHeight + " " + mDisplayWidth);
    }

    /**
     * Set the position and scale of an image in screen coordinates
     */
    public boolean setPos(MultiTouchController.PositionAndScale newImgPosAndScale) {
        float newScaleX;
        float newScaleY;

        if ((mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0) {
            newScaleX = newImgPosAndScale.getScaleX();
        } else {
            newScaleX = newImgPosAndScale.getScale();
        }

        if ((mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0) {
            newScaleY = newImgPosAndScale.getScaleY();
        } else {
            newScaleY = newImgPosAndScale.getScale();
        }
        Log.d(TAG, "setPos() called with: newImgPosAndScale = [" + newImgPosAndScale + "]");
        return setPos(newImgPosAndScale.getXOff(), newImgPosAndScale.getYOff(),
                newScaleX, newScaleY, newImgPosAndScale.getAngle());
    }

    /**
     * Set the position and scale of an image in screen coordinates
     */
    boolean setPos(float centerX, float centerY, float scaleX,
                   float scaleY, float angle) {
        float[] size = calculateHalfDrawableSize(scaleX, scaleY);
        float ws = size[0];//(mWidth / 2) * scaleX;
        float hs = size[1];//(mHeight / 2) * scaleY;

        mMinX = centerX - ws;
        mMinY = centerY - hs;
        mMaxX = centerX + ws;
        mMaxY = centerY + hs;

        mGrabAreaX1 = mMaxX - GRAB_AREA_SIZE;
        mGrabAreaY1 = mMaxY - GRAB_AREA_SIZE;
        mGrabAreaX2 = mMaxX;
        mGrabAreaY2 = mMaxY;

        mCenterX = centerX;
        mCenterY = centerY;
        mScaleX = scaleX;
        mScaleY = scaleY;
        mAngle = angle;
        Log.d(TAG, "setPos() called with: centerX = [" + centerX + "], centerY = [" + centerY + "], scaleX = [" + scaleX + "], scaleY = [" + scaleY + "], angle = [" + angle + "]");
        return true;
    }

    /**
     * Calculate half drawable size
     *
     * @param scaleX : scale X number
     * @param scaleY : scale Y number
     * @return : array size include 2 attributes is width and height
     */
    protected float[] calculateHalfDrawableSize(float scaleX,
                                                float scaleY) {
        float[] size = new float[2];
        size[0] = (mWidth / 2) * scaleX;
        size[1] = (mHeight / 2) * scaleY;
        Log.d(TAG, "calculateHalfDrawableSize() called with: scaleX = [" + scaleX + "], scaleY = [" + scaleY + "]");
        return size;
    }

    /**
     * @deprecated Return whether or not the given screen coords are inside this image
     */
    public boolean containsPoint(float touchX, float touchY) {
        Log.d(TAG, "containsPoint() called with: touchX = [" + touchX + "], touchY = [" + touchY + "]");
        return (touchX >= mMinX && touchX <= mMaxX && touchY >= mMinY && touchY <= mMaxY);
    }

    /**
     * Return whether or not the given screen coords are inside this image
     */
    public boolean contain(float touchX, float touchY) {
        final float dx = (mMaxX + mMinX) / 2;
        final float dy = (mMaxY + mMinY) / 2;

        matrix.reset();
        matrix.setRotate(mAngle * 180.0f / (float) Math.PI, dx, dy);
        //draw mapped points
        mappedPoints.clear();
        point[0] = mMinX;
        point[1] = mMinY;
        matrix.mapPoints(point);
        mappedPoints.add(new PointF(point[0], point[1]));

        point[0] = mMaxX;
        point[1] = mMinY;
        matrix.mapPoints(point);
        mappedPoints.add(new PointF(point[0], point[1]));

        point[0] = mMaxX;
        point[1] = mMaxY;
        matrix.mapPoints(point);
        mappedPoints.add(new PointF(point[0], point[1]));

        point[0] = mMinX;
        point[1] = mMaxY;
        matrix.mapPoints(point);
        mappedPoints.add(new PointF(point[0], point[1]));
        Log.d(TAG, "contain() called with: touchX = [" + touchX + "], touchY = [" + touchY + "]");
        return contains(mappedPoints, new PointF(touchX, touchY));
    }

    /**
     * Return true if the given point is contained inside the boundary.
     * See: http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
     *
     * @param test The point to check
     * @return true if the point is inside the boundary, false otherwise
     */
    private static boolean contains(List<PointF> points, PointF test) {
        int i;
        int j;
        boolean result = false;
        for (i = 0, j = points.size() - 1; i < points.size(); j = i++) {
            if ((points.get(i).y > test.y) != (points.get(j).y > test.y) &&
                    (test.x < (points.get(j).x - points.get(i).x) * (test.y - points.get(i).y) / (points.get(j).y - points.get(i).y) + points.get(i).x)) {
                result = !result;
            }
        }
        Log.d(TAG, "contains() called with: points = [" + points + "], test = [" + test + "]");
        return result;
    }

    public boolean grabAreaContainsPoint(float touchX, float touchY) {
        Log.d(TAG, "grabAreaContainsPoint() called with: touchX = [" + touchX + "], touchY = [" + touchY + "]");
        return (touchX >= mGrabAreaX1 && touchX <= mGrabAreaX2
                && touchY >= mGrabAreaY1 && touchY <= mGrabAreaY2);
    }

    public void reload(Context context) {
        Log.d(TAG, "reload() called with: context = [" + context + "]");
        mFirstLoad = false; // Let the load know properties have changed so
        // reload those,
        // don't go back and start with defaults
        load(context, mCenterX, mCenterY);
    }

    // draw
    public abstract void draw(Canvas canvas);

    // load with 2 parameter
    public abstract void load(Context context, float startMidX, float startMidY);

    // load
    public abstract void load(Context context);

    // unload
    public abstract void unload();

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public float getCenterX() {
        return mCenterX;
    }

    public float getCenterY() {
        return mCenterY;
    }

    public float getScaleX() {
        return mScaleX;
    }

    public float getScaleY() {
        return mScaleY;
    }

    public float getAngle() {
        return mAngle;
    }

    public float getMinX() {
        return mMinX;
    }

    public float getMaxX() {
        return mMaxX;
    }

    public float getMinY() {
        return mMinY;
    }

    public float getMaxY() {
        return mMaxY;
    }

    public void setIsGrabAreaSelected(boolean selected) {
        mIsGrabAreaSelected = selected;
    }

    public boolean isGrabAreaSelected() {
        return mIsGrabAreaSelected;
    }

    /**
     * Describe content
     *
     * @return : 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write an object to Parcel
     *
     * @param dest  : The Parcel in which the object should be written.
     * @param flags : Additional flags about how the object should be written
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBooleanArray(new boolean[]{mFirstLoad, mIsGrabAreaSelected,
                mIsLatestSelected});
        dest.writeInt(mWidth);
        dest.writeInt(mHeight);
        dest.writeInt(mDisplayWidth);
        dest.writeInt(mDisplayHeight);
        dest.writeFloat(mCenterX);
        dest.writeFloat(mCenterY);
        dest.writeFloat(mScaleX);
        dest.writeFloat(mScaleY);
        dest.writeFloat(mAngle);
        dest.writeFloat(mMinX);
        dest.writeFloat(mMaxX);
        dest.writeFloat(mMinY);
        dest.writeFloat(mMaxY);
        dest.writeFloat(mGrabAreaX1);
        dest.writeFloat(mGrabAreaY1);
        dest.writeFloat(mGrabAreaX2);
        dest.writeFloat(mGrabAreaY2);
        dest.writeFloat(mStartMidX);
        dest.writeFloat(mStartMidY);
        dest.writeInt(mUIMode);
        Log.d(TAG, "writeToParcel() called with: dest = [" + dest + "], flags = [" + flags + "]");
    }

    /**
     * Set the point's coordinates from the data stored in the specified parcel
     *
     * @param in : The parcel to read the point's coordinates ( never null )
     */
    public void readFromParcel(Parcel in) {
        boolean[] val = new boolean[3];
        in.readBooleanArray(val);
        mFirstLoad = val[0];
        mIsGrabAreaSelected = val[1];
        mIsLatestSelected = val[2];
        mWidth = in.readInt();
        mHeight = in.readInt();
        mDisplayWidth = in.readInt();
        mDisplayHeight = in.readInt();
        mCenterX = in.readFloat();
        mCenterY = in.readFloat();
        mScaleX = in.readFloat();
        mScaleY = in.readFloat();
        mAngle = in.readFloat();
        mMinX = in.readFloat();
        mMaxX = in.readFloat();
        mMinY = in.readFloat();
        mMaxY = in.readFloat();
        mGrabAreaX1 = in.readFloat();
        mGrabAreaY1 = in.readFloat();
        mGrabAreaX2 = in.readFloat();
        mGrabAreaY2 = in.readFloat();
        mStartMidX = in.readFloat();
        mStartMidY = in.readFloat();
        mUIMode = in.readInt();
        Log.d(TAG, "readFromParcel() called with: in = [" + in + "]");
    }
}
