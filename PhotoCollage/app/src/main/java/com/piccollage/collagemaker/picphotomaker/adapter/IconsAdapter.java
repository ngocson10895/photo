package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.entity.Icon;

import java.util.ArrayList;

/**
 * Adapter cho icon ở các màn hình edit
 */
public class IconsAdapter extends RecyclerView.Adapter<IconsAdapter.ViewHolder> {

    private ArrayList<Icon> iconArrayList;
    private Context context;
    private int initPosition = 0;
    private IPickFeature iPickFeature;
    private String type;

    public interface IPickFeature {
        void pickFeature(int position);
    }

    public IconsAdapter(ArrayList<Icon> iconArrayList, Context context, IPickFeature iPickFeature, String type) {
        this.iconArrayList = iconArrayList;
        this.context = context;
        this.iPickFeature = iPickFeature;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        switch (type) {
            case "editor":
                view = LayoutInflater.from(context).inflate(R.layout.item_icon_editor, viewGroup, false);
                break;
            case "bgIcon":
                view = LayoutInflater.from(context).inflate(R.layout.item_bg_icon, viewGroup, false);
                break;
            case "collage":
            default:
                view = LayoutInflater.from(context).inflate(R.layout.item_feature_collage, viewGroup, false);
                break;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (iconArrayList.get(i).isPick()) {
            viewHolder.ivIcon.setImageResource(iconArrayList.get(i).getIconPicked());
        } else viewHolder.ivIcon.setImageResource(iconArrayList.get(i).getIcon());
        viewHolder.itemView.setOnClickListener(v -> {
            iPickFeature.pickFeature(i);
            viewHolder.ivIcon.setImageResource(iconArrayList.get(viewHolder.getAdapterPosition()).getIconPicked());
            int currentPosition = viewHolder.getAdapterPosition();
            iconArrayList.get(currentPosition).setPick(true);
            if (initPosition != currentPosition) {
                int temp = initPosition;
                initPosition = currentPosition;
                iconArrayList.get(temp).setPick(false);
                notifyItemChanged(temp);
            }
        });
        viewHolder.tvName.setText(iconArrayList.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return iconArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;
        TextView tvName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }

    public void setIconArrayList(ArrayList<Icon> iconArrayList) {
        this.iconArrayList = iconArrayList;
        notifyDataSetChanged();
    }
}
