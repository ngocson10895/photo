package com.piccollage.collagemaker.picphotomaker.ui.vipactivity.dialograting;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;

import es.dmoral.toasty.Toasty;

/* renamed from: com.tohsoft.lib.a */
public class C3638a {

    /* renamed from: a */
    public static String f12386a = "KEY_SHOW_RATEDIALOG_APPEAR";

    /* renamed from: b */
    public static String f12387b = "PRE_SHARING_ENABLE_SHOW_RATE";

    /* renamed from: c */
    public static String f12388c = "PRE_SHARING_IS_SHOW_RATE";

    /* renamed from: d */
    private static int f12389d = 0;

    /* renamed from: e */
    private static boolean f12390e = false;

    /* renamed from: a */
    public static boolean m16909a(Activity context, int i) {
        f12389d = i;
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences(RateDialog.f12357m, 0);
        Editor edit = sharedPreferences.edit();
        int i2 = sharedPreferences.getInt(RateDialog.f12359o, 0);
        if (i2 > i) {
            return false;
        }
        if (i2 == i) {
            edit.putInt(RateDialog.f12359o, i + 1);
            edit.apply();
//            context.startActivity(new Intent(context, RateDialog.class));
            RateDialog dialogRate = new RateDialog(context);
            dialogRate.setString(Constant.SHARE_EMAIL);
            dialogRate.show();
            return true;
        }
        edit.putInt(RateDialog.f12359o, i2 + 1);
        edit.commit();
        return false;
    }

    /* renamed from: a */
    public static void m16905a(Context context) {
        Editor edit = context.getSharedPreferences("PRE_SHARING_ENABLE_SHOW_RATE", 0).edit();
        edit.putBoolean(f12388c, true);
        edit.apply();
    }

    /* renamed from: a */
    public static boolean m16910a(Activity context, int i, String str, String str2) {
        Editor edit = context.getApplicationContext().getSharedPreferences(RateDialog.f12364t, 0).edit();
        edit.putBoolean(RateDialog.f12364t, true);
        edit.putString(RateDialog.f12365u, str);
        edit.putString(RateDialog.f12366v, str2);
        edit.apply();
        return m16909a(context, i);
    }

    /* renamed from: a */
    public static void m16906a(Context context, String str, String str2, String str3) {
        Intent intent = new Intent("android.intent.action.SENDTO");
        intent.setData(Uri.parse("mailto:" + str));
        intent.putExtra("android.intent.extra.SUBJECT", str2);
        intent.putExtra("android.intent.extra.TEXT", "");
        try {
            context.startActivity(Intent.createChooser(intent, str3));
        } catch (ActivityNotFoundException e) {
            Toasty.error(context.getApplicationContext(),
                    context.getResources().getString(R.string.no_email_intent),//no_email_client_toast3
                    Toasty.LENGTH_SHORT).show();
        }
    }

    /* renamed from: a */
    public static boolean m16908a() {
        return f12390e;
    }

    /* renamed from: a */
    public static void m16907a(boolean z) {
        f12390e = z;
    }
}
