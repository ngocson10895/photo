package com.piccollage.collagemaker.picphotomaker.ui.collageactivity.layout;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.DragEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;

import java.util.ArrayList;
import java.util.List;

/**
 * Create from Administrator
 * Purpose: Khung của ảnh. Chứa ảnh của user ở bên trên
 * Des: Tạo khung ảnh collage, chứa các ảnh của user ở bên trên
 */
@SuppressLint("ViewConstructor")
public class FramePhotoLayout extends RelativeLayout implements PhotoItemLayout.OnImageClickListener {
    private List<Photo> listPhotoPicked;
    private int frameWidth, frameHeight;
    private List<PhotoItemLayout> photoItemLayouts;
    private IClickImage iClickImage;
    private List<HiddenLayout> imageViews;
    private int index, count = 0;
    private float space = 0, corner = 0;

    public interface IClickImage {
        void clickImage(int index, List<Photo> photosPicked);

        void drop();
    }

    // thực hiện sự kiện event DRAG and DROP cho việc chuyển đổi giữa 2 ảnh
    OnDragListener mOnDragListener = (v, event) -> {
        int dragEvent = event.getAction();

        switch (dragEvent) {
            case DragEvent.ACTION_DRAG_ENTERED:
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP:
                PhotoItemLayout target = (PhotoItemLayout) v; // ảnh muốn thay thế
                PhotoItemLayout selectedView = getSelectedFrameImageView(target, event); // ảnh được chọn
                if (selectedView != null) {
                    target = selectedView;
                    PhotoItemLayout dragged = (PhotoItemLayout) event.getLocalState();
                    if (target.getPhotoItem() != null && dragged.getPhotoItem() != null) {
                        // tráo 2 đường link của 2 ảnh
                        String targetPath = target.getPhotoItem().getPathPhoto();
                        String draggedPath = dragged.getPhotoItem().getPathPhoto();
                        if (targetPath == null) targetPath = "";
                        if (draggedPath == null) draggedPath = "";
                        if (!targetPath.equals(draggedPath)) {
                            target.getPhotoItem().setPathPhoto(draggedPath);
                            dragged.getPhotoItem().setPathPhoto(targetPath);
                            target.swapImage(dragged);
                        }
                    }
                }
                iClickImage.drop();
                break;
        }
        return true;
    };

    public FramePhotoLayout(Context context, List<Photo> listPhotoPicked, IClickImage iClickImage) {
        super(context);
        this.listPhotoPicked = listPhotoPicked;
        photoItemLayouts = new ArrayList<>();
        imageViews = new ArrayList<>();
        this.iClickImage = iClickImage;
    }

    /**
     * Build Frame with size of list photo picked
     *
     * @param containerWidth  : container Width
     * @param containerHeight : container Height
     * @param space           : space between photo items
     * @param corner          : corner of each photo
     */
    public void buildFrameLayout(int containerWidth, int containerHeight, float space, float corner) {
        frameWidth = containerWidth;
        frameHeight = containerHeight;
        this.space = space;
        this.corner = corner;

        // tạo background cho khung
        ImageView ivBackground = new ImageView(getContext());
        ivBackground.setImageBitmap(ImageDecoder.drawableToBitmap(new ColorDrawable(Color.TRANSPARENT)));
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        addView(ivBackground, params);

        for (Photo photo : listPhotoPicked) {
            PhotoItemLayout photoItemLayout = addPhotoItem(photo, frameWidth, frameHeight, space, corner, listPhotoPicked.indexOf(photo));
            photoItemLayouts.add(photoItemLayout);
        }
    }

    // bật tắt di chuyển ảnh
    public void setIdle(boolean idle) {
        for (PhotoItemLayout photo : photoItemLayouts)
            photo.setIdle(idle);
    }

    // thay đổi ảnh được chọn sang ảnh khác
    public void changeImage(int index, Photo photoUpdate) {
        if (photoItemLayouts != null && photoItemLayouts.get(index) != null && photoUpdate != null) {
            photoItemLayouts.get(index).changeImage(addPhotoItem(photoUpdate, frameWidth, frameHeight, space, corner, index));
        }
    }

    /**
     * Set space photo item
     *
     * @param space  : space
     * @param corner : corner
     */
    public void setSpace(float space, float corner) {
        for (PhotoItemLayout photo : photoItemLayouts)
            photo.setSpace(space, corner);
    }

    /**
     * Add Photo Item for FrameLayout
     *
     * @param photo           : photo
     * @param containerWidth  : container Width
     * @param containerHeight : container Height
     * @param space           : space between photo items
     * @param corner          : corner of each photo
     */
    private PhotoItemLayout addPhotoItem(Photo photo, int containerWidth, int containerHeight, float space, float corner, int index) {
        PhotoItemLayout photoItemLayout = new PhotoItemLayout(getContext(), photo, index);
        int leftMargin = (int) (containerWidth * photo.bound.left); // set margin left
        int topMargin = (int) (containerHeight * photo.bound.top); // set margin top
        int photoWidth, photoHeight; // width and height of photo item
        if (photo.bound.right == 1) {
            photoWidth = containerWidth - leftMargin;
        } else {
            photoWidth = (int) (containerWidth * photo.bound.width() + 0.5f);
        }

        if (photo.bound.bottom == 1) {
            photoHeight = containerHeight - topMargin;
        } else {
            photoHeight = (int) (containerHeight * photo.bound.height() + 0.5f);
        }

        photoItemLayout.build(photoWidth, photoHeight, space, corner);
        photoItemLayout.setOnImageClickListener(this);
        photoItemLayout.setOnDragListener(mOnDragListener);

        LayoutParams params = new LayoutParams(photoWidth, photoHeight);
        params.leftMargin = leftMargin;
        params.topMargin = topMargin;

        // tạo các ảnh ẩn để thể hiện ảnh không được chọn
        HiddenLayout photo1 = new HiddenLayout(getContext(), photo);
        photo1.build(photoWidth, photoHeight, space, corner);
        photo1.setVisibility(INVISIBLE);

        addView(photoItemLayout, params);
        addView(photo1, params);
        imageViews.add(photo1);
        return photoItemLayout;
    }

    // get ảnh được chọn khi sử dụng drag drop event
    private PhotoItemLayout getSelectedFrameImageView(PhotoItemLayout target, DragEvent event) {
        PhotoItemLayout dragged = (PhotoItemLayout) event.getLocalState();
        int leftMargin = (int) (frameWidth * target.getPhotoItem().bound.left);
        int topMargin = (int) (frameHeight * target.getPhotoItem().bound.top);
        final float globalX = leftMargin + event.getX();
        final float globalY = topMargin + event.getY();
        for (int idx = photoItemLayouts.size() - 1; idx >= 0; idx--) {
            PhotoItemLayout view = photoItemLayouts.get(idx);
            float x = globalX - frameWidth * view.getPhotoItem().bound.left;
            float y = globalY - frameHeight * view.getPhotoItem().bound.top;
            if (view.isSelected(x, y)) {
                if (view == dragged) {
                    return null;
                } else {
                    return view;
                }
            }
        }
        return null;
    }

    @Override
    public void onLongClickImage(PhotoItemLayout view) {
        view.setTag("x=" + view.getPhotoItem().x + ",y=" + view.getPhotoItem().y + ",path=" + view.getPhotoItem().getPathPhoto());
        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData dragData = new ClipData(view.getTag().toString(), mimeTypes, item);
        DragShadowBuilder myShadow = new DragShadowBuilder(view);
        view.startDrag(dragData, myShadow, view, 0);
    }

    @Override
    public void clickImage(int index) {
        if (count == 0) {
            this.index = index;
            for (HiddenLayout photo : imageViews) {
                if (imageViews.indexOf(photo) == index) {
                    photo.setVisibility(INVISIBLE);
                } else
                    photo.setVisibility(VISIBLE);
            }
            for (PhotoItemLayout photoItemLayout : photoItemLayouts) {
                photoItemLayout.setIdle(true);
            }
            iClickImage.clickImage(index, listPhotoPicked);
        }
    }

    @Override
    public void move() {
        iClickImage.drop();
    }

    public void setCount(int count) {
        this.count = count;
    }

    // khi hoàn thành edit thì phải ẩn hết hidden layout
    public void editDone() {
        for (HiddenLayout photo : imageViews) {
            photo.setVisibility(INVISIBLE);
        }
    }

    // flip image được chọn
    public void flipImage(int pos) {
        photoItemLayouts.get(index).setScaleX(pos);
    }
}
