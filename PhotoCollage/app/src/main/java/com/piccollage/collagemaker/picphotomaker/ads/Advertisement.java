package com.piccollage.collagemaker.picphotomaker.ads;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.ads.consent.ConsentInfoLoadingListener;
import com.google.ads.consent.ConsentStatus;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.corebase.logger;
import com.google.android.gms.corebase.sh;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.BannerAdsUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsS.InterstitialAdsPopupOthers;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.MyUtils;
import com.piccollage.collagemaker.picphotomaker.ads.adsnew.NativeAdUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.BannerAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.ConstantAds;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.InterstitialAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.ads.facebook.NativeAdsFacebookUtils;
import com.piccollage.collagemaker.picphotomaker.networking.MyApplication;
import com.piccollage.collagemaker.picphotomaker.ui.vipactivity.BuySubActivity;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;


public class Advertisement {
    public static void initSdk(AppCompatActivity activity) {
        long timeStart = System.currentTimeMillis();
        logger.f(activity, new ConsentInfoLoadingListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus, boolean b) {

            }

            @Override
            public void onFailedToUpdateConsentInfo(String s) {

            }

            @Override
            public void onSyncError() {
                long timeEnd = System.currentTimeMillis();
                int time = MyUtils.convertLongTimeMillis(timeEnd - timeStart);
                MyTrackingFireBase.trackingEventNew(TrackingUtils.TrackingConstant.EVENT_NEW.LOADING_INFO_FAIL + time);
            }

            @Override
            public void onSyncOK() {
                long timeEnd = System.currentTimeMillis();
                int time = MyUtils.convertLongTimeMillis(timeEnd - timeStart);
                MyTrackingFireBase.trackingEventNew(TrackingUtils.TrackingConstant.EVENT_NEW.LOADING_INFO_OK + time);
            }
        });

    }

    public Context context = MyApplication.getContext();

    public static void addAdsToContainer(final ViewGroup container, final AdView adView, Context context, boolean isExit) {
        if (container == null || adView == null || context == null || MyUtils.isAppPurchased()) {
            if (context != null && container != null && !MyUtils.isAppPurchased()) {
                ImageView imageView = new ImageView(context);
                imageView.setOnClickListener(v -> {
                    Intent intent = new Intent(context, BuySubActivity.class);
                    context.startActivity(intent);
                });
                if (!isExit) {
                    imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.view_pro));
                    container.addView(imageView);
                } else {
                    imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ads));
                    container.addView(imageView);
                }
            }
            return;
        }
        try {
            if (adView.getParent() != null) {
                ((ViewGroup) adView.getParent()).removeAllViews();
            }
            container.setVisibility(View.VISIBLE);
            container.removeAllViews();
            container.addView(adView);
        } catch (Exception ignored) {
        }
    }

    private static void addAdsToContainer(final ViewGroup container, View adView, Context context) {
        if (container == null || adView == null || context == null) {
            return;
        }
        try {
            if (adView.getParent() != null) {
                ((ViewGroup) adView.getParent()).removeAllViews();
            }
            container.addView(adView);
        } catch (Exception ignored) {
        }
    }

    private static int[] arrFlagAdsDefault = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

    private static int[] arrFlagAds = (sh.getInstance(MyApplication.getContext()).getUapps() == null || sh.getInstance(MyApplication.getContext()).getUapps().getFlag_ads_10() == null) ? arrFlagAdsDefault
            : sh.getInstance(MyApplication.getContext()).getUapps().getFlag_ads_10();

    /**
     * FlagAds = 1 Hiển thị quảng cáo Google.
     * FlagAds = 2 Hiển thị quảng cáo Facebook.
     * todo FlagAds = 3,4
     *
     * @param context
     * @param container
     */
    public static void showBannerAdHome(Context context, ViewGroup container, AdSize adSize, BannerAdsUtils.BannerAdListener adListener, BannerAdsFacebookUtils.IFacebookAdsListener facebookAdsListener) {
        switch (arrFlagAds[0]) {
            case 1:
                BannerAdsUtils.getInstances().loadBannerAdHome(context, container, adSize, adListener);
                break;
            case 2:
                BannerAdsFacebookUtils.getInstances().loadBannerAdHome(context, container, com.facebook.ads.AdSize.RECTANGLE_HEIGHT_250, facebookAdsListener);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
            default:
                break;
        }
    }

    public static void showBannerAdSettings(Context context, ViewGroup container) {
        switch (arrFlagAds[1]) {
            case 1:
                BannerAdsUtils.getInstances().loadBannerAdSettings(context, container);
                break;
            case 2:
//                BannerAdsFacebookUtils.getInstances().loadBannerAdSettings(context, container);
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }
    }

    public static void showBannerAdOther(Context context, ViewGroup container, boolean isPreLoad, BannerAdsUtils.BannerAdListener bannerAdListener) {
        switch (arrFlagAds[1]) {
            case 1:
                BannerAdsUtils.getInstances().loadBannerAdOther(context, container, isPreLoad, bannerAdListener);
                break;
            case 2:
//                BannerAdsFacebookUtils.getInstances().loadBannerAdLock(context, container);
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }
    }

    public static void loadBannerAdExit(Context context) {
        switch (arrFlagAds[2]) {
            case 2:
                BannerAdsFacebookUtils.getInstances().loadBannerAdExitApp(context);
                break;
            default:
                BannerAdsUtils.getInstances().loadBannerAdExitApp(context);
                break;
        }
    }

    public static void showBannerAdExit(Context context, ViewGroup container) {
        switch (arrFlagAds[2]) {
            case 2:
                addAdsToContainer(container, ConstantAds.BANNER_FACEBOOK_AD_EXIT, context);
                break;
            default:
                addAdsToContainer(container, ConstantAds.BANNER_GOOGLE_AD_EXIT, context, true);
                break;
        }
    }

    // 19/03/2020: show nếu pre Load banner
    public static void showBannerAdOther(Context context, ViewGroup container) {
        switch (arrFlagAds[1]) {
            case 1:
                addAdsToContainer(container, ConstantAds.BANNER_GOOGLE_AD_OTHER, context, false);
                break;
            case 2:
//                addAdsToContainer(container, ConstantAds.BANNER_FACEBOOK_AD_EXIT, context);
                break;
            default:
//                addAdsToContainer(container, ConstantAds.BANNER_GOOGLE_AD_EXIT, context);
                break;
        }
    }

    public static void showNativeAdTopHome(Activity activity, ViewGroup container, NativeAdUtils.INativeAdsListener listener) {
        switch (arrFlagAds[3]) {
            case 1:
                NativeAdUtils.inflateNativeAd(activity, logger.getAdmob_native_tophome3(activity), container, R.layout.ad_unified, listener);
                break;
            case 2:
                NativeAdsFacebookUtils.getInstances().loadNativeAdHomeTop(activity, container, R.layout.ad_native_facebook_home);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
            default:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdTop(context, R.layout.ad_native_home, container);
                break;
        }
    }

    public static void showNativeAdBottomHome(Context context, ViewGroup container) {
        switch (arrFlagAds[4]) {
            case 1:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdBottom(context, R.layout.ad_native_home, container);
                break;
            case 2:
//                NativeAdsFacebookUtils.getInstances().loadNativeAdHomeBottom(context, container, R.layout.ad_native_facebook_home);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
            default:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdBottom(context, R.layout.ad_native_home, container);
                break;
        }
    }

    public static void showNativeCenterHome(Context context, ViewGroup container) {
        switch (arrFlagAds[6]) {
            case 1:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdCenter(context, R.layout.ad_native_home, container);
                break;
            case 2:
//                NativeAdsFacebookUtils.getInstances().loadNativeAdHomeCenter(context, container, R.layout.ad_native_facebook_home);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
        }
    }

    public static void showNativeAdMyLocation(Context context, ViewGroup container) {
        switch (arrFlagAds[5]) {
            case 1:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdSettings(context, R.layout.ad_native_my_location, container);
                break;
            case 2:
//                NativeAdsFacebookUtils.getInstances().loadNativeAdMyLocation(context, container, R.layout.ad_native_facebook_my_location);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
        }
    }

    public static void showNativeAdNews(Context context, ViewGroup container) {
        switch (arrFlagAds[12]) {
            case 1:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdNews(context, R.layout.ad_native_home, container);
                break;
            case 2:
//                NativeAdsFacebookUtils.getInstances().loadNativeAdNews(context, container, R.layout.ad_native_facebook_home);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
        }
    }

    public static void loadNativeAdPage(Context context) {
        switch (arrFlagAds[13]) {
            case 1:
//                NativeAdsMediationUtils.getInstances().loadAdapterNativeAdPage(context, R.layout.ad_native_home);
                break;
            case 2:
//                NativeAdsFacebookUtils.getInstances().loadNativeAdHomePage(context, R.layout.ad_native_facebook_home);
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
        }
    }

    public static void showNativePage(Context context, ViewGroup container, View ivBackground) {
        switch (arrFlagAds[9]) {
            case 1:
                if (ConstantAds.NATIVE_AD_GOOGLE != null) {
                    addAdsToContainer(container, ConstantAds.NATIVE_AD_GOOGLE, context);
                    ivBackground.setVisibility(View.VISIBLE);
                } else {
                    ivBackground.setVisibility(View.GONE);
                }
                break;
            case 2:
                if (ConstantAds.NATIVE_AD_FACEBOOK != null) {
                    addAdsToContainer(container, ConstantAds.NATIVE_AD_FACEBOOK, context);
                    ivBackground.setVisibility(View.VISIBLE);
                } else {
                    ivBackground.setVisibility(View.GONE);
                }
                break;
            case 3:
                //todo
                break;
            case 4:
                break;
        }
    }

    public static void loadInterstitialAdInApp(Context context) {
        switch (arrFlagAds[8]) {
            case 1:
                InterstitialAdsPopupOthers.getInstance().loadAd(context);
                break;
            case 2:
                InterstitialAdsFacebookUtils.getInstance().loadAdInApp(context);
                break;
        }
    }

    public static void showInterstitialAdInApp(Context context, InterstitialAdsPopupOthers.OnAdsListener listener, InterstitialAdsFacebookUtils.IFbIntersAdsListener listenerFb) {
        switch (arrFlagAds[8]) {
            case 1:
                InterstitialAdsPopupOthers.getInstance().showAdsNow(listener, context);
                break;
            case 2:
                InterstitialAdsFacebookUtils.getInstance().showAdInApp(listenerFb);
                break;
        }
    }
}
