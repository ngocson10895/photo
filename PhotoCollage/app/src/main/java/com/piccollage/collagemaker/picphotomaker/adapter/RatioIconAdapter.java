package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemRatio2Binding;
import com.piccollage.collagemaker.picphotomaker.entity.Ratio;

import java.util.List;

/**
 * Ratio icon for view ratio
 */
public class RatioIconAdapter extends BaseQuickAdapter<Ratio, BaseViewHolder> {
    private Context context;

    public RatioIconAdapter(@Nullable List<Ratio> data, Context context) {
        super(R.layout.item_ratio_2, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Ratio item) {
        ItemRatio2Binding binding = ItemRatio2Binding.bind(helper.itemView);
        if (item.isPick()) {
            Glide.with(context).load(context.getResources().getDrawable(item.getIconPicked())).into(binding.icon);
        } else
            Glide.with(context).load(context.getResources().getDrawable(item.getIcon())).into(binding.icon);
        binding.name.setText(item.getName());
    }
}
