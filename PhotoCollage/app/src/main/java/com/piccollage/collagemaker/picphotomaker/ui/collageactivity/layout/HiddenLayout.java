package com.piccollage.collagemaker.picphotomaker.ui.collageactivity.layout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;

import androidx.appcompat.widget.AppCompatImageView;

import com.piccollage.collagemaker.picphotomaker.entity.Photo;
import com.piccollage.collagemaker.picphotomaker.multitouch.MultiTouchHandler;
import com.piccollage.collagemaker.picphotomaker.utils.GeometryUtils;
import com.piccollage.collagemaker.picphotomaker.utils.ImageDecoder;
import com.piccollage.collagemaker.picphotomaker.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Create from Administrator
 * Purpose: ảnh thể hiện các photo không được chọn
 * Des: có hình dáng giống với photo item nhưng chỉ có màu xám để hiển thị các ảnh không được chọn
 */
@SuppressLint("ViewConstructor")
public class HiddenLayout extends AppCompatImageView {
    private Photo photoPicked;
    private Bitmap mImage;
    private Matrix mImageMatrix, mScaleMatrix;
    private Paint mPaint;
    private float mViewWidth, mViewHeight;
    private Path mPath = new Path();
    private Path mBackgroundPath = new Path();
    private List<PointF> mPolygon = new ArrayList<>();
    private Rect mPathRect = new Rect(0, 0, 0, 0);
    private MultiTouchHandler mTouchHandler;
    private List<PointF> mConvertedPoints = new ArrayList<>();

    //Clear area
    private Path mClearPath = new Path();
    private List<PointF> mConvertedClearPoints = new ArrayList<>();

    public HiddenLayout(Context context, Photo photoPicked) {
        super(context);
        this.photoPicked = photoPicked;
        mImage = ImageDecoder.drawableToBitmap(new ColorDrawable(Color.parseColor("#95ffffff")));
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG); // to paint a view
        mPaint.setFilterBitmap(true);

        setScaleType(ScaleType.MATRIX);
        setLayerType(LAYER_TYPE_HARDWARE, mPaint);
        mImageMatrix = new Matrix();
        mScaleMatrix = new Matrix();
    }

    /**
     * Draw smt on view.
     * If you want to add image to this view, you have to draw it
     *
     * @param canvas : to draw on Android
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int mBackgroundColor = Color.WHITE;
        drawImage(canvas, mPath, mPaint, mPathRect, mImage, mImageMatrix,
                getWidth(), getHeight(), mBackgroundColor, mBackgroundPath,
                mClearPath, mPolygon);
    }

    // Draw image với các thuộc tính
    private void drawImage(Canvas canvas, Path path, Paint paint, Rect pathRect, Bitmap image, Matrix imageMatrix,
                           float viewWidth, float viewHeight,
                           int color, Path backgroundPath,
                           Path clearPath, List<PointF> touchPolygon) {

        if (image != null && !image.isRecycled()) {
            canvas.drawBitmap(image, imageMatrix, paint); // draw bitmap on image view
        }
        //clip outside
        if (pathRect.left == pathRect.right) {
            canvas.save();
            canvas.clipPath(path);
            pathRect.set(canvas.getClipBounds());
            canvas.restore();
        }
        canvas.save();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawARGB(0x00, 0x00, 0x00, 0x00);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0, 0, viewWidth, pathRect.top, paint); // top
        canvas.drawRect(0, 0, pathRect.left, viewHeight, paint); // left
        canvas.drawRect(pathRect.right, 0, viewWidth, viewHeight, paint); // right
        canvas.drawRect(0, pathRect.bottom, viewWidth, viewHeight, paint); // bottom
        paint.setXfermode(null);
        canvas.restore();
        //clip inside
        canvas.save();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawARGB(0x00, 0x00, 0x00, 0x00);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);
        final Path.FillType currentFillType = path.getFillType();
        path.setFillType(Path.FillType.INVERSE_WINDING);
        canvas.drawPath(path, paint);
        paint.setXfermode(null);
        path.setFillType(currentFillType);
        canvas.restore();
        //clear area
        if (clearPath != null) {
            canvas.save();
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            canvas.drawARGB(0x00, 0x00, 0x00, 0x00);
            paint.setColor(Color.RED);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPath(clearPath, paint);
            paint.setXfermode(null);
            canvas.restore();
        }
        //draw out side
        if (backgroundPath != null) {
            canvas.save();
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
            canvas.drawARGB(0x00, 0x00, 0x00, 0x00);
            paint.setColor(color);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPath(backgroundPath, paint);
            paint.setXfermode(null);
            canvas.restore();
        }
        //touch polygon
        if (touchPolygon != null && touchPolygon.isEmpty()) {
            touchPolygon.add(new PointF(pathRect.left, pathRect.top));
            touchPolygon.add(new PointF(pathRect.right, pathRect.top));
            touchPolygon.add(new PointF(pathRect.right, pathRect.bottom));
            touchPolygon.add(new PointF(pathRect.left, pathRect.bottom));
        }
    }

    public Bitmap getImage() {
        return mImage;
    }

    public void setImage(Bitmap image) {
        mImage = image;
    }

    /**
     * Build photo item
     *
     * @param viewWidth  : photo width
     * @param viewHeight : photo Height
     * @param space      : space between photo items
     * @param corner     : corner of photo item
     */
    public void build(float viewWidth, float viewHeight, float space, float corner) {
        mViewWidth = viewWidth;
        mViewHeight = viewHeight;

        float scale = 1f; // scale image

        if (mImage != null) {
            mImageMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(viewWidth, viewHeight, mImage.getWidth(), mImage.getHeight()));
            mScaleMatrix.set(ImageUtils.createMatrixToDrawImageInCenterView(scale * viewWidth, scale * viewHeight, mImage.getWidth(), mImage.getHeight()));
        }
        mTouchHandler = new MultiTouchHandler();
        mTouchHandler.setMatrices(mImageMatrix, mScaleMatrix);
        mTouchHandler.setScale(scale);
        mTouchHandler.setEnableRotation(true);
        setSpace(space, corner);
    }

    /**
     * Set space for photo items
     *
     * @param space  : space between photo items
     * @param corner : corner of photo item
     */
    public void setSpace(float space, float corner) {
        setSpace(mViewWidth - space, mViewHeight - space, photoPicked,
                mConvertedPoints, mConvertedClearPoints,
                mPath, mClearPath, mBackgroundPath, mPolygon, mPathRect, space, corner);
        invalidate();
    }

    // set Space
    private static void setSpace(final float viewWidth, final float viewHeight, final Photo photoItem,
                                 final List<PointF> convertedPoints,
                                 final List<PointF> convertedClearPoints,
                                 final Path path,
                                 final Path clearPath,
                                 final Path backgroundPath,
                                 final List<PointF> polygon,
                                 final Rect pathRect,
                                 final float space, final float corner) {

        if (photoItem.pointList != null && convertedPoints.isEmpty()) {
            for (PointF p : photoItem.pointList) {
                PointF convertedPoint = new PointF(p.x * viewWidth, p.y * viewHeight);
                convertedPoints.add(convertedPoint);
                if (photoItem.shrinkMap != null) {
                    photoItem.shrinkMap.put(convertedPoint, photoItem.shrinkMap.get(p));
                }
            }
        }
        if (photoItem.clearAreaPoints != null && photoItem.clearAreaPoints.size() > 0) {
            clearPath.reset();
            if (convertedClearPoints.isEmpty())
                for (PointF p : photoItem.clearAreaPoints) {
                    convertedClearPoints.add(new PointF(p.x * viewWidth, p.y * viewHeight));
                }
            GeometryUtils.createPathWithCircleCorner(clearPath, convertedClearPoints, corner);
        } else if (photoItem.clearPath != null) {
            clearPath.reset();
            buildRealClearPath(viewWidth, viewHeight, photoItem, clearPath, corner);
        }

        if (photoItem.path != null) {
            buildRealPath(viewWidth, viewHeight, photoItem, path, space, corner);
            polygon.clear();
        } else {
            List<PointF> shrunkPoints;
            if (photoItem.shrinkMethod == Photo.SHRINK_METHOD_3_3) {
                int centerPointIdx = findCenterPointIndex(photoItem);
                shrunkPoints = GeometryUtils.shrinkPathCollage_3_3(convertedPoints, centerPointIdx, space, photoItem.bound);
            } else if (photoItem.shrinkMethod == Photo.SHRINK_METHOD_USING_MAP && photoItem.shrinkMap != null) {
                shrunkPoints = GeometryUtils.shrinkPathCollageUsingMap(convertedPoints, space, photoItem.shrinkMap);
            } else if (photoItem.shrinkMethod == Photo.SHRINK_METHOD_COMMON && photoItem.shrinkMap != null) {
                shrunkPoints = GeometryUtils.commonShrinkPath(convertedPoints, space, photoItem.shrinkMap);
            } else {
                if (photoItem.disableShrink) {
                    shrunkPoints = GeometryUtils.shrinkPath(convertedPoints, 0, photoItem.bound);
                } else {
                    shrunkPoints = GeometryUtils.shrinkPath(convertedPoints, space, photoItem.bound);
                }
            }
            polygon.clear();
            polygon.addAll(shrunkPoints);
            GeometryUtils.createPathWithCircleCorner(path, shrunkPoints, corner);
            if (photoItem.hasBackground) {
                backgroundPath.reset();
                GeometryUtils.createPathWithCircleCorner(backgroundPath, convertedPoints, corner);
            }
        }
        pathRect.set(0, 0, 0, 0);
    }

    // find center point of photo
    private static int findCenterPointIndex(Photo photoItem) {
        int centerPointIdx = 0;
        if (photoItem.bound.left == 0 && photoItem.bound.top == 0) {
            float minX = 1;
            for (int idx = 0; idx < photoItem.pointList.size(); idx++) {
                PointF p = photoItem.pointList.get(idx);
                if (p.x > 0 && p.x < 1 && p.y > 0 && p.y < 1 && p.x < minX) {
                    centerPointIdx = idx;
                    minX = p.x;
                }
            }
        } else {
            float maxX = 0;
            for (int idx = 0; idx < photoItem.pointList.size(); idx++) {
                PointF p = photoItem.pointList.get(idx);
                if (p.x > 0 && p.x < 1 && p.y > 0 && p.y < 1 && p.x > maxX) {
                    centerPointIdx = idx;
                    maxX = p.x;
                }
            }
        }
        return centerPointIdx;
    }

    // build real path
    private static void buildRealPath(final float viewWidth, final float viewHeight,
                                      final Photo photoItem, final Path outPath,
                                      float space, final float corner) {
        if (photoItem.path != null) {
            RectF rect = new RectF();
            photoItem.path.computeBounds(rect, true);
            final float pathWidthPixels = rect.width();
            final float pathHeightPixels = rect.height();
            space = 2 * space;
            outPath.set(photoItem.path);
            Matrix m = new Matrix();
            float ratioX, ratioY;
            if (photoItem.fitBound) {
                ratioX = photoItem.pathScaleRatio * (viewWidth * photoItem.pathRatioBound.width() - 2 * space) / pathWidthPixels;
                ratioY = photoItem.pathScaleRatio * (viewHeight * photoItem.pathRatioBound.height() - 2 * space) / pathHeightPixels;
            } else {
                float ratio = Math.min(photoItem.pathScaleRatio * (viewHeight - 2 * space) / pathHeightPixels,
                        photoItem.pathScaleRatio * (viewWidth - 2 * space) / pathWidthPixels);
                ratioX = ratio;
                ratioY = ratio;
            }
            m.postScale(ratioX, ratioY);
            outPath.transform(m);
            RectF bound = new RectF();
            if (photoItem.cornerMethod == Photo.CORNER_METHOD_3_6) {
                outPath.computeBounds(bound, true);
                GeometryUtils.createRegularPolygonPath(outPath, Math.min(bound.width(), bound.height()), 6, corner);
                outPath.computeBounds(bound, true);
            } else if (photoItem.cornerMethod == Photo.CORNER_METHOD_3_13) {
                outPath.computeBounds(bound, true);
                GeometryUtils.createRectanglePath(outPath, bound.width(), bound.height(), corner);
                outPath.computeBounds(bound, true);
            } else {
                outPath.computeBounds(bound, true);
            }
            float x, y;
            if (photoItem.shrinkMethod == Photo.SHRINK_METHOD_3_6 || photoItem.shrinkMethod == Photo.SHRINK_METHOD_3_8) {
                x = viewWidth / 2 - bound.width() / 2;
                y = viewHeight / 2 - bound.height() / 2;
                m.reset();
                m.postTranslate(x, y);
                outPath.transform(m);
            } else {
                if (photoItem.pathAlignParentRight) {
                    x = photoItem.pathRatioBound.right * viewWidth - bound.width() - space / ratioX;
                    y = photoItem.pathRatioBound.top * viewHeight + space / ratioY;
                } else {
                    x = photoItem.pathRatioBound.left * viewWidth + space / ratioX;
                    y = photoItem.pathRatioBound.top * viewHeight + space / ratioY;
                }

                if (photoItem.pathInCenterHorizontal) {
                    x = viewWidth / 2.0f - bound.width() / 2.0f;
                }

                if (photoItem.pathInCenterVertical) {
                    y = viewHeight / 2.0f - bound.height() / 2.0f;
                }
                m.reset();
                m.postTranslate(x, y);
                outPath.transform(m);
            }
        }
    }

    // build real clear path
    private static void buildRealClearPath(final float viewWidth, final float viewHeight, final Photo photoItem, final Path clearPath, final float corner) {
        if (photoItem.clearPath != null) {
            RectF rect = new RectF();
            photoItem.clearPath.computeBounds(rect, true);
            final float clearPathWidthPixels = rect.width();
            final float clearPathHeightPixels = rect.height();

            clearPath.set(photoItem.clearPath);
            Matrix m = new Matrix();
            float ratioX, ratioY;
            if (photoItem.fitBound) {
                ratioX = photoItem.clearPathScaleRatio * viewWidth * photoItem.clearPathRatioBound.width() / clearPathWidthPixels;
                ratioY = photoItem.clearPathScaleRatio * viewHeight * photoItem.clearPathRatioBound.height() / clearPathHeightPixels;
            } else {
                float ratio = Math.min(photoItem.clearPathScaleRatio * viewHeight / clearPathHeightPixels,
                        photoItem.clearPathScaleRatio * viewWidth / clearPathWidthPixels);
                ratioX = ratio;
                ratioY = ratio;
            }
            m.postScale(ratioX, ratioY);
            clearPath.transform(m);
            RectF bound = new RectF();
            if (photoItem.cornerMethod == Photo.CORNER_METHOD_3_6) {
                clearPath.computeBounds(bound, true);
                GeometryUtils.createRegularPolygonPath(clearPath, Math.min(bound.width(), bound.height()), 6, corner);
                clearPath.computeBounds(bound, true);
            } else if (photoItem.cornerMethod == Photo.CORNER_METHOD_3_13) {
                clearPath.computeBounds(bound, true);
                GeometryUtils.createRectanglePath(clearPath, bound.width(), bound.height(), corner);
                clearPath.computeBounds(bound, true);
            } else {
                clearPath.computeBounds(bound, true);
            }

            float x, y;
            if (photoItem.shrinkMethod == Photo.SHRINK_METHOD_3_6) {
                if (photoItem.clearPathRatioBound.left > 0) {
                    x = viewWidth - bound.width() / 2;
                } else {
                    x = -bound.width() / 2;
                }
                y = viewHeight / 2 - bound.height() / 2;
            } else {
                if (photoItem.centerInClearBound) {
                    x = photoItem.clearPathRatioBound.left * viewWidth + (viewWidth / 2 - bound.width() / 2);
                    y = photoItem.clearPathRatioBound.top * viewHeight + (viewHeight / 2 - bound.height() / 2);
                } else {
                    x = photoItem.clearPathRatioBound.left * viewWidth;
                    y = photoItem.clearPathRatioBound.top * viewHeight;
                    if (photoItem.clearPathInCenterHorizontal) {
                        x = viewWidth / 2.0f - bound.width() / 2.0f;
                    }
                    if (photoItem.clearPathInCenterVertical) {
                        y = viewHeight / 2.0f - bound.height() / 2.0f;
                    }
                }
            }

            m.reset();
            m.postTranslate(x, y);
            clearPath.transform(m);
        }
    }

}
