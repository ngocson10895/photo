package com.piccollage.collagemaker.picphotomaker.api;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.utils.ParseUtil;
import com.google.android.gms.corebase.ex;
import com.google.android.gms.corebase.logger;
import com.google.android.gms.corebase.se;
import com.piccollage.collagemaker.picphotomaker.data.home.DataHome;
import com.piccollage.collagemaker.picphotomaker.data.home.DataResponseHome;
import com.piccollage.collagemaker.picphotomaker.data.item.DataItem;
import com.piccollage.collagemaker.picphotomaker.data.item.DataResponseItem;
import com.piccollage.collagemaker.picphotomaker.data.item.DataSpecShop;
import com.piccollage.collagemaker.picphotomaker.data.news.DataNews;
import com.piccollage.collagemaker.picphotomaker.data.news.DataResponseNews;
import com.piccollage.collagemaker.picphotomaker.data.pip.Content;
import com.piccollage.collagemaker.picphotomaker.data.pip.Data;
import com.piccollage.collagemaker.picphotomaker.data.pip.DataPIP;
import com.piccollage.collagemaker.picphotomaker.entity.BannerOfHome;
import com.piccollage.collagemaker.picphotomaker.entity.Checking;
import com.piccollage.collagemaker.picphotomaker.entity.ItemShop;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;
import com.piccollage.collagemaker.picphotomaker.entity.Quote;
import com.piccollage.collagemaker.picphotomaker.entity.ThemeQuote;
import com.piccollage.collagemaker.picphotomaker.utils.Constant;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.sharePrefs.SharedPrefsImpl;

import java.util.ArrayList;
import java.util.List;

import dauroi.photoeditor.utils.NetworkUtils;

/**
 * Create from Administrator
 * Purpose: lấy dữ liệu từ api
 * Des: lấy các dữ liệu từ api để user có thể download và sử dụng
 */
public class APIServiceWrapper {
    private static final String TAG = "APIServiceWrapper";
    public static final String HOME_API = "home_api";
    public static final String STORE_API = "store_api";
    public static final String NEWS_API = "news_api";
    public static final String CHECKING = "checking";
    public static final String PIP_API = "pip_api";

    private volatile static APIServiceWrapper instance;

    public static APIServiceWrapper getInstance() {
        if (instance == null) {
            synchronized (APIServiceWrapper.class) {
                instance = new APIServiceWrapper();
            }
        }

        return instance;
    }

    // Phải clear thread sau khi đa hoàn tất lấy api hoặc khi activity destroy
    public static void clearTag(String tag) {
        AndroidNetworking.cancel(tag);
    }

    // Checking info user throw api
    public void checking(Context context) {
        String url = "https://ipinfo.io/json";
        AndroidNetworking.get(url).setTag(CHECKING).build().getAsObject(Checking.class, new ParsedRequestListener<Checking>() {
            @Override
            public void onResponse(Checking o) {
                MyTrackingFireBase.trackingCountry(context, o);
            }

            @Override
            public void onError(ANError anError) {

            }
        });
    }

    public interface IHomeLoad {
        void loadDone(ArrayList<BannerOfHome> list);

        void error();

        void getMaterial(String api);

        void getFont(String api);
    }

    // load Api home cho banner và shop home
    public void loadAPIHome(IHomeLoad iHomeLoad, Context context) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, "dataApi");
        String d = sharedPrefs.get("Home", String.class);
        ArrayList<BannerOfHome> list = new ArrayList<>();

        if (NetworkUtils.checkNetworkAvailable(context) || d.isEmpty()) {
            String url = Constant.URL_BANNER + "?t=" + System.currentTimeMillis();
            AndroidNetworking.get(url).setTag(HOME_API).build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String response) {
                    try {
                        String key = logger.getKeyData(context);
                        String data1 = ex.getStApp3(key);
                        String de = se.decrypt(response, data1);
                        sharedPrefs.put("Home", de); // cache data from api
                        DataResponseItem item = (DataResponseItem) ParseUtil.getParserFactory()
                                .getObject(de, DataResponseItem.class);
                        if (item != null && item.getData() != null) {
                            for (DataItem dataItem : item.getData()) {
                                for (DataSpecShop data : dataItem.getDataSpecShops()) {
                                    list.add(new BannerOfHome(item.getUrlRoot().concat(data.getUrlThumb()), data.getTitle()));
                                }
                            }
                            iHomeLoad.getMaterial(item.getUrlSticker()); // get url của sticker shop
                            iHomeLoad.getFont(item.getUrlFont()); // get url của font shop
                        } else {
                            list.add(new BannerOfHome("", ""));
                        }
                        iHomeLoad.loadDone(list); // get item cho banner ở màn hình home
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    iHomeLoad.getMaterial("");
                    iHomeLoad.getFont("");
                    iHomeLoad.error();
                }
            });
        } else {
            // nếu không có mạng thì sử dụng data được cache trong share pref để hiện thị cho user
            d = sharedPrefs.get("Home", String.class);
            DataResponseItem item = (DataResponseItem) ParseUtil.getParserFactory()
                    .getObject(d, DataResponseItem.class);
            if (item != null && item.getData() != null) {
                for (DataItem dataItem : item.getData()) {
                    for (DataSpecShop data : dataItem.getDataSpecShops()) {
                        list.add(new BannerOfHome(item.getUrlRoot().concat(data.getUrlThumb()), data.getTitle()));
                    }
                }
                iHomeLoad.getMaterial(item.getUrlSticker());
            } else {
                list.add(new BannerOfHome("", ""));
            }
            iHomeLoad.loadDone(list);
        }
    }

    public interface ILoadNews {
        void loadDone(ArrayList<DataNews> news);

        void loadError();
    }

    // load api news để hiển thị ở màn hình home
    public void loadAPINews(ILoadNews iLoadNews, Context context) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, "dataApi");
        String d = sharedPrefs.get("NewsApi", String.class);
        ArrayList<DataNews> news = new ArrayList<>();
        if (NetworkUtils.checkNetworkAvailable(context) || d.isEmpty()) {
            String url = Constant.URL_NEWS + "?t=" + System.currentTimeMillis();
            AndroidNetworking.get(url).setTag(NEWS_API).build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String response) {
                    try {
                        String key = logger.getKeyData(context);
                        String data1 = ex.getStApp3(key);
                        String de = se.decrypt(response, data1);
                        sharedPrefs.put("NewsApi", de); // cache data
                        DataResponseNews item = (DataResponseNews) ParseUtil.getParserFactory()
                                .getObject(de, DataResponseNews.class);
                        if (item != null && item.getData() != null) {
                            for (DataNews dataItem : item.getData()) {
                                dataItem.setThumbMagazine(item.getUrlRoot());
                                news.add(dataItem);
                            }
                            iLoadNews.loadDone(news);
                        }
                    } catch (Exception e) {
                        iLoadNews.loadError();
                        Log.d(TAG, "onResponse: " + e.getMessage());
                    }
                }

                @Override
                public void onError(ANError anError) {
                    iLoadNews.loadError();
                }
            });
        } else {
            // nếu không có mạng thì sử dụng data được cache trong share pref để hiện thị cho user
            d = sharedPrefs.get("NewsApi", String.class);
            DataResponseNews item = (DataResponseNews) ParseUtil.getParserFactory()
                    .getObject(d, DataResponseNews.class);
            if (item != null && item.getData() != null) {
                for (DataNews dataItem : item.getData()) {
                    dataItem.setThumbMagazine(item.getUrlRoot());
                    news.add(dataItem);
                }
                iLoadNews.loadDone(news);
            }
        }
    }

    public interface ILoadStoreHomeDone {
        void done(List<String> themes, List<String> urls);

        void error();
    }

    // load api store
    public void loadAPIStore(ILoadStoreHomeDone iLoadStoreHomeDone, Context context) {
        String url = Constant.URL_CONTENT + "?t=" + System.currentTimeMillis();
        AndroidNetworking.get(url).setTag(STORE_API).build().getAsString(new StringRequestListener() {

            @Override
            public void onResponse(String response) {
                try {
                    String key = logger.getKeyData(context);
                    String data = ex.getStApp3(key);
                    String de = se.decrypt(response, data);

                    DataResponseHome item = (DataResponseHome) ParseUtil.getParserFactory()
                            .getObject(de, DataResponseHome.class);
                    List<String> themes = new ArrayList<>();
                    List<String> urls = new ArrayList<>();
                    if (item != null && item.data != null) {
                        for (DataHome dataHome : item.data) {
                            themes.add(dataHome.getEffectName());
                            urls.add(dataHome.getUrlRequest());
                        }
                    } else {
                        themes.add("");
                        urls.add("");
                    }
                    themes.remove(0);
                    urls.remove(0);
                    iLoadStoreHomeDone.done(themes, urls);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ANError anError) {
                Log.d(TAG, "onError: " + anError.getErrorCode());
                iLoadStoreHomeDone.error();
            }
        });
    }

    public interface ILoadEachStoreDone {
        void done(ArrayList<ItemShop> itemShops);

        void error();
    }

    // load riêng biệt từng api của từng theme
    public void loadAPIEachStore(String nameApi, ArrayList<ItemShop> itemShops, String theme, ILoadEachStoreDone iLoadEachStoreDone, Context context) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, "dataApi");
        String d = sharedPrefs.get(theme, String.class);

        if (NetworkUtils.checkNetworkAvailable(context) || d.isEmpty()) {
            String url = nameApi + "?t=" + System.currentTimeMillis();
            AndroidNetworking.get(url).setTag(theme).build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String response) {
                    try {
                        String key = logger.getKeyData(context);
                        String data1 = ex.getStApp3(key);
                        String de = se.decrypt(response, data1);
                        sharedPrefs.put(theme, de); // cache data

                        DataResponseItem item = (DataResponseItem) ParseUtil.getParserFactory()
                                .getObject(de, DataResponseItem.class);

                        if (item != null && item.getData() != null) {
                            for (DataItem data : item.getData()) {
                                ArrayList<DataSpecShop> path = new ArrayList<>();

                                for (DataSpecShop dataSpecShop : data.getDataSpecShops()) {
                                    if (theme.equals(Constant.GRADIENT) || theme.equals(Constant.PALETTE)) {
                                        dataSpecShop.setUrlThumb(dataSpecShop.getUrlThumb());
                                        path.add(dataSpecShop);
                                    } else {
                                        dataSpecShop.setUrlThumb(item.getUrlRoot().concat(dataSpecShop.getUrlThumb()));
                                        path.add(dataSpecShop);
                                    }
                                }
                                itemShops.add(new ItemShop(data.getEventName(), false,
                                        data.getIsPro(), path, item.getUrlRoot() + data.getUrlZip(),
                                        item.getUrlRoot() + data.getUrlThumb(), item.getUrlRoot()));
                            }
                            iLoadEachStoreDone.done(itemShops);
                        } else {
                            Log.d(TAG, "onResponse: data null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    Log.d(TAG, "onError: " + anError.getErrorCode());
                    iLoadEachStoreDone.error();
                }
            });
        } else {
            // nếu không có mạng thì sử dụng data được cache trong share pref để hiện thị cho user
            d = sharedPrefs.get(theme, String.class);
            DataResponseItem item = (DataResponseItem) ParseUtil.getParserFactory()
                    .getObject(d, DataResponseItem.class);

            if (item != null && item.getData() != null) {
                for (DataItem data : item.getData()) {
                    ArrayList<DataSpecShop> path = new ArrayList<>();

                    for (DataSpecShop dataSpecShop : data.getDataSpecShops()) {
                        if (theme.equals(Constant.GRADIENT) || theme.equals(Constant.PALETTE)) {
                            dataSpecShop.setUrlThumb(dataSpecShop.getUrlThumb());
                            path.add(dataSpecShop);
                        } else {
                            dataSpecShop.setUrlThumb(item.getUrlRoot().concat(dataSpecShop.getUrlThumb()));
                            path.add(dataSpecShop);
                        }
                    }
                    itemShops.add(new ItemShop(data.getEventName(), false,
                            data.getIsPro(), path, item.getUrlRoot() + data.getUrlZip(),
                            item.getUrlRoot() + data.getUrlThumb(), item.getUrlRoot()));
                    iLoadEachStoreDone.done(itemShops);
                }
            } else {
                Log.d(TAG, "onResponse: data null");
            }
        }
    }

    public interface ILoadQuote {
        void loadDone(ArrayList<ThemeQuote> themeQuotes);

        void error();
    }

    // load api quote
    public void loadAPIQuote(ILoadQuote iLoadQuote, Context context) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, "dataApi");
        String d = sharedPrefs.get("QuoteApi", String.class);
        if (NetworkUtils.checkNetworkAvailable(context) || d.isEmpty()) {
            String url = Constant.URL_QUOTES + "?t=" + System.currentTimeMillis();
            AndroidNetworking.get(url).setTag(Constant.QUOTE).build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String response) {
                    try {
                        String key = logger.getKeyData(context);
                        String data1 = ex.getStApp3(key);
                        String de = se.decrypt(response, data1);

                        sharedPrefs.put("QuoteApi", de); // cache data
                        DataResponseItem item = (DataResponseItem) ParseUtil.getParserFactory()
                                .getObject(de, DataResponseItem.class);
                        if (item != null && item.getData() != null) {
                            ArrayList<ThemeQuote> themeQuotes = new ArrayList<>();
                            for (DataItem data : item.getData()) {
                                ThemeQuote themeQuote = new ThemeQuote();
                                ArrayList<Quote> quotes = new ArrayList<>();
                                for (DataSpecShop dataSpecShop : data.getDataSpecShops()) {
                                    quotes.add(new Quote(data.getEventName(), dataSpecShop.getTitle(), item.getUrlRoot().concat(dataSpecShop.getUrlThumb())));
                                }
                                themeQuote.setQuotes(quotes);
                                themeQuote.setNameTheme(data.getEventName());
                                themeQuotes.add(themeQuote);
                            }
                            iLoadQuote.loadDone(themeQuotes);

                        } else {
                            Log.d(TAG, "onResponse: data null");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    Log.d(TAG, "onError: " + anError.getErrorCode());
                    iLoadQuote.error();
                }
            });
        } else {
            // nếu không có mạng thì sử dụng data được cache trong share pref để hiện thị cho user
            d = sharedPrefs.get("QuoteApi", String.class);
            DataResponseItem item = (DataResponseItem) ParseUtil.getParserFactory()
                    .getObject(d, DataResponseItem.class);
            if (item != null && item.getData() != null) {
                ArrayList<ThemeQuote> themeQuotes = new ArrayList<>();
                for (DataItem data : item.getData()) {
                    ThemeQuote themeQuote = new ThemeQuote();
                    ArrayList<Quote> quotes = new ArrayList<>();
                    for (DataSpecShop dataSpecShop : data.getDataSpecShops()) {
                        quotes.add(new Quote(data.getEventName(), dataSpecShop.getTitle(), item.getUrlRoot().concat(dataSpecShop.getUrlThumb())));
                    }
                    themeQuote.setQuotes(quotes);
                    themeQuote.setNameTheme(data.getEventName());
                    themeQuotes.add(themeQuote);
                }
                iLoadQuote.loadDone(themeQuotes);
            } else {
                iLoadQuote.error();
            }
        }
    }

    // 16/03/2020: interface for load Pip Api
    public interface IPipAPIListener {
        void loadDone(ArrayList<PipPicture> pipPictures);

        void error(String error);
    }

    // 16/03/2020: load api pip mode
    public void loadAPIPipMode(Context context, IPipAPIListener iPipAPIListener) {
        SharedPrefsImpl sharedPrefs = new SharedPrefsImpl(context, "dataApi");
        String d = sharedPrefs.get("PipApi", String.class);

        String url = Constant.URL_PIP + "?t=" + System.currentTimeMillis();
        if (NetworkUtils.checkNetworkAvailable(context) || d.isEmpty()) {
            AndroidNetworking.get(url).setTag(PIP_API).build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String response) {
                    try {
                        String key = logger.getKeyData(context);
                        String data1 = ex.getStApp3(key);
                        String de = se.decrypt(response, data1);

                        sharedPrefs.put("PipApi", de);
                        Data item = (Data) ParseUtil.getParserFactory()
                                .getObject(de, Data.class);
                        if (item != null && !item.getData().isEmpty()) {
                            String urlRoot = item.getUrlRoot();
                            ArrayList<PipPicture> pipPictures = new ArrayList<>(); // list of pip picture
                            for (DataPIP dataPIP : item.getData()) {
                                for (Content content : dataPIP.getContent()) {
                                    content.setZipFile(urlRoot + dataPIP.getPath() + (dataPIP.getContent().indexOf(content) + 1) + content.getZipFile()); // link to download
                                    content.setUrlPreview(urlRoot + dataPIP.getPath() + (dataPIP.getContent().indexOf(content) + 1) + content.getUrlPreview()); // url preview
                                    pipPictures.add(convertPip(content)); // add picture
                                }
                            }
                            iPipAPIListener.loadDone(pipPictures);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(ANError anError) {
                    iPipAPIListener.error(anError.getMessage());
                    iPipAPIListener.loadDone(new ArrayList<>());
                }
            });
        } else {
            // nếu không có mạng thì sử dụng data được cache trong share pref để hiện thị cho user
            Data item = (Data) ParseUtil.getParserFactory()
                    .getObject(d, Data.class);
            if (item != null && !item.getData().isEmpty()) {
                String urlRoot = item.getUrlRoot();
                ArrayList<PipPicture> pipPictures = new ArrayList<>(); // list of pip picture
                for (DataPIP dataPIP : item.getData()) {
                    for (Content content : dataPIP.getContent()) {
                        content.setZipFile(urlRoot + dataPIP.getPath() + (dataPIP.getContent().indexOf(content) + 1) + content.getZipFile()); // link to download
                        content.setUrlPreview(urlRoot + dataPIP.getPath() + (dataPIP.getContent().indexOf(content) + 1) + content.getUrlPreview()); // url preview
                        pipPictures.add(convertPip(content)); // add picture
                    }
                }
                iPipAPIListener.loadDone(pipPictures);
            }
        }
    }

    // convert content to pip picture
    private PipPicture convertPip(Content content) {
        PipPicture pipPicture = new PipPicture();
        pipPicture.setPathPreview(content.getUrlPreview());
        pipPicture.setPathPreviewOnline(content.getUrlPreview());
        pipPicture.setType(content.getImgMask().size());
        pipPicture.setPro(content.isPro());
        pipPicture.setLinkToDownload(content.getZipFile());
        pipPicture.setId(content.getId());
        pipPicture.setDownloaded(false);
        pipPicture.setPick(false);
        return pipPicture;
    }
}
