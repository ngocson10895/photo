package com.piccollage.collagemaker.picphotomaker.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class FontTheme implements Parcelable {
    private String theme;
    private ArrayList<FontItem> fontItems;
    private boolean isDownload;
    private String urlZip;
    private boolean isPro;

    public FontTheme(String theme, ArrayList<FontItem> fontItems, boolean isDownload, String urlZip, boolean isPro) {
        this.theme = theme;
        this.fontItems = fontItems;
        this.isDownload = isDownload;
        this.urlZip = urlZip;
        this.isPro = isPro;
    }

    public boolean isPro() {
        return isPro;
    }

    private FontTheme(Parcel in) {
        theme = in.readString();
        fontItems = in.createTypedArrayList(FontItem.CREATOR);
        isDownload = in.readByte() != 0;
        urlZip = in.readString();
        isPro = in.readByte() != 0;
    }

    public static final Creator<FontTheme> CREATOR = new Creator<FontTheme>() {
        @Override
        public FontTheme createFromParcel(Parcel in) {
            return new FontTheme(in);
        }

        @Override
        public FontTheme[] newArray(int size) {
            return new FontTheme[size];
        }
    };

    public String getTheme() {
        return theme;
    }

    public List<FontItem> getFontItems() {
        return fontItems;
    }

    public boolean isDownload() {
        return isDownload;
    }

    public String getUrlZip() {
        return urlZip;
    }

    public void setDownload(boolean download) {
        isDownload = download;
    }

    @NonNull
    @Override
    public String toString() {
        return theme + " " + fontItems.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(theme);
        dest.writeTypedList(fontItems);
        dest.writeByte((byte) (isDownload ? 1 : 0));
        dest.writeString(urlZip);
        dest.writeByte((byte) (isPro ? 1 : 0));
    }
}
