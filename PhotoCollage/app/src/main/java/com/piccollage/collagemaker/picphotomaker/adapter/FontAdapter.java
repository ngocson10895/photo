package com.piccollage.collagemaker.picphotomaker.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.databinding.ItemFontBinding;
import com.piccollage.collagemaker.picphotomaker.entity.FontItem;
import com.piccollage.collagemaker.picphotomaker.utils.TextUtils;

import java.util.List;

/**
 * Font adapter cho phần pick font
 */
public class FontAdapter extends BaseQuickAdapter<FontItem, BaseViewHolder> {
    private Context context;

    public FontAdapter(@Nullable List<FontItem> data, Context context) {
        super(R.layout.item_font, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, FontItem item) {
        ItemFontBinding binding = ItemFontBinding.bind(helper.itemView);
        if (item.isPick()) {
            binding.tvName.setBackgroundColor(context.getResources().getColor(R.color.pink));
            binding.tvName.setTextColor(Color.WHITE);
        } else {
            binding.tvName.setTextColor(Color.BLACK);
            binding.tvName.setBackgroundColor(context.getResources().getColor(R.color.textItem));
        }
        binding.tvName.setText(item.getFontName());
        if (item.getFontPath().contains("http")) {
            Glide.with(context).load(item.getFontPath()).into(binding.ivBg);
            binding.tvIllustration.setVisibility(View.INVISIBLE);
            binding.ivBg.setVisibility(View.VISIBLE);
        } else {
            Typeface typeface = TextUtils.loadTypeface(context, item.getFontPath());
            binding.tvIllustration.setTypeface(typeface);
            binding.tvIllustration.setVisibility(View.VISIBLE);
            binding.ivBg.setVisibility(View.INVISIBLE);
        }
    }
}
