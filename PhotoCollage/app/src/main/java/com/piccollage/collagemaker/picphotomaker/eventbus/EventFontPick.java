package com.piccollage.collagemaker.picphotomaker.eventbus;

import com.piccollage.collagemaker.picphotomaker.entity.FontItem;

/**
 * event khi user lựa chọn font.
 */
public class EventFontPick {
    private FontItem fontItem;

    public EventFontPick(FontItem fontItem) {
        this.fontItem = fontItem;
    }

    public FontItem getFontItem() {
        return fontItem;
    }
}
