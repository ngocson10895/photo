package com.piccollage.collagemaker.picphotomaker.ui.settingactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.piccollage.collagemaker.picphotomaker.R;
import com.piccollage.collagemaker.picphotomaker.adapter.ChangeLanguageAdapter;
import com.piccollage.collagemaker.picphotomaker.base.BaseDialog;
import com.piccollage.collagemaker.picphotomaker.base.LocaleHelper;
import com.piccollage.collagemaker.picphotomaker.databinding.DialogLanguageBinding;
import com.piccollage.collagemaker.picphotomaker.entity.Language;
import com.piccollage.collagemaker.picphotomaker.ui.DialogLoading;
import com.piccollage.collagemaker.picphotomaker.ui.SplashActivity;
import com.piccollage.collagemaker.picphotomaker.utils.MyTrackingFireBase;
import com.piccollage.collagemaker.picphotomaker.utils.TrackingUtils;
import com.piccollage.collagemaker.picphotomaker.utils.uui.WidthHeightScreen;

import java.util.ArrayList;
import java.util.Objects;

/**
 * dialog change language
 */
public class DialogChangeLanguage extends BaseDialog implements ChangeLanguageAdapter.ISelectLanguage {
    private ChangeLanguageAdapter adapter;
    private DialogLoading dialog;
    private String selectLanguage = "";
    private Activity activity;
    private DialogLanguageBinding binding;

    public DialogChangeLanguage(@NonNull Activity context) {
        super(context);
        this.activity = context;
    }

    @Override
    public void beforeSetContentView() {
        binding = DialogLanguageBinding.inflate(getLayoutInflater());
    }

    @Override
    public View getLayoutView() {
        return binding.getRoot();
    }

    @Override
    public void initView() {
        binding.rvLanguage.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        onViewClicked();
    }

    @Override
    public void initData() {
        selectLanguage = LocaleHelper.getLanguageCountryName(getContext());
        ArrayList<Language> languageArrayList = new ArrayList<>();
        for (String name : LocaleHelper.getListCountries()) {
            if (!name.equals(getContext().getString(R.string.auto))) {
                if (name.equals(LocaleHelper.getLanguageCountryName(getContext()))) {
                    languageArrayList.add(new Language(name, true));
                } else
                    languageArrayList.add(new Language(name, false));
            }
        }

        adapter = new ChangeLanguageAdapter(getContext(), languageArrayList);
        adapter.setiSelectLanguage(this);
        binding.rvLanguage.setAdapter(adapter);

        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
    }

    @Override
    public void customDialog() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.corner_dialog);
            try {
                int width = (int) (WidthHeightScreen.getScreenWidthInPixels(Objects.requireNonNull(getContext())) * 0.9);
                int height = (int) (WidthHeightScreen.getScreenHeightInPixels(getContext()) * 0.8);
                window.setLayout(width, height);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void show() {
        super.show();
        MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogChangeLanguage.NAME);
    }

    public void onViewClicked() {
        binding.tvDone.setOnClickListener(v -> {
            MyTrackingFireBase.trackingScreen(getContext(), TrackingUtils.TrackingConstant.DialogChangeLanguage.DONE);
            if (selectLanguage.equals(LocaleHelper.getLanguageCountryName(getContext()))) {
                dismiss();
            } else {
                getContext();
                dialog = new DialogLoading(getContext(), DialogLoading.CREATE);
                dialog.show();
                changeLanguage(LocaleHelper.getLanguageCountryName(getContext()), selectLanguage);
            }
        });
    }

    private static void restartApp(Activity activity) {
        if (activity != null) {
            Intent intent = new Intent(activity, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activity.startActivity(intent);
            Runtime.getRuntime().exit(0);
        }
    }

    private void changeLanguage(String languageSelected, String selectedLanguage) {
        LocaleHelper.setSelectedLanguage(getContext(), languageSelected);
        String languageCode = LocaleHelper.getLanguageCodeByDisplayName(selectedLanguage);
        LocaleHelper.setNewLocale(getContext(), languageCode);
        LocaleHelper.applyLanguageForApplicationContext(languageCode);
        restartToApplyLanguage();
    }

    private void restartToApplyLanguage() {
        dismiss();
        new Handler().postDelayed(() -> {
            restartApp(activity);
            dialog.dismiss();
            dismiss();
        }, 1000);
    }

    @Override
    public void select(String selectLanguage) {
        this.selectLanguage = selectLanguage;
    }
}
