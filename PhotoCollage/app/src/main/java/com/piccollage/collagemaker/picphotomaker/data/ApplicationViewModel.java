package com.piccollage.collagemaker.picphotomaker.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.piccollage.collagemaker.picphotomaker.entity.ItemDownloaded;
import com.piccollage.collagemaker.picphotomaker.entity.PipPicture;

import java.util.List;

/**
 * Create viewmodel. chứa các livedata để cập nhập ui khi dữ liệu thay đổi
 */
public class ApplicationViewModel extends AndroidViewModel {
    private ApplicationRepo applicationRepo;
    private LiveData<List<ItemDownloaded>> allItemsDownloaded;
    private LiveData<List<ItemDownloaded>> dataGradients;
    private LiveData<List<ItemDownloaded>> dataPatterns;
    private LiveData<List<ItemDownloaded>> dataStickers;
    private LiveData<List<ItemDownloaded>> dataFonts;
    private LiveData<List<ItemDownloaded>> dataPalettes;
    private LiveData<List<PipPicture>> pipPictures;

    public ApplicationViewModel(@NonNull Application application) {
        super(application);
        applicationRepo = new ApplicationRepo(application);
        allItemsDownloaded = applicationRepo.getAllData();
        dataGradients = applicationRepo.getDataGradients();
        dataPatterns = applicationRepo.getDataPatterns();
        dataStickers = applicationRepo.getDataStickers();
        dataFonts = applicationRepo.getDataFonts();
        dataPalettes = applicationRepo.getDataPalettes();
        pipPictures = applicationRepo.getPipPictures();
    }

    public LiveData<List<PipPicture>> getPipPictures() {
        return pipPictures;
    }

    public void insertPip(PipPicture pipPicture){
        applicationRepo.insertPIP(pipPicture);
    }

    public void insert(ItemDownloaded itemDownloaded) {
        applicationRepo.insertItem(itemDownloaded);
    }

    public void delete(ItemDownloaded itemDownloaded) {
        applicationRepo.deleteItem(itemDownloaded);
    }

    public void deleteAll() {
        applicationRepo.deleteAll();
    }

    public LiveData<List<ItemDownloaded>> getAllData() {
        return allItemsDownloaded;
    }

    public LiveData<List<ItemDownloaded>> getDataGradients() {
        return dataGradients;
    }

    public LiveData<List<ItemDownloaded>> getDataPatterns() {
        return dataPatterns;
    }

    public LiveData<List<ItemDownloaded>> getDataStickers() {
        return dataStickers;
    }

    public LiveData<List<ItemDownloaded>> getDataFonts() {
        return dataFonts;
    }

    public LiveData<List<ItemDownloaded>> getDataPalettes() {
        return dataPalettes;
    }
}
