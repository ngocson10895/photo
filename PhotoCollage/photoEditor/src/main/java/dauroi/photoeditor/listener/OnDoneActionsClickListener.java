package dauroi.photoeditor.listener;

import android.content.Context;

public interface OnDoneActionsClickListener {
	void onDoneButtonClick(Context context);

	void onApplyButtonClick(Context context);
}
