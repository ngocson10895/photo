package dauroi.photoeditor.ui.activity.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import dauroi.photoeditor.R;

//import android.support.annotation.NonNull;

public class DialogLeaveFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "DialogLeaveFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_leave_editor, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        TextView tvOk = view.findViewById(R.id.btn_ok);
        tvOk.setOnClickListener(this);

        TextView tvCancel = view.findViewById(R.id.btn_cancel);
        tvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_ok) {
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        } else if (i == R.id.btn_cancel) {
            dismiss();
        }
    }
}
