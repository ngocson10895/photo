package dauroi.photoeditor.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
//import androidx.core.content.FileProvider;
import androidx.core.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;

import dauroi.photoeditor.R;
//import dauroi.photoeditor.ads.BannerAdsUtils;
//import dauroi.photoeditor.ads.InterstitialAdsUtils;
import dauroi.photoeditor.utils.PhotoUtils;

public class PreviewActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "PreviewActivity";
    private ImageView image;
    private TextView tvShare, tvSave;
    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        initView();
        initData();
    }

    private void initData() {
        if (getIntent() != null) {
            String imagePath = getIntent().getStringExtra("image");
            Log.d(TAG, "imagePath: " + imagePath);
            file = new File(imagePath);
            if (file.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                Glide.with(this).load(myBitmap).into(image);
            }
        }
    }

    private void initView() {
        image = findViewById(R.id.iv_image);

        tvShare = findViewById(R.id.tv_share);
        tvShare.setOnClickListener(this);

        tvSave = findViewById(R.id.tv_save);
        tvSave.setOnClickListener(this);

        ImageView ivClose = findViewById(R.id.iv_close);
        ivClose.setOnClickListener(this);

        RelativeLayout relativeLayout = findViewById(R.id.adView);
//        BannerAdsUtils.getInstance().loadAds(relativeLayout, this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_save) {
            saveImage();
        } else if (v.getId() == R.id.tv_share) {
            shareImage();
        } else if (v.getId() == R.id.iv_close) {
            finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
    }

    private void shareImage() {
        Uri photoURI = FileProvider.getUriForFile(this,
                "com.piccollage.collagemaker.picphotomaker.provider",
                file);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/png");
        share.putExtra(Intent.EXTRA_STREAM, photoURI);
        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(share, getString(R.string.photo_editor_share_image)));
    }

    private void saveImage() {
//        final ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Please waiting .....");
//        InterstitialAdsUtils.getInstances().showAdIfNeeded(this, new InterstitialAdsUtils.InterstitialListener() {
//            @Override
//            public void showLoading() {
//                Log.d(TAG, "showLoading: ");
//                dialog.show();
//            }
//
//            @Override
//            public void hideLoading() {
//                Log.d(TAG, "hideLoading: ");
//                PhotoUtils.addImageToGallery(file.getAbsolutePath(), PreviewActivity.this);
//                Log.d(TAG, "saveImage: " + file.getAbsolutePath());
//                Toast.makeText(PreviewActivity.this, "Save Done", Toast.LENGTH_SHORT).show();
//                dialog.dismiss();
//            }
//        });
        PhotoUtils.addImageToGallery(file.getAbsolutePath(), PreviewActivity.this);
        Log.d(TAG, "saveImage: " + file.getAbsolutePath());
        Toast.makeText(PreviewActivity.this, "Save Done", Toast.LENGTH_SHORT).show();
    }
}
