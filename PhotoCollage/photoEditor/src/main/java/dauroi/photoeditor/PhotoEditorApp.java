package dauroi.photoeditor;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

public class PhotoEditorApp extends MultiDexApplication {
	private static Context context;
	private static PhotoEditorApp instance;

	public static PhotoEditorApp getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		context = this;
	}

	public static Context getAppContext() {
		return context;
	}
}
