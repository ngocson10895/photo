package dauroi.photoeditor.actions;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;

import dauroi.photoeditor.listener.OnDoneActionsClickListener;
import dauroi.photoeditor.ui.activity.ImageProcessingActivity;
import dauroi.photoeditor.ui.activity.PreviewActivity;
import dauroi.photoeditor.utils.DateTimeUtils;
import dauroi.photoeditor.utils.PhotoUtils;
import dauroi.photoeditor.utils.Utils;

public abstract class BaseAction implements OnDoneActionsClickListener {
    private static final String TAG = "BaseAction";
    protected ImageProcessingActivity mActivity;
    protected View mRootActionView;
    private boolean mAttached = false;

    public BaseAction(ImageProcessingActivity activity) {
        mActivity = activity;
        onInit();
        mRootActionView = inflateMenuView();
    }

    private void logToFacebook() {
        Bundle parameters = new Bundle();
        parameters.putString("actionName", getActionName());
    }

    protected void onInit() {

    }

    public void attach() {
        logToFacebook();
        mActivity.setDoneActionsClickListener(this);
        if (mRootActionView != null) {
            BaseAction action = mActivity.getCurrentAction();
            if (action != null) {
                action.onDetach();
                action.mAttached = false;
            }

            mActivity.attachBottomMenu(mRootActionView);
            mActivity.setCurrentAction(this);
            mAttached = true;
        }
    }

    public void onDetach() {

    }

    public boolean isAttached() {
        return mAttached;
    }

    public void done(final Context context) {
        if (mActivity.isEditingImage() && mActivity.getEditingImagePath() != null && mActivity.getEditingImagePath().length() > 0) {
            String path = mActivity.getEditingImagePath();
            File oldFile = new File(path);
            oldFile.delete();
            File oldWhiteFile = new File(path.substring(0, path.length() - 4).concat(PhotoUtils.EDITED_WHITE_IMAGE_SUFFIX));
            oldWhiteFile.delete();
            File thumbnail = new File(Utils.EDITED_IMAGE_THUMBNAIL_FOLDER, oldFile.getName());
            thumbnail.delete();
        }

        File out = new File(
                Utils.EDITED_IMAGE_FOLDER + DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png"));
        File parentFile = out.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }

        try {
            mActivity.getImage().compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(out));
        } catch (Exception ignored) {
        }
        Intent intent = new Intent(context, PreviewActivity.class);
        intent.putExtra("image", out.getAbsolutePath());
        context.startActivity(intent);

    }

    public abstract void apply(final boolean finish, Context context);

    /**
     * Create root action view and find child widgets
     */
    public abstract View inflateMenuView();

    public abstract String getActionName();

    /**
     * Should be call to save instance state
     *
     * @param bundle
     */
    public void saveInstanceState(Bundle bundle) {
        bundle.putBoolean("dauroi.photoeditor.actions.".concat(getActionName()).concat(".mAttached"), mAttached);
    }

    /**
     * Should be call before calling attach() method if has saved instance
     * state.
     *
     * @param bundle
     */
    public void restoreInstanceState(Bundle bundle) {
        mAttached = bundle.getBoolean("dauroi.photoeditor.actions.".concat(getActionName()).concat(".mAttached"), mAttached);
    }

    public void onActivityResume() {

    }

    public void onActivityPause() {

    }

    public void onActivityDestroy() {

    }

    @Override
    public void onDoneButtonClick(Context context) {
        apply(true, context);
    }

    @Override
    public void onApplyButtonClick(Context context) {
        apply(false, context);
    }

    public void onClicked() {
//        if (mActivity.getAdCreator() != null)
//            mActivity.getAdCreator().onClicked();
    }
}
