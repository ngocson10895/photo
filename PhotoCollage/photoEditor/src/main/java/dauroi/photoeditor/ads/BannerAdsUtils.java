//package dauroi.photoeditor.ads;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.AdSize;
//import com.google.android.gms.ads.AdView;
//
//public class BannerAdsUtils {
//    private static final String TAG = "BannerAdsUtils";
//    private static BannerAdsUtils instance;
//
//    public static BannerAdsUtils getInstance() {
//        if (instance == null) {
//            instance = new BannerAdsUtils();
//        }
//        return instance;
//    }
//
//    private static int countReloadBanner = 0;
//
//    private final String[] arrIdAdsBannerOpenApp = {
//            "ca-app-pub-9820030150756925/7773278197",
//            "ca-app-pub-9820030150756925/8757334601",
//            "ca-app-pub-9820030150756925/5394526863",
//            "ca-app-pub-9820030150756925/5394526863",
//            "ca-app-pub-9820030150756925/8565762919"
//    };
//
//    public void loadAds(final ViewGroup container, final Context context) {
//        String[] ads_id = arrIdAdsBannerOpenApp;
//        if (ads_id == null || ads_id.length == 0 || countReloadBanner >= ads_id.length) {
//            return;
//        }
//        final AdView adView = new AdView(context);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.setAdSize(AdSize.SMART_BANNER);
//        adView.setAdUnitId(arrIdAdsBannerOpenApp[countReloadBanner]);
//        adView.loadAd(adRequest);
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdFailedToLoad(int i) {
//                super.onAdFailedToLoad(i);
//                countReloadBanner++;
//                loadAds(container, context);
//                Log.d(TAG, "onAdFailedToLoad: ");
//            }
//
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                Log.d(TAG, "onAdLoaded: ");
//                addBannerAdsToContainer(container, adView);
//                countReloadBanner = 0;
//            }
//        });
//    }
//
//    private static void addBannerAdsToContainer(final ViewGroup container, final AdView adView) {
//        if (container == null || adView == null) {
//            return;
//        }
//        try {
//            if (adView.getParent() != null) {
//                ((ViewGroup) adView.getParent()).removeAllViews();
//            }
//            container.setVisibility(View.VISIBLE);
//            container.removeAllViews();
//            container.addView(adView);
//        } catch (Exception e) {
//            Log.d(TAG, "addBannerAdsToContainer: " + e.toString());
//        }
//    }
//
//}
