//package dauroi.photoeditor.ads;
//
//import android.content.Context;
//import android.os.Bundle;
//
//import com.google.ads.mediation.admob.AdMobAdapter;
//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.InterstitialAd;
//
//public class InterstitialAdsUtils {
//
//    public static InterstitialAdsUtils instances;
//
//    public static InterstitialAdsUtils getInstances() {
//        if (null == instances) {
//            instances = new InterstitialAdsUtils();
//        }
//        return instances;
//    }
//
//    public AdRequest getAdRequest() {
//        Bundle extras = new Bundle();
//        return new AdRequest.Builder()
//                .addNetworkExtrasBundle(AdMobAdapter.class, extras)//mediation
//                .build();
//    }
//
//    private int countReloadInterstitial = 0;
//
//    private String[] arrIdAdsInterstitialOpenApp = {
//            "ca-app-pub-9820030150756925/7006991433",
//            "ca-app-pub-9820030150756925/6869537862",
//            "ca-app-pub-9820030150756925/3315158438",
//            "ca-app-pub-9820030150756925/8375913428",
//            "ca-app-pub-9820030150756925/3123586740"
//    };
//
//    private void loadAd(final Context context, final InterstitialListener listener) {
//        if (context == null || listener == null) {
//            return;
//        }
//        String[] ads_id = arrIdAdsInterstitialOpenApp;
//        if (ads_id == null || ads_id.length == 0 || countReloadInterstitial >= ads_id.length) {
//            listener.hideLoading();
//            return;
//        }
//        final InterstitialAd adView = new InterstitialAd(context);
//        adView.setAdUnitId(ads_id[countReloadInterstitial]);
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdClosed() {
//                super.onAdClosed();
//            }
//
//            @Override
//            public void onAdLoaded() {
//                super.onAdLoaded();
//                listener.hideLoading();
//                adView.show();
//            }
//
//            @Override
//            public void onAdFailedToLoad(int i) {
//                super.onAdFailedToLoad(i);
//                countReloadInterstitial++;
//                loadAd(context, listener);
//            }
//        });
//        adView.loadAd(getAdRequest());
//    }
//
//    public void showAdIfNeeded(Context context, InterstitialListener listener) {
////        if (Utils.isAppPurchase(context)) {
////            return;
////        }
//        listener.showLoading();
//        loadAd(context, listener);
//    }
//
//    public interface InterstitialListener {
//        void showLoading();
//
//        void hideLoading();
//    }
//}
