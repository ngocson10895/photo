package dauroi.photoeditor.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import dauroi.photoeditor.utils.NetworkUtils;

public class NetworkStateReceiver extends BroadcastReceiver {
    public static interface NetworkStateReceiverListener {
        void onNetworkAvailable();

        void onNetworkUnavailable();
    }

    private static List<NetworkStateReceiverListener> networkStateListeners = new ArrayList<>();
    private static boolean connected;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getExtras() == null) {
            connected = false;
        } else {
            connected = NetworkUtils.checkNetworkAvailable(context);
            notifyStateToAll();
        }
    }

    private void notifyStateToAll() {
        for (NetworkStateReceiverListener listener : networkStateListeners)
            notifyState(listener);
    }

    private static void notifyState(NetworkStateReceiverListener listener) {
        if (!connected || listener == null)
            return;

        listener.onNetworkAvailable();
    }

    public static void addListener(NetworkStateReceiverListener l) {
        networkStateListeners.add(l);
        notifyState(l);
    }

    public static void removeListener(NetworkStateReceiverListener l) {
        networkStateListeners.remove(l);
    }

    public static void clear() {
        connected = false;
        networkStateListeners.clear();
    }
}
