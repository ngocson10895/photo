# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/MinThu/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn com.evernote.android.job.gcm.**
-dontwarn com.evernote.android.job.util.GcmAvailableHelper

-keep public class com.evernote.android.job.v21.PlatformJobService
-keep public class com.evernote.android.job.v14.PlatformAlarmService
-keep public class com.evernote.android.job.v14.PlatformAlarmReceiver
-keep public class com.evernote.android.job.JobBootReceiver
-keep public class com.evernote.android.job.JobRescheduleService

-keep class com.google.android.gms.corebase.** { *; } # All classes in the com.example package
-keep class com.startapp.android.publish.**{ *; }
-keep class com.commonsware.cwac.wakeful.**{ *; }
-keep class com.androidnetworking.**{ *; }
-keep class com.google.gson.**{ *; }
-keep class com.google.android.exoplayer2.**{ *; }
-keep class com.facebook.ads.**{ *; }
-dontwarn okio.**
-dontwarn com.facebook.ads.internal.**
-keeppackagenames com.facebook.*

-keep class com.startapp.** {
      *;
}

-keep class com.truenet.** {
      *;
}

-keepattributes Exceptions, InnerClasses, Signature, Deprecated, SourceFile,LineNumberTable, *Annotation*, EnclosingMethod
-dontwarn android.webkit.JavascriptInterface
-dontwarn com.startapp.**

-dontwarn org.jetbrains.annotations.**
