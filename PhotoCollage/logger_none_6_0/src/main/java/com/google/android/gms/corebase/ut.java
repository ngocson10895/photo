package com.google.android.gms.corebase;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;

//import android.support.annotation.NonNull;

import com.facebook.ads.DisplayAdController;
import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class ut {//utils
//    public static boolean isConnectInternet(Context context) {
//		return isConnectInternet(context);
//	}
	public static boolean isConnectInternet(Context context) {
		if(context == null)
			return false;
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null) { // connected to the internet
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
				// connected to wifi
				return true;
			} else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
				// connected to the mobile provider's data plan
				return true;
			}
		} else {
			return false;
		}
		return false;
	}

    public static boolean isAppPurchased(Context context) {
//        if (BuildConfig.DEBUG)
//            return true; //just for testing
       return sh.getInstance(context).getInAppPurchase();
    }

	public static AdView getInstancesSmartBannerAdsView(Context context, int count) {
		final AdView adView = new AdView(context);
		adView.setAdSize(AdSize.SMART_BANNER);
//        if (!BuildConfig.DEBUG) {
		adView.setAdUnitId(logger.getAdmob_banner_home0(context)[count]);
//        } else {
//            adView.setAdUnitId(admob_id_banner_ads_unit_test);
//        }

		adView.setVisibility(View.VISIBLE);
		adView.loadAd(DisplayAdController.getDefaultAdRequest());
		return adView;
	}

	public static void inflateSmartBannerAds(Context context, final ViewGroup container) {
		if (isAppPurchased(context)) {
			lg.logs("App is purchased", "not showing ads");
			return;
		}
		if (context == null || container == null)
			return;
		inflateSmartBannerAds(context, container, View.GONE, 0);

	}

	public static void inflateSmartBannerAds(final Context context, final ViewGroup container, final int visibilityOnFail,final int count) {
		if (isAppPurchased(context)) {
			lg.logs("App is purchased", "not showing ads");
			return;
		}
		if (count >= logger.getAdmob_banner_home0(context).length) {
			return;
		}
		container.setVisibility(visibilityOnFail);
		AdView smartBannerAdsView = getInstancesSmartBannerAdsView(context, count);
		container.removeAllViews();
		if (smartBannerAdsView.getParent() != null) {
			((ViewGroup) smartBannerAdsView.getParent()).removeView(smartBannerAdsView);
		}
		smartBannerAdsView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				container.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAdFailedToLoad(int i) {
				inflateSmartBannerAds(context, container, visibilityOnFail, count + 1);
			}
		});
		container.addView(smartBannerAdsView);
	}

//	@NonNull
//	public static AdRequest getDefaultAdRequest(Context context) {
//		//mediation
//		Bundle extras = new Bundle();
//		boolean isPass = sh.getInstance(context).getUserConsent();
//		if (!isPass)
//			extras.putString("npa", "1");
//		String content_rate = "T";
//		if(sh.getInstance(context)!=null &&
//				sh.getInstance(context).getUapps()!=null &&
//				sh.getInstance(context).getUapps().getAdmob_fil() != null){
//			content_rate = sh.getInstance(context).getUapps().getAdmob_fil();
//		}
//		extras.putString("max_ad_content_rating", content_rate);
//
////        AdRequest adRequest = new AdRequest.Builder()
////                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
////                .build();
//		//boolean mediation = false;
//		//if(sh.getInstance(context)!=null && sh.getInstance(context).getUapps()!=null)
//		//	mediation = sh.getInstance(context).getUapps().getIsmediation() ==1;
//		//TODO addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")pla
//		if(lg.isDebug){
//			return new AdRequest.Builder()
//					//.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
//					//.addNetworkExtrasBundle(AdMobAdapter.class, extras)//mediation
//					.build();
//		}else {
//			return new AdRequest.Builder()
//					//.addNetworkExtrasBundle(AdMobAdapter.class, extras)//mediation
//					.build();
//		}
//
////		if(mediation)
////			return new AdRequest.Builder()
////				.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
////				.addNetworkExtrasBundle(AdMobAdapter.class, extras)//mediation
////				.build();
////		else
////			return new AdRequest.Builder()
////					.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
////					.build();
//	}

//	@NonNull
//	public static AdRequest getDefaultNativeAdRequest(Context context) {
//		return new AdRequest.Builder()
////					.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
//					.build();
//	}

	private static viewdlg progressDialog;

	public static void showProgress(Context context) {
		showProgress(context, context.getString(R.string.loading_ads));
	}

	public static void dismissCurrentProgress() {
		if (progressDialog != null) {
			try {
				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			progressDialog = null;
		}
	}

	public static void showProgress(Context context, String message) {
		try {
			dismissCurrentProgress();
			progressDialog = new viewdlg(context);
			progressDialog.showDialog(message);

		} catch (Exception e) {
			lg.logs("Utils showProgress failed ", e.getMessage());
			e.printStackTrace();
		}
	}

	public static void sendCustomAction(String screenName, String action) {
//		String tag = screenName + "_" + action;
//		FaTracking.sendAction(tag, action, action);
//		FbTracking.sendAction(tag, action, action);
//		Bundle bundle = new Bundle();
//		FirebaseTracking.logEventFirebase(tag, bundle);
	}

	public static String getString(Application application,int stringId) {
		return application.getResources().getString(stringId);
	}


}
