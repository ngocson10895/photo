package com.google.android.gms.corebase;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;

public class adl extends AlertDialog
{

    private AlertListenner mListenner;

    public adl(final AppCompatActivity context, String title, final String content, String cancel, String ok,
               final Object fullAds, AlertListenner listenner)
    {
        super(context);//,R.style.AlertDialogCustom
        mListenner = listenner;
        setTitle(title);
        setMessage(content);
        setButton(-2, cancel, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(mListenner!=null)
                    mListenner.onCancelClick();
            }
        });
        setButton(-1, ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(mListenner!=null){
                    mListenner.onOkClick();
                }else {
//                    logger.showFullAllAds(context,fullAds,null);
                    Ins.getInstance(context.getApplication()).showAdBackIfNeeded(context,
                            true,"back",true,new logger.OnAdsCloseListenner(){
                                @Override
                                public void onAdsClose() {
                                    context.finish();
                                }
                            });
                }
            }
        });
    }

    public interface AlertListenner{
        public void onCancelClick();
        public void onOkClick();
    }
}

