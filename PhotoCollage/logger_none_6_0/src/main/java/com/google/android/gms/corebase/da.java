package com.google.android.gms.corebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by MinThu on 9/25/17.
 */

public class da extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //lg.logs("data", "onReceive");
        if (mListenner != null) {
            int type = intent.getExtras().getInt(lg.mActionData, 0);
            switch (type) {
                case lg.EXTRA_ACTION.INFO_ERROR:
                    mListenner.onInfoError();
                    break;
                case lg.EXTRA_ACTION.INFO_SUCCESS:
                    mListenner.onInfoSuccess();
                    break;
                case lg.EXTRA_ACTION.UPDATE_ERROR:
                    mListenner.onUpdateError();
                    break;
                case lg.EXTRA_ACTION.UPDATE_SUCCESS:
                    mListenner.onUpdateSuccess();
                    break;
                default:
                    break;
            }
        }
    }

    public interface Listenner {
        void onUpdateError();

        void onUpdateSuccess();

        void onInfoSuccess();

        void onInfoError();
    }

    private Listenner mListenner;

    public void setListenner(Listenner listenner) {
        this.mListenner = listenner;
    }
}