package com.google.android.gms.corebase;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;

//import android.support.v7.app.AppCompatActivity;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by minthu on 2/21/17.
 */

public class fb {
    private AppCompatActivity mContext;
    private InterstitialAd mInterstitialAd;

    public static fb mInstance;
    private static boolean isInApp;

    public static fb getInstance(AppCompatActivity context, boolean inApp){
        if(mInstance == null){
            mInstance = new fb(context);
        }
        isInApp = inApp;
        return  mInstance;
    }

    public fb(final AppCompatActivity context){
        mContext = context;
//        lg.logs("fb", sh.getInstance(context).getUapps().fb_popup_inapp8());//"995941930538453_995942387205074");//sh.getInstance(context).getUapps().getFb_popup());

        if(sh.getInstance(context).getUapps().getFb_popup_inapp8() ==null ||
                sh.getInstance(context).getUapps().getFb_popup_inapp8().length<=0) return;

        //if(isInApp){
            String popInapp = (sh.getInstance(context).getUapps().getFb_popup_inapp8()!=null &&
                    sh.getInstance(context).getUapps().getFb_popup_inapp8().length>0 &&
                    sh.getInstance(context).getUapps().getFb_popup_inapp8()[0]!=null)?
                    sh.getInstance(context).getUapps().getFb_popup_inapp8()[0]:
                    "";
            mInterstitialAd = new InterstitialAd(context,popInapp);;//"995941930538453_995942387205074");// sh.getInstance(context).getUapps().getFb_popup());
//        }else {
//            mInterstitialAd = new InterstitialAd(context,
//                    sh.getInstance(context).getUapps().getFb_popup());;//"995941930538453_995942387205074");// sh.getInstance(context).getUapps().getFb_popup());
//        }
        //AdSettings.addTestDevice("aee34a992935090c3f584f114a34eb78");
        mInterstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                lg.logs("fb","onInterstitialDisplayed");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                lg.logs("fb","onInterstitialDismissed");
                if(!isInApp && mContext instanceof Activity)
                    ((Activity) mContext).finish();
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                lg.logs("fb","onError");
                if(!isInApp){
                    logger.showPopupExtenal(1);
                }else {
                    logger.showPopupTypeInApp(mContext,1);
                }
            }

            @Override
            public void onAdLoaded(Ad ad) {
                lg.logs("fb","onAdLoaded");
            }

            @Override
            public void onAdClicked(Ad ad) {
                lg.logs("fb","onAdClicked");
                if(!isInApp && mContext instanceof Activity)
                    ((Activity) mContext).finish();
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                lg.logs("fb","onLoggingImpression");
            }
        });
        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        mInterstitialAd.loadAd();
    }

    public void loadAds(){
        try{
            if(!mInterstitialAd.isAdLoaded())
                mInterstitialAd.loadAd();
            else
                mInterstitialAd.show();
        }catch (Exception e){
            lg.logs("fb",e.toString());
        }
    }

    public void showAds(){
        mInterstitialAd.show();
    }

    public static void getKeyHash(Context context){
        // Add code to print out the key hash
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.gmtubefree.musicvideotube",//com.vincommerce.adayroi com.facebook.samples.hellofacebook
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                lg.logs("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            lg.logs("NameNotFoundException",e.toString());
        } catch (NoSuchAlgorithmException e) {
            lg.logs("NoSuchAlgorithmException",e.toString());
        }
    }
}

