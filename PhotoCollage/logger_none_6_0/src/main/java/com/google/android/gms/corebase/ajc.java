package com.google.android.gms.corebase;

/**
 * Created by MinThu on 9/19/17.
 */

import android.content.Context;
import androidx.annotation.NonNull;

//import android.support.annotation.NonNull;
//
import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;
import com.evernote.android.job.JobManager;

/**
 * @author rwondratschek
 */
public class ajc implements JobCreator {

    @Override
    public Job create(String tag) {
        switch (tag) {
            case lg.EXTRA_JOB.SYNC:
                return new asj();
            case lg.EXTRA_JOB.SYNC_EVERYDAY:
                return new asj();
            case lg.EXTRA_JOB.EXTENAL:
                return new adj();
            case lg.EXTRA_JOB.ALARM:
                return new adj();
            case lg.EXTRA_JOB.UNLOCK:
                return new adj();
            case lg.EXTRA_JOB.NET:
                return new adj();
            case lg.EXTRA_JOB.ALARM_NEW:
                return new adj();
            default:
                return null;
        }
    }

    public static final class AddReceiver extends AddJobCreatorReceiver {
        @Override
        protected void addJobCreator(@NonNull Context context, @NonNull JobManager manager) {
            // manager.addJobCreator(new DemoJobCreator());
            lg.logs("AddReceiver - addJobCreator" ,"manager");
        }
    }
}