package com.google.android.gms.corebase;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MinThu on 6/19/17.
 */

public class t {
    private String id;//: "1",
    @SerializedName("package")
    private String packageName;//": "com.lichvannien.lichviet.licham",
    private String name;//": "L\u1ecbch v\u1ea1n ni\u00ean",
    private String des;//": "Khoa h\u1ecdc c\u1ee7a t\u00e2m linh",
    private String icon;//": "https:\/\/lh3.googleusercontent.com\/sFJFvBojhSxQ8Yl2zBYsgI10mUIT93QGoOmaEWQtDCb3ZkSwA1_AKzc-UNlAWtMOzmc=w300-rw",
    private String link;//": "http:\/\/bit.ly\/2szqajK"

    public t(String id, String packageName, String name, String des, String icon, String link) {
        this.id = id;
        this.packageName = packageName;
        this.name = name;
        this.des = des;
        this.icon = icon;
        this.link = link;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String toString(){
        return id+ " " +name+ " "+des + " "+icon+ " ";
    }
}
