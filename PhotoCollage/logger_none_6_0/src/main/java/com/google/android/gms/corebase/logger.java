package com.google.android.gms.corebase;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.evernote.android.job.JobApi;
import com.evernote.android.job.JobConfig;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.evernote.android.job.util.support.PersistableBundleCompat;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentInfoLoadingListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;

import java.util.Calendar;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.google.android.gms.corebase.lg.logs;

//import android.support.v7.app.AppCompatActivity;
//import androidx.localbroadcastmanager.content.LocalBroadcastManager;

//import android.support.v4.content.LocalBroadcastManager;
//import androidx.localbroadcastmanager.content.LocalBroadcastManager;
//import com.startapp.android.publish.adsCommon.StartAppAd;
//import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;

//import com.facebook.ads.AudienceNetworkAds;

/**
 * Created by MinThu on 9/21/17.
 */

public class logger {

    public static ConsentForm mConsentForm;
    public static boolean isShowMessage = false;
    //    public static String oA = "1257948764309267_1257949124309231";
//    public static String b = "1257948764309267_1257949360975874";
    public static String oy = "";//"ca-app-pub-4274132623784237/8768169246";
    public static String oz = "";//"ca-app-pub-4274132623784237/5048238473"; //y banner, z: popup

    //test
    public static String VIDEO_REWARD_ADM = "";//"ca-app-pub-3336041397705909/8563422513";//"/6499/example/rewarded-video";
//    public static HashMap<String, InterstitialAd> mAdmobs = new HashMap<>();
//    public static HashMap<String, com.facebook.ads.InterstitialAd> mFaces = new HashMap<>();
//    public static HashMap<String, StartAppAd> mStartapps = new HashMap<>();


    public static void e(final Context context) {
        //fix android >= M
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
        //
//        StrictMode.setThreadPolicy(
//                new StrictMode.ThreadPolicy.Builder()
//                        .detectAll()
//                        .penaltyLog()
//                        .penaltyDeath()
//                        .build());

//        StrictMode.setVmPolicy(
//                new StrictMode.VmPolicy.Builder()
//                        .detectAll()
//                        .penaltyLog()
//                        .penaltyDeath()
//                        .build());
        //init
        initJob(context);

//        //debug >= 10
//        if (sh.getInstance(context).getUapps() != null &&
//                sh.getInstance(context).isDebugLog()) {
//            context.startActivity(new Intent(context, hs.class));
//        }

//        if (AudienceNetworkAds.isInAdsProcess(context)) {
//            return;
//        } // else continue with publisher's initialization code

        initAudienceNetwork(context);
    }

    public static void c(final AppCompatActivity context) {
        f(context, new ConsentInfoLoadingListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus, boolean isLoadAds) {

            }

            @Override
            public void onFailedToUpdateConsentInfo(String reason) {

            }

            @Override
            public void onSyncError() {

            }

            @Override
            public void onSyncOK() {

            }
        });
    }

    public static void f(final AppCompatActivity context, final ConsentInfoLoadingListener consentInfoUpdateListener) {
        initUnlockScreen(context);
        initAlarm(context);
        if (!sh.getInstance(context).isDayActive()) {
//            syncApiEveryDay(context);
        }

        isShowMessage = false;
//        URL privacyUrl = null;
//        try {
//            // : Replace with your app's privacy policy URL.
//            u mU = sh.getInstance(context).getUapps();
//            String urlNote = (mU != null && mU.getUrlNote() != null && mU.getUrlNote().length() > 10) ? mU.getUrlNote() : "https://www.google.com/about/company/user-consent-policy.html";//urlNote
//            privacyUrl = new URL(urlNote);
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//            // Handle error.
//        }
//
//        if(privacyUrl ==null) {
//            try {
//                privacyUrl =  new URL("https://www.google.com/about/company/user-consent-policy.html");
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//                //privacyUrl =  new URL("https://www.google.com/about/company/user-consent-policy.html");
//            }
//        }
//
//        mConsentForm = new ConsentForm.Builder(context, privacyUrl)
//                .withListener(new ConsentFormListener() {
//                    @Override
//                    public void onConsentFormLoaded() {
//                        // Consent form loaded successfully.
//                        //if (lg.isDebug) Log.e("ConsentFormLoaded", "onConsentFormLoaded= ");
//
//                    }
//
//                    @Override
//                    public void onConsentFormOpened() {
//                        // Consent form was displayed.
//                        //if (lg.isDebug) Log.e("ConsentFormOpened", "onConsentFormOpened= ");
//                    }
//
//                    @Override
//                    public void onConsentFormClosed(
//                            ConsentStatus consentStatus, Boolean userPrefersAdFree) {
//                        // Consent form was closed.
//                        // Check status = UNKNOW to finish
//                        //if (lg.isDebug)
//                        //    Log.e("ConsentFormClosed", "onConsentFormClosed= consentStatus " + consentStatus + " >"
//                        //            + userPrefersAdFree);
//                        boolean isStatus = ConsentInformation.getInstance(context).isRequestLocationInEeaOrUnknown();
//                        //Log.e("onConsentInfoUpdated", "isRequestLocationInEeaOrUnknown= " + isStatus);
//                        //form.isShowing();
//                        // check isStatus = TRUE && consentStatus == ConsentStatus is PERSONALIZED or NON_PERSONALIZED
//
//                        if (!isStatus) {
//                            // NO EU
//                            sh.getInstance(context).setUserConsent(true);
//                            //if (lg.isDebug) Log.e("ConsentFormClosed", "no handle 1");
//                        } else {
//                            if (consentStatus == ConsentStatus.NON_PERSONALIZED) {
//                                sh.getInstance(context).setUserConsent(false);
//                                // if (lg.isDebug) Log.e("ConsentFormClosed", "no handle 2");
//                            } else if (consentStatus == ConsentStatus.PERSONALIZED) {
//                                //consent in EU or Unknow
//                                sh.getInstance(context).setUserConsent(true);
//                                //if (lg.isDebug) Log.e("ConsentFormClosed", "no handle 3");
//                            } else {
//                                //EU or UNKNOWN: not consent
//                                //finish app and show app paid version
//                                sh.getInstance(context).setUserConsent(false);
//                                //if (lg.isDebug) Log.e("ConsentFormClosed", "show paid version");
//                                actionPaidApp(context);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onConsentFormError(String errorDescription) {
//                        // Consent form error.
//                        if (isShowMessage)
//                            Toast.makeText(context, errorDescription, Toast.LENGTH_LONG).show();
//                        //if (lg.isDebug)
//                        //    Log.e("ConsentFormError", "onConsentFormError= " + errorDescription);
//                    }
//                })
//                .withPersonalizedAdsOption()
//                .withNonPersonalizedAdsOption()
//                .withAdFreeOption()
//                .build();
//        mConsentForm.load();

        syncApi(context, new da.Listenner() {
            @Override
            public void onUpdateError() {
                if (consentInfoUpdateListener != null)
                    consentInfoUpdateListener.onSyncError();
            }

            @Override
            public void onUpdateSuccess() {
                if (consentInfoUpdateListener != null)
                    consentInfoUpdateListener.onSyncOK();

                //checkConsentInfo(context, mConsentForm, false, consentInfoUpdateListener);
            }

            @Override
            public void onInfoSuccess() {
                if (consentInfoUpdateListener != null)
                    consentInfoUpdateListener.onSyncOK();

                //checkConsentInfo(context, mConsentForm, false, consentInfoUpdateListener);
            }

            @Override
            public void onInfoError() {
                if (consentInfoUpdateListener != null)
                    consentInfoUpdateListener.onSyncError();
            }
        });
    }

    private static void initAudienceNetwork(Context context) {
//        AudienceNetworkAds.initialize(context);
//        if (AudienceNetworkAds.isInAdsProcess(context)) {
//            return;
//        } // else continue with publisher's initialization code

        //validate view
//        "android.annotation.SuppressLint",
//        "android.annotation.TargetApi",
//        "android.security.NetworkSecurityPolicy",
//        "android.support.annotation.AnyThread",
//        "android.support.annotation.FloatRange",
//        "android.support.annotation.IntRange",
//        "android.support.annotation.Keep",
//        "android.support.annotation.NonNull",
//        "android.support.annotation.Nullable",
//        "android.support.annotation.RequiresApi",
//        "android.support.annotation.UiThread",
//        "android.support.annotation.VisibleForTesting",
//        "android.support.annotation.WorkerThread",
//        "android.support.v4.content.LocalBroadcastManager",
//        "android.support.v4.graphics.ColorUtils",
//        "androidx.core.view.MotionEventCompat",
//        "android.support.v4.view.PagerAdapter",
//        "android.support.v4.view.ViewPager",
//        "android.support.v7.widget.LinearLayoutManager",
//        "android.support.v7.widget.LinearSmoothScroller",
//        "android.support.v7.widget.PagerSnapHelper",
//        "android.support.v7.widget.RecyclerView",
//        "android.support.v7.widget.SnapHelper",
//        "com.google.android.gms.ads.identifier.AdvertisingIdClient"

    }

    private static void initJob(Context context) {
        //JobCat.addLogPrinter(new cp());
        //JobCat.setLogcatEnabled(false);
        try {
            JobManager.create(context).addJobCreator(new ajc());
        } catch (Exception e) {
            JobConfig.setForceAllowApi14(true);
            JobConfig.setApiEnabled(JobApi.GCM, false);// is only important for Android 4.X
            JobManager.create(context).addJobCreator(new ajc());
        }
    }

    private static void initUnlockScreen(Context context) {
        //ad service check screen on off
        // android 8
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            //context.startForegroundService(new Intent(context, sc.class));
//        } else {
//            context.startService(new Intent(context, sc.class));
//        }
    }

    public static void initAlarm(Context context) {
        if (!sh.getInstance(context).isAlarmActive()) {
            initJob(context);
//            syncAlarm(context);
        }

    }

    public static void syncApi(final AppCompatActivity activity, final da.Listenner listenner) {
//        PersistableBundleCompat extras = new PersistableBundleCompat();
//        extras.putString(lg.EXTRA_INTENT_PARAM, lg.EXTRA_JOB.SYNC);
//        try {
//            new JobRequest.Builder(lg.EXTRA_JOB.SYNC)
//                    .setExecutionWindow(1_000L, 15_000L)
//                    .setBackoffCriteria(60_000L, JobRequest.BackoffPolicy.LINEAR)
//                    .setRequiresCharging(false)
//                    .setRequiresDeviceIdle(false)
//                    .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
//                    .setExtras(extras)
//                    .setRequirementsEnforced(true)
//                    //.setPersisted(true)
//                    .setUpdateCurrent(true)
//                    .build()
//                    .schedule();
//        } catch (IllegalArgumentException e) {
//            lg.logs("api", "" + e.toString());
//        }

        new se(activity).getV();
        //set permision phone
        //isPermissionGranted(activity);
        //listenner data to responsive

        IntentFilter filter = new IntentFilter(lg.mAction);
        final da dataReceiver = new da();
        //activity <==>  LocalBroadcastManager.getInstance(activity)
        LocalBroadcastManager.getInstance(activity)
                .registerReceiver(dataReceiver, filter);

        dataReceiver.setListenner(new da.Listenner() {
            @Override
            public void onUpdateError() {
                lg.logs("sync", "onUpdateError");
                listenner.onUpdateError();
                LocalBroadcastManager.getInstance(activity)
                        .unregisterReceiver(dataReceiver);
            }

            @Override
            public void onUpdateSuccess() {
                lg.logs("sync", "onUpdateSucces");
                listenner.onUpdateSuccess();
                LocalBroadcastManager.getInstance(activity)
                        .unregisterReceiver(dataReceiver);
            }

            @Override
            public void onInfoSuccess() {
                lg.logs("sync", "onInfoSucces");
                listenner.onInfoSuccess();
                LocalBroadcastManager.getInstance(activity)
                        .unregisterReceiver(dataReceiver);
            }

            @Override
            public void onInfoError() {
                lg.logs("sync", "onInfoError");
                listenner.onInfoError();
                LocalBroadcastManager.getInstance(activity)
                        .unregisterReceiver(dataReceiver);
            }
        });

    }

    public static void syncApiEveryDay(Context context) {
        sh.getInstance(context).storeInt(lg.mDay, 1);

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // 12 PM - 1 PM, ignore seconds
        long startMs = TimeUnit.MINUTES.toMillis(60 - minute)
                + TimeUnit.HOURS.toMillis((24 - hour + 12) % 24);//12
        long endMs = TimeUnit.MINUTES.toMillis(20);
        lg.logs("day", "startTime " + startMs);
        try {
            if (startMs > 0)
                new JobRequest.Builder(lg.EXTRA_JOB.SYNC_EVERYDAY)
                        //.setPeriodic(JobRequest.MIN_INTERVAL, JobRequest.MIN_FLEX)//test
                        //.setPeriodic(TimeUnit.HOURS.toMillis(24),TimeUnit.HOURS.toMillis(2))//test
                        //.setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))//test
                        .setPeriodic(startMs, endMs)//every day
                        .setRequiresCharging(false)
                        .setRequiresDeviceIdle(false)
                        .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                        //.setPersisted(true)
                        .setUpdateCurrent(true)
                        .setRequirementsEnforced(false)
                        .build()
                        .schedule();
        } catch (IllegalArgumentException e) {
            lg.logs("day", "" + e.toString());
        }

    }

    public static void syncAlarm(Context context) {
        u mU = sh.getInstance(context).getUapps();
        if (mU != null && sh.getInstance(context).isAlarmAvaiable()) {
            sh.getInstance(context).storeInt(lg.mAlarm, 1);
            int type = mU.getPoptimeshow();
            long timeAlarm = mU.getTimepopshow() > 0 ? mU.getTimepopshow() : 1;//alway hour

//            long timeUnit;
//            switch (mU.getUnitTime()) {
//                case 1:// min
//                    timeUnit = 1 * 60;
//                    break;
//                case 2://second
//                    timeUnit = 1;
//                    break;
//                default://hour
//                    timeUnit = 1 * 60 * 60;
//                    break;
//            }
//            long timeDelay = mU.getTimepopshow() * timeUnit;//test 60s
//            long startTime = TimeUnit.SECONDS.toMillis(timeDelay);
//            long endTime = startTime + 30_000L;


            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);

            // 12 PM - 1 PM, ignore seconds
            long startMs = TimeUnit.MINUTES.toMillis(60 - minute)
                    + TimeUnit.HOURS.toMillis((24 - hour) % (24 / timeAlarm));//timeAlarm = 6 -> Periodic 24/6 = 4 times
            long endMs = TimeUnit.MINUTES.toMillis(10);
            lg.logs("al", "startTime " + startMs);

            PersistableBundleCompat extras = new PersistableBundleCompat();
            extras.putInt(lg.EXTRA_INTENT_PARAM, type);
            try {
                if (startMs > 0)
                    new JobRequest.Builder(lg.EXTRA_JOB.ALARM)
                            //.setPeriodic(JobRequest.MIN_INTERVAL, JobRequest.MIN_FLEX)
                            //.setPeriodic(TimeUnit.HOURS.toMillis(24), TimeUnit.HOURS.toMillis(2))
                            //.setPeriodic(TimeUnit.MINUTES.toMillis(15),TimeUnit.MINUTES.toMillis(5))
                            .setPeriodic(startMs, endMs)//every day
                            .setRequiresCharging(false)
                            .setRequiresDeviceIdle(false)
                            .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                            //.setPersisted(true)
                            .setExtras(extras)
                            .setUpdateCurrent(true)
                            .setRequirementsEnforced(true)
                            .build()
                            .schedule();

            } catch (IllegalArgumentException e) {
                lg.logs("al", "" + e.toString());
            }
        }
    }

    public static void syncAlarmNewest(Context context) {
        Set<JobRequest> jobRequests = JobManager.instance().getAllJobRequestsForTag(lg.EXTRA_JOB.ALARM_NEW);
        int size = jobRequests.size();
        logs("alN", "size " + size);

        u mU = sh.getInstance(context).getUapps();
        if (mU != null && jobRequests.size() <= 1)
//                && Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1)//&& sh.getInstance(context).isAlarmAvaiable()
        {
            int type = mU.getPoptimeshow();
            long timeAlarm = mU.getPopunlockscreen() > 15 ? mU.getPopunlockscreen() : 16;//getPopunlockscreen change to minute

            long minuteServer = timeAlarm * 1000 * 60;
            long repeat = (minuteServer < JobRequest.MIN_INTERVAL) ? JobRequest.MIN_INTERVAL : minuteServer;//Min 15' = 900000ms
            lg.logs("alN", "startTime " + minuteServer);
            long delayMs = TimeUnit.MINUTES.toMillis(5);// min = 5, max 16 => 15
//            type = 1;
            PersistableBundleCompat extras = new PersistableBundleCompat();
            extras.putInt(lg.EXTRA_INTENT_PARAM, type);
            try {
                JobRequest.Builder builder = new JobRequest.Builder(lg.EXTRA_JOB.ALARM_NEW)
                        .setPeriodic(repeat, delayMs)
                        .setRequiresCharging(false)
                        .setRequiresDeviceIdle(false)
                        .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                        .setUpdateCurrent(false)
                        .setRequirementsEnforced(true);
                builder.setExtras(extras);
                builder.build().schedule();
            } catch (Exception e) {
                lg.logs("alN", "" + e.toString());
            }
        }

//        long minuteServer = 16 * 1000*60;
//        long repeat = (minuteServer <JobRequest.MIN_INTERVAL) ? JobRequest.MIN_INTERVAL: minuteServer;//Min 15' = 900000ms
//
//        JobRequest.Builder builder = new JobRequest.Builder(lg.EXTRA_JOB.ALARM_NEW)
//                .setPeriodic(repeat)
//                .setRequiresCharging(false)
//                .setRequiresDeviceIdle(false)
//                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
//                .setUpdateCurrent(true)
//                .setRequirementsEnforced(true);
//
//        PersistableBundleCompat extras = new PersistableBundleCompat();
//        extras.putInt(EXTRA_INTENT_PARAM, 1);
//        builder.setExtras(extras);
//        builder.build().schedule();
    }

    public static void showPopupTypeInAppHome(AppCompatActivity context) {
        if (sh.getInstance(context).getUapps() != null) {
            int type = sh.getInstance(context).getUapps().getPoptrangchu();
//            sh.getInstance(context).getAds(type, true);
//            if(type>0){
            Ins.getInstance(context.getApplication())
                    .showAdIfNeeded(context, true, "home");
//            }
        }
    }

    public static void showPopupTypeInAppDetail(AppCompatActivity context) {
        if (sh.getInstance(context).getUapps() != null) {
            int type = sh.getInstance(context).getUapps().getPopvideo();
//            sh.getInstance(context).getAds(type, true);
            if (type > 0) {
                Ins.getInstance(context.getApplication())
                        .showAdIfNeeded(context, true, "detail");
            }
        }
    }


    public static void showPopupTypeInAppNext(AppCompatActivity context) {
        if (sh.getInstance(context).getUapps() != null) {
            int type = sh.getInstance(context).getUapps().getPopmenu();
//            sh.getInstance(context).getAds(type, true);
            if (type > 0) {
                Ins.getInstance(context.getApplication())
                        .showAdIfNeeded(context, true, "menu");
            }
        }
    }

    public static void showPopupTypeInApp(AppCompatActivity context, int type) {
//        sh.getInstance(context).getAds(type, true);
        if (type > 0) {
            Ins.getInstance(context.getApplication())
                    .showAdIfNeeded(context, true, "detail");
        }
    }

    //test job with ads id or when facebook ads error ->show admob
    public static void showPopupExtenal(int type) {
        PersistableBundleCompat extras = new PersistableBundleCompat();
        extras.putInt(lg.EXTRA_INTENT_PARAM, type);
        try {
            new JobRequest.Builder(lg.EXTRA_JOB.EXTENAL)
                    .setBackoffCriteria(60_000L, JobRequest.BackoffPolicy.LINEAR)
                    .setExtras(extras)
                    .setExact(121_000L)
                    //.setPersisted(true)
                    .setUpdateCurrent(true)
                    .build()
                    .schedule();
        } catch (IllegalArgumentException e) {
            lg.logs("ex", "" + e.toString());
        }
    }

    public static void showPopupUnlock(Context context) {
        u mU = sh.getInstance(context).getUapps();
        if (mU != null && sh.getInstance(context).isAlarmAvaiable()) {
            int type = mU.getPopunlockscreen();//test fb
            long timeDelay = mU.getDelayunlockscreen();//test 60s
            long startTime = TimeUnit.SECONDS.toMillis(timeDelay);
            long endTime = startTime + 60_000L;
            lg.logs("sc", type + "startTime " + startTime);
            PersistableBundleCompat extras = new PersistableBundleCompat();
            extras.putInt(lg.EXTRA_INTENT_PARAM, type);

            try {
                if (startTime > 0)
                    new JobRequest.Builder(lg.EXTRA_JOB.UNLOCK)
                            .setExecutionWindow(startTime, endTime)
                            .setBackoffCriteria(60_000L, JobRequest.BackoffPolicy.LINEAR)
                            .setRequiresCharging(false)
                            .setRequiresDeviceIdle(false)
                            .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                            .setExtras(extras)
                            .setRequirementsEnforced(true)
                            //.setPersisted(true)
                            .setUpdateCurrent(true)
                            .build()
                            .schedule();
            } catch (IllegalArgumentException e) {
                lg.logs("sc", "" + e.toString());
            }
        }
    }

    public static void showPopupNet(Context context) {
        u mU = sh.getInstance(context).getUapps();
        if (mU != null && sh.getInstance(context).isAlarmAvaiable()) {
            int type = mU.getPopchangenet();
            long timeDelay = mU.getDelaynet();//test 70s
            long startTime = TimeUnit.SECONDS.toMillis(timeDelay);
            long endTime = startTime + 60_000L;
            lg.logs("ne", type + "startTime " + startTime);

            PersistableBundleCompat extras = new PersistableBundleCompat();
            extras.putInt(lg.EXTRA_INTENT_PARAM, type);
            try {
                if (startTime > 0)
                    new JobRequest.Builder(lg.EXTRA_JOB.NET)
                            .setExecutionWindow(startTime, endTime)
                            .setBackoffCriteria(60_000L, JobRequest.BackoffPolicy.LINEAR)
                            .setRequiresCharging(false)
                            .setRequiresDeviceIdle(false)
                            .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                            .setExtras(extras)
                            .setRequirementsEnforced(true)
                            //.setPersisted(true)
                            .setUpdateCurrent(true)
                            .build()
                            .schedule();
            } catch (IllegalArgumentException e) {
                lg.logs("ne", "" + e.toString());
            }
        }
    }

    public static void clearAllJob() {
        JobManager.instance().cancelAll();
    }

    public static void clearJobId(int id) {
        JobManager.instance().cancel(id);
    }

//    public static void loadBnAdsHome(Activity context, final View viewParent) {
//        if (sh.getInstance(context).getUapps() != null) {
//            int type = sh.getInstance(context).getUapps().getBantrangchu();
//            loadBnAds(context, viewParent, type);
//        }
//
//    }

//    public static void loadBnAdsDetail(Activity context, final View viewParent) {
//        if (sh.getInstance(context).getUapps() != null) {
//            int type = sh.getInstance(context).getUapps().getBanvideo();
//            loadBnAds(context, viewParent, type);
//        }
//    }

//    public static void loadBnAds(Activity context, final View viewParent, int adType) {
//        // adType = 2;test
//        switch (adType) {
//            case 1:
//                loadBnAdm(context, viewParent);
//                break;
//            case 2:
//                loadBnSt(context, viewParent);
//                break;
//            case 3:
//                loadBnFb(context, viewParent);
//                break;
//            case 12:
//                int rand12 = lg.randomAB(1, 2);
//                if (rand12 == 1) {
//                    loadBnAdm(context, viewParent);
//                } else {
//                    loadBnSt(context, viewParent);
//                }
//                break;
//            case 13:
//                int rand13 = lg.randomAB(1, 2);
//                if (rand13 == 1) {
//                    loadBnAdm(context, viewParent);
//                } else {
//                    loadBnFb(context, viewParent);
//                }
//                break;
//            case 23:
//                int rand23 = lg.randomAB(1, 2);
//                if (rand23 == 1) {
//                    loadBnSt(context, viewParent);
//                } else {
//                    loadBnFb(context, viewParent);
//                }
//                break;
//            case 123:
//                int rand123 = lg.randomAB(1, 3);
//                if (rand123 == 1) {
//                    loadBnAdm(context, viewParent);
//                } else if (rand123 == 2) {
//                    loadBnSt(context, viewParent);
//                } else {
//                    loadBnFb(context, viewParent);
//                }
//                break;
//            default:
//                break;
//        }
//    }

//    public static void loadBnAdm(Context context, final View viewParent) {
//        if (viewParent != null && sh.getInstance(context).getUapps() != null
//                && sh.getInstance(context).getUapps().getBanner() != null) {
//
//            Bundle extras = new Bundle();
//            // Check non-personalized ads
//            boolean isPass = sh.getInstance(context).getUserConsent();
//            if (!isPass)
//                extras.putString("npa", "1");
//            String content_rate = sh.getInstance(context).getUapps().getAdmob_fil() != null ?
//                    sh.getInstance(context).getUapps().getAdmob_fil() : "T";
//            extras.putString("max_ad_content_rating", content_rate);
//
//            AdRequest adRequest = new AdRequest.Builder()
//                    //.addNetworkExtrasBundle(AdMobAdapter.class, extras)
//                    .build();
//            final com.google.android.gms.ads.AdView adView = new AdView(context);
//            adView.setAdUnitId(sh.getInstance(context).getUapps().getBanner());
//            adView.setAdSize(com.google.android.gms.ads.AdSize.BANNER);
//            adView.loadAd(adRequest);
//            adView.setAdListener(new AdListener() {
//                @Override
//                public void onAdLoaded() {
//                    super.onAdLoaded();
//                    if (viewParent instanceof RelativeLayout) {
//                        ((RelativeLayout) viewParent).removeAllViews();
//                        ((RelativeLayout) viewParent).addView(adView);
//                    } else if (viewParent instanceof LinearLayout) {
//                        ((LinearLayout) viewParent).removeAllViews();
//                        ((LinearLayout) viewParent).addView(adView);
//                    } else if (viewParent instanceof FrameLayout) {
//                        ((FrameLayout) viewParent).removeAllViews();
//                        ((FrameLayout) viewParent).addView(adView);
//                    }
//                }
//            });
//        }
//    }

//    public static void loadBnFb(Context context, final View viewParent) {
////        if (viewParent != null && sh.getInstance(context).getUapps() != null
////                && sh.getInstance(context).getUapps().getFb_banner() != null) {
//        if (viewParent != null && lg.bannerFbs != null &&lg.bannerFbs.length > 0) {
//            com.facebook.ads.AdView bannerAdView = new com.facebook.ads.AdView(context,
//                    lg.bannerFbs[0], AdSize.BANNER_HEIGHT_50);//"1250456204977488_1250456744977434"
////            AdSettings.addTestDevice("730956B1D4BC960D721C06E14B209260");
////            AdSettings.addTestDevice("aee34a992935090c3f584f114a34eb78");
//
//            //208219069811463_219987578634612
//            if (viewParent instanceof RelativeLayout) {
//                ((RelativeLayout) viewParent).removeAllViews();
//                ((RelativeLayout) viewParent).addView(bannerAdView);
//            } else if (viewParent instanceof LinearLayout) {
//                ((LinearLayout) viewParent).removeAllViews();
//                ((LinearLayout) viewParent).addView(bannerAdView);
//            } else if (viewParent instanceof FrameLayout) {
//                ((FrameLayout) viewParent).removeAllViews();
//                ((FrameLayout) viewParent).addView(bannerAdView);
//            }
//
//            bannerAdView.loadAd();
//            bannerAdView.setAdListener(new com.facebook.ads.AdListener() {
//                @Override
//                public void onError(Ad ad, AdError adError) {
//                    lg.logs("AdError",adError.getErrorMessage());
//                }
//
//                @Override
//                public void onAdLoaded(Ad ad) {
//                    lg.logs("onAdLoaded");
//                }
//
//                @Override
//                public void onAdClicked(Ad ad) {
//
//                }
//
//                @Override
//                public void onLoggingImpression(Ad ad) {
//                    lg.logs("onLoggingImpression");
//                }
//            });
//        }
//    }

//    public static void loadBnSt(Activity context, final View viewParent) {
//        if (viewParent != null && sh.getInstance(context).getUapps() != null
//                && sh.getInstance(context).getUapps().getAppid() != null) {
//            st.getInstance(context);
//            Banner startAppBanner = new Banner(context, new BannerListener() {
//                @Override
//                public void onReceiveAd(View view) {
//                    lg.logs("loadBnSt", "onReceiveAd");
//                }
//
//                @Override
//                public void onFailedToReceiveAd(View view) {
//                    lg.logs("loadBnSt", "onFailedToReceiveAd");
//                }
//
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
//
//            // Add to main Layout
//            if (viewParent instanceof RelativeLayout) {
//                ((RelativeLayout) viewParent).removeAllViews();
//                ((RelativeLayout) viewParent).addView(startAppBanner);
//            } else if (viewParent instanceof LinearLayout) {
//                ((LinearLayout) viewParent).removeAllViews();
//                ((LinearLayout) viewParent).addView(startAppBanner);
//            } else if (viewParent instanceof FrameLayout) {
//                ((FrameLayout) viewParent).removeAllViews();
//                ((FrameLayout) viewParent).addView(startAppBanner);
//            }
//        }
//    }

//    public static boolean isPermissionGranted(Activity context) {
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (context.checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE)
//                    == PackageManager.PERMISSION_GRANTED) {
//                //Log.v("TAG","Permission is granted");
//                return true;
//            } else {
//                //Log.v("TAG","Permission is revoked");
//                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.READ_PHONE_STATE}, 2);
//                return false;
//            }
//        } else { //permission is automatically granted on sdk<23 upon installation
//            //Log.v("TAG","Permission is granted");
//            return true;
//        }
//    }

    private static void setMute(Context context) {
    }


    private static void checkConsentInfo(final Context context, final ConsentForm form, final boolean forceShow, final ConsentInfoLoadingListener consentInfoUpdateListener) {
        // consent Info
        //ConsentInformation.getInstance(context).addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1");
        // Geography appears as in EEA for test devices.
        //ConsentInformation.getInstance(context).
        //        setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
        // Geography appears as not in EEA for debug devices.
        //ConsentInformation.getInstance(context).
        //        setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_NOT_EEA);
        //init
        ConsentInformation consentInformation = ConsentInformation.getInstance(context);
        //String banner = sh.getInstance(mContext).getUapps().getBanner();
        //String popupId = sh.getInstance(mContext).getUapps().getBanner();
        String publisher = sh.getInstance(context).getUapps().getPublisherIds();
        String[] publisherIds = {publisher};
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                // User's consent status successfully updated.
                //if (lg.isDebug) Log.e("onConsentInfoUpdated", "consentStatus= " + consentStatus);
                boolean isStatus = ConsentInformation.getInstance(context).isRequestLocationInEeaOrUnknown();
                //if (lg.isDebug)
                //    Log.e("onConsentInfoUpdated", "isRequestLocationInEeaOrUnknown= " + isStatus);
                //form.isShowing();
                // check isStatus = TRUE && consentStatus == ConsentStatus is PERSONALIZED or NON_PERSONALIZED

//                if (forceShow)
//                    form.show();

                boolean isLoadAds;
                if (!isStatus) {
                    // NO EU
                    sh.getInstance(context).setUserConsent(true);
                    isLoadAds = true;
                } else {
                    if (consentStatus == ConsentStatus.NON_PERSONALIZED) {
                        sh.getInstance(context).setUserConsent(false);
                        isLoadAds = true;
                    } else if (consentStatus == ConsentStatus.PERSONALIZED) {
                        //consent in EU or Unknow
                        sh.getInstance(context).setUserConsent(true);
                        isLoadAds = true;
                    } else {
                        //EU or UNKNOWN: not consent
                        sh.getInstance(context).setUserConsent(false);
                        isLoadAds = false;
//                        if (!forceShow)
//                            form.show();
                    }
                }
//                if (consentStatus == ConsentStatus.NON_PERSONALIZED && isStatus) {
//                    sh.getInstance(context).setUserConsent(false);
//                } else {
//                    sh.getInstance(context).setUserConsent(true);
//                }
                sh.getInstance(context).setAdsConsentVisible(isLoadAds);
                if (consentInfoUpdateListener != null)
                    consentInfoUpdateListener.onConsentInfoUpdated(consentStatus, isLoadAds);

            }

            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                // User's consent status failed to update.
                //if (lg.isDebug)
                //    Log.e("onFailedConsentInfo", "errorDescription= " + errorDescription);
                //if(forceShow)
                //form.show();
                if (consentInfoUpdateListener != null)
                    consentInfoUpdateListener.onFailedToUpdateConsentInfo(errorDescription);
            }
        });

    }

    public static void showAdsSetting(AppCompatActivity context) {
        if (mConsentForm != null) {
            isShowMessage = true;
            mConsentForm.load();
            checkConsentInfo(context, mConsentForm, true, null);
        } else {
            Toast.makeText(context, "Consent form is not ready to be displayed.", Toast.LENGTH_LONG).show();
        }

    }

    // fix in app admob
//    public static void initFullAdm(final Activity mActivity, final InterstitialAd mInterstitialAd) {
//        try {
//            if (mInterstitialAd != null) {
//                String popInApp = sh.getInstance(mActivity).getUapps().getPopupinapp() != null ?
//                        sh.getInstance(mActivity).getUapps().getPopupinapp() :
//                        sh.getInstance(mActivity).getUapps().getPopup();
//                mInterstitialAd.setAdUnitId(popInApp);
//
//                Bundle extras = new Bundle();
//                boolean isPass = sh.getInstance(mActivity).getUserConsent();
//                if (!isPass)
//                    extras.putString("npa", "1");
//                String content_rate = sh.getInstance(mActivity).getUapps().getAdmob_fil() != null ?
//                        sh.getInstance(mActivity).getUapps().getAdmob_fil() : "T";
//                extras.putString("max_ad_content_rating", content_rate);
//
//                AdRequest adRequest = new AdRequest.Builder()
//                        //.addNetworkExtrasBundle(AdMobAdapter.class, extras)
//                        .build();
//
//                mInterstitialAd.loadAd(adRequest);
//                loadNewAdmFull(mActivity, mInterstitialAd);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            //   printLogException(e);
//        }
//    }

    //refresh
//    private static void loadNewAdmFull(Activity mActivity, InterstitialAd mInterstitialAd) {
//        Bundle extras = new Bundle();
//        boolean isPass = sh.getInstance(mActivity).getUserConsent();
//        if (!isPass)
//            extras.putString("npa", "1");
//        String content_rate = sh.getInstance(mActivity).getUapps().getAdmob_fil() != null ?
//                sh.getInstance(mActivity).getUapps().getAdmob_fil() : "T";
//        extras.putString("max_ad_content_rating", content_rate);
//
//        AdRequest adRequest = new AdRequest.Builder()
//                //.addNetworkExtrasBundle(AdMobAdapter.class, extras)
//                .build();
//
//        mInterstitialAd.loadAd(adRequest);
//    }

//    public static void showFullAdm(final Activity activity, final InterstitialAd mInterstitialAd,
//                                   final OnAdsCloseListenner listenner) {
//        final viewdlg dlg = new viewdlg(activity);
//        dlg.showDialog();
//        if (mInterstitialAd != null) {
//            //Utils.trackEvent(TrackerConstant.ADS, TrackerConstant.ADS_FULL_SCREEN);
//            mInterstitialAd.setAdListener(new AdListener() {
//                @Override
//                public void onAdClosed() {
//                    // Proceed to the next level.
//                    dlg.hideDialog();
//                    loadNewAdmFull(activity, mInterstitialAd);
//                    if (listenner != null) {
//                        listenner.onAdsClose();
//                    } else {
//                        activity.finish();
//                    }
//                }
//
//                @Override
//                public void onAdFailedToLoad(int i) {
//                    super.onAdFailedToLoad(i);
//
//                }
//            });
//
//            if (mInterstitialAd.isLoaded()) {
//                mInterstitialAd.show();
////                dlg.hideDialog();
//            } else {
//                if (listenner != null) {
//                    listenner.onAdsClose();
//                } else {
//                    activity.finish();
//                }
//                dlg.hideDialog();
//            }
//
//        } else {
//            if (listenner != null) {
//                listenner.onAdsClose();
//            } else {
//                activity.finish();
//            }
//            dlg.hideDialog();
//        }
////        dlg.hideDialog();
//    }


    // fix in app facebook
//    public static com.facebook.ads.InterstitialAd initFullFb(final Activity mActivity) {
//
//         try {
//             String popInApp = sh.getInstance(mActivity).getUapps().getFb_popupinapp() != null ?
//                     sh.getInstance(mActivity).getUapps().getFb_popupinapp() :
//                     sh.getInstance(mActivity).getUapps().getFb_popup();
//             final com.facebook.ads.InterstitialAd mInterstitialAd =
//                     new com.facebook.ads.InterstitialAd(mActivity,popInApp);
//
//             // Load a new interstitial.
//             mInterstitialAd.loadAd(EnumSet.of(CacheFlag.VIDEO));
//             mInterstitialAd.setAdListener(new InterstitialAdListener() {
//                 @Override
//                 public void onInterstitialDisplayed(Ad ad) {
//                     lg.logs("fb","onInterstitialDisplayed");
//                 }
//
//                 @Override
//                 public void onInterstitialDismissed(Ad ad) {
//                     lg.logs("fb","onInterstitialDismissed");
//                     mInterstitialAd.loadAd(EnumSet.of(CacheFlag.VIDEO));
////                     if(listenner!=null)listenner.onAdsClose();
//                 }
//
//                 @Override
//                 public void onError(Ad ad, AdError adError) {
//                     lg.logs("fb","onError"+adError.getErrorMessage());
////                if(listenner!=null)listenner.onAdsClose();
//                 }
//
//                 @Override
//                 public void onAdLoaded(Ad ad) {
//                     lg.logs("fb","onAdLoaded");
//                 }
//
//                 @Override
//                 public void onAdClicked(Ad ad) {
//                     lg.logs("fb","onAdClicked");
//                 }
//
//                 @Override
//                 public void onLoggingImpression(Ad ad) {
//                     lg.logs("fb","onLoggingImpression");
//                 }
//             });
//             return mInterstitialAd;
//         }catch (Exception e){
//            e.printStackTrace();
//            return null;
//         }
//
//    }

//    private static void requestNewFullFb(Activity mActivity, com.facebook.ads.InterstitialAd mInterstitialAd) {
//        // Load a new interstitial.
//        mInterstitialAd.loadAd(EnumSet.of(CacheFlag.VIDEO));
//    }

    public static void showFullFb(final AppCompatActivity activity, final com.facebook.ads.InterstitialAd mInterstitialAd,
                                  final OnAdsCloseListenner listenner) {

        mInterstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                lg.logs("fb", "onInterstitialDisplayed");
            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                lg.logs("fb", "onInterstitialDismissed");
//                mInterstitialAd.loadAd(EnumSet.of(CacheFlag.VIDEO));
                mInterstitialAd.destroy();
                if (listenner != null) {
                    listenner.onAdsClose();
                } else {
                    activity.finish();
                }
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                lg.logs("fb", "onError" + adError.getErrorMessage());
                mInterstitialAd.destroy();
//                if(listenner!=null)listenner.onAdsClose();
            }

            @Override
            public void onAdLoaded(Ad ad) {
                lg.logs("fb", "onAdLoaded");
            }

            @Override
            public void onAdClicked(Ad ad) {
                lg.logs("fb", "onAdClicked");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                lg.logs("fb", "onLoggingImpression");
            }
        });

        if (mInterstitialAd == null ||
                !mInterstitialAd.isAdLoaded()) {
            // Ad not ready to show.
//            setLabel("Ad not loaded. Click load to request an ad.");
            if (listenner != null) {
                listenner.onAdsClose();
            } else {
                activity.finish();
            }
        } else {
            // Ad was loaded, show it!
            mInterstitialAd.show();
//            setLabel("");
//            if(listenner!=null)listenner.onAdsClose();
        }
    }
    // fix in app startapp
//    public static StartAppAd initFullStartAd(Activity activity){
//
//    }

//    public static void showFullStartAd(final Activity activity, StartAppAd startAppAd,
//                                       final OnAdsCloseListenner listenner) {
//        startAppAd.showAd(new AdDisplayListener() {
//            @Override
//            public void adHidden(com.startapp.android.publish.adsCommon.Ad ad) {
//                if (listenner != null) {
//                    listenner.onAdsClose();
//                } else {
//                    activity.finish();
//                }
//            }
//
//            @Override
//            public void adDisplayed(com.startapp.android.publish.adsCommon.Ad ad) {
//
//            }
//
//            @Override
//            public void adClicked(com.startapp.android.publish.adsCommon.Ad ad) {
//
//            }
//
//            @Override
//            public void adNotDisplayed(com.startapp.android.publish.adsCommon.Ad ad) {
//                if (listenner != null) {
//                    listenner.onAdsClose();
//                } else {
//                    activity.finish();
//                }
//            }
//        });
//    }

    public static void actionPaidApp(AppCompatActivity activity) {
        String urlPaid = "https://play.google.com/store/apps/details?id=" + activity.getPackageName() + "paid";
        //if (lg.isDebug) Log.e("actionPaidApp", urlPaid);
        activity.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(urlPaid)));

    }

    public static boolean isInstallAppPro(Context context) {
//        String p1 = context.getPackageName() + "pro";
//        String p2 = context.getPackageName() + "paid";
//        String p3 = context.getPackageName() + "propaid";
//        String p4 = context.getPackageName() + ".pro";
//        String p5 = context.getPackageName() + ".paid";
//        String p6 = context.getPackageName() + ".propaid";
        // check new inappuchase
        boolean isPaid = getInAppPurchase(context);
        return isPaid;//|| isInstallApp(context, p1) || isInstallApp(context, p2) || isInstallApp(context, p3));
        // test app pro
//        return false;//test
    }

    public static boolean isInstallApp(Context context, final String packageName) {
        return !isSpace(packageName) && getLaunchAppIntent(context, packageName) != null;
    }

    private static Intent getLaunchAppIntent(Context context, final String packageName) {
        return context.getPackageManager().getLaunchIntentForPackage(packageName);
    }

    private static boolean isSpace(final String s) {
        if (s == null) return true;
        for (int i = 0, len = s.length(); i < len; ++i) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void showDialogExitApp(final AppCompatActivity context, Object fullAds, adl.AlertListenner listenner) {
        (new adl(context, "Exit App", "Do you want exit app?",
                "Cancel", "Ok", fullAds, listenner)).show();
    }

    public static void showOpenApp(AppCompatActivity context) {
        logger.showPopupTypeInAppHome(context);
//        logger.showPopupTypeInApp(context,3);//test
    }

    public static void showNextScreen(AppCompatActivity context) {
        logger.showPopupTypeInAppNext(context);
//        logger.showPopupTypeInApp(context,3);//test

    }

    //NEW FOLLOW LOADING INAPP?????????????????????

//    public static Object initHomeAds(Activity context) {
//        if (sh.getInstance(context).getUapps() != null) {
//            int type = sh.getInstance(context).getUapps().getPoptrangchu();
//            return initFullAllAds(context, type);
//        }
//        return null;
//    }

//    public static Object initMenuAds(Activity context) {
//        if (sh.getInstance(context).getUapps() != null) {
//            int type = sh.getInstance(context).getUapps().getPopmenu();
//            return initFullAllAds(context, type);
//        }
//        return null;
//    }
//
//    public static Object initDetailAds(Activity context) {
//        if (sh.getInstance(context).getUapps() != null) {
//            int type = sh.getInstance(context).getUapps().getPopvideo();
//            return initFullAllAds(context, type);
//        }
//        return null;
//    }


    //init ads
//    public static Object initFullAllAds(Activity context, int type) {
//        //type = 1;// test type
//        switch (type) {
//            case 1://admob
//                InterstitialAd interstitialAd = new InterstitialAd(context);
//                initFullAdm(context, interstitialAd);
//                return interstitialAd;
//            case 2://startapp
//                try {
//                    String appid = sh.getInstance(context).getUapps().getAppid();
//                    if (appid != null) {
//                        StartAppSDK.init(context, appid, false);//204348715
//                        StartAppAd.disableSplash();
//                        StartAppAd startAppAd = new StartAppAd(context);
//                        return startAppAd;
//                    }
//                    return null;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            case 3://facebook
////                    com.facebook.ads.InterstitialAd fbInterstitialAd1 = new com.facebook.ads.InterstitialAd();
////                    return initFullFb(context);
//                try {
//                    String popInApp = sh.getInstance(context).getUapps().getFb_popupinapp() != null ?
//                            sh.getInstance(context).getUapps().getFb_popupinapp() :
//                            sh.getInstance(context).getUapps().getFb_popup();
////                    popInApp = "236506170534964_349803259205254";
//                    final com.facebook.ads.InterstitialAd mInterstitialAd =
//                            new com.facebook.ads.InterstitialAd(context, popInApp);
//
//                    // Load a new interstitial.
//                    mInterstitialAd.loadAd(EnumSet.of(CacheFlag.VIDEO));
//                    mInterstitialAd.setAdListener(new InterstitialAdListener() {
//                        @Override
//                        public void onInterstitialDisplayed(Ad ad) {
//                            lg.logs("fb", "onInterstitialDisplayed");
//                        }
//
//                        @Override
//                        public void onInterstitialDismissed(Ad ad) {
//                            lg.logs("fb", "onInterstitialDismissed");
//                            mInterstitialAd.destroy();
//                            //mInterstitialAd.loadAd(EnumSet.of(CacheFlag.VIDEO));
//                            //if(listenner!=null)listenner.onAdsClose();
//                        }
//
//                        @Override
//                        public void onError(Ad ad, AdError adError) {
//                            lg.logs("fb", "onError" + adError.getErrorMessage());
////                if(listenner!=null)listenner.onAdsClose();
//                        }
//
//                        @Override
//                        public void onAdLoaded(Ad ad) {
//                            lg.logs("fb", "onAdLoaded");
//                        }
//
//                        @Override
//                        public void onAdClicked(Ad ad) {
//                            lg.logs("fb", "onAdClicked");
//                        }
//
//                        @Override
//                        public void onLoggingImpression(Ad ad) {
//                            lg.logs("fb", "onLoggingImpression");
//                        }
//                    });
//                    return mInterstitialAd;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            case 4://unity
//
//            default:
//                break;
//        }
//        return null;
//    }

    //start screen => check ads loaded? => load ads
//    public static void loadHomeAds() {
//
//    }

    //show when click or gameover => show ads
//    public static void showFullAllAds(Activity activity, Object interstitialAd, OnAdsCloseListenner listenner) {
//        // check minute >=2 show
//        if (checkMinute(activity)) {
//            if (interstitialAd != null && interstitialAd instanceof InterstitialAd) {
//                showFullAdm(activity, ((InterstitialAd) interstitialAd), listenner);
//            } else if (interstitialAd != null && interstitialAd instanceof com.facebook.ads.InterstitialAd) {
//                showFullFb(activity, ((com.facebook.ads.InterstitialAd) interstitialAd), listenner);
//            } else if (interstitialAd != null && interstitialAd instanceof StartAppAd) {
//                showFullStartAd(activity, ((StartAppAd) interstitialAd), listenner);
//            } else {
////            lg.logs("showFullAllAds", "error");
//                if (listenner != null) {
//                    listenner.onAdsClose();
//                } else {
//                    activity.finish();
//                }
//            }
//        } else {
//            if (listenner != null) {
//                listenner.onAdsClose();
//            } else {
//                activity.finish();
//            }
//        }
//
//    }

    ////VIDEO reward  ////
//    public static void initVideoRewardAdm(final Activity mActivity, final RewardedVideoAd rewardedVideoAd) {
//        loadNewVideoRewardAdm(mActivity, rewardedVideoAd);
//    }

//    private static void loadNewVideoRewardAdm(Activity mActivity, RewardedVideoAd rewardedVideoAd) {
//        if (!rewardedVideoAd.isLoaded()) {
//
//            Bundle extras = new Bundle();
//            boolean isPass = sh.getInstance(mActivity).getUserConsent();
//            if (!isPass)
//                extras.putString("npa", "1");
//            String content_rate = sh.getInstance(mActivity).getUapps().getAdmob_fil() != null ?
//                    sh.getInstance(mActivity).getUapps().getAdmob_fil() : "T";
//            extras.putString("max_ad_content_rating", content_rate);
//
//            AdRequest adRequest = new AdRequest.Builder()
//                    //.addNetworkExtrasBundle(AdMobAdapter.class, extras)
////                    .addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
//                    .build();
//
//            rewardedVideoAd.loadAd(VIDEO_REWARD_ADM,
//                    adRequest);
//        }
//    }

//    public static void showVideoRewardAdm(final Activity activity, final RewardedVideoAd rewardedVideoAd,
//                                          final OnAdsCloseListenner listenner) {
//        final viewdlg dlg = new viewdlg(activity);
//        dlg.showDialog();
//        if (rewardedVideoAd != null) {
//            rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
//                @Override
//                public void onRewardedVideoAdLoaded() {
//                    lg.logs("rewar", "onRewardedVideoAdLoaded");
//                }
//
//                @Override
//                public void onRewardedVideoAdOpened() {
//                    lg.logs("rewar", "onRewardedVideoAdOpened");
//                }
//
//                @Override
//                public void onRewardedVideoStarted() {
//                    lg.logs("rewar", "onRewardedVideoStarted");
//                }
//
//                @Override
//                public void onRewardedVideoAdClosed() {
//                    lg.logs("rewar", "onRewardedVideoAdClosed");
//                    dlg.hideDialog();
//                    loadNewVideoRewardAdm(activity, rewardedVideoAd);
//                    if (listenner != null) {
//                        listenner.onAdsClose();
//                    } else {
//                        activity.finish();
//                    }
//                    ;
//                }
//
//                @Override
//                public void onRewarded(RewardItem rewardItem) {
//                    lg.logs("rewar", "onRewarded");
//                    //addCoins(reward.getAmount());//HERE
//                }
//
//                @Override
//                public void onRewardedVideoAdLeftApplication() {
//                    lg.logs("rewar", "onRewardedVideoAdLeftApplication");
//                }
//
//                @Override
//                public void onRewardedVideoAdFailedToLoad(int i) {
//                    lg.logs("rewar", "onRewardedVideoAdFailedToLoad id= " + i);
//                }
//
////                @Override
////                public void onRewardedVideoCompleted() {
////                    lg.logs("rewar", "onRewardedVideoCompleted");
////                }
//            });
//
//            if (rewardedVideoAd.isLoaded()) {
//                rewardedVideoAd.show();
//            } else {
//                if (listenner != null) {
//                    listenner.onAdsClose();
//                } else {
//                    activity.finish();
//                }
//                dlg.hideDialog();
//            }
//        } else {
//            if (listenner != null) {
//                listenner.onAdsClose();
//            } else {
//                activity.finish();
//            }
//            dlg.hideDialog();
//        }
//    }

//    public static Object initVideoRewardAllAds(Activity context, int type) {
//        type = 1;// test type
//        switch (type) {
//            case 1://admob
//                RewardedVideoAd rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
//                initVideoRewardAdm(context, rewardedVideoAd);
//                return rewardedVideoAd;
//        }
//        return null;
//    }

//    public static void showVideoRewardAllAds(Activity activity, Object rewardedVideoAd, OnAdsCloseListenner listenner) {
//        if (rewardedVideoAd instanceof RewardedVideoAd) {
//            showVideoRewardAdm(activity, (RewardedVideoAd) rewardedVideoAd, listenner);
//        }
//    }

//    public static void onPauseVideoRewardAll(Activity activity, Object rewardedVideoAd) {
//        if (rewardedVideoAd != null && rewardedVideoAd instanceof RewardedVideoAd) {
//            ((RewardedVideoAd) rewardedVideoAd).pause(activity);
//        }
//    }

//    public static void onResumeVideoRewardAll(Activity activity, Object rewardedVideoAd) {
//        if (rewardedVideoAd != null && rewardedVideoAd instanceof RewardedVideoAd) {
//            ((RewardedVideoAd) rewardedVideoAd).resume(activity);
//        }
//    }

    private static boolean checkMinute(AppCompatActivity activity) {
        if (getInAppPurchase(activity))
            return false;//if InAPP alway hide ads
//
//        //type = 1;
//        int type = 11;
//        long timeCurrent = SystemClock.elapsedRealtime();
//        long timerSave = sh.getInstance(activity).getLongData("MinuteTime" + type);
//        int timeDelay = lg.TIME_DELAY_SHOW * 60000;
//
//        if (sh.getInstance(activity).getUapps() != null && sh.getInstance(activity).getUapps().getAdsdelay() >= lg.TIME_DELAY_SHOW &&
//                sh.getInstance(activity).getUapps().getAdsdelay() <= 10)
//            timeDelay = sh.getInstance(activity).getUapps().getAdsdelay() * 60000;
//        logs("checkMinute", "checkMinute type>>" + timerSave + " " + type + ">>" + timeCurrent + ">>" + timeDelay);
//        if ((timeCurrent - timerSave > timeDelay) || (timeCurrent - timerSave < -timeDelay)) {
//            sh.getInstance(activity).storeLong("MinuteTime" + type, timeCurrent);
//            return true;
//        } else
//            // remove check minute;
////            return true;//test
        return true;
    }

    public static String[] getAdmob_banner_home0(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_banner_home0() != null &&
                sh.getInstance(context).getUapps().getAdmob_banner_home0().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_banner_home0();
        } else
            return lg.admob_banner_home0;
    }

    public static String[] getadmob_banner_other1(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_banner_other1() != null &&
                sh.getInstance(context).getUapps().getAdmob_banner_other1().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_banner_other1();
        } else
            return lg.admob_banner_other1;
    }

    public static String[] getAdmob_banner_exit2(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_banner_exit2() != null &&
                sh.getInstance(context).getUapps().getAdmob_banner_exit2().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_banner_exit2();
        } else
            return lg.admob_banner_exit2;
    }

    public static String[] getAdmob_native_tophome3(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_tophome3() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_tophome3().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_native_tophome3();
        } else
            return lg.admob_native_tophome3;
    }

    public static String[] getAdmob_native_bottomhome4(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_bottomhome4() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_bottomhome4().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_native_bottomhome4();
        } else
            return lg.admob_native_bottomhome4;
    }

    public static String[] getAdmob_native_setting5(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_setting5() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_setting5().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_native_setting5();
        } else
            return lg.admob_native_setting5;
    }

    public static String[] getAdmob_native_other6(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_other6() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_other6().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_native_other6();
        } else
            return lg.admob_native_other6;
    }

    public static String[] getAdmob_popup_openapp7(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_openapp7() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_openapp7().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_popup_openapp7();
        } else
            return lg.admob_popup_openapp7;
    }

    public static String[] getAdmob_popup_inapp8(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_inapp8() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_inapp8().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_popup_inapp8();
        } else
            return lg.admob_popup_inapp8;
    }

    public static String[] getAdmob_popup_exit9(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_exit9() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_exit9().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_popup_exit9();
        } else
            return lg.admob_popup_exit9;
    }

    public static String[] getAdmob_reward10(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_reward10() != null &&
                sh.getInstance(context).getUapps().getAdmob_reward10().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_reward10();
        } else
            return lg.admob_reward10;
    }

    public static String[] getAdmob_popup_detail11(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_detail11() != null &&
                sh.getInstance(context).getUapps().getAdmob_popup_detail11().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_popup_detail11();
        } else
            return lg.admob_popup_detail11;
    }

    public static String[] getAdmob_new12(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_news12() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_news12().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_native_news12();
        } else
            return lg.admob_native_new12;
    }


    public static String[] getAdmob_page13(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_page13() != null &&
                sh.getInstance(context).getUapps().getAdmob_native_page13().length > 0) {
            return sh.getInstance(context).getUapps().getAdmob_native_page13();
        } else
            return lg.admob_native_page13;
    }


    //facebook

    public static String[] getFb_banner_home0(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_banner_home0() != null &&
                sh.getInstance(context).getUapps().getFb_banner_home0().length > 0) {
            return sh.getInstance(context).getUapps().getFb_banner_home0();
        } else
            return lg.fb_banner_home0;
    }


    public static String[] getFb_banner_other1(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_banner_other1() != null &&
                sh.getInstance(context).getUapps().getFb_banner_other1().length > 0) {
            return sh.getInstance(context).getUapps().getFb_banner_other1();
        } else
            return lg.fb_banner_other1;
    }

    public static String[] getFb_banner_exit2(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_banner_exit2() != null &&
                sh.getInstance(context).getUapps().getFb_banner_exit2().length > 0) {
            return sh.getInstance(context).getUapps().getFb_banner_exit2();
        } else
            return lg.fb_banner_exit2;
    }

    public static String[] getFb_native_tophome3(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_native_tophome3() != null &&
                sh.getInstance(context).getUapps().getFb_native_tophome3().length > 0) {
            return sh.getInstance(context).getUapps().getFb_native_tophome3();
        } else
            return lg.fb_native_tophome3;
    }

    public static String[] getFb_native_bottomhome4(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_native_bottomhome4() != null &&
                sh.getInstance(context).getUapps().getFb_native_bottomhome4().length > 0) {
            return sh.getInstance(context).getUapps().getFb_native_bottomhome4();
        } else
            return lg.fb_native_bottomhome4;
    }

    public static String[] getFb_native_setting5(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_native_setting5() != null &&
                sh.getInstance(context).getUapps().getFb_native_setting5().length > 0) {
            return sh.getInstance(context).getUapps().getFb_native_setting5();
        } else
            return lg.fb_native_setting5;
    }

    public static String[] getFb_native_other6(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_native_other6() != null &&
                sh.getInstance(context).getUapps().getFb_native_other6().length > 0) {
            return sh.getInstance(context).getUapps().getFb_native_other6();
        } else
            return lg.fb_native_other6;
    }

    public static String[] getFb_popup_openapp7(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_popup_openapp7() != null &&
                sh.getInstance(context).getUapps().getFb_popup_openapp7().length > 0) {
            return sh.getInstance(context).getUapps().getFb_popup_openapp7();
        } else
            return lg.fb_popup_openapp7;
    }

    public static String[] getFb_popup_inapp8(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_popup_inapp8() != null &&
                sh.getInstance(context).getUapps().getFb_popup_inapp8().length > 0) {
            return sh.getInstance(context).getUapps().getFb_popup_inapp8();
        } else
            return lg.fb_popup_inapp8;
    }

    public static String[] getFb_popup_out9(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_popup_out9() != null &&
                sh.getInstance(context).getUapps().getFb_popup_out9().length > 0) {
            return sh.getInstance(context).getUapps().getFb_popup_out9();
        } else
            return lg.fb_popup_out9;
    }

    public static String[] getFb_reward10(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_reward10() != null &&
                sh.getInstance(context).getUapps().getFb_reward10().length > 0) {
            return sh.getInstance(context).getUapps().getFb_reward10();
        } else
            return lg.fb_reward10;
    }

    public static String[] getFb_popup_detail11(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_popup_detail11() != null &&
                sh.getInstance(context).getUapps().getFb_popup_detail11().length > 0) {
            return sh.getInstance(context).getUapps().getFb_popup_detail11();
        } else
            return lg.fb_popup_detail11;
    }

    public static String[] getFb_new12(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_native_news12() != null &&
                sh.getInstance(context).getUapps().getFb_native_news12().length > 0) {
            return sh.getInstance(context).getUapps().getFb_native_news12();
        } else
            return lg.fb_native_new12;
    }


    public static String[] getFb_page13(Context context) {
        if (sh.getInstance(context).getUapps() != null &&
                sh.getInstance(context).getUapps().getFb_native_page13() != null &&
                sh.getInstance(context).getUapps().getFb_native_page13().length > 0) {
            return sh.getInstance(context).getUapps().getFb_native_page13();
        } else
            return lg.fb_native_page13;
    }

    public static int isShowPopupInapp(Context context) {// =1 show  else not show
        if (sh.getInstance(context).getUapps() != null)
            return sh.getInstance(context).getUapps().getIsmyads();
        else return 1;
    }

//    private int count_show_ads_openapp; //Dùng cho Client điểu khiển biến sau số lần xuất hiện popup
//    private int count_dialog_premium_openapp;//Dùng cho Client điểu khiển biến sau số lần xuất hiện popup Premium

    public static int getCount_show_ads_openapp(Context context) {
        if (sh.getInstance(context).getUapps() != null) {
            return sh.getInstance(context).getUapps().getCount_show_ads_openapp();
        } else
            return 1;

    }

    public static int getCount_dialog_premium_openapp(Context context) {
        if (sh.getInstance(context).getUapps() != null) {
            return sh.getInstance(context).getUapps().getCount_dialog_premium_openapp();
        } else
            return 1;

    }

    public interface OnAdsCloseListenner {
        public void onAdsClose();
    }

    //06/09/2019

    public static void showDialogUpdate(final AppCompatActivity context) {
        (new adl(context, "New Update !!!", "Lots of new features!! upgrade to the newest version.",
                "Cancel", "Update", null, new adl.AlertListenner() {
            @Override
            public void onCancelClick() {

            }

            @Override
            public void onOkClick() {
                String PACKAGE_NAME = BuildConfig.APPLICATION_ID;
                try {
                    String packageId = sh.getInstance(context).getUapps().getPackId();
                    context.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + packageId)));
                } catch (Exception e) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + PACKAGE_NAME)));
                }
            }
        })).show();
    }


    //18/09/2019 update premium user
    public static void setInAppPurchase(Context context, boolean paid) {
        sh.getInstance(context).storeInAppPurchase(paid);
    }

    public static boolean getInAppPurchase(Context context) {
        return sh.getInstance(context).getInAppPurchase();
    }

    public static void setUserType(Context context, int type) {
        sh.getInstance(context).storeUserType(type);
    }

    public static int getUserType(Context context) {
        return sh.getInstance(context).getUserType();
    }

    public static void updateDataUserInfo(final AppCompatActivity context, int type) {
        logger.setUserType(context, type);
        new se(context).getE();
    }

    public static String getKeyData(Context context) {
        if (context != null && sh.getInstance(context).getUapps() != null && sh.getInstance(context).getUapps().getKeydata() != null) {
            return sh.getInstance(context).getUapps().getKeydata();
        } else return "V1ZoT2EyVnVhR3BhTTFwcC1VZHdkV0V5TUhoTlp8MDk=";
    }
}

