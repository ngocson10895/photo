//package com.google.android.gms.corebase;
//
//import android.app.Activity;
//import android.os.Bundle;
//
//import com.google.ads.mediation.admob.AdMobAdapter;
//import com.google.android.gms.ads.AdListener;
//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.InterstitialAd;
//import com.google.android.gms.ads.MobileAds;
//
///**
// * Created by minthu on 2/21/17.
// */
//
//public class ad {
//    private Activity mContext;
//    private InterstitialAd mInterstitialAd;
//
//    public static ad mInstance;
//    private static boolean isInApp;
//
//    public static ad getInstance(Activity context, boolean background) {
//        if (mInstance == null) {
//            mInstance = new ad(context);
//        }
//        isInApp = background;
//        return mInstance;
//    }
//
//    public ad(Activity context) {
//        mContext = context;
//        lg.logs("ad", sh.getInstance(mContext).getUapps().getPopup());
//        MobileAds.initialize(context);
//        MobileAds.setAppMuted(true);
//        mInterstitialAd = new InterstitialAd(context);
//        if (isInApp) {
//            String popInApp = sh.getInstance(mContext).getUapps().getPopupinapp() != null ?
//                    sh.getInstance(mContext).getUapps().getPopupinapp() :
//                    sh.getInstance(mContext).getUapps().getPopup();
//            mInterstitialAd.setAdUnitId(popInApp);
//        } else {
//            mInterstitialAd.setAdUnitId(sh.getInstance(mContext).getUapps().getPopup());
//        }
//
//        mInterstitialAd.setAdListener(adListener);
//
//    }
//
//    public void loadAds() {
////        Bundle extras = new Bundle();
////        //TODO Check non-personalized ads
////        boolean isPass = sh.getInstance(mContext).getUserConsent();
////        if (!isPass)
////            extras.putString("npa", "1");
////        String content_rate = sh.getInstance(mContext).getUapps().getAdmob_fil() != null ?
////                sh.getInstance(mContext).getUapps().getAdmob_fil() : "T";
////        extras.putString("max_ad_content_rating", content_rate);
////
////        AdRequest adRequest = new AdRequest.Builder()
////                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
////                .build();
//        try {
//            mInterstitialAd.loadAd(ut.getDefaultAdRequest(mContext));
//        } catch (Exception e) {
//            lg.logs("ad", e.toString());
//        }
//
//    }
//
//    AdListener adListener = new AdListener() {
//        @Override
//        public void onAdClosed() {
//            super.onAdClosed();
//            if (!isInApp && mContext instanceof Activity)
//                ((Activity) mContext).finish();
//            lg.logs("ad", "onAdClosed");
//        }
//
//        @Override
//        public void onAdFailedToLoad(int i) {
//            super.onAdFailedToLoad(i);
//            //loadAds();
//            if (!isInApp && mContext instanceof Activity)
//                ((Activity) mContext).finish();
//            lg.logs("ad", "onAdFailedToLoad");
//        }
//
//        @Override
//        public void onAdLeftApplication() {
//            super.onAdLeftApplication();
//            if (!isInApp && mContext instanceof Activity)
//                ((Activity) mContext).finish();
//            lg.logs("ad", "onAdLeftApplication");
//        }
//
//        @Override
//        public void onAdOpened() {
//            super.onAdOpened();
//            if (!isInApp && mContext instanceof Activity)
//                ((Activity) mContext).finish();
//            lg.logs("ad", "onAdOpened");
//        }
//
//        @Override
//        public void onAdLoaded() {
//            super.onAdLoaded();
//            lg.logs("ad", "onAdLoaded");
//            //RxBus.$().post(ad.class,true);
//            mInterstitialAd.show();
//        }
//    };
//
//    public void showAds() {
//        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
//            mInterstitialAd.show();
//        } else {
//            loadAds();
//        }
//    }
//}
//
