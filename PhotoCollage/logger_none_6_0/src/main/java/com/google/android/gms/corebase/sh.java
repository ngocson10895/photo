package com.google.android.gms.corebase;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;

//import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by minthu on 2/24/17.
 */

public class sh {
    private Context mContext;
    private static AppCompatActivity mActivity;
    public static sh mInstance;
    private SharedPreferences mW;

    public static sh getInstance(Context context) {
        if (mInstance == null)
            mInstance = new sh(context);

        return mInstance;
    }


    public static sh getInstance(AppCompatActivity context) {
        if (mInstance == null)
            mInstance = new sh(context, context);
        mActivity = context;
        return mInstance;
    }

    public sh(Context context) {
        mContext = context;
        mW = context.getSharedPreferences(lg.mD, Context.MODE_PRIVATE);
    }

    public sh(AppCompatActivity activity, Context context) {
        mActivity = activity;
        mContext = context;
        mW = context.getSharedPreferences(lg.mD, Context.MODE_PRIVATE);
    }

    public void storeString(String key, String content) {
        SharedPreferences.Editor editor = mW.edit();
        editor.putString(key, content);
        editor.apply();
    }

    public String getStringData(String key) {
        return mW.getString(key, "");
    }

    public String getStringData(String key, String name) {
        return mW.getString(key, name);
    }

    public void storeInt(String key, int content) {
//        String version = "1";
//        if (mContext != null &&
//                sh.getInstance(mContext).getUapps() != null)
//            version = sh.getInstance(mContext).getUapps().getVersionCode() + "";

        SharedPreferences.Editor editor = mW.edit();
        editor.putInt(key , content);//+ version
        editor.apply();
    }

//    public void storeFloat(String key, float content) {
//        SharedPreferences.Editor editor = mW.edit();
//        editor.putFloat(key, content);
//        editor.apply();
//    }

//    public float getFloatData(String key, float name) {
//        return mW.getFloat(key, name);
//    }

//    public int getIntData(String key) {
//        return mW.getInt(key, 0);
//    }

    public int getIntData(String key, int name) {
//        String version = "1";
//        if (mContext != null &&
//                sh.getInstance(mContext).getUapps() != null)
//            version = sh.getInstance(mContext).getUapps().getVersionCode() + "";

        return mW.getInt(key , name);//+ version
    }

    public void storeLong(String key, long content) {
//        String version = "1";
//        if (mContext != null &&
//                sh.getInstance(mContext).getUapps() != null)
//            version = sh.getInstance(mContext).getUapps().getVersionCode() + "";

        SharedPreferences.Editor editor = mW.edit();
        editor.putLong(key, content);// + version
        editor.apply();
    }

    public long getLongData(String key) {
//        String version = "1";
//        if (mContext != null &&
//                sh.getInstance(mContext).getUapps() != null)
//            version = sh.getInstance(mContext).getUapps().getVersionCode() + "";

        return mW.getLong(key , 0);//+ version
    }

    public SharedPreferences getmW() {
        return mW;
    }

    public void setmW(SharedPreferences mW) {
        this.mW = mW;
    }

    public void storeU(u item) {
        Gson gson = new Gson();
        Type type = new TypeToken<u>() {
        }.getType();
        storeString(lg.mUapp, gson.toJson(item, type));
    }

    public u getUapps() {
        Gson gson = new Gson();
        Type type = new TypeToken<u>() {
        }.getType();
        return gson.fromJson(getStringData(lg.mUapp, "{}"), type);
    }

    public void loadAdmob(boolean isInApp) {
////        if (!checkMinute(1)) return;
////        if (isInApp && mActivity != null) {
////            //ad.getInstance(mContext).loadAds();
////            ad.getInstance(mActivity, isInApp).showAds();
////        } else {
//            if(!logger.isInstallAppPro(mActivity)){
//                Intent i = new Intent(mContext, ac.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.putExtra(lg.EXTRA_INTENT_PARAM, lg.EXTRA_ACTIVITY.ADMOB);
//                i.putExtra(lg.EXTRA_INTENT_MESSAGE,"");
//                mContext.startActivity(i);
//            }
////        }
    }

    public void loadStartApp(boolean isInApp) {
//        if (!checkMinute(2)) return;
////        st.getInstance(mContext).loadAds();
//        if (isInApp && mActivity != null) {
//            //ad.getInstance(mContext).loadAds();
//            st.getInstance(mActivity).loadAds();//
//        } else {
//            if(!logger.isInstallAppPro(mActivity)){
//                Intent i = new Intent(mContext, ac.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.putExtra(lg.EXTRA_INTENT_PARAM, lg.EXTRA_ACTIVITY.STARTAPP);
//                mContext.startActivity(i);
//            }
//        }
    }

    public void loadFacebook(boolean isInApp) {
////        if (!checkMinute(3)) return;
////        if (isInApp && mContext instanceof Activity) {
////            fb.getInstance(mActivity, isInApp).loadAds();
////        } else {
//            if(!logger.isInstallAppPro(mActivity)) {
//                Intent i = new Intent(mContext, ac.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.putExtra(lg.EXTRA_INTENT_PARAM, lg.EXTRA_ACTIVITY.FACBOOK);
//                mContext.startActivity(i);
//            }
////        }
//        //fb.getInstance(mContext).loadAds();
    }

    public void getAds(int adType, boolean inApp) {
        switch (adType) {
            case 1:
                loadAdmob(inApp);
                break;
            case 2:
                loadStartApp(inApp);
                break;
            case 3:
                loadFacebook(inApp);
                break;
            case 12:
                int rand12 = lg.randomAB(1, 2);
                if (rand12 == 1) {
                    loadAdmob(inApp);
                } else {
                    loadStartApp(inApp);
                }
                break;
            case 13:
                int rand13 = lg.randomAB(1, 2);
                if (rand13 == 1) {
                    loadAdmob(inApp);
                } else {
                    loadFacebook(inApp);
                }
                break;
            case 23:
                int rand23 = lg.randomAB(1, 2);
                if (rand23 == 1) {
                    loadStartApp(inApp);
                } else {
                    loadFacebook(inApp);
                }
                break;
            case 123:
                int rand123 = lg.randomAB(1, 3);
                if (rand123 == 1) {
                    loadAdmob(inApp);
                } else if (rand123 == 2) {
                    loadStartApp(inApp);
                } else {
                    loadFacebook(inApp);
                }
                break;
            default:
                break;
        }
    }

    private boolean checkMinute(int type) {
        if(getInAppPurchase())
            return false;
        //type = 1;
//        long timeCurrent = SystemClock.elapsedRealtime();
//        long timerSave = getLongData("MinuteTime" + type);
//        int timeDelay = lg.TIME_DELAY_SHOW * 60000;
//
//        if (getUapps() != null && getUapps().getAdsdelay() >= 2 && getUapps().getAdsdelay() <= 10)
//            timeDelay = getUapps().getAdsdelay() * 60000;
//        logs("checkMinute", "checkMinute type>>" + timerSave + " " + type + ">>" + timeCurrent + ">>" + timeDelay);
//        if ((timeCurrent - timerSave > timeDelay) || (timeCurrent - timerSave < -timeDelay)) {
//            storeLong("MinuteTime" + type, timeCurrent);
//            return true;
//        } else
        // remove check minute;
        return true;//test
//        return false;
    }

    public boolean getUpdateMenu() {
        return getIntData("UpdateTVMenu", 1) == 1;
    }

    public void storeUpdateMenu(boolean status) {
        storeInt("UpdateTVMenu", status ? 1 : 0);
    }

    public boolean isAlarmAvaiable() {
        int timeInstall;//        int timeInstall, ;
        if (getLongData(lg.mTimer) <= 0) {
            storeLong(lg.mTimer, new Date().getTime());
        }
        long timeClock = getLongData(lg.mTimer);

        if (getUapps() != null) {
            u mU = getUapps();
            int timeUnit;
            switch (mU.getUnitTime()) {
                case 1:// min
                    timeUnit = 1 * 60;
                    break;
                case 2://second
                    timeUnit = 1;
                    break;
                default://hour
                    timeUnit = 1 * 60 * 60;
                    break;
            }
            //fixbug init = 0
            timeInstall = mU.getTimeafterintall() > 0 ?
                    mU.getTimeafterintall() * timeUnit :
                    lg.TIME_AFTER_INSTALL * 3600;
            //timeDelayShow = mU.getTimepopshow() * timeUnit;
        } else {
            timeInstall = lg.TIME_AFTER_INSTALL * 3600;//defaul hour
            //timeDelayShow = lg.TIME_SHOW_DEFAULT * 3600;//defaul hour
        }

        long timeCurrent = new Date().getTime();
        long subTime = (timeCurrent - timeClock) / 1000;//seconds

        boolean avaiable = (subTime - timeInstall) > 0;
        lg.logs("avaiable", "subTime " + subTime + " timeInstall " + timeInstall + avaiable);

        //todo test true
//        return true;//test
        return avaiable;
    }

    public boolean isDailyAvaiable(){
        if (getLongData(lg.mDaily) <= 0) {
            storeLong(lg.mDaily, new Date().getTime());
        }
        long timeClock = getLongData(lg.mDaily);
        long timeCurrent = new Date().getTime();
        long subTime = (timeCurrent - timeClock) / 1000;//seconds
        int timeInstall = 12 * 3600;
        boolean avaiable = (subTime - timeInstall) > 0;

        // test true
//        return true;//todo test
        return avaiable;
    }

//    public static String getApp3(String app3) {//update
//        String s1 = fu.Detoken(app3);
//        String s2 = se.decrypt(s1, ex.getStZ());
//        String s3 = fu.Detoken(s2);
//        return s3;
//    }

    public static String getApp3(String app3, String app4) {//update
        String s1 = fu.Detoken(app3);
        String s2 = se.decrypt(s1, app4);
        String s3 = fu.Detoken(s2);
        return s3;
    }


    public boolean isAlarmActive() {
        return getIntData(lg.mAlarm, 0) == 1;
    }

    public boolean isDayActive() {
        return getIntData(lg.mDay, 0) == 1;
    }

    public boolean isCallIdle() {
        return getIntData(lg.mCall, 0) == 0;
    }

    public boolean isDebugLog() {
        if (sh.mInstance != null && sh.mInstance.getUapps() != null &&
                sh.mInstance.getUapps().getDebug() >= 10) {//test == 0
            return true;
        } else
//            return true;//test
            return false;

    }

    // User Consent (GDPR)
    public boolean getUserConsent() {
        return getIntData("Consent", 1) == 1;
    }

    public void setUserConsent(boolean isConsent) {
        storeInt("Consent", isConsent ? 1 : 0);
    }

    public boolean getAdsConsentVisible() {
        return getIntData("AdsConsent", 1) == 1;
    }

    public void setAdsConsentVisible(boolean isVisible) {
        storeInt("AdsConsent", isVisible ? 1 : 0);
    }

    public boolean getInAppPurchase() {
        return getIntData("InAppPurchase", 0) == 1;
    }

    public void storeInAppPurchase(boolean status) {
        storeInt("InAppPurchase", status ? 1 : 0);
    }

    public int getUserType() {
        return getIntData("UserType", 1);
    }

    public void storeUserType(int type) {
        storeInt("UserType", type);
    }
}
