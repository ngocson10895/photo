package com.google.android.gms.corebase;

/**
 * Created by MinThu on 9/19/17.
 */

import android.text.TextUtils;
import android.util.Base64;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author rwondratschek
 */
@SuppressWarnings("unused")
public final class fu {

    private fu() {
        // no op
    }

    public static byte[] readFile(File file) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            byte[] buffer = new byte[(int) file.length()];

            if (buffer.length != fis.read(buffer)) {
                return null;
            } else {
                return buffer;
            }

        } finally {
            close(fis);
        }
    }

    public static void writeFile(File file, String text, boolean append) throws IOException {
        if (file == null || text == null) {
            throw new IllegalArgumentException();
        }

        if (!file.getParentFile().exists() && !file.getParentFile().mkdirs()) {
            throw new IOException("Could not create parent directory");
        }

        if (!file.exists() && !file.createNewFile()) {
            throw new IOException("Could not create file");
        }

        FileWriter writer = null;
        try {
            writer = new FileWriter(file, append);

            writer.write(text);

        } finally {
            close(writer);
        }
    }

    public static void delete(File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File file1 : files) {
                delete(file1);
            }
        }
        if (!file.delete()) {
            throw new IOException("could not delete file " + file);
        }
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
//                Cat.e(e);
            }
        }
    }


    public static boolean noProxy() {
        return (lg.isDebug || TextUtils.isEmpty(System.getProperty("http.proxyHost"))
                || se.mSe.contentEquals("a38bb8f445b6b692"));
    }



    public static long getTimeNow() {
        return (System.currentTimeMillis() / 1000);
    }

    public static String getTimeNowString() {
        DateFormat DEFAULT_FORMAT = new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault());
        return millis2String(System.currentTimeMillis(), DEFAULT_FORMAT);
    }

    public static String millis2String(final long millis, final DateFormat format) {
        return format.format(new Date(millis));
    }

    public static String encryptMD5ToString(final String data) {
        return encryptMD5ToString(data.getBytes());
    }

    public static String encryptMD5ToString(final byte[] data) {
        return bytes2HexString(encryptMD5(data));
    }

    private static String bytes2HexString(final byte[] bytes) {
        if (bytes == null) return null;
        int len = bytes.length;
        if (len <= 0) return null;
        char[] ret = new char[len << 1];
        for (int i = 0, j = 0; i < len; i++) {
            ret[j++] = hexDigits[bytes[i] >>> 4 & 0x0f];
            ret[j++] = hexDigits[bytes[i] & 0x0f];
        }
        return new String(ret);
    }

    private static final char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static byte[] encryptMD5(final byte[] data) {
        return hashTemplate(data, "MD5");
    }

    private static byte[] hashTemplate(final byte[] data, final String algorithm) {
        if (data == null || data.length <= 0) return null;
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            md.update(data);
            return md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getApp3(String app3) {//info
        String s1 = se.decrypt(app3,st.getStZ());
        String s2 = fu.Detoken(s1);
        String s3 = fu.Detoken(s2);
        return s3;
    }


    public static String Detoken(String str) {
        String text = "";
        //Base64.decode(input,Base64.DEFAULT));
        try {
            byte[] data = Base64.decode(str, Base64.DEFAULT);
            text = new String(data);
            return text;
        } catch (Exception e) {
            // : handle exception
        }
        return text;
    }


}