package com.google.android.gms.corebase;

/**
 * Created by MinThu on 2/20/17.
 */

public class u {
    private String LinkDownload;
    private long versionCode;
    private String versionName;

//    @SerializedName("package")
//    private String pack;

    private String store;
    //admob
//    private String banner;
//    private String popup;
//    private String popupinapp;
//    private String popuphome;
//    private String popupdetail;
//    private String ad_native;
//    private String ad_nativehome;
//    private String ad_nativedetail;
//    private String ad_nativemenu;
//    private String ad_rewardhome;
//    private String ad_rewarddetail;

//    private String devid;//startapp
//    private String appid;//startapp

    //fb
//    private String fb_banner;
//    private String fb_popup;
//    private String fb_popupinapp;
//    private String fb_native;

//    private String fb_nativehome;
//    private String fb_nativedetail;
//    private String fb_nativemenu;
//    private String fb_rewardhome;
//    private String fb_rewarddetail;

    //    private String uni_popupinapp;
//    private String uni_popup;
//    private String ga;
    private String adwordtracking;
    private String facebookappid;
    private int timeafterintall;
    private int timepopshow;
    private int poptimeshow;
    private int popunlockscreen;
    private int popchangenet;
    private int popopenapp;//open
    private int popopenapp2;//exitapp
    private int issplash;

    private int bantrangchu;
    private int banvideo;
    private int poptrangchu;
    private int popmenu;
    private int popvideo;
    private int israte;
    private int maxran;
    private String sendid;
    private String reg;
    private String ads;
    private String androidkey;
    private int unitTime;
    private String titleNote;
    private String mesageNote;
    private String urlNote;
    private String configapp;
    private String linkdata;
    private String keydata;
    private long delaynet;
    private long delayunlockscreen;
    private long delaypopup;
    private int exittype;
    private int adsdelay;

    private int isservices;
    private int debug;
    private String admob_fil; //T
    private String publisherIds;
    private int ismediation;
    private int ismyads;

//    private String[] ad_native_all;
//    private String[] ad_nativemenu_all;
//    private String[] popup_all;
//    private String[] banner_all;
//    private String[] popupout_all;
//    private String[] ad_reward_all;

    //06/09/2019
    private String packid;
    private String[] admob_banner_home0;
    private String[] admob_banner_other1;
    private String[] admob_banner_exit2;
    private String[] admob_native_tophome3;
    private String[] admob_native_bottomhome4;
    private String[] admob_native_setting5;
    private String[] admob_native_other6;
    private String[] admob_popup_openapp7;
    private String[] admob_popup_inapp8;
    private String[] admob_popup_exit9;
    private String[] admob_reward10;
    private String[] admob_popup_detail11;
    private String[] admob_native_news12;
    private String[] admob_native_page13;

    //facebook
    private String[] fb_banner_home0;
    private String[] fb_banner_other1;
    private String[] fb_banner_exit2;
    private String[] fb_native_tophome3;
    private String[] fb_native_bottomhome4;
    private String[] fb_native_setting5;
    private String[] fb_native_other6;
    private String[] fb_popup_openapp7;
    private String[] fb_popup_inapp8;
    private String[] fb_popup_out9;
    private String[] fb_reward10;
    private String[] fb_popup_detail11;
    private String[] fb_native_news12;
    private String[] fb_native_page13;

    //flag
    private int[] flag_ads_10;//-> Cở điều khiển 10 biến quảng cáo, 1 admob, 2 facebook,. 3, Mediation
    private int count_show_ads_openapp; //Dùng cho Client điểu khiển biến sau số lần xuất hiện popup
    private int count_dialog_premium_openapp;//Dùng cho Client điểu khiển biến sau số lần xuất hiện popup Premium
    private String admobappid;
    private String listcountry;//"AU|CA|DE|ID|JP|KR|MK|PT|TR|US|UK|VN|ZA",
    //    private String adwordtracking;
    private String geoid;
    private String link1;
    private String link2;
    private String link3;
    private String link4;
    private String link5;
    private String link6;
    private String key1;
    private String key2;
    private String key3;
    private String control1;
    private String control2;
    private String control3;
    private String ip;
    private String location;
    private String isforceupdate;
    private int shortcut;

    public String getLinkDownload() {
        return LinkDownload;
    }

    public void setLinkDownload(String linkDownload) {
        LinkDownload = linkDownload;
    }

    public long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(long versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

//    public String getBanner() {
//        return banner;
//    }
//
//    public void setBanner(String banner) {
//        this.banner = banner;
//    }
//
//    public String getPopup() {
//        return popup;
//    }
//
//    public void setPopup(String popup) {
//        this.popup = popup;
//    }
//
//    public String getDevid() {
//        return devid;
//    }
//
//    public void setDevid(String devid) {
//        this.devid = devid;
//    }
//
//    public String getAppid() {
//        return appid;
//    }
//
//    public void setAppid(String appid) {
//        this.appid = appid;
//    }
//
//    public String getFb_banner() {
//        return fb_banner;
//    }
//
//    public void setFb_banner(String fb_banner) {
//        this.fb_banner = fb_banner;
//    }
//
//    public String getFb_popup() {
//        return fb_popup;
//    }
//
//    public void setFb_popup(String fb_popup) {
//        this.fb_popup = fb_popup;
//    }
//
//    public String getFb_native() {
//        return fb_native;
//    }
//
//    public void setFb_native(String fb_native) {
//        this.fb_native = fb_native;
//    }
//
//    public String getGa() {
//        return ga;
//    }
//
//    public void setGa(String ga) {
//        this.ga = ga;
//    }

    public String getAdwordtracking() {
        return adwordtracking;
    }

    public void setAdwordtracking(String adwordtracking) {
        this.adwordtracking = adwordtracking;
    }

    public String getFacebookappid() {
        return facebookappid;
    }

    public void setFacebookappid(String facebookappid) {
        this.facebookappid = facebookappid;
    }

    public int getTimeafterintall() {
        return timeafterintall;
    }

    public void setTimeafterintall(int timeafterintall) {
        this.timeafterintall = timeafterintall;
    }

    public int getTimepopshow() {
        return timepopshow;
    }

    public void setTimepopshow(int timepopshow) {
        this.timepopshow = timepopshow;
    }

    public int getPoptimeshow() {
        return poptimeshow;
    }

    public void setPoptimeshow(int poptimeshow) {
        this.poptimeshow = poptimeshow;
    }

    public int getPopunlockscreen() {
        return popunlockscreen;
    }

    public void setPopunlockscreen(int popunlockscreen) {
        this.popunlockscreen = popunlockscreen;
    }

    public int getPopopenapp() {
        return popopenapp;
    }

    public void setPopopenapp(int popopenapp) {
        this.popopenapp = popopenapp;
    }

    public int getPopopenapp2() {
        return popopenapp2;
    }

    public void setPopopenapp2(int popopenapp2) {
        this.popopenapp2 = popopenapp2;
    }

    public int getPopchangenet() {
        return popchangenet;
    }

    public void setPopchangenet(int popchangenet) {
        this.popchangenet = popchangenet;
    }

    public int getIssplash() {
        return issplash;
    }

    public void setIssplash(int issplash) {
        this.issplash = issplash;
    }

    public int getIsmyads() {
        return ismyads;
    }

    public void setIsmyads(int ismyads) {
        this.ismyads = ismyads;
    }

    public int getBantrangchu() {
        return bantrangchu;
    }

    public void setBantrangchu(int bantrangchu) {
        this.bantrangchu = bantrangchu;
    }

    public int getBanvideo() {
        return banvideo;
    }

    public void setBanvideo(int banvideo) {
        this.banvideo = banvideo;
    }

    public int getPoptrangchu() {
        return poptrangchu;
    }

    public void setPoptrangchu(int poptrangchu) {
        this.poptrangchu = poptrangchu;
    }

    public int getPopmenu() {
        return popmenu;
    }

    public void setPopmenu(int popmenu) {
        this.popmenu = popmenu;
    }

    public int getPopvideo() {
        return popvideo;
    }

    public void setPopvideo(int popvideo) {
        this.popvideo = popvideo;
    }

    public int getIsrate() {
        return israte;
    }

    public void setIsrate(int israte) {
        this.israte = israte;
    }

    public int getMaxran() {
        return maxran;
    }

    public void setMaxran(int maxran) {
        this.maxran = maxran;
    }

    public String getSendid() {
        return sendid;
    }

    public void setSendid(String sendid) {
        this.sendid = sendid;
    }

    public String getReg() {
        return reg;
    }

    public void setReg(String reg) {
        this.reg = reg;
    }

    public String getAds() {
        return ads;
    }

    public void setAds(String ads) {
        this.ads = ads;
    }

    public String getAndroidkey() {
        return androidkey;
    }

    public void setAndroidkey(String androidkey) {
        this.androidkey = androidkey;
    }

    public int getUnitTime() {
        return unitTime;
    }

    public void setUnitTime(int unitTime) {
        this.unitTime = unitTime;
    }

    public String getTitleNote() {
        return titleNote;
    }

    public void setTitleNote(String titleNote) {
        this.titleNote = titleNote;
    }

    public String getMesageNote() {
        return mesageNote;
    }

    public void setMesageNote(String mesageNote) {
        this.mesageNote = mesageNote;
    }

    public String getUrlNote() {
        return urlNote;
    }

    public void setUrlNote(String urlNote) {
        this.urlNote = urlNote;
    }

    public String getConfigapp() {
        return configapp;
    }

    public void setConfigapp(String configapp) {
        this.configapp = configapp;
    }

    public String getLinkdata() {
        return linkdata;
    }

    public void setLinkdata(String linkdata) {
        this.linkdata = linkdata;
    }

    public String getKeydata() {

        return keydata;
    }

    public void setKeydata(String keydata) {
        this.keydata = keydata;
    }

    public long getDelaynet() {
        return delaynet;
    }

    public void setDelaynet(long delaynet) {
        this.delaynet = delaynet;
    }

    public long getDelayunlockscreen() {
        return delayunlockscreen;
    }

    public void setDelayunlockscreen(long delayunlockscreen) {
        this.delayunlockscreen = delayunlockscreen;
    }

    public long getDelaypopup() {
        return delaypopup;
    }

    public void setDelaypopup(long delaypopup) {
        this.delaypopup = delaypopup;
    }

    public int getExittype() {
        return exittype;
    }

    public void setExittype(int exittype) {
        this.exittype = exittype;
    }

    public int getAdsdelay() {
        return adsdelay;
    }

    public void setAdsdelay(int adsdelay) {
        this.adsdelay = adsdelay;
    }

//    public String getPopupinapp() {
//        return popupinapp;
//    }
//
//    public void setPopupinapp(String popupinapp) {
//        this.popupinapp = popupinapp;
//    }
//
//    public String getFb_popupinapp() {
//        return fb_popupinapp;
//    }
//
//    public void setFb_popupinapp(String fb_popupinapp) {
//        this.fb_popupinapp = fb_popupinapp;
//    }

    public int getDebug() {
        return debug;
    }

    public void setDebug(int debug) {
        this.debug = debug;
    }

//    public String getAd_native() {
//        return ad_native;
//    }
//
//    public void setAd_native(String ad_native) {
//        this.ad_native = ad_native;
//    }

    public String getAdmob_fil() {
        return admob_fil;
    }

    public void setAdmob_fil(String admob_fil) {
        this.admob_fil = admob_fil;
    }

    public String getPublisherIds() {
        return publisherIds;
    }

    public void setPublisherIds(String publisherIds) {
        this.publisherIds = publisherIds;
    }
//
//    public String getAd_nativehome() {
//        return ad_nativehome;
//    }
//
//    public void setAd_nativehome(String ad_nativehome) {
//        this.ad_nativehome = ad_nativehome;
//    }
//
//    public String getAd_nativedetail() {
//        return ad_nativedetail;
//    }
//
//    public void setAd_nativedetail(String ad_nativedetail) {
//        this.ad_nativedetail = ad_nativedetail;
//    }

//    public String getAd_nativemenu() {
//        return ad_nativemenu;
//    }
//
//    public void setAd_nativemenu(String ad_nativemenu) {
//        this.ad_nativemenu = ad_nativemenu;
//    }
//
//    public String getFb_nativehome() {
//        return fb_nativehome;
//    }
//
//    public void setFb_nativehome(String fb_nativehome) {
//        this.fb_nativehome = fb_nativehome;
//    }
//
//    public String getFb_nativedetail() {
//        return fb_nativedetail;
//    }
//
//    public void setFb_nativedetail(String fb_nativedetail) {
//        this.fb_nativedetail = fb_nativedetail;
//    }

//    public String getFb_nativemenu() {
//        return fb_nativemenu;
//    }
//
//    public void setFb_nativemenu(String fb_nativemenu) {
//        this.fb_nativemenu = fb_nativemenu;
//    }
//
//    public String getUni_popupinapp() {
//        return uni_popupinapp;
//    }
//
//    public void setUni_popupinapp(String uni_popupinapp) {
//        this.uni_popupinapp = uni_popupinapp;
//    }
//
//    public String getUni_popup() {
//        return uni_popup;
//    }
//
//    public void setUni_popup(String uni_popup) {
//        this.uni_popup = uni_popup;
//    }
//
//    public String getAd_rewardhome() {
//        return ad_rewardhome;
//    }
//
//    public void setAd_rewardhome(String ad_rewardhome) {
//        this.ad_rewardhome = ad_rewardhome;
//    }
//
//    public String getAd_rewarddetail() {
//        return ad_rewarddetail;
//    }
//
//    public void setAd_rewarddetail(String ad_rewarddetail) {
//        this.ad_rewarddetail = ad_rewarddetail;
//    }
//
//    public String getFb_rewardhome() {
//        return fb_rewardhome;
//    }
//
//    public void setFb_rewardhome(String fb_rewardhome) {
//        this.fb_rewardhome = fb_rewardhome;
//    }
//
//    public String getFb_rewarddetail() {
//        return fb_rewarddetail;
//    }
//
//    public void setFb_rewarddetail(String fb_rewarddetail) {
//        this.fb_rewarddetail = fb_rewarddetail;
//    }
//
//    public String getPopuphome() {
//        return popuphome;
//    }
//
//    public void setPopuphome(String popuphome) {
//        this.popuphome = popuphome;
//    }
//
//    public String getPopupdetail() {
//        return popupdetail;
//    }
//
//    public void setPopupdetail(String popupdetail) {
//        this.popupdetail = popupdetail;
//    }

    public int getIsservices() {
        return isservices;
    }

    public void setIsservices(int isservices) {
        this.isservices = isservices;
    }

    //new 06/09/2019


    public int getIsmediation() {
        return ismediation;
    }

    public void setIsmediation(int ismediation) {
        this.ismediation = ismediation;
    }

    public String[] getAdmob_banner_home0() {
        return admob_banner_home0;
    }

    public void setAdmob_banner_home0(String[] admob_banner_home0) {
        this.admob_banner_home0 = admob_banner_home0;
    }

    public String[] getAdmob_banner_other1() {
        return admob_banner_other1;
    }

    public void setAdmob_banner_other1(String[] admob_banner_other1) {
        this.admob_banner_other1 = admob_banner_other1;
    }

    public String[] getAdmob_banner_exit2() {
        return admob_banner_exit2;
    }

    public void setAdmob_banner_exit2(String[] admob_banner_exit2) {
        this.admob_banner_exit2 = admob_banner_exit2;
    }

    public String[] getAdmob_native_tophome3() {
        return admob_native_tophome3;
    }

    public void setAdmob_native_tophome3(String[] admob_native_tophome3) {
        this.admob_native_tophome3 = admob_native_tophome3;
    }

    public String[] getAdmob_native_bottomhome4() {
        return admob_native_bottomhome4;
    }

    public void setAdmob_native_bottomhome4(String[] admob_native_bottomhome4) {
        this.admob_native_bottomhome4 = admob_native_bottomhome4;
    }

    public String[] getAdmob_native_setting5() {
        return admob_native_setting5;
    }

    public void setAdmob_native_setting5(String[] admob_native_setting5) {
        this.admob_native_setting5 = admob_native_setting5;
    }

    public String[] getAdmob_native_other6() {
        return admob_native_other6;
    }

    public void setAdmob_native_other6(String[] admob_native_other6) {
        this.admob_native_other6 = admob_native_other6;
    }

    public String[] getAdmob_popup_openapp7() {
        return admob_popup_openapp7;
    }

    public void setAdmob_popup_openapp7(String[] admob_popup_openapp7) {
        this.admob_popup_openapp7 = admob_popup_openapp7;
    }

    public String[] getAdmob_popup_inapp8() {
        return admob_popup_inapp8;
    }

    public void setAdmob_popup_inapp8(String[] admob_popup_inapp8) {
        this.admob_popup_inapp8 = admob_popup_inapp8;
    }

    public String[] getAdmob_popup_exit9() {
        return admob_popup_exit9;
    }

    public void setAdmob_popup_exit9(String[] admob_popup_exit9) {
        this.admob_popup_exit9 = admob_popup_exit9;
    }

    public String[] getAdmob_reward10() {
        return admob_reward10;
    }

    public void setAdmob_reward10(String[] admob_reward10) {
        this.admob_reward10 = admob_reward10;
    }

    public String[] getFb_banner_home0() {
        return fb_banner_home0;
    }

    public void setFb_banner_home0(String[] fb_banner_home0) {
        this.fb_banner_home0 = fb_banner_home0;
    }

    public String[] getFb_banner_other1() {
        return fb_banner_other1;
    }

    public void setFb_banner_other1(String[] fb_banner_other1) {
        this.fb_banner_other1 = fb_banner_other1;
    }

    public String[] getFb_banner_exit2() {
        return fb_banner_exit2;
    }

    public void setFb_banner_exit2(String[] fb_banner_exit2) {
        this.fb_banner_exit2 = fb_banner_exit2;
    }

    public String[] getFb_native_tophome3() {
        return fb_native_tophome3;
    }

    public void setFb_native_tophome3(String[] fb_native_tophome3) {
        this.fb_native_tophome3 = fb_native_tophome3;
    }

    public String[] getFb_native_bottomhome4() {
        return fb_native_bottomhome4;
    }

    public void setFb_native_bottomhome4(String[] fb_native_bottomhome4) {
        this.fb_native_bottomhome4 = fb_native_bottomhome4;
    }

    public String[] getFb_native_setting5() {
        return fb_native_setting5;
    }

    public void setFb_native_setting5(String[] fb_native_setting5) {
        this.fb_native_setting5 = fb_native_setting5;
    }

    public String[] getFb_native_other6() {
        return fb_native_other6;
    }

    public void setFb_native_other6(String[] fb_native_other6) {
        this.fb_native_other6 = fb_native_other6;
    }

    public String[] getFb_popup_openapp7() {
        return fb_popup_openapp7;
    }

    public void setFb_popup_openapp7(String[] fb_popup_openapp7) {
        this.fb_popup_openapp7 = fb_popup_openapp7;
    }

    public String[] getFb_popup_inapp8() {
        return fb_popup_inapp8;
    }

    public void setFb_popup_inapp8(String[] fb_popup_inapp8) {
        this.fb_popup_inapp8 = fb_popup_inapp8;
    }

    public String[] getFb_popup_out9() {
        return fb_popup_out9;
    }

    public void setFb_popup_out9(String[] fb_popup_out9) {
        this.fb_popup_out9 = fb_popup_out9;
    }

    public String[] getFb_reward10() {
        return fb_reward10;
    }

    public void setFb_reward10(String[] fb_reward10) {
        this.fb_reward10 = fb_reward10;
    }

    public int[] getFlag_ads_10() {
        return flag_ads_10;
    }

    public void setFlag_ads_10(int[] flag_ads_10) {
        this.flag_ads_10 = flag_ads_10;
    }

    public int getCount_show_ads_openapp() {
        return count_show_ads_openapp;
    }

    public void setCount_show_ads_openapp(int count_show_ads_openapp) {
        this.count_show_ads_openapp = count_show_ads_openapp;
    }

    public int getCount_dialog_premium_openapp() {
        return count_dialog_premium_openapp;
    }

    public void setCount_dialog_premium_openapp(int count_dialog_premium_openapp) {
        this.count_dialog_premium_openapp = count_dialog_premium_openapp;
    }

    public String getAdmobappid() {
        return admobappid;
    }

    public void setAdmobappid(String admobappid) {
        this.admobappid = admobappid;
    }

    public String getListcountry() {
        return listcountry;
    }

    public void setListcountry(String listcountry) {
        this.listcountry = listcountry;
    }

    public String getGeoid() {
        return geoid;
    }

    public void setGeoid(String geoid) {
        this.geoid = geoid;
    }

    public String getLink1() {
        return link1;
    }

    public void setLink1(String link1) {
        this.link1 = link1;
    }

    public String getLink2() {
        return link2;
    }

    public void setLink2(String link2) {
        this.link2 = link2;
    }

    public String getLink3() {
        return link3;
    }

    public void setLink3(String link3) {
        this.link3 = link3;
    }

    public String getLink4() {
        return link4;
    }

    public void setLink4(String link4) {
        this.link4 = link4;
    }

    public String getLink5() {
        return link5;
    }

    public void setLink5(String link5) {
        this.link5 = link5;
    }

    public String getLink6() {
        return link6;
    }

    public void setLink6(String link6) {
        this.link6 = link6;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getKey3() {
        return key3;
    }

    public void setKey3(String key3) {
        this.key3 = key3;
    }

    public String getControl1() {
        return control1;
    }

    public void setControl1(String control1) {
        this.control1 = control1;
    }

    public String getControl2() {
        return control2;
    }

    public void setControl2(String control2) {
        this.control2 = control2;
    }

    public String getControl3() {
        return control3;
    }

    public void setControl3(String control3) {
        this.control3 = control3;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getIsforceupdate() {
        return isforceupdate;
    }

    public void setIsforceupdate(String isforceupdate) {
        this.isforceupdate = isforceupdate;
    }

    public int getShortcut() {
        return shortcut;
    }

    public void setShortcut(int shortcut) {
        this.shortcut = shortcut;
    }

    public String getPackId() {
        return packid;
    }

    public void setPackId(String packId) {
        this.packid = packId;
    }

    public String[] getAdmob_popup_detail11() {
        return admob_popup_detail11;
    }

    public void setAdmob_popup_detail11(String[] admob_popup_detail11) {
        this.admob_popup_detail11 = admob_popup_detail11;
    }

    public String[] getAdmob_native_news12() {
        return admob_native_news12;
    }

    public void setAdmob_native_news12(String[] admob_native_news12) {
        this.admob_native_news12 = admob_native_news12;
    }

    public String[] getAdmob_native_page13() {
        return admob_native_page13;
    }

    public void setAdmob_native_page13(String[] admob_native_page13) {
        this.admob_native_page13 = admob_native_page13;
    }

    public String[] getFb_popup_detail11() {
        return fb_popup_detail11;
    }

    public void setFb_popup_detail11(String[] fb_popup_detail11) {
        this.fb_popup_detail11 = fb_popup_detail11;
    }

    public String[] getFb_native_news12() {
        return fb_native_news12;
    }

    public void setFb_native_news12(String[] fb_native_news12) {
        this.fb_native_news12 = fb_native_news12;
    }

    public String[] getFb_native_page13() {
        return fb_native_page13;
    }

    public void setFb_native_page13(String[] fb_native_page13) {
        this.fb_native_page13 = fb_native_page13;
    }
}

