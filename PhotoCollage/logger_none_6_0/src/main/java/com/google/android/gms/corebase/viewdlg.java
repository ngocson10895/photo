package com.google.android.gms.corebase;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.corebase.R;

//import com.bumptech.glide.Glide;
//import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

public class viewdlg {

    Context activity;
    Dialog dialog;

    public viewdlg(Context activity) {
        this.activity = activity;
    }

    public void showDialog() {
        showDialog(activity.getResources().getString(R.string.loading_ads));
    }
    public void showDialog(String message) {
        dialog = new Dialog(activity);//,R.style.LoadingDialogCustom
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setPadding(16,16,16,16);
        ProgressBar progressBar = new ProgressBar(activity);
        progressBar.setLayoutParams(new LinearLayout.LayoutParams(80,80));
        linearLayout.addView(progressBar);

        TextView valueTV = new TextView(activity);
        valueTV.setText(message);
        valueTV.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        valueTV.setTextColor(Color.parseColor("#353535"));
        valueTV.setPadding(8,8,8,8);
        valueTV.setTextSize(14);
        valueTV.setGravity(Gravity.CENTER);
        linearLayout.addView(valueTV);
        dialog.setContentView(linearLayout);

//        dialog.setContentView(R.layout.custom_loading_layout);
//
//        ImageView gifImageView = dialog.findViewById(R.id.custom_loading_imageView);
//
//        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImageView);
//        Glide.with(activity)
//                .load(R.drawable.loading)
//                .placeholder(R.drawable.loading)
//                .centerCrop()
//                .crossFade()
//                .into(imageViewTarget);

        dialog.show();
    }

    public boolean isShowing(){
        return dialog.isShowing();
    }

    public void dismiss(){
        dialog.dismiss();
    }
    public void hideDialog() {
        dialog.dismiss();
    }

}