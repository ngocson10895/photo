package com.facebook.ads;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.corebase.R;
import com.google.android.gms.corebase.lg;
import com.google.android.gms.corebase.logger;
import com.google.android.gms.corebase.ut;

public class DisplayAdController {
    public long MIN_TIME_TO_SHOW_INTERSTITIAL_AD = 1 * 60 * 1000;//

    private static DisplayAdController instance;
    public long lastTimeShownAd = 0;
    private InterstitialAd interstitialAd;
    private Application mContext;

    private DisplayAdController(Application context) {
        mContext = context;
        MobileAds.initialize(mContext);
        MobileAds.setAppVolume(0.0f);
    }

    public static DisplayAdController getInstance(Application context) {
        if (instance == null) {
            instance = new DisplayAdController(context);
        }
        return instance;
    }

    public void showAdIfNeeded(final AppCompatActivity activity, boolean waitForLoadingAds, final String eventName) {
        showAdIfNeeded(activity, waitForLoadingAds, eventName, false);
    }

    public void showAdIfNeeded(final AppCompatActivity activity, boolean waitForLoadingAds, final String eventName,
                               boolean isForce) {
        showAdIfNeeded(activity, waitForLoadingAds, eventName, isForce, true);
    }

    public void showAdIfNeeded(final AppCompatActivity activity, boolean waitForLoadingAds, final String eventName,
                               boolean isForce, final boolean isShowLoading) {
        lg.logs("DisplayAdController", "showAdIfNeeded: " + waitForLoadingAds);
        if (activity == null) {
            return;
        }
        if (ut.isAppPurchased(activity)) {
            return;
        }
        long minTimeShowingInterstitialAd = MIN_TIME_TO_SHOW_INTERSTITIAL_AD;
        if (System.currentTimeMillis() - lastTimeShownAd < minTimeShowingInterstitialAd) {
            if (!isForce)
                return; //do not show ad continuously
        }

        if (!isAdLoaded()) {
            if (ut.isConnectInternet(activity)) {
//                loadAdIfNeeded();
                if (isAdLoading())
                    return;
                loadAd();
            } else {
                lg.logs("DisplayAdController", "showAdIfNeeded: no internet, do not load ad");
                return;
            }
            if (!waitForLoadingAds) {
                lg.logs("DisplayAdController", "showAdIfNeeded failed: do not waitForLoadingAds");
                return;
            }
        }
        //prepare to show ad
        ut.sendCustomAction(eventName, "rtsa");
        if (isShowLoading)
            ut.showProgress(activity, mContext.getString(R.string.loading_ads));
//        int maxCount = logger.getOutPopupAdsId(mContext)!=null?
//                logger.getOutPopupAdsId(mContext).length:3;

        CountDownTimer countDownTimer = new CountDownTimer(3000, 1000) {
            boolean isLoaded = false;

            @Override
            public void onTick(long millisUntilFinished) {
                Long count = millisUntilFinished / 1000;
                lg.logs("DisplayAdController", "onPrev: " + count);
//                int maxCount = logger.getOutPopupAdsId(mContext)!=null?
//                        logger.getOutPopupAdsId(mContext).length:3;
                if (isAdLoaded() && (millisUntilFinished < 2000 || !isShowLoading)) {
                    isLoaded = true;
                    showAd(activity, eventName);
                }
            }

            @Override
            public void onFinish() {
                if (isShowLoading)
                    ut.dismissCurrentProgress();

            }
        };
        countDownTimer.start();
    }

    public void showAdIfNeeded(AppCompatActivity activity, String eventName) {
        showAdIfNeeded(activity, false, eventName);
    }

    private boolean isAdLoaded() {
        return interstitialAd != null && interstitialAd.isLoaded();
    }

    private boolean isAdLoading() {
        return interstitialAd != null && interstitialAd.isLoading();
    }

    private void loadAd() {
        loadAd(0, null);
    }

    private void loadAd(logger.OnAdsCloseListenner listenner) {
        loadAd(0, listenner);
    }

    private void loadAdBack(AppCompatActivity activity) {
        loadAdBack(activity, 0, null);
    }

    private void loadAdBack(AppCompatActivity activity, logger.OnAdsCloseListenner listenner) {
        loadAdBack(activity, 0, listenner);
    }

    private void loadAd(final int count, final logger.OnAdsCloseListenner listenner) {
        lg.logs("DisplayAdController", "loadAd: " + count);
        if (logger.getAdmob_popup_exit9(mContext) == null ||
                count >= logger.getAdmob_popup_exit9(mContext).length) {
            checkAdsListenner(listenner);
            return;
        }
        interstitialAd = new InterstitialAd(mContext);
        interstitialAd.setAdUnitId(logger.getAdmob_popup_exit9(mContext)[count]);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                loadAd(count + 1, listenner);
            }

            @Override
            public void onAdClosed() {
//                super.onAdClosed();
//                loadAdIfNeeded();
                if (ut.isAppPurchased(mContext)) {
                    checkAdsListenner(listenner);
                    return;
                }
                if (isAdLoaded())
                    return;
//                loadAd();
                checkAdsListenner(listenner);
            }
        });
//        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
        lg.logs("DisplayAdController", "loadAd: start loading");
        interstitialAd.loadAd(DisplayAdController.getDefaultAdRequest());
//            Crashlytics.logException(new Throwable("loadNormalAd"));
//        }
    }

    private void loadAdBack(final AppCompatActivity activity, final int count, final logger.OnAdsCloseListenner listenner) {
        lg.logs("DisplayAdController", "loadAd: " + count);
        if (logger.getAdmob_popup_exit9(mContext) == null ||
                count >= logger.getAdmob_popup_exit9(mContext).length) {
            checkAdsBackListenner(activity, listenner);
            return;
        }
        interstitialAd = new InterstitialAd(mContext);
        interstitialAd.setAdUnitId(logger.getAdmob_popup_exit9(mContext)[count]);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                loadAdBack(activity, count + 1, listenner);
            }

            @Override
            public void onAdClosed() {
//                super.onAdClosed();
                if (ut.isAppPurchased(mContext)) {
                    checkAdsListenner(listenner);
                    return;
                }
                if (isAdLoaded())
                    return;
//                loadAdBack(activity);
                checkAdsBackListenner(activity, listenner);
            }
        });
        if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
            lg.logs("DisplayAdController", "loadAd: start loading");
            interstitialAd.loadAd(DisplayAdController.getDefaultAdRequest());
//            Crashlytics.logException(new Throwable("loadNormalAd"));
        }
    }

    public void loadAdIfNeeded() {
        if (ut.isAppPurchased(mContext)) {
            return;
        }
        if (isAdLoaded() || isAdLoading())
            return;
        loadAd();
    }

    private void showAd(AppCompatActivity activity, String eventName) {
        if (interstitialAd != null && interstitialAd.isLoaded()) {
//            if (BaseApp.isAppRunning) {
//
//            }
            interstitialAd.show();
            lastTimeShownAd = System.currentTimeMillis();
            ut.sendCustomAction(eventName, "showAd");
        }
    }

    private void showAdWithListener(final AppCompatActivity activity, String eventName, final logger.OnAdsCloseListenner listenner) {
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
//                    super.onAdClosed();
                    if (ut.isAppPurchased(mContext)) {
                        checkAdsBackListenner(activity, listenner);
                        return;
                    }
                    if (isAdLoaded())
                        return;
//                    loadAdBack(activity);
                    checkAdsBackListenner(activity, listenner);
                }
            });
            interstitialAd.show();
            lastTimeShownAd = System.currentTimeMillis();
            ut.sendCustomAction(eventName, "showAd");
        }
    }

    public boolean isAdReadyToShow() {
        long minTimeShowingInterstitialAd = MIN_TIME_TO_SHOW_INTERSTITIAL_AD;
        return isAdLoaded() && (System.currentTimeMillis() - lastTimeShownAd > minTimeShowingInterstitialAd);
    }

    public void showAdBackIfNeeded(final AppCompatActivity activity, boolean waitForLoadingAds,
                                   final String eventName, final logger.OnAdsCloseListenner listenner) {
        showAdBackIfNeeded(activity, waitForLoadingAds, eventName, false, listenner);
    }

    //new loading back screen to show
    public void showAdBackIfNeeded(final AppCompatActivity activity, boolean waitForLoadingAds,
                                   final String eventName, boolean isForce, final logger.OnAdsCloseListenner listenner) {
        lg.logs("DisplayAdController Back", "showAdIfNeeded: " + waitForLoadingAds);
        if (activity == null) {
            return;
        }
        if (ut.isAppPurchased(activity)) {
            checkAdsBackListenner(activity, listenner);
            return;
        }
        long minTimeShowingInterstitialAd = MIN_TIME_TO_SHOW_INTERSTITIAL_AD;
        if (System.currentTimeMillis() - lastTimeShownAd < minTimeShowingInterstitialAd) {
            if (!isForce) {
                checkAdsBackListenner(activity, listenner);
                return; //do not show ad continuously
            }
        }
        if (!isAdLoaded()) {
            if (ut.isConnectInternet(activity)) {
                if (ut.isAppPurchased(mContext)) {
                    checkAdsBackListenner(activity, listenner);
                    return;
                }
                if (isAdLoaded() || isAdLoading())
                    return;

                loadAdBack(activity, listenner);
            } else {
                lg.logs("DisplayAdController Back", "showAdIfNeeded: no internet, do not load ad");
                checkAdsBackListenner(activity, listenner);
                return;
            }
            if (!waitForLoadingAds) {
                lg.logs("DisplayAdController Back", "showAdIfNeeded failed: do not waitForLoadingAds");
                checkAdsBackListenner(activity, listenner);
                return;
            }
        }
        //prepare to show ad
        ut.sendCustomAction(eventName, "rtsa");
        ut.showProgress(activity, "Loading Ad...");
        CountDownTimer countDownTimer = new CountDownTimer(3000, 1000) {
            boolean isLoaded = false;

            @Override
            public void onTick(long millisUntilFinished) {
                Long count = millisUntilFinished / 1000;
                lg.logs("DisplayAdController", "onPrev: " + count);
//                int maxCount = logger.getOutPopupAdsId(mContext)!=null?
//                        logger.getOutPopupAdsId(mContext).length:3;
                if (isAdLoaded()) {
                    isLoaded = true;
                    showAdWithListener(activity, eventName, listenner);
                }
            }

            @Override
            public void onFinish() {
                ut.dismissCurrentProgress();
                if (!isLoaded) {
                    checkAdsBackListenner(activity, listenner);
                }
            }
        };
        countDownTimer.start();

    }

    private void checkAdsListenner(logger.OnAdsCloseListenner listenner) {
        if (listenner != null) {
            listenner.onAdsClose();
        }
    }

    private void checkAdsBackListenner(AppCompatActivity activity, logger.OnAdsCloseListenner listenner) {
        if (listenner != null) {
            listenner.onAdsClose();
        } else {
            activity.finish();
        }
    }


    public static AdRequest getDefaultNativeAdRequest(Context context) {
        return new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, new Bundle())
//					.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
                .build();
    }

    public static AdRequest getDefaultAdRequest() {
        return new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, new Bundle())
//					.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
                .build();
    }

    public static AdRequest getOpenAdRequest() {
        return new AdRequest.Builder()
//                .addNetworkExtrasBundle(AdMobAdapter.class, new Bundle())
//					.addTestDevice("BE171A2519FB8E4C0BB94E38C163F4D1")
                .build();
    }
}