package dauroi.com.imageprocessing;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import java.io.File;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import dauroi.com.imageprocessing.filter.ImageFilter;

public class ImageProcessingView extends GLSurfaceView implements GLSurfaceView.Renderer{

	private ImageProcessor mImageProcessor;
	private ImageFilter mFilter;
	private float mRatio = 0.0f;
	private Bitmap bm;

	public ImageProcessingView(Context context) {
		super(context);
		init();
	}

	public ImageProcessingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		mImageProcessor = new ImageProcessor(getContext());
		mImageProcessor.setGLSurfaceView(this);
	}

    public ImageProcessor getImageProcessor() {
		return mImageProcessor;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mRatio == 0.0f) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		} else {
			int width = MeasureSpec.getSize(widthMeasureSpec);
			int height = MeasureSpec.getSize(heightMeasureSpec);

			int newHeight;
			int newWidth;
			if (width / mRatio < height) {
				newWidth = width;
				newHeight = Math.round(width / mRatio);
			} else {
				newHeight = height;
				newWidth = Math.round(height * mRatio);
			}

			int newWidthSpec = MeasureSpec.makeMeasureSpec(newWidth,
					MeasureSpec.EXACTLY);
			int newHeightSpec = MeasureSpec.makeMeasureSpec(newHeight,
					MeasureSpec.EXACTLY);
			super.onMeasure(newWidthSpec, newHeightSpec);
		}
	}

	public void setScaleType(ImageProcessor.ScaleType scaleType) {
		mImageProcessor.setScaleType(scaleType);
	}

	public void setBackground(float r, float g, float b, float alpha){
		mImageProcessor.setBackground(r, g, b, alpha);
	}
	
	// Should be an xml attribute. But then GPUImage can not be distributed
	// as .jar anymore.
	public void setRatio(float ratio) {
		mRatio = ratio;
		requestLayout();
		mImageProcessor.deleteImage();
	}

	/**
	 * Set the filter to be applied on the image.
	 * 
	 * @param filter
	 *            Filter that should be applied on the image.
	 */
	public void setFilter(ImageFilter filter) {
		mFilter = filter;
		mImageProcessor.setFilter(filter);
		requestRender();
	}

	/**
	 * Get the current applied filter.
	 * 
	 * @return the current filter
	 */
	public ImageFilter getFilter() {
		return mFilter;
	}

	/**
	 * Sets the image on which the filter should be applied.
	 * 
	 * @param bitmap
	 *            the new image
	 */
	public void setImage(final Bitmap bitmap) {
        mImageProcessor.setImage(bitmap);
        bm = bitmap;
    }

	/**
	 * Sets the image on which the filter should be applied from a Uri.
	 * 
	 * @param uri
	 *            the uri of the new image
	 */
	public void setImage(final Uri uri) {
		mImageProcessor.setImage(uri);
	}

	/**
	 * Sets the image on which the filter should be applied from a File.
	 * 
	 * @param file
	 *            the file of the new image
	 */
	public void setImage(final File file) {
		mImageProcessor.setImage(file);
	}

	/**
	 * Save current image with applied filter to Pictures. It will be stored on
	 * the default Picture folder on the phone below the given folerName and
	 * fileName. <br />
	 * This method is async and will notify when the image was saved through the
	 * listener.
	 * 
	 * @param folderName
	 *            the folder name
	 * @param fileName
	 *            the file name
	 * @param listener
	 *            the listener
	 */
	public void saveToPictures(final String folderName, final String fileName,
			final ImageProcessor.OnPictureSavedListener listener) {
		mImageProcessor.saveToPictures(folderName, fileName, listener);
	}


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

    }

    @Override
    public void onDrawFrame(GL10 gl) {
	    bm = createBitmapFromGLSurface(getWidth(), getHeight(), gl);
    }

    public Bitmap getBm() {
        return bm;
    }

    private Bitmap createBitmapFromGLSurface(int w, int h, GL10 gl)
            throws OutOfMemoryError {
        int[] bitmapBuffer = new int[w * h];
        int[] bitmapSource = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(0, 0, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }
}
